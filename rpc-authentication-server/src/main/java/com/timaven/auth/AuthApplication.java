package com.timaven.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
// Enable Resource server so that client can fetch auth/me
@EnableResourceServer
public class AuthApplication extends SpringBootServletInitializer {
    //test
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }

}
