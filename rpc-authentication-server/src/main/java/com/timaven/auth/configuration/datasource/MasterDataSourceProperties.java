package com.timaven.auth.configuration.datasource;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Map;

@Getter
@Component
@ConfigurationProperties(prefix = "spring.master")
public class MasterDataSourceProperties {

    private DataSource masterDataSource;

    // Map application.yml tenants -> datasources
    public void setDatasource(Map<String, String> source) {
        this.masterDataSource = DataSourceBuilder.create()
                .url(source.get("url"))
                .driverClassName(source.get("driver-class-name"))
                .username(source.get("username"))
                .password(source.get("password"))
                .build();
    }
}
