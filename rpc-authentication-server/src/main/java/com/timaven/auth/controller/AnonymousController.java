package com.timaven.auth.controller;

import com.timaven.auth.configuration.tenant.TenantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AnonymousController extends BaseController {

    private final TenantProperties tenantProperties;

    @Autowired
    public AnonymousController(TenantProperties tenantProperties) {
        this.tenantProperties = tenantProperties;
    }

    /**
     * Index page
     */
    @GetMapping(value = "/index")
    public ModelAndView accessIndexPage() {
        return new ModelAndView("index");
    }
}
