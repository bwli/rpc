package com.timaven.auth.controller;

import com.timaven.auth.configuration.web.ThreadTenantStorage;
import com.timaven.auth.util.Aes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    /**
     * Show the login page
     *
     * @param model  model
     * @param error  error
     * @param logout logout
     * @return login.jsp
     */
    @GetMapping("/login")
    public String login(Model model, String error, String logout, @RequestParam(required = false) String service) {
        if (error != null) {
            if (error.equals("IllegalTenantException")) {
                return "redirect:index";
            }

            model.addAttribute("error", "Your username and password is " +
                    "invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out " +
                    "successfully.");
        }

        if (StringUtils.hasText(service)) {
            model.addAttribute("service", service);
        } else {
//            if (RequestContextHolder.getRequestAttributes() == null)
//                throw new IllegalStateException("No request attributes");
//            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//            String tenantId = (String) attr.getAttribute(TenantRoutingDataSource.SAVED_TENANT_ID,
//                    RequestAttributes.SCOPE_REQUEST);
            String tenantId = ThreadTenantStorage.getTenantId();
            if (StringUtils.hasText(tenantId)) {
                String errorParam = error == null ? "" : "&error=" + error;
                String logoutParam = logout == null ? "" : "&logout=" + logout;
                return String.format("redirect:login?service=%s%s%s", Aes.encryptService(tenantId), errorParam,
                        logoutParam);
            } else {
                return "redirect:index";
            }
        }
        return "login";
    }
}
