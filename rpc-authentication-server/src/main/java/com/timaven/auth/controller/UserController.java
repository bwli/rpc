package com.timaven.auth.controller;

import com.timaven.auth.configuration.web.ThreadTenantStorage;
import com.timaven.auth.model.User;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @RequestMapping("/me")
    public Map<String, Object> user(Principal principal) {
        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) principal;
        User user = (User) oAuth2Authentication.getPrincipal();
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("id", user.getId());
        map.put("name", principal.getName());
        map.put("displayName", user.getDisplayName());
        map.put("ipAddress", user.getIpAddress());
        map.put("tenantId", ThreadTenantStorage.getTenantId());
        if (user.getLoggedInAt() != null) {
            map.put("loggedInAt", user.getLoggedInAt().toString());
        }
        List<LinkedHashMap<String, String>> authorityList = new ArrayList<>();
        oAuth2Authentication.getAuthorities().forEach(
                e -> {
                    LinkedHashMap<String, String> authorities = new LinkedHashMap<>();
                    authorities.put("authority", e.getAuthority());
                    authorityList.add(authorities);
                }
        );
        map.put("authorities", authorityList);
        return map;
    }
}
