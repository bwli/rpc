package com.timaven.auth.dao.repositories;

import com.timaven.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select l from User l where l.username = :username")
    User findUserByName(@Param("username") String username);

    @Query("select l from User l where l.username = :username and l.active = true")
    User findValidUserByName(@Param("username") String username);
}
