package com.timaven.auth.lang;

public class IllegalTenantException extends RuntimeException {
    public IllegalTenantException(String errorMessage) {
        super(errorMessage);
    }
}
