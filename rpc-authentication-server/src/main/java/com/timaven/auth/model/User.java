package com.timaven.auth.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity bean with JPA annotations Hibernate provides JPA implementation
 */
@Getter
@Setter
@Entity
@Table(name = "user", schema = "auth")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "display_name")
    private String displayName;

    @NotNull
    @Column(name = "password")
    private String password;

    @Column(name = "ip_address")
    private String ipAddress;

    @Transient
    private String passwordConfirm;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", schema = "auth", joinColumns = @JoinColumn(name =
            "user_id", referencedColumnName = "id", columnDefinition = "BIGINT"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    referencedColumnName = "id", columnDefinition = "BIGINT"))
    @OrderBy("name ASC")
    private Set<Role> roles;

    @Column(name = "logged_at")
    private LocalDateTime loggedInAt;

    @NotNull
    @Column(name = "is_active")
    private Boolean active = true;

    @Transient
    private String tenantId;

    public User() {
    }

    @Override
    public boolean isAccountNonExpired() {
        return active;
    }

    @Override
    public boolean isAccountNonLocked() {
        return active;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return active;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map(e -> new SimpleGrantedAuthority(e.getName())).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(username);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User that = (User) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(username, that.username);
        return eb.isEquals();
    }
}
