package com.timaven.auth.model.dto;

import lombok.Data;

@Data
public class TenantDto {
    private String name;
    private String host;
    private String title;
    private String description;
    private String url;
    private String iconSrc;
}
