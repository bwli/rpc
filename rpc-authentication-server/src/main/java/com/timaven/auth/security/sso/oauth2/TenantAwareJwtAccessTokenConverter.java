package com.timaven.auth.security.sso.oauth2;

import com.timaven.auth.configuration.web.ThreadTenantStorage;
import com.timaven.auth.model.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.LinkedHashMap;
import java.util.Map;

public class TenantAwareJwtAccessTokenConverter extends JwtAccessTokenConverter {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//        if (RequestContextHolder.getRequestAttributes() == null) throw new IllegalStateException("No request attributes");
//        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//        String tenantId = (String) attr.getAttribute(TenantRoutingDataSource.SAVED_TENANT_ID, RequestAttributes.SCOPE_REQUEST);
        String tenantId = ThreadTenantStorage.getTenantId();
        Map<String, Object> info = new LinkedHashMap<>(accessToken.getAdditionalInformation());
        info.put("tenant", tenantId);
        User user = (User) authentication.getPrincipal();
        info.put("id", user.getId());
        info.put("name", authentication.getName());
        info.put("displayName", user.getDisplayName());
        info.put("ipAddress", user.getIpAddress());
        info.put("tenantId", ThreadTenantStorage.getTenantId());
        if (user.getLoggedInAt() != null) {
            info.put("loggedInAt", user.getLoggedInAt().toString());
        }
        DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
        customAccessToken.setAdditionalInformation(info);
        return super.enhance(customAccessToken, authentication);
    }

    @Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey("tenant")) {
            ThreadTenantStorage.setTenantId((String) map.get("tenant"));
        }
        return super.extractAuthentication(map);
    }
}
