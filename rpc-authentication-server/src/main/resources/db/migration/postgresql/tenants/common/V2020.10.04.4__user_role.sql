create table if not exists auth.user_role
(
    id         bigserial primary key,
    user_id   bigint references auth.user (id) on delete cascade on update cascade,
    role_id    bigint references auth.role (id) on delete cascade on update cascade,
    created_at timestamp with time zone default now(),
    unique (user_id, role_id)
);
