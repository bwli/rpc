-- INSERT INTO public.oauth_client_details
-- (client_id, client_secret, scope, authorized_grant_types, access_token_validity,
--  web_server_redirect_uri, autoapprove)
-- VALUES ('timaven-client',
--         crypt('timaven-secret', gen_salt('bf', 10)),
--         'user_info',
--         'authorization_code', 43200,
--         'http://localhost:8082/timaven/login,https://client-uat.timaven-pro.com/login,https://client.timaven-pro.com/login',
--         true)
-- ON CONFLICT DO NOTHING;

update public.oauth_client_details
set access_token_validity = 43200
where client_id = 'timaven-client';

