INSERT INTO auth.role (name, description, display_order)
VALUES ('ROLE_timekeeper',
        '1. Daily time sheet operation including allocate cost code, report rain out, manage exception and manage per diem if manual. 2. Punch record upload. 3. Daily timesheet approval. 4. Weekly timesheet processing including reconciling OT and per diem',
        80)
ON CONFLICT DO NOTHING;
