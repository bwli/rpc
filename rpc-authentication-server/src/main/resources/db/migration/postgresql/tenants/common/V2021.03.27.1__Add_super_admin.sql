CREATE EXTENSION IF NOT EXISTS pgcrypto;
insert into auth."user" (username, password, display_name)
values ('timaven', crypt('9@e5H9h1cZUohTxROY#3', gen_salt('bf', 10)), 'Super Admin')
on conflict (username) do nothing;

insert into auth.role (name, description, display_order)
values ('ROLE_super_admin', 'Super admin role', -1)
ON CONFLICT (name) DO NOTHING;

WITH t0 as (SELECT id FROM auth."user" WHERE username = 'timaven'),
     t1 as (SELECT id FROM auth.role WHERE name = 'ROLE_super_admin')
INSERT
INTO auth.user_role (user_id, role_id)
VALUES ((SELECT t0.id FROM t0), (SELECT t1.id FROM t1))
ON CONFLICT on constraint user_role_user_id_role_id_key DO NOTHING;

DROP EXTENSION IF EXISTS pgcrypto;
