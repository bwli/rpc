INSERT INTO auth.role (name, description, display_order)
VALUES ('ROLE_purchasing', 'Manage purchasing', 85)
ON CONFLICT DO NOTHING;
