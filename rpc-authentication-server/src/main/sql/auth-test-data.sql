/**
This command is used to crypto password when you add records from sql
 */
CREATE EXTENSION IF NOT EXISTS pgcrypto;

INSERT INTO auth.login (username, password, display_name)
VALUES ('demo_foreman', crypt('password', gen_salt('bf', 10)), 'Foreman 1')
ON CONFLICT DO NOTHING;

INSERT INTO auth.login (username, password, display_name)
VALUES ('demo_accountant', crypt('password', gen_salt('bf', 10)), 'Accountant 1')
ON CONFLICT DO NOTHING;

WITH t0 as (SELECT id FROM auth.login WHERE username = 'demo_accountant'),
     t1 as (SELECT id FROM auth.role WHERE name = 'ROLE_accountant')
INSERT
INTO auth.login_role (login_id, role_id)
VALUES ((SELECT t0.id FROM t0), (SELECT t1.id FROM t1))
ON CONFLICT DO NOTHING;

WITH t0 as (SELECT id FROM auth.login WHERE username = 'demo_foreman'),
     t1 as (SELECT id FROM auth.role WHERE name = 'ROLE_foreman')
INSERT
INTO auth.login_role (login_id, role_id)
VALUES ((SELECT t0.id FROM t0), (SELECT t1.id FROM t1))
ON CONFLICT DO NOTHING;

WITH t0 as (SELECT id FROM auth.login WHERE username = 'demo_accountant_foreman'),
     t1 as (SELECT id FROM auth.role WHERE name = 'ROLE_accountant_foreman')
INSERT
INTO auth.login_role (login_id, role_id)
VALUES ((SELECT t0.id FROM t0), (SELECT t1.id FROM t1))
ON CONFLICT DO NOTHING;

DROP EXTENSION IF EXISTS pgcrypto;

