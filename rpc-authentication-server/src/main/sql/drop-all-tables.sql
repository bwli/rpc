drop schema if exists auth cascade;
drop table if exists oauth_client_details;
drop schema if exists pm cascade;
drop schema if exists project cascade;
drop schema if exists rule cascade;
drop schema if exists stage cascade;
drop schema if exists time cascade;
drop schema if exists flyway_history_schema cascade ;
