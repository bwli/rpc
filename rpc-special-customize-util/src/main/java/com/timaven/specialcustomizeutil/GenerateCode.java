package com.timaven.specialcustomizeutil;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class GenerateCode {
    public static void main(String[] args) {
        String projectPath  = System.getProperty("user.dir");
        // 1.Global configuration
        GlobalConfig config = new GlobalConfig();
        // Whether to support AR mode
        config.setActiveRecord(true)
                // Generate catalog
                .setOutputDir(projectPath +"/src/main/java")
                // File overwrite
                .setFileOverride(true)
                .setIdType(IdType.AUTO)
                // Set whether the first letter of the name of the generated service interface is I
                .setServiceName("%sService")
                .setServiceImplName("%sServiceImpl")
                // Basic ResultMap
                .setBaseResultMap(true)
                // Basic Column
                .setBaseColumnList(true)
                .setSwagger2(true);

        // 2. Data source configuration
        DataSourceConfig dbConfig = new DataSourceConfig();
        dbConfig.setDbType(DbType.POSTGRE_SQL)
                .setDriverName("org.postgresql.Driver")
                .setUrl("jdbc:postgresql://3.140.246.69:5432/timekeeping")
                .setUsername("timekeeping")
                .setPassword("escORown")
//                .setSchemaName("time")
                .setSchemaName("auth");
//                .setSchemaName("rule");

        // 3. Strategy configuration
        StrategyConfig sgConfig = new StrategyConfig();
        // Global uppercase naming
        sgConfig.setCapitalMode(true)
                // Specify the table name, whether to use underscore in the field name
                .setColumnNaming(NamingStrategy.no_change)
                // Naming strategy for mapping database tables to entities
                .setNaming(NamingStrategy.underline_to_camel)
                .setTablePrefix("")
                .setEntityLombokModel(true)
                // Specify which tables to generate
                .setInclude("allocation_exception",
                        "allocation_record",
                        "allocation_submission",
                        "allocation_time",
                        "cost_code_perc",
                        "cost_code_tag_association",
                        "craft",
                        "employee",
                        "per_diem",
                        "punch_exception",
                        "user",
                        "weekly_process");

        // 4. Package name configuration strategy
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.timaven.specialcustomizeutil")
                .setMapper("mapper")
                .setService("service")
                .setServiceImpl("service.impl")
                .setEntity("entity")
                .setXml("mapper");

        // 5. Integration configuration
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(config)
                .setDataSource(dbConfig)
                .setStrategy(sgConfig)
                .setPackageInfo(pkConfig);

        // execute
        autoGenerator.execute();
    }
}
