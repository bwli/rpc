package com.timaven.specialcustomizeutil;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
@MapperScan("com.timaven.specialcustomizeutil.mapper")
public class SpecialCustomizeUtilApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpecialCustomizeUtilApplication.class, args);
    }
}
