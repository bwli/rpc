package com.timaven.specialcustomizeutil.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class Swagger2Configuration {
    @Value("${swagger.enable}")
    private boolean enable;
    @Value("${swagger.package}")
    private String basePackage;
    @Value("${swagger.url}")
    private String pathMapping;
    @Value("${swagger.api-info.title}")
    private String title;
    @Value("${swagger.api-info.description}")
    private String description;
    @Value("${swagger.api-info.version}")
    private String version;
    @Value("${swagger.api-info.contact.user-name}")
    private String userName;
    @Value("${swagger.api-info.contact.user-url}")
    private String userUrl;
    @Value("${swagger.api-info.contact.user-email}")
    private String userMail;

    @Bean
    public Docket productApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(enable)
                .apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build()
                .pathMapping(pathMapping);
    }

    private ApiInfo getApiInfo(){
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .contact(new Contact(userName, userUrl, userMail))
                .version(version)
                .build();
    }
}
