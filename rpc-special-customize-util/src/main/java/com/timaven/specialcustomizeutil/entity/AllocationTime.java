package com.timaven.specialcustomizeutil.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("allocation_time")
@ApiModel(value="AllocationTime", description="")
public class AllocationTime extends Model<AllocationTime> {
    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("emp_id")
    private String empId;

    @TableField("team_name")
    private String teamName;

    @TableField("first_name")
    private String firstName;

    @TableField("last_name")
    private String lastName;

    @TableField("st_hour")
    private BigDecimal stHour;

    @TableField("ot_hour")
    private BigDecimal otHour;

    @TableField("dt_hour")
    private BigDecimal dtHour;

    @TableField("total_hour")
    private BigDecimal totalHour;

    @TableField("allocated_hour")
    private BigDecimal allocatedHour;

    @TableField("net_hour")
    private BigDecimal netHour;

    @TableField("has_per_diem")
    private Boolean hasPerDiem;

    @TableField("rig_pay")
    private BigDecimal rigPay;

    @TableField("payroll_date")
    private LocalDate payrollDate;

    @TableField("submission_id")
    private Long submissionId;

    @TableField("created_at")
    private LocalDateTime createdAt;

    @TableField("max_time_cost_code")
    private String maxTimeCostCode;

    @TableField("per_diem_cost_code")
    private String perDiemCostCode;

    private String department;

    @TableField("job_number")
    private String jobNumber;

    @ApiModelProperty(value = "0: st. 1: sick. 2: holiday. 3: vacation. 4: other")
    @TableField("extra_time_type")
    private Integer extraTimeType;

    @TableField("weekly_process_id")
    private Long weeklyProcessId;

    @TableField("mob_cost_code")
    private String mobCostCode;

    @TableField("mob_amount")
    private BigDecimal mobAmount;

    @TableField("client_emp_id")
    private String clientEmpId;

    private String badge;

    @TableField("per_diem_amount")
    private BigDecimal perDiemAmount;

    @TableField("mob_mileage_rate")
    private BigDecimal mobMileageRate;

    @TableField("mob_mileage")
    private BigDecimal mobMileage;

    @TableField("has_mob")
    private Boolean hasMob;

    @TableField("is_offsite")
    private Boolean isOffsite;

    @TableField("mob_display_order")
    private Integer mobDisplayOrder;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
