package com.timaven.specialcustomizeutil.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Data
public class CostCodeProfitDetailsDTO {
    @NotNull(message = "projectId cannot be empty!")
    private Long projectId;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "startDate cannot be empty!")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "endDate cannot be empty!")
    private LocalDate endDate;

    @NotNull(message = "costCodes cannot be empty!")
    private Set<String> costCodes;
}
