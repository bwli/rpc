package com.timaven.specialcustomizeutil.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Data
public class EmployeeProfitDetailsDTO {
    @NotNull(message = "projectId cannot be empty!")
    private Long projectId;

    @NotNull(message = "startDate cannot be empty!")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate startDate;

    @NotNull(message = "endDate cannot be empty!")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate endDate;

    @NotNull(message = "employeeIds cannot be empty!")
    private Set<String> employeeIds;
}
