package com.timaven.specialcustomizeutil.entity.queryResult;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProfitDetailsQueryResult {
    private String employeeID;
    private String firstName;
    private String lastName;
    private String costCode;
    private BigDecimal st_cost;
    private BigDecimal ot_cost;
    private BigDecimal dt_cost;
    private BigDecimal totalHour;
}
