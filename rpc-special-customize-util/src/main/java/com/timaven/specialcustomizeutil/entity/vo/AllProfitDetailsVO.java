package com.timaven.specialcustomizeutil.entity.vo;

import lombok.Data;
import java.math.BigDecimal;
import java.util.ArrayList;

@Data
public class AllProfitDetailsVO {
    private String costCode;
    private BigDecimal costCodeCost;
    private BigDecimal costCodeBilled;
    private BigDecimal costCodeProfit;
    private ArrayList<EmployeeVO> employeeVOS;
}
