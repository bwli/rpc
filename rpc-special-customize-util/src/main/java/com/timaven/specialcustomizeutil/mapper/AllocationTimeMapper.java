package com.timaven.specialcustomizeutil.mapper;

import com.timaven.specialcustomizeutil.entity.AllocationTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.timaven.specialcustomizeutil.entity.queryResult.ProfitDetailsQueryResult;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.ArrayList;

@Repository
public interface AllocationTimeMapper extends BaseMapper<AllocationTime> {
    /**
     * getEmployeeProfitDetails
     * @param projectId
     * @param startDate
     * @param endDate
     * @param employeeId
     * @return
     */
    ArrayList<ProfitDetailsQueryResult> getEmployeeProfitDetails(@Param("projectId") Long projectId,
                                                                 @Param("startDate") LocalDate startDate,
                                                                 @Param("endDate") LocalDate endDate,
                                                                 @Param("employeeId") String employeeId);


    /**
     * getCostCodeProfitDetails
     * @param projectId
     * @param startDate
     * @param endDate
     * @param costCode
     * @return
     */
    ArrayList<ProfitDetailsQueryResult> getCostCodeProfitDetails(@Param("projectId") Long projectId,
                                                                @Param("startDate") LocalDate startDate,
                                                                @Param("endDate") LocalDate endDate,
                                                                @Param("costCode") String costCode);


    /**
     * getEmployeeByCostCode
     * @param costCode
     * @param projectId
     * @param startDate
     * @param endDate
     * @return
     */
    ArrayList<ProfitDetailsQueryResult> getEmployeeByCostCode(@Param("costCode") String costCode,
                                                @Param("projectId") Long projectId,
                                                @Param("startDate") LocalDate startDate,
                                                @Param("endDate") LocalDate endDate);


    /**
     * getCostCodeByPayrollDate
     * @param projectId
     * @param startDate
     * @param endDate
     * @return
     */
    ArrayList<String> getCostCodeByPayrollDate(@Param("projectId") Long projectId,
                                               @Param("startDate") LocalDate startDate,
                                               @Param("endDate") LocalDate endDate);
}
