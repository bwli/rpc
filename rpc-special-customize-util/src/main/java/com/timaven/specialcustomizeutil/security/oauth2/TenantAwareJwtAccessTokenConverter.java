package com.timaven.specialcustomizeutil.security.oauth2;
import com.timaven.specialcustomizeutil.entity.dto.Principal;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

public class TenantAwareJwtAccessTokenConverter extends JwtAccessTokenConverter {
    @Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> map) {

        OAuth2Authentication authentication = super.extractAuthentication(map);
        Authentication userAuthentication = authentication.getUserAuthentication();

        if (userAuthentication != null) {
            Principal user = new Principal();
            user.setId(Long.valueOf((Integer) map.get("id")));
            user.setUsername((String) map.get("name"));
            user.setDisplayName((String) map.get("displayName"));
            user.setIpAddress((String) map.get("ipAddress"));
            user.setTenantId((String) map.get("tenantId"));
            if (map.containsKey("loggedInAt")) {
                user.setLoggedInAt(LocalDateTime.parse((CharSequence) map.get("loggedInAt")));
            }
            Collection<? extends GrantedAuthority> authorities = userAuthentication.getAuthorities();
            userAuthentication = new UsernamePasswordAuthenticationToken(user,
                    userAuthentication.getCredentials(), authorities);
        }
        return new OAuth2Authentication(authentication.getOAuth2Request(), userAuthentication);
    }
}
