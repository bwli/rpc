package com.timaven.timekeeping.api;

import com.timaven.timekeeping.api.ExportServer.ExportAPI;
import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.provider.WebClientAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
public class APIConfig {

    @Value("${api.url.provider}")
    private String providerApiUrl;

    @Bean
    @RequestScope
    public ExportAPI exportAPI() {
        return new ExportAPI();
    }

    @Bean
    @RequestScope
    public ProviderAPI providerAPI() {
        return new ProviderAPI();
    }

    @Bean
    @RequestScope
    public WebClientAPI webClientAPI() {
        return new WebClientAPI(providerApiUrl);
    }
}
