package com.timaven.timekeeping.api.ExportServer;

import com.timaven.timekeeping.api.ApiBinding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExportAPI extends ApiBinding {

    private final WebClient webClient;

    @Value("${api.url.export}")
    private String exportApiUrl;

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    public ExportAPI() {
        this.webClient = WebClient.builder()
                .filter(((clientRequest, exchangeFunction) -> {
                    ClientRequest clientRequest1 = ClientRequest.from(clientRequest)
                            .header("Authorization", "Bearer " + oAuth2RestTemplate.getAccessToken().getValue())
                            .build();
                    return exchangeFunction.exchange(clientRequest1);
                }))
                .build();
    }


    public void exportByAllocationSubmissionId(Long id, HttpServletResponse httpServletResponse) {
        Flux<DataBuffer> fileDataStream = webClient.post()
                .uri(exportApiUrl, uriBuilder -> uriBuilder.path("/allocationSubmissions/{id}").build(id))
                .header("Content-type", "application/x-www-form-urlencoded")
                .accept(MediaType.APPLICATION_OCTET_STREAM)
                .exchangeToFlux((response) -> {
                    ClientResponse.Headers headers = response.headers();
                    String contentTypeHeader = headers.header("Content-Type").get(0);
                    httpServletResponse.setContentType(contentTypeHeader);
                    httpServletResponse.setHeader("Content-Disposition", headers.header("Content-Disposition").get(0));
                    httpServletResponse.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
                    return response.bodyToFlux(DataBuffer.class);
                });

        try {
            DataBufferUtils.write(fileDataStream, httpServletResponse.getOutputStream())
                    .map(DataBufferUtils::release)
                    .blockLast();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
