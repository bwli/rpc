package com.timaven.timekeeping.common;

import com.timaven.timekeeping.model.User;
import com.timaven.timekeeping.model.dto.Principal;
import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public interface SUtils {

    static boolean contains(Collection collection, Object o) {
        return collection.contains(o);
    }

    static String escapeHtml4(String value) {
        return StringEscapeUtils.escapeHtml4(value);
    }

    static String convertToJson(Set<String> stringSet) {
        if (CollectionUtils.isEmpty(stringSet)) {
            return "[]";
        } else {
            JSONArray jsonArray = new JSONArray();
            for (String value : stringSet) {
                jsonArray.put(value);
            }
            return jsonArray.toString();
        }
    }

    static String formatDecimal(Number target, Integer decimalDigits) {
        if (target == null) return "";
        try {
            if (decimalDigits == null || decimalDigits < 1) {
                return String.valueOf(target.intValue());
            }
            String pattern = "#." + IntStream.range(0, decimalDigits)
                    .mapToObj(i -> "#").collect(Collectors.joining(""));
            DecimalFormat df = new DecimalFormat(pattern);
            df.setRoundingMode(RoundingMode.DOWN);
            return df.format(target);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    static String getDayOfWeekName(LocalDate date) {
        if (null == date) return "";
        return switch (date.getDayOfWeek()) {
            case MONDAY -> "M";
            case TUESDAY -> "T";
            case WEDNESDAY -> "W";
            case THURSDAY -> "Th";
            case FRIDAY -> "F";
            case SATURDAY -> "Sa";
            case SUNDAY -> "Su";
        };
    }

    static String getDisplayName() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return ((Principal) principal).getDisplayName();
    }
}
