package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.*;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.DateRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate", "costCodeClipboard"})
@PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper')")
public class AccountantController extends BaseController {

    private final ContentService mContentService;
    private final WeeklyProcessService weeklyProcessService;
    private final PurchaseOrderService purchaseOrderService;
    private final EquipmentService equipmentService;
    private final ProjectService projectService;
    private final CostCodeService costCodeService;
    private final EmployeeService employeeService;

    @Autowired
    public AccountantController(ContentService contentService,
                                WeeklyProcessService weeklyProcessService, PurchaseOrderService purchaseOrderService,
                                EquipmentService equipmentService, ProjectService projectService,
                                CostCodeService costCodeService, EmployeeService employeeService) {
        this.mContentService = contentService;
        this.weeklyProcessService = weeklyProcessService;
        this.purchaseOrderService = purchaseOrderService;
        this.equipmentService = equipmentService;
        this.projectService = projectService;
        this.costCodeService = costCodeService;
        this.employeeService = employeeService;
    }

    /**
     * Show time manage page
     */
    @GetMapping(value = "/manual-time")
    public ModelAndView timeManage(@ModelAttribute("sessionProjectId") Long projectId,
                                   @RequestParam boolean isCustomizedTeam,
                                   @RequestParam String teamName,
                                   @RequestParam LocalDate dateOfService,
                                   @RequestParam LocalDate payrollDate) {
        ModelAndView modelAndView = new ModelAndView("manual-time");
        List<Employee> employees;
        if (!isCustomizedTeam) {
            EmployeeFilter filter = new EmployeeFilter(projectId, teamName, null, null, null, dateOfService, null);
            employees = employeeService.getEmployees(filter);
        } else {
            EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, dateOfService, null);
            employees = employeeService.getEmployees(filter);
        }

        modelAndView.addObject("teamName", teamName);
        modelAndView.addObject("isCustomizedTeam", isCustomizedTeam);
        modelAndView.addObject("submitUserId", getUserId());
        modelAndView.addObject("employees", employees);
        modelAndView.addObject("dateOfService", dateOfService);
        modelAndView.addObject("payrollDate", payrollDate);
        return modelAndView;
    }

    @GetMapping(value = "/weekly-process")
    public ModelAndView weeklyProcess(@ModelAttribute("sessionProjectId") Long projectId,
                                      @ModelAttribute("rule") Rule rule,
                                      @RequestParam LocalDate weekEndDate,
                                      @RequestParam String weeklyProcessType,
                                      ModelMap model) {

        if ("employee-timesheet".equalsIgnoreCase(weeklyProcessType)
                || "employee-time-left-off".equalsIgnoreCase(weeklyProcessType)) {
            ModelAndView modelAndView = new ModelAndView("weekly-process");
            LocalDate weekStartDate = weekEndDate.minusDays(6);
            Set<AllocationTime> allocationTimes;
            WeeklyProcessType type;
            if ("employee-timesheet".equals(weeklyProcessType)) {
                allocationTimes = mContentService
                        .getAllocationTimesByProjectIdAndDates(projectId, weekStartDate, weekEndDate);
                type = WeeklyProcessType.NORMAL;
            } else {
                allocationTimes = mContentService
                        .getTimeLeftOffAllocationTimesByProjectIdAndDates(projectId, weekStartDate, weekEndDate);
                type = WeeklyProcessType.TLO;
            }
            Map<EmpIdTeamDto, EmpCostCodeDateDto> idTeamEmpCostCodeDateMap = new HashMap<>();

            TreeSet<LocalDate> days = new TreeSet<>();

            Set<String> teams = new TreeSet<>();
            Set<EmpIdNameDto> empIdNameDtos = new TreeSet<>();
            if (!CollectionUtils.isEmpty(allocationTimes)) {

                Set<Long> keys = allocationTimes.stream().map(AllocationTime::getId).collect(toSet());
                List<CostCodeTagAssociation> perDiemCostCodeTagAssociations = costCodeService.getCostCodeTagAssociationsByKeysAndType(keys, CostCodeType.PERDIEM);
                List<CostCodeTagAssociation> mobCostCodeTagAssociations = costCodeService.getCostCodeTagAssociationsByKeysAndType(keys, CostCodeType.MOB);
                Map<Long, List<CostCodeTagAssociation>> perDiemAssociationMap = perDiemCostCodeTagAssociations.stream()
                        .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
                Map<Long, List<CostCodeTagAssociation>> mobAssociationMap = mobCostCodeTagAssociations.stream()
                        .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));

                Set<Long> laborKeys = allocationTimes.stream()
                        .map(AllocationTime::getCostCodePercs)
                        .filter(Objects::nonNull)
                        .flatMap(Collection::stream)
                        .map(CostCodePerc::getId)
                        .collect(toSet());
                List<CostCodeTagAssociation> laborCostCodeTagAssociations = costCodeService
                        .getCostCodeTagAssociationsByKeysAndType(laborKeys, CostCodeType.DEFAULT);
                Map<Long, List<CostCodeTagAssociation>> idLaborCostCodeTagAssociationMap =
                        laborCostCodeTagAssociations.stream()
                                .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));

                for (AllocationTime allocationTime : allocationTimes) {
                    Set<CostCodePerc> costCodePercs = allocationTime.getCostCodePercs();
                    if (!CollectionUtils.isEmpty(costCodePercs)) {
                        costCodePercs.forEach(ccp -> {
                            ccp.setCostCodeTagAssociations(idLaborCostCodeTagAssociationMap.getOrDefault(ccp.getId(), new ArrayList<>()));
                        });
                    }

                    PerDiem perDiem = allocationTime.getPerDiem();
                    String dedicatedPerDiemCostCode = perDiem.getCostCode();

                    AllocationSubmission allocationSubmission = allocationTime.getAllocationSubmission();

                    String strDateOfService = allocationSubmission.getDateOfService().toString();

                    days.add(allocationSubmission.getDateOfService());

                    EmpIdTeamDto empIdTeamDto = new EmpIdTeamDto(allocationTime.getEmpId(), allocationTime.getTeamName());
                    EmpCostCodeDateDto empCostCodeDateDto = idTeamEmpCostCodeDateMap
                            .getOrDefault(empIdTeamDto,
                                    new EmpCostCodeDateDto(allocationTime.getEmpId(),
                                            allocationTime.getFirstName(),
                                            allocationTime.getLastName(),
                                            allocationSubmission.getTeamName(),
                                            allocationTime.getEmployee().getHasPerDiem()));
                    idTeamEmpCostCodeDateMap.putIfAbsent(empIdTeamDto, empCostCodeDateDto);
                    empCostCodeDateDto.setPerDiemEntity(perDiem);
                    Set<String> perDiemCostCodes = empCostCodeDateDto.getPerDiemCostCodes();
                    Set<String> mobCostCodes = empCostCodeDateDto.getMobCostCodes();

                    WeeklyProcessAllocationTimeDto allocationTimeDto = new WeeklyProcessAllocationTimeDto(allocationTime,
                            allocationSubmission.getDateOfService());
                    allocationTimeDto.setPerDiemEntity(perDiem);
                    allocationTimeDto.setPerDiemCostCodeTagAssociations(perDiemAssociationMap
                            .getOrDefault(allocationTime.getId(), new ArrayList<>()).stream().
                            map(CostCodeTagAssociationDto::new).sorted().collect(Collectors.toList()));

                    allocationTimeDto.setMobCostCodeTagAssociations(mobAssociationMap
                            .getOrDefault(allocationTime.getId(), new ArrayList<>()).stream().
                                    map(CostCodeTagAssociationDto::new).sorted().collect(Collectors.toList()));

                    if (StringUtils.hasText(allocationTime.getPerDiemCostCode())) {
                        allocationTimeDto.getPerDiemCostCodes().add(allocationTime.getPerDiemCostCode());
                        perDiemCostCodes.add(allocationTime.getPerDiemCostCode());
                    } else {
                        if (!StringUtils.hasText(dedicatedPerDiemCostCode)) {
                            perDiemCostCodes.addAll(allocationTime.getCostCodePercs().stream()
                                    .map(CostCodePerc::getCostCodeTag)
                                    .collect(toSet()));
                            if (!StringUtils.hasText(allocationTime.getMaxTimeCostCode())) {
                                allocationTime.getCostCodePercs().stream()
                                        .max((o1, o2) -> {
                                            BigDecimal t1 = o1.getTotalHour() == null ? BigDecimal.ZERO : o1.getTotalHour();
                                            BigDecimal t2 = o2.getTotalHour() == null ? BigDecimal.ZERO : o2.getTotalHour();
                                            return t1.compareTo(t2);
                                        }).map(CostCodePerc::getCostCodeFull)
                                        .ifPresent(c -> allocationTimeDto.getPerDiemCostCodes().add(c));
                            } else {
                                allocationTimeDto.getPerDiemCostCodes().add(allocationTime.getMaxTimeCostCode());
                            }
                        } else {
                            allocationTimeDto.getPerDiemCostCodes().add(dedicatedPerDiemCostCode);
                        }
                    }
                    if (StringUtils.hasText(allocationTime.getMobCostCode())) {
                        allocationTimeDto.getMobCostCodes().add(allocationTime.getMobCostCode());
                        mobCostCodes.add(allocationTime.getMobCostCode());
                    }
                    if (StringUtils.hasText(dedicatedPerDiemCostCode)) {
                        // Always add assigned per diem's cost code
                        perDiemCostCodes.add(dedicatedPerDiemCostCode);
                    }
                    if (!allocationTime.isHasPerDiem()) {
                        // No costCode if no per diem
                        allocationTimeDto.getPerDiemCostCodes().clear();
                    }
                    empCostCodeDateDto.getEmpAllocationTimeMap().put(strDateOfService, allocationTimeDto);
                    Set<String> costCodes = empCostCodeDateDto.getCostCodes();
                    if (!CollectionUtils.isEmpty(costCodePercs)) {
                        costCodes.addAll(costCodePercs.stream()
                                .map(CostCodePerc::getCostCodeTag)
                                .collect(toSet()));

                        Map<String, WeeklyProcessCostCodePercDto> codeDtoMap = costCodePercs.stream()
                                .map(WeeklyProcessCostCodePercDto::new)
                                .collect(toMap(WeeklyProcessCostCodePercDto::getCostCodeFull, Function.identity()));

                        allocationTimeDto.setEmpCostCodePercMap(codeDtoMap);
                    }
                }
                teams = allocationTimes.stream()
                        .map(AllocationTime::getAllocationSubmission)
                        .map(AllocationSubmission::getTeamName)
                        .collect(Collectors.toCollection(TreeSet::new));
                empIdNameDtos = allocationTimes.stream()
                        .map(t -> new EmpIdNameDto(t.getEmpId(),
                                t.getFullName()))
                        .collect(Collectors.toCollection(TreeSet::new));
            }

            List<EmpCostCodeDateDto> empCostCodeDateDtos = new ArrayList<>(idTeamEmpCostCodeDateMap.values());
            if (type == WeeklyProcessType.NORMAL) {
                empCostCodeDateDtos.forEach(e -> {
                    e.getEmpAllocationTimeMap().values()
                            .forEach(t -> {
                                if (t.getTotalHour().compareTo(BigDecimal.ZERO) == 0) {
                                    t.setAbsent(true);
                                }
                            });
                });
            }

            WeeklyProcessDto weeklyProcessDto = new WeeklyProcessDto(empCostCodeDateDtos.stream().sorted().collect(Collectors.toList()),
                    new ArrayList<>(days), weekEndDate, type.getValue());
            WeeklyProcess weeklyProcess = weeklyProcessService.findByProjectIdAndDateAndType(projectId, weekEndDate, type);
            if (null != weeklyProcess) {
                weeklyProcessDto.setFinalized(weeklyProcess.getFinalizedAt() != null);
            }
            modelAndView.addObject("weeklyProcessDto", weeklyProcessDto);

            Set<CostCodeLog> validCostCodeLogSet;
            Set<CostCodeLog> otherCostCodeLogSet;
            List<CostCodeLog> allCostCodeLogList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(days)) {
                List<LocalDate> dayList = new ArrayList<>(days);
                allCostCodeLogList = costCodeService.getAllCostCodeLogsByProjectIdAndDateRange(projectId, dayList);
                allCostCodeLogList = new ArrayList<>(allCostCodeLogList.stream()
                        .filter(c -> c.getEndDate() == null
                                || c.getEndDate().isAfter(dayList.get(0))
                                || c.getEndDate().isEqual(dayList.get(0)))
                        .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, Function.identity(),
                                BinaryOperator.maxBy(Comparator.comparing(CostCodeLog::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                                        .thenComparing(CostCodeLog::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())))))
                        .values());
            }
            validCostCodeLogSet = allCostCodeLogList.stream()
                    .filter(c -> c.getProjectId() != null && c.getProjectId().equals(projectId))
                    .collect(Collectors.toCollection(TreeSet::new));
            otherCostCodeLogSet = allCostCodeLogList.stream()
                    .filter(c -> c.getProjectId() == null || !c.getProjectId().equals(projectId))
                    .collect(Collectors.toCollection(TreeSet::new));
            modelAndView.addObject("validCostCodeLogSet", validCostCodeLogSet);
            modelAndView.addObject("otherCostCodeLogSet", otherCostCodeLogSet);

            modelAndView.addObject("teams", teams);
            modelAndView.addObject("empIdNameDtos", empIdNameDtos);
            modelAndView.addObject("weekEndDate", weekEndDate);
            List<CostCodeTagType> costCodeTagTypes = costCodeService.getAllCostCodeTagTypesByProjectId(projectId);
            modelAndView.addObject("costCodeTagTypes", costCodeTagTypes);

            return modelAndView;
        } else if ("equipment-timesheet".equalsIgnoreCase(weeklyProcessType)
                || "equipment-time-left-off".equalsIgnoreCase(weeklyProcessType)) {
            ModelAndView modelAndView = new ModelAndView("equipment-process");
            LocalDate weekStartDate = weekEndDate.minusDays(6);

            Set<EquipmentAllocationTime> equipmentAllocationTimes;
            EquipmentWeeklyProcessType equipmentWeeklyProcessType;
            if ("equipment-timesheet".equalsIgnoreCase(weeklyProcessType)) {
                equipmentAllocationTimes = equipmentService
                        .getEquipmentAllocationTimesByProjectIdAndDates(projectId, weekStartDate, weekEndDate);
                equipmentWeeklyProcessType = EquipmentWeeklyProcessType.NORMAL;
            } else {
                equipmentAllocationTimes = equipmentService
                        .getTimeLeftOffEquipmentAllocationTimesByProjectIdAndDates(projectId, weekStartDate, weekEndDate);
                equipmentWeeklyProcessType = EquipmentWeeklyProcessType.TLO;
            }

            Map<String, EquipmentCostCodeDateDto> equipmentCostCodeDateDtoMap = new HashMap<>();
            for (EquipmentAllocationTime time : equipmentAllocationTimes) {
                EquipmentCostCodeDateDto equipmentCostCodeDateDto = equipmentCostCodeDateDtoMap
                        .getOrDefault(time.getEquipmentId(), new EquipmentCostCodeDateDto(
                                time.getEquipmentId(), time.getSerialNumber(), time.getDescription(), time.getAlias(),
                                time.getDepartment(), time.getEquipmentClass(), time.getEmpId(),
                                time.getHourlyType().getDisplayValue(), time.getOwnershipType().getDisplayValue()));
                equipmentCostCodeDateDtoMap.putIfAbsent(time.getEquipmentId(), equipmentCostCodeDateDto);
                Set<String> costCodes = equipmentCostCodeDateDto.getCostCodes();
                EquipmentProcessAllocationTimeDto equipmentProcessAllocationTimeDto
                        = new EquipmentProcessAllocationTimeDto(time, time.getEquipmentAllocationSubmission().getDateOfService());
                equipmentProcessAllocationTimeDto.setId(time.getId());
                Set<EquipmentCostCodePerc> equipmentCostCodePercs = time.getEquipmentCostCodePercs();
                if (!CollectionUtils.isEmpty(equipmentCostCodePercs)) {
                    costCodes.addAll(equipmentCostCodePercs.stream()
                            .map(EquipmentCostCodePerc::getCostCodeFull).collect(toSet()));
                    Map<String, EquipmentProcessCostCodePercDto> codeDtoMap = equipmentCostCodePercs.stream()
                            .map(EquipmentProcessCostCodePercDto::new)
                            .collect(toMap(EquipmentProcessCostCodePercDto::getCostCodeFull, Function.identity()));
                    equipmentProcessAllocationTimeDto.setEquipmentCostCodePercMap(codeDtoMap);
                }
                equipmentCostCodeDateDto.getEquipmentAllocationTimeMap()
                        .put(time.getEquipmentAllocationSubmission().getDateOfService().toString(),
                                equipmentProcessAllocationTimeDto);
            }
            Set<LocalDate> processedDate = equipmentAllocationTimes.stream()
                    .map(EquipmentAllocationTime::getEquipmentAllocationSubmission)
                    .map(EquipmentAllocationSubmission::getDateOfService)
                    .collect(toSet());
            boolean excludeWeekend = rule.isEquipmentExcludeWeekend();
            DateRange weekRange = new DateRange(weekStartDate, weekEndDate, excludeWeekend);
            TreeSet<LocalDate> days = new TreeSet<>(weekRange.toList());
            Set<LocalDate> notProcessedDate = weekRange.toList()
                    .stream().filter(d -> !processedDate.contains(d)).sorted().collect(Collectors.toCollection(TreeSet::new));

            if (!CollectionUtils.isEmpty(notProcessedDate)) {
                LocalDate startDate = weekEndDate.with(TemporalAdjusters.firstInMonth(rule.getWeekEndDay())).minusDays(6);
                Set<EquipmentAllocationTime> previousEquipmentAllocationTimes = equipmentService
                        .getEquipmentAllocationTimesByProjectIdAndDates(projectId, startDate, weekStartDate.minusDays(1), null, weekStartDate.minusDays(1));

                Map<String, List<EquipmentAllocationTime>> previousEquipmentAllocationTimeMap = previousEquipmentAllocationTimes.stream()
                        .collect(Collectors.groupingBy(EquipmentAllocationTime::getEquipmentId));

                Set<String> equipmentClasses = Stream.of(equipmentAllocationTimes, previousEquipmentAllocationTimes)
                        .flatMap(Collection::stream)
                        .map(EquipmentAllocationTime::getEquipmentClass)
                        .collect(toSet());
                Set<EquipmentPrice> equipmentPrices = equipmentService
                        .getEquipmentPricesByProjectIdAndClasses(projectId, equipmentClasses);
                Map<String, EquipmentPrice> equipmentPriceMap = equipmentPrices.stream()
                        .collect(Collectors.toMap(EquipmentPrice::getEquipmentClass, Function.identity()));

                List<EquipmentUsage> equipmentUsages = equipmentService
                        .getEquipmentUsagesByProjectIdAndDates(projectId, weekStartDate, weekEndDate);
                Set<EquipmentUsage> nonHourlyEquipmentUsage = equipmentUsages.stream()
                        .filter(e -> e.getEquipment().getHourlyType() == EquipmentHourlyType.DAILY).collect(toSet());
                Set<EquipmentUsage> hourlyEquipmentUsage = equipmentUsages.stream()
                        .filter(e -> e.getEquipment().getHourlyType() == EquipmentHourlyType.HOURLY).collect(toSet());
                BigDecimal hourOfDay = rule.getEquipmentWorkdayHour();
                String equipmentCostCode = rule.getEquipmentCostCode();

                // TM-275: the user needs the timesheet to be generated regardless; only remove condition test blocks with code indentation
                for (EquipmentUsage usage : nonHourlyEquipmentUsage) {
                    Equipment equipment = usage.getEquipment();

                    EquipmentCostCodeDateDto equipmentCostCodeDateDto = equipmentCostCodeDateDtoMap
                            .getOrDefault(equipment.getEquipmentId(), new EquipmentCostCodeDateDto(
                                    equipment.getEquipmentId(), equipment.getSerialNumber(), equipment.getDescription(),
                                    equipment.getAlias(), equipment.getDepartment(), equipment.getEquipmentClass(),
                                    equipment.getEmpId(), equipment.getHourlyType().getDisplayValue(),
                                    equipment.getOwnershipType().getDisplayValue()));
                    equipmentCostCodeDateDtoMap.putIfAbsent(equipment.getEquipmentId(), equipmentCostCodeDateDto);
                    Set<String> costCodes = equipmentCostCodeDateDto.getCostCodes();
                    costCodes.add(equipmentCostCode);

                    BigDecimal weeklyChargeTillNow = BigDecimal.ZERO;
                    BigDecimal monthlyChargeTillNow = previousEquipmentAllocationTimeMap
                            .getOrDefault(equipment.getEquipmentId(), new ArrayList<>()).stream()
                            .map(EquipmentAllocationTime::getTotalCharge)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    BigDecimal dailyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getDailyRate();
                    BigDecimal weeklyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getWeeklyRate();
                    BigDecimal monthlyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getMonthlyRate();
                    for (LocalDate date : notProcessedDate) {
                        EquipmentProcessCostCodePercDto percDto = new EquipmentProcessCostCodePercDto();
                        percDto.setCostCodeFull(equipmentCostCode);
                        percDto.setTotalHour(hourOfDay);

                        BigDecimal monthlyRemaining = BigDecimal.ZERO.max(monthlyRate.subtract(monthlyChargeTillNow));
                        BigDecimal weeklyRemaining = BigDecimal.ZERO.max(weeklyRate.subtract(weeklyChargeTillNow));
                        BigDecimal toCharge = dailyRate.min(weeklyRemaining.min(monthlyRemaining));
                        monthlyChargeTillNow = monthlyChargeTillNow.add(toCharge);
                        weeklyChargeTillNow = weeklyChargeTillNow.add(toCharge);
                        percDto.setTotalCharge(toCharge);

                        EquipmentProcessAllocationTimeDto allocationTimeDto = new EquipmentProcessAllocationTimeDto();
                        allocationTimeDto.getEquipmentCostCodePercMap().put(equipmentCostCode, percDto);
                        allocationTimeDto.setDateOfService(date);
                        allocationTimeDto.setEquipmentId(equipment.getEquipmentId());
                        allocationTimeDto.setPayrollDate(weekEndDate);
                        equipmentCostCodeDateDto.getEquipmentAllocationTimeMap().put(date.toString(), allocationTimeDto);
                    }
                }

                Set<String> empIds = hourlyEquipmentUsage.stream()
                        .map(u -> u.getEquipment().getEmpId())
                        .filter(StringUtils::hasText)
                        .collect(toSet());
                Set<AllocationTime> allocationTimes = mContentService
                        .getAllocationTimesByEmpIdsAndProjectIdAndDates(empIds, projectId, notProcessedDate, weekEndDate);
                Map<String, List<AllocationTime>> empIdAllocationTimesMap = allocationTimes.stream()
                        .collect(Collectors.groupingBy(AllocationTime::getEmpId));

                for (EquipmentUsage usage : hourlyEquipmentUsage) {
//                    LocalDate startDate = usage.getStartDate().compareTo(weekStartDate) < 0 ? weekStartDate : usage.getStartDate();
                    // Exclude usage end date
//                    LocalDate endDate = (usage.getEndDate() == null ||  usage.getEndDate().compareTo(weekEndDate) > 0) ? weekEndDate : usage.getEndDate().minusDays(1);
                    Equipment equipment = usage.getEquipment();
                    if (equipment.getEmpId() != null && empIdAllocationTimesMap.containsKey(equipment.getEmpId())) {

                        List<AllocationTime> empAllocationTimes = empIdAllocationTimesMap.get(equipment.getEmpId());

                        Map<LocalDate, AllocationTime> dateTimeMap = empAllocationTimes.stream()
                                .collect(toMap(a -> a.getAllocationSubmission().getDateOfService(), Function.identity(),
                                        (e1, e2) -> e1));

                        EquipmentCostCodeDateDto equipmentCostCodeDateDto = equipmentCostCodeDateDtoMap
                                .getOrDefault(equipment.getEquipmentId(), new EquipmentCostCodeDateDto(
                                        equipment.getEquipmentId(), equipment.getSerialNumber(),
                                        equipment.getDescription(), equipment.getAlias(),
                                        equipment.getDepartment(), equipment.getEquipmentClass(), equipment.getEmpId(),
                                        equipment.getHourlyType().getDisplayValue(),
                                        equipment.getOwnershipType().getDisplayValue()));
                        equipmentCostCodeDateDtoMap.putIfAbsent(equipment.getEquipmentId(), equipmentCostCodeDateDto);
                        Set<String> costCodes = equipmentCostCodeDateDto.getCostCodes();

                        BigDecimal weeklyChargeTillNow = BigDecimal.ZERO;
                        BigDecimal monthlyChargeTillNow = previousEquipmentAllocationTimeMap
                                .getOrDefault(equipment.getEquipmentId(), new ArrayList<>()).stream()
                                .map(EquipmentAllocationTime::getTotalCharge)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
                        BigDecimal hourlyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getHourlyRate();
                        BigDecimal dailyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getDailyRate();
                        BigDecimal weeklyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getWeeklyRate();
                        BigDecimal monthlyRate = equipmentPriceMap.getOrDefault(equipment.getEquipmentClass(), new EquipmentPrice()).getMonthlyRate();
                        for (LocalDate date : notProcessedDate) {
                            if (dateTimeMap.containsKey(date)) {
                                AllocationTime time = dateTimeMap.get(date);

                                EquipmentProcessAllocationTimeDto timeDto = new EquipmentProcessAllocationTimeDto();
                                timeDto.setDateOfService(time.getAllocationSubmission().getDateOfService());
                                timeDto.setEquipmentId(equipment.getEquipmentId());
                                timeDto.setPayrollDate(weekEndDate);

                                equipmentCostCodeDateDto.getEquipmentAllocationTimeMap()
                                        .put(time.getAllocationSubmission().getDateOfService().toString(), timeDto);

                                Set<CostCodePerc> costCodePercs = time.getCostCodePercs();
                                if (!CollectionUtils.isEmpty(costCodePercs)) {
                                    costCodes.addAll(costCodePercs.stream()
                                            .map(CostCodePerc::getCostCodeFull).collect(toSet()));
                                    Map<String, EquipmentProcessCostCodePercDto> codeDtoMap = costCodePercs.stream()
                                            .map(EquipmentProcessCostCodePercDto::new)
                                            .collect(toMap(EquipmentProcessCostCodePercDto::getCostCodeFull, Function.identity()));
                                    for (Map.Entry<String, EquipmentProcessCostCodePercDto> entry : codeDtoMap.entrySet()) {
                                        EquipmentProcessCostCodePercDto percDto = entry.getValue();
                                        BigDecimal hourlyCharge = percDto.getTotalHour().multiply(hourlyRate);
                                        BigDecimal monthlyRemaining = BigDecimal.ZERO.max(monthlyRate.subtract(monthlyChargeTillNow));
                                        BigDecimal weeklyRemaining = BigDecimal.ZERO.max(weeklyRate.subtract(weeklyChargeTillNow));
                                        BigDecimal toCharge = hourlyCharge.min(dailyRate.min(weeklyRemaining.min(monthlyRemaining)));
                                        monthlyChargeTillNow = monthlyChargeTillNow.add(toCharge);
                                        weeklyChargeTillNow = weeklyChargeTillNow.add(toCharge);
                                        percDto.setTotalCharge(toCharge);
                                    }
                                    timeDto.setEquipmentCostCodePercMap(codeDtoMap);
                                }
                                equipmentCostCodeDateDto.getEquipmentAllocationTimeMap()
                                        .put(time.getAllocationSubmission().getDateOfService().toString(),
                                                timeDto);
                            }
                        }
                    }
                }
            }

            // TM-275, user be able to copy cost code
            if (model.get("costCodeClipboard") != null && !"removed".equals(model.get("costCodeClipboard"))) {
                applyCostCodeWithSessionClipboard(equipmentCostCodeDateDtoMap,
                        model.get("costCodeClipboard"));
            }

            modelAndView.addObject("equipmentProcessDto",
                    new EquipmentProcessDto(new ArrayList<>(equipmentCostCodeDateDtoMap.values()), new ArrayList<>(days),
                            equipmentWeeklyProcessType.getValue(), weekEndDate));
            Set<String> equipmentIds = equipmentCostCodeDateDtoMap.values().stream()
                    .map(EquipmentCostCodeDateDto::getEquipmentId)
                    .collect(Collectors.toCollection(TreeSet::new));
            modelAndView.addObject("equipmentIds", equipmentIds);
            modelAndView.addObject("weekEndDate", weekEndDate);

            Set<CostCodeLog> validCostCodeLogSet;
            Set<CostCodeLog> otherCostCodeLogSet;
            List<CostCodeLog> allCostCodeLogList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(days)) {
                List<LocalDate> dayList = new ArrayList<>(days);
                allCostCodeLogList = costCodeService.getAllCostCodeLogsByProjectIdAndDateRange(projectId, dayList);
                allCostCodeLogList = new ArrayList<>(allCostCodeLogList.stream()
                        .filter(c -> c.getEndDate() == null
                                || c.getEndDate().isAfter(dayList.get(0))
                                || c.getEndDate().isEqual(dayList.get(0)))
                        .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, Function.identity(),
                                BinaryOperator.maxBy(Comparator.comparing(CostCodeLog::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                                        .thenComparing(CostCodeLog::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())))))
                        .values());
            }
            validCostCodeLogSet = allCostCodeLogList.stream()
                    .filter(c -> c.getProjectId() != null && c.getProjectId().equals(projectId))
                    .collect(Collectors.toCollection(TreeSet::new));
            otherCostCodeLogSet = allCostCodeLogList.stream()
                    .filter(c -> c.getProjectId() == null || !c.getProjectId().equals(projectId))
                    .collect(Collectors.toCollection(TreeSet::new));
            modelAndView.addObject("validCostCodeLogSet", validCostCodeLogSet);
            modelAndView.addObject("otherCostCodeLogSet", otherCostCodeLogSet);
            return modelAndView;
        } else {
            throw new RuntimeException("Unsupported weeklyProcessType");
        }
    }

    @PostMapping(value = "/equipment-process")
    public String equipmentProcess(@RequestParam(required = false) String status,
                                   @ModelAttribute("sessionProjectId") Long projectId,
                                   @ModelAttribute("equipmentProcessDto") EquipmentProcessDto dto) {
        if (!CollectionUtils.isEmpty(dto.getEquipmentCostCodeDateDtos())) {

            Set<EquipmentProcessAllocationTimeDto> equipmentAllocationTimeDtos = dto.getEquipmentCostCodeDateDtos().stream()
                    .map(EquipmentCostCodeDateDto::getEquipmentAllocationTimeMap)
                    .map(Map::values)
                    .flatMap(Collection::stream)
                    .filter(e -> e.getId() != null).collect(toSet());
            Set<Long> equipmentAllocationTimeIds = equipmentAllocationTimeDtos.stream()
                    .map(EquipmentProcessAllocationTimeDto::getId)
                    .collect(toSet());

            List<EquipmentProcessAllocationTimeDto> newEquipmentAllocationTimeDtos = dto.getEquipmentCostCodeDateDtos().stream()
                    .map(EquipmentCostCodeDateDto::getEquipmentAllocationTimeMap)
                    .map(Map::values)
                    .flatMap(Collection::stream)
                    .filter(t -> t.getId() == null).collect(Collectors.toList());

            Set<EquipmentAllocationTime> existingEquipmentAllocationTimes = equipmentService
                    .getEquipmentAllocationTimesByIds(equipmentAllocationTimeIds);
            Map<Long, EquipmentAllocationTime> idEquipmentAllocationTimeMap = existingEquipmentAllocationTimes.stream()
                    .collect(toMap(EquipmentAllocationTime::getId, Function.identity()));

            Set<Long> toDeleteCCIds = new HashSet<>(); // TM-275, total_hour is not function now in table time.equipment_cost_code_perc

            for (EquipmentProcessAllocationTimeDto timeDto : equipmentAllocationTimeDtos) {
                EquipmentAllocationTime allocationTime = idEquipmentAllocationTimeMap.get(timeDto.getId());

                Set<EquipmentCostCodePerc> costCodePercs = allocationTime.getEquipmentCostCodePercs();
                Map<Long, EquipmentCostCodePerc> costCodePercMap = costCodePercs.stream()
                        .collect(toMap(EquipmentCostCodePerc::getId, Function.identity()));

                BigDecimal totalHour = timeDto.getEquipmentCostCodePercMap().values().stream()
                        .map(EquipmentProcessCostCodePercDto::getTotalHour)
                        .filter(Objects::nonNull)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
                BigDecimal totalCharge = timeDto.getEquipmentCostCodePercMap().values().stream()
                        .map(EquipmentProcessCostCodePercDto::getTotalCharge)
                        .filter(Objects::nonNull)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                for (EquipmentProcessCostCodePercDto costCodePercDto : timeDto.getEquipmentCostCodePercMap().values()) {
                    EquipmentCostCodePerc costCodePerc;
                    if (costCodePercDto.getId() != null && costCodePercMap.containsKey(costCodePercDto.getId())) {
                        costCodePerc = costCodePercMap.get(costCodePercDto.getId());
                    } else {
                        costCodePerc = new EquipmentCostCodePerc();
                        costCodePercs.add(costCodePerc);
                    }
                    costCodePerc.setCostCodeFull(costCodePercDto.getCostCodeFull()); // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
                    costCodePerc.setTotalHour(costCodePercDto.getTotalHour());
                    costCodePerc.setTotalCharge(costCodePercDto.getTotalCharge()); // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
                }
                allocationTime.setTotalHour(totalHour);
                allocationTime.setTotalCharge(totalCharge); // TM-275, total_hour is not function now in table time.equipment_cost_code_perc

                // TM-275 Start
                Set<Long> tempSet = new HashSet<>(costCodePercMap.keySet());
                Set<Long> dtoCCIdSet = timeDto.getEquipmentCostCodePercMap().values().stream()
                        .map(EquipmentProcessCostCodePercDto::getId)
                        .collect(toSet());
                tempSet.removeAll(dtoCCIdSet);
                tempSet.forEach(id -> costCodePercs.remove(costCodePercMap.get(id)));
                toDeleteCCIds.addAll(tempSet);
                // TM-275 End
            }

            Map<String, EquipmentCostCodeDateDto> equipmentCostCodeDateDtoMap = dto.getEquipmentCostCodeDateDtos().stream()
                    .collect(toMap(EquipmentCostCodeDateDto::getEquipmentId, Function.identity()));

            Map<LocalDate, List<EquipmentProcessAllocationTimeDto>> dateTimeMap = newEquipmentAllocationTimeDtos.stream()
                    .collect(Collectors.groupingBy(EquipmentProcessAllocationTimeDto::getDateOfService));

            Set<EquipmentAllocationSubmission> submissions = new HashSet<>();

            for (Map.Entry<LocalDate, List<EquipmentProcessAllocationTimeDto>> entry : dateTimeMap.entrySet()) {
                EquipmentAllocationSubmission submission = new EquipmentAllocationSubmission();
                submission.setProjectId(projectId);
                submission.setDateOfService(entry.getKey());
                List<EquipmentAllocationTime> allocationTimes = entry.getValue().stream()
                        .map(t -> {
                            EquipmentCostCodeDateDto equipmentCostCodeDateDto = equipmentCostCodeDateDtoMap.get(t.getEquipmentId());
                            return new EquipmentAllocationTime(t, equipmentCostCodeDateDto);
                        }).collect(Collectors.toList());
                submission.setEquipmentAllocationTimes(new HashSet<>(allocationTimes));
                submissions.add(submission);
            }

            // Change EquipmentAllocationTime and EquipmentAllocationSubmission hierarchy
            Map<EquipmentAllocationSubmission, List<EquipmentAllocationTime>> submissionTimeMap = existingEquipmentAllocationTimes.stream()
                    .collect(Collectors.groupingBy(EquipmentAllocationTime::getEquipmentAllocationSubmission));
            submissionTimeMap.forEach((k, v) -> {
                k.setEquipmentAllocationTimes(new HashSet<>(v));
                v.forEach(t -> t.setEquipmentAllocationSubmission(null));
            });
            submissions.addAll(submissionTimeMap.keySet());

            EquipmentAllocationSubmissionStatus equipmentStatus = null == status ?
                    EquipmentAllocationSubmissionStatus.PENDING : EquipmentAllocationSubmissionStatus.fromString(status);
            submissions.forEach(s -> s.setStatus(equipmentStatus));
            if (equipmentStatus == EquipmentAllocationSubmissionStatus.APPROVED) {
                EquipmentWeeklyProcessType type = EquipmentWeeklyProcessType.fromValue(dto.getType());
                LocalDate weekEndDate = dto.getWeekEndDate();
                EquipmentWeeklyProcess weeklyProcess = equipmentService.getEquipmentWeeklyProcessByProjectIdAndDateAndType(projectId, dto.getWeekEndDate(), type);
                if (weeklyProcess == null) {
                    weeklyProcess = new EquipmentWeeklyProcess();
                }
                weeklyProcess.setWeekEndDate(weekEndDate);
                weeklyProcess.setProjectId(projectId);
                weeklyProcess.setType(type);
                weeklyProcess.setApproveUserId(getUserId());
                weeklyProcess.setApprovedAt(LocalDateTime.now());
                //TODO process cost?
                BigDecimal totalHours = submissions.stream()
                        .map(EquipmentAllocationSubmission::getEquipmentAllocationTimes)
                        .flatMap(Collection::stream)
                        .map(EquipmentAllocationTime::getTotalHour)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                weeklyProcess.setTotalHours(totalHours);
                weeklyProcess = equipmentService.saveEquipmentWeeklyProcess(weeklyProcess);
                Long weeklyProcessId = weeklyProcess.getId();
                submissions.stream()
                        .map(EquipmentAllocationSubmission::getEquipmentAllocationTimes)
                        .flatMap(Collection::stream)
                        .forEach(t -> t.setWeeklyProcessId(weeklyProcessId));
                submissions.forEach(s -> {
                    s.setApprovedAt(LocalDateTime.now());
                    s.setApproveUserId(getUserId());
                });


                Set<String> costCodes = submissions.stream()
                        .map(EquipmentAllocationSubmission::getEquipmentAllocationTimes)
                        .flatMap(Collection::stream)
                        .map(EquipmentAllocationTime::getEquipmentCostCodePercs)
                        .flatMap(Collection::stream)
                        .map(EquipmentCostCodePerc::getCostCodeFull)
                        .collect(Collectors.toSet());

                costCodeService.lockCostCodes(costCodes, projectId);
            }
            if (!toDeleteCCIds.isEmpty()) { // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
                equipmentService.deleteEquipmentCostCodePercBatch(toDeleteCCIds);
            }
            equipmentService.saveEquipmentAllocationSubmissions(submissions);
        }

        return "redirect:home";
    }

    @PostMapping(value = "/weekly-process")
    public String weeklyProcess(@ModelAttribute("sessionProjectId") Long projectId,
                                @ModelAttribute("weeklyProcessDto") WeeklyProcessDto dto) {
        if (!CollectionUtils.isEmpty(dto.getEmpCostCodeDateDtos())) {

            Set<Long> allocationTimeIds = dto.getEmpCostCodeDateDtos().stream()
                    .map(EmpCostCodeDateDto::getEmpAllocationTimeMap)
                    .map(Map::values)
                    .flatMap(Collection::stream)
                    .map(WeeklyProcessAllocationTimeDto::getId)
                    .collect(toSet());

            costCodeService.deleteCostCodeTagAssociationsByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.PERDIEM);
            costCodeService.deleteCostCodeTagAssociationsByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.MOB);

            List<AllocationTime> allocationTimes = mContentService.getAllocationTimesById(projectId, allocationTimeIds, Set.of("costCodePerc", "crafts"));

            Map<Long, AllocationTime> idAllocationTimeMap = allocationTimes.stream()
                    .collect(toMap(AllocationTime::getId, Function.identity()));

            Set<CostCodeTagAssociation> costCodeTagAssociations = new HashSet<>();
            for (EmpCostCodeDateDto empCostCodeDateDto : dto.getEmpCostCodeDateDtos()) {
                for (Map.Entry<String, WeeklyProcessAllocationTimeDto> allocationTimeEntry :
                        empCostCodeDateDto.getEmpAllocationTimeMap().entrySet()) {
                    AllocationTime allocationTime = idAllocationTimeMap.get(allocationTimeEntry.getValue().getId());

                    Set<CostCodePerc> costCodePercs = allocationTime.getCostCodePercs();
                    Map<Long, CostCodePerc> costCodePercMap = costCodePercs.stream()
                            .collect(toMap(CostCodePerc::getId, Function.identity()));

                    List<CostCodeTagAssociationDto> perDiemAssociations = allocationTimeEntry.getValue().getPerDiemCostCodeTagAssociations();
                    if (!CollectionUtils.isEmpty(perDiemAssociations)) {
                        Long key = allocationTime.getId();
                        costCodeTagAssociations.addAll(perDiemAssociations.stream()
                                .map(a -> new CostCodeTagAssociation(a, CostCodeType.PERDIEM, key)).collect(Collectors.toList()));
                    }
                    List<CostCodeTagAssociationDto> mobAssociations = allocationTimeEntry.getValue().getMobCostCodeTagAssociations();
                    if (!CollectionUtils.isEmpty(mobAssociations)) {
                        Long key = allocationTime.getId();
                        costCodeTagAssociations.addAll(mobAssociations.stream()
                                .map(a -> new CostCodeTagAssociation(a, CostCodeType.MOB, key)).collect(Collectors.toList()));
                    }

                    BigDecimal allocatedHour = allocationTimeEntry.getValue().getEmpCostCodePercMap().values().stream()
                            .map(costCodePercDto -> {
                                BigDecimal stHour = costCodePercDto.getStHour();
                                stHour = stHour == null ? BigDecimal.ZERO : stHour;
                                BigDecimal otHour = costCodePercDto.getOtHour();
                                otHour = otHour == null ? BigDecimal.ZERO : otHour;
                                BigDecimal dtHour = costCodePercDto.getDtHour();
                                dtHour = dtHour == null ? BigDecimal.ZERO : dtHour;
                                return stHour.add(otHour).add(dtHour);
                            }).reduce(BigDecimal.ZERO, BigDecimal::add);
                    for (WeeklyProcessCostCodePercDto costCodePercDto
                            : allocationTimeEntry.getValue().getEmpCostCodePercMap().values()) {
                        CostCodePerc costCodePerc;
                        if (costCodePercDto.getId() != null && costCodePercMap.containsKey(costCodePercDto.getId())) {
                            costCodePerc = costCodePercMap.get(costCodePercDto.getId());
                        } else {
                            costCodePerc = new CostCodePerc();
                            costCodePerc.setCostCodeFull(costCodePercDto.getCostCodeFull());
                            costCodePerc.setAllocationTime(allocationTime);
                            costCodePercs.add(costCodePerc);
                        }
                        costCodePerc.setStHour(costCodePercDto.getStHour());
                        costCodePerc.setOtHour(costCodePercDto.getOtHour());
                        costCodePerc.setDtHour(costCodePercDto.getDtHour());

                        BigDecimal stHour = costCodePercDto.getStHour();
                        stHour = stHour == null ? BigDecimal.ZERO : stHour;
                        BigDecimal otHour = costCodePercDto.getOtHour();
                        otHour = otHour == null ? BigDecimal.ZERO : otHour;
                        BigDecimal dtHour = costCodePercDto.getDtHour();
                        dtHour = dtHour == null ? BigDecimal.ZERO : dtHour;
                        costCodePerc.setTotalHour(stHour.add(otHour).add(dtHour));
                        int perc = 0;
                        if (BigDecimal.ZERO.compareTo(allocatedHour) < 0) {
                            perc = costCodePerc.getTotalHour().multiply(BigDecimal.valueOf(100))
                                    .divide(allocatedHour, 2, RoundingMode.FLOOR).intValue();
                        }
                        costCodePerc.setPercentage(perc);
                    }
                    allocationTime.setAllocatedHour(allocatedHour);
                    if (!CollectionUtils.isEmpty(allocationTimeEntry.getValue().getPerDiemCostCodes())) {
                        allocationTime.setHasPerDiem(true);
//                    allocationTime.setHasPerDiem(allocationTimeEntry.getValue().isHasPerDiem());
                        allocationTime.setPerDiemCostCode(allocationTimeEntry.getValue().getPerDiemCostCodes().iterator().next());
                    } else {
                        allocationTime.setHasPerDiem(false);
                        allocationTime.setPerDiemCostCode(null);
                    }
                    if (!CollectionUtils.isEmpty(allocationTimeEntry.getValue().getMobCostCodes())) {
                        allocationTime.setHasMob(true);
                        allocationTime.setMobCostCode(allocationTimeEntry.getValue().getMobCostCodes().iterator().next());
                        allocationTime.setMobMileage(allocationTimeEntry.getValue().getMobMileage());
                        allocationTime.setMobAmount(allocationTimeEntry.getValue().getMobAmount());
                    } else {
                        allocationTime.setHasMob(false);
                        allocationTime.setMobCostCode(null);
                        allocationTime.setMobMileage(BigDecimal.ZERO);
                        allocationTime.setMobAmount(BigDecimal.ZERO);
                    }
                    allocationTime.setPerDiemAmount(allocationTimeEntry.getValue().getPerDiem());
                }
            }
            if (!CollectionUtils.isEmpty(costCodeTagAssociations)) {
                costCodeService.saveCostCodeTagAssociations(costCodeTagAssociations);
            }
            if (!CollectionUtils.isEmpty(allocationTimes)) {
                WeeklyProcess weeklyProcess = weeklyProcessService.findOrGenerateWeeklyProcess(projectId,
                        dto.getWeekEndDate(), WeeklyProcessType.fromValue(dto.getType()));
                allocationTimes.forEach(t -> t.setWeeklyProcessId(weeklyProcess.getId()));
                mContentService.saveAllocationTimes(new HashSet<>(allocationTimes));
                weeklyProcessService.calculateCost(weeklyProcess, new HashSet<>(allocationTimes));
            }
        }
        return "redirect:timesheets";
    }

    @PostMapping(value = "/weekly-process-table")
    public ModelAndView weeklyProcessTable(@RequestBody WeeklyProcessDto weeklyProcessDto,
                                           @RequestParam int costCodeType,
                                           @RequestParam LocalDate dateOfService,
                                           @RequestParam(required = false) String team,
                                           // TM-314 multiple empId
                                           @RequestParam(name = "empId", required = false) List<String> empIdList,
                                           @RequestParam String costCode) {
        if (!CollectionUtils.isEmpty(weeklyProcessDto.getEmpCostCodeDateDtos())) {

            // TM-314 extract common filter
            Predicate<EmpCostCodeDateDto> empFilter = e -> (StringUtils.hasText(team) && e.getTeam().equalsIgnoreCase(team))
                    || (empIdList != null && empIdList.contains(e.getEmpId()));

            if (costCodeType == 0) {
                weeklyProcessDto.getEmpCostCodeDateDtos().stream()
                        .filter(empFilter) // TM-314 extract common filter
                        .forEach(e -> {
                            // TreeSet becomes to HashSet. We need to revert it back.
                            e.setCostCodes(new TreeSet<>(e.getCostCodes()));
                            e.setPerDiemCostCodes(new TreeSet<>(e.getPerDiemCostCodes()));
                            e.setMobCostCodes(new TreeSet<>(e.getMobCostCodes()));
                            e.getCostCodes().add(costCode);
                        });
                Set<WeeklyProcessAllocationTimeDto> allocationTimeDtos = weeklyProcessDto.getEmpCostCodeDateDtos().stream()
                        .filter(empFilter) // TM-314 extract common filter
                        .map(EmpCostCodeDateDto::getEmpAllocationTimeMap)
                        .flatMap(e -> e.values().stream())
                        .filter(at -> at.getDateOfService().equals(dateOfService))
                        .collect(Collectors.toSet());
                if (!CollectionUtils.isEmpty(allocationTimeDtos)) {
                    for (WeeklyProcessAllocationTimeDto allocationTimeDto : allocationTimeDtos) {
                        if (!allocationTimeDto.getEmpCostCodePercMap().containsKey(costCode)) {
                            WeeklyProcessCostCodePercDto costCodePercDto = new WeeklyProcessCostCodePercDto();
                            costCodePercDto.setCostCodeFull(costCode);
                            allocationTimeDto.getEmpCostCodePercMap().put(costCode, costCodePercDto);
                        }
                    }
                }  // else There is no allocationSubmission for this team and date
            } else if (costCodeType == 1) {
                weeklyProcessDto.getEmpCostCodeDateDtos().stream()
                        .filter(empFilter) // TM-314 extract common filter
                        .forEach(e -> {
                            // TreeSet becomes to HashSet. We need to revert it back.
                            e.setCostCodes(new TreeSet<>(e.getCostCodes()));
                            e.setPerDiemCostCodes(new TreeSet<>(e.getPerDiemCostCodes()));
                            e.setMobCostCodes(new TreeSet<>(e.getMobCostCodes()));
                            e.getPerDiemCostCodes().add(costCode);
                        });
            } else if (costCodeType == 2) {
                weeklyProcessDto.getEmpCostCodeDateDtos().stream()
                        .filter(empFilter)
                        .forEach(e -> {
                            // TreeSet becomes to HashSet. We need to revert it back.
                            e.setCostCodes(new TreeSet<>(e.getCostCodes()));
                            e.setPerDiemCostCodes(new TreeSet<>(e.getPerDiemCostCodes()));
                            e.setMobCostCodes(new TreeSet<>(e.getMobCostCodes()));
                            e.getMobCostCodes().add(costCode);
                        });
            }  // else There is no allocationSubmission for this team and date
        }
        //noinspection SpringMVCViewInspection
        ModelAndView modelAndView = new ModelAndView("weekly-process::weekly_timesheet");
        modelAndView.addObject("weeklyProcessDto", weeklyProcessDto);
        return modelAndView;
    }

    @PostMapping(value = "/weekly-process-tag")
    public ModelAndView weeklyProcessTable(@RequestBody WeeklyProcessDto weeklyProcessDto,
                                           @RequestParam int tagFor,
                                           @RequestParam LocalDate dateOfService,
                                           @RequestParam String empId,
                                           @RequestParam String tagType,
                                           @RequestParam String code,
                                           @RequestParam(required = false) String tagDesc,
                                           @RequestParam(required = false) String codeDesc) {
        if (!CollectionUtils.isEmpty(weeklyProcessDto.getEmpCostCodeDateDtos())) {
            weeklyProcessDto.getEmpCostCodeDateDtos().stream()
                    .filter(e -> e.getEmpId().equalsIgnoreCase(empId))
                    .findFirst()
                    .ifPresent(e -> {
                        WeeklyProcessAllocationTimeDto allocationTimeDto = e.getEmpAllocationTimeMap().get(dateOfService.toString());
                        if (allocationTimeDto != null) {
                            allocationTimeDto.addCostCodeTagAssociationDto(tagType, tagDesc, code, codeDesc, CostCodeType.values()[tagFor]);
                        }
                    });
            weeklyProcessDto.getEmpCostCodeDateDtos()
                    .forEach(e -> {
                        // TreeSet becomes to HashSet. We need to revert it back.
                        e.setCostCodes(new TreeSet<>(e.getCostCodes()));
                        e.setPerDiemCostCodes(new TreeSet<>(e.getPerDiemCostCodes()));
                        e.setMobCostCodes(new TreeSet<>(e.getMobCostCodes()));
                    });
        }
        //noinspection SpringMVCViewInspection
        ModelAndView modelAndView = new ModelAndView("weekly-process::weekly_timesheet");
        modelAndView.addObject("weeklyProcessDto", weeklyProcessDto);
        return modelAndView;
    }

    @DeleteMapping(value = "/weekly-process-tag")
    public ModelAndView deleteWeeklyProcessTag(@RequestBody WeeklyProcessDto weeklyProcessDto,
                                               @RequestParam int tagFor,
                                               @RequestParam LocalDate dateOfService,
                                               @RequestParam String empId,
                                               @RequestParam String tagType,
                                               @RequestParam String code) {
        if (!CollectionUtils.isEmpty(weeklyProcessDto.getEmpCostCodeDateDtos())) {
            weeklyProcessDto.getEmpCostCodeDateDtos().stream()
                    .filter(e -> e.getEmpId().equalsIgnoreCase(empId))
                    .findFirst()
                    .ifPresent(e -> {
                        WeeklyProcessAllocationTimeDto allocationTimeDto = e.getEmpAllocationTimeMap().get(dateOfService.toString());
                        if (allocationTimeDto != null) {
                            allocationTimeDto.removeCostCodeTagAssociationDto(tagType, code, CostCodeType.values()[tagFor]);
                        }
                    });
            weeklyProcessDto.getEmpCostCodeDateDtos()
                    .forEach(e -> {
                        // TreeSet becomes to HashSet. We need to revert it back.
                        e.setCostCodes(new TreeSet<>(e.getCostCodes()));
                        e.setPerDiemCostCodes(new TreeSet<>(e.getPerDiemCostCodes()));
                        e.setMobCostCodes(new TreeSet<>(e.getMobCostCodes()));
                    });
        }
        //noinspection SpringMVCViewInspection
        ModelAndView modelAndView = new ModelAndView("weekly-process::weekly_timesheet");
        modelAndView.addObject("weeklyProcessDto", weeklyProcessDto);
        return modelAndView;
    }

    // TM-275, add additional params to query cost codes
    @PostMapping(value = "/equipment-process-table")
    public ModelAndView equipmentProcessTable(@RequestBody EquipmentProcessDto equipmentProcessDto,
                                           @RequestParam LocalDate dateOfService,
                                           @RequestParam String equipmentId,
                                           @RequestParam String costCode,
                                           @ModelAttribute("sessionProjectId") Long projectId,
                                           @ModelAttribute("rule") Rule rule) {
        if (!CollectionUtils.isEmpty(equipmentProcessDto.getEquipmentCostCodeDateDtos())) {
            equipmentProcessDto.getEquipmentCostCodeDateDtos().stream()
                    .filter(e -> e.getEquipmentId().equals(equipmentId))
                    .findFirst().ifPresent(e -> {
                e.setCostCodes(new TreeSet<>(e.getCostCodes()));
                e.getCostCodes().add(costCode);
            });

            Set<EquipmentProcessAllocationTimeDto> allocationTimeDtos = equipmentProcessDto.getEquipmentCostCodeDateDtos().stream()
                    .filter(e -> e.getEquipmentId().equals(equipmentId))
                    .map(EquipmentCostCodeDateDto::getEquipmentAllocationTimeMap)
                    .flatMap(e -> e.values().stream())
                    .filter(at -> at.getDateOfService().equals(dateOfService))
                    .collect(Collectors.toSet());

            if (!CollectionUtils.isEmpty(allocationTimeDtos)) {
                for (EquipmentProcessAllocationTimeDto allocationTimeDto : allocationTimeDtos) {
                    if (!allocationTimeDto.getEquipmentCostCodePercMap().containsKey(costCode)) {
                        EquipmentProcessCostCodePercDto costCodePercDto = new EquipmentProcessCostCodePercDto();
                        costCodePercDto.setCostCodeFull(costCode);
                        costCodePercDto.setTotalCharge(BigDecimal.ZERO);
                        allocationTimeDto.getEquipmentCostCodePercMap().put(costCode, costCodePercDto);
                    }
                }
            }  // else There is no allocationSubmission for this team and date

        }
        //noinspection SpringMVCViewInspection
        ModelAndView modelAndView = new ModelAndView("equipment-process::equipment_timesheet");

        // TM-275 Start
        boolean excludeWeekend = rule.isEquipmentExcludeWeekend();
        LocalDate startDate = dateOfService.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate endDate = dateOfService.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        DateRange dateRange = new DateRange(startDate, endDate, excludeWeekend);
        List<LocalDate> days = dateRange.toList();

        Set<CostCodeLog> validCostCodeLogSet;
        Set<CostCodeLog> otherCostCodeLogSet;
        List<CostCodeLog> allCostCodeLogList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(days)) {
            List<LocalDate> dayList = new ArrayList<>(days);
            allCostCodeLogList = costCodeService.getAllCostCodeLogsByProjectIdAndDateRange(projectId, dayList);
            allCostCodeLogList = new ArrayList<>(allCostCodeLogList.stream()
                    .filter(c -> c.getEndDate() == null
                            || c.getEndDate().isAfter(dayList.get(0))
                            || c.getEndDate().isEqual(dayList.get(0)))
                    .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, Function.identity(),
                            BinaryOperator.maxBy(Comparator.comparing(CostCodeLog::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                                    .thenComparing(CostCodeLog::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())))))
                    .values());
        }
        validCostCodeLogSet = allCostCodeLogList.stream()
                .filter(c -> c.getProjectId() != null && c.getProjectId().equals(projectId))
                .collect(Collectors.toCollection(TreeSet::new));
        otherCostCodeLogSet = allCostCodeLogList.stream()
                .filter(c -> c.getProjectId() == null || !c.getProjectId().equals(projectId))
                .collect(Collectors.toCollection(TreeSet::new));
        modelAndView.addObject("validCostCodeLogSet", validCostCodeLogSet);
        modelAndView.addObject("otherCostCodeLogSet", otherCostCodeLogSet);
        // TM-275 End

        modelAndView.addObject("equipmentProcessDto", equipmentProcessDto);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_purchasing')")
    @DeleteMapping(value = "/requisition")
    public String updateEquipmentValidity(@RequestParam String requisitionNumber) {
        purchaseOrderService.deleteByRequisitionNumber(requisitionNumber);
        return "redirect:purchase-orders";
    }

    @PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/project-payroll")
    public ModelAndView getProjectPayroll(@ModelAttribute("sessionProjectId") Long projectId) {
        ModelAndView modelAndView = new ModelAndView("project-payroll");
        Project project = projectService.getProjectById(projectId);
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    // TM-275, users be able to copy cost code
    @PostMapping(value = "/copy-costcodes")
    public @ResponseBody String copyCostCodes(@RequestBody Map<String, Map<String, Object>> costCodeMapper, ModelMap model) {
        model.addAttribute("costCodeClipboard", costCodeMapper);
        return "success";
    }

    // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
    @GetMapping(value = "/clear-copied-costcodes")
    public @ResponseBody String clearCopiedCostCodes(ModelMap model) {
        model.addAttribute("costCodeClipboard", "removed");
        return "success";
    }

    // TM-275, user be able to copy cost code
    private void applyCostCodeWithSessionClipboard(Map<String, EquipmentCostCodeDateDto> equipmentCostCodeDateDtoMap,
                                                   Object obj) {
        @SuppressWarnings("unchecked")
        Map<String, Map<String, Object>> mapper = (Map<String, Map<String, Object>>) obj;

        for (String equipmentId : equipmentCostCodeDateDtoMap.keySet()) {
            EquipmentCostCodeDateDto equipmentCostCodeDateDto = equipmentCostCodeDateDtoMap.get(equipmentId);

            Set<String> dtoCostCodes = equipmentCostCodeDateDto.getCostCodes();
            if (dtoCostCodes == null || dtoCostCodes.size() > 1) continue;

            String dtoCostCode = dtoCostCodes.iterator().next();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(dtoCostCode)) continue;

            if (mapper.get(equipmentId) == null) continue;

            Map<String, Object> code2timesMap = mapper.get(equipmentId);
            equipmentCostCodeDateDto.setCostCodes(new LinkedHashSet<>(code2timesMap.keySet()));

            Optional<LocalDate> dateOptional = equipmentCostCodeDateDto.getEquipmentAllocationTimeMap().values().stream()
                    .map(EquipmentProcessAllocationTimeDto::getDateOfService)
                    .filter(Objects::nonNull)
                    .findAny();
            if (dateOptional.isEmpty()) continue;
            LocalDate startDate = dateOptional.get().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));

            Map<String, EquipmentProcessAllocationTimeDto> equipmentAllocationTimeMap = equipmentCostCodeDateDto.getEquipmentAllocationTimeMap();
            Map<String, EquipmentProcessAllocationTimeDto> newEquipmentAllocationTimeMap = new HashMap<>();
            equipmentCostCodeDateDto.setEquipmentAllocationTimeMap(newEquipmentAllocationTimeMap);

            for (String cc : code2timesMap.keySet()) {
                LocalDate currentDate = startDate;
                List<Integer> costArray = (List<Integer>)code2timesMap.get(cc);
                for (Object cost : costArray) {
                    if (cost != null) {
                        String timeKey = currentDate.toString();
                        EquipmentProcessAllocationTimeDto oldTimeDto = equipmentAllocationTimeMap.get(timeKey);
                        EquipmentProcessAllocationTimeDto currentTimeDto =
                                newEquipmentAllocationTimeMap.computeIfAbsent(timeKey,
                                        key -> EquipmentProcessAllocationTimeDto.cloneEquipmentProcessAllocationTimeDto(oldTimeDto));
                        EquipmentProcessCostCodePercDto templateCodeDto = oldTimeDto.getEquipmentCostCodePercMap()
                                .values().stream().findAny().orElseGet(EquipmentProcessCostCodePercDto::new);
                        currentTimeDto.getEquipmentCostCodePercMap()
                                .computeIfAbsent(cc, key -> EquipmentProcessCostCodePercDto.cloneCodeDtoWithSpecifiedCostCode(templateCodeDto, cc));
                    }
                    currentDate = currentDate.plusDays(1);
                }
            }
        }
    }
}
