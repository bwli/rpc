package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.service.BillingCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@PreAuthorize("hasRole('ROLE_admin')")
public class AdminRestController extends BaseController {

    private final BillingCodeService billingCodeService;

    @Autowired
    public AdminRestController(BillingCodeService billingCodeService) {
        this.billingCodeService = billingCodeService;
    }

    @PostMapping(value = "/cost-code-structure")
    public String saveBillingCode(@RequestBody List<Long> ids) {
        billingCodeService.saveCostCodeStructure(ids);
        return "success";
    }
}
