package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.AuthenticationHelper;
import com.timaven.timekeeping.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("isAuthenticated()")
public class AnyAuthenticatedController extends BaseController {

    private final AccountService mAccountService;
    private final PasswordEncoder mPasswordEncoder;
    private final UserValidator mUserValidator;
    private final ContentService mContentService;
    private final NotificationService notificationService;
    private final EmployeeService employeeService;
    private final OAuth2RestTemplate oAuth2RestTemplate;
    private final PerDiemService perDiemService;
    // TM-233 add rule service
    private final RuleService ruleService;
    private final ProjectService projectService;
    private final UserProjectService userProjectService;
    private final BillingCodeService billingCodeService;
    private final CostCodeService costCodeService;
    private final RPCUserPoolService rpcUserPoolService;

    @Autowired
    public AnyAuthenticatedController(AccountService mAccountService, PasswordEncoder mPasswordEncoder,
                                      UserValidator mUserValidator, ContentService mContentService,
                                      NotificationService notificationService, EmployeeService employeeService,
                                      OAuth2RestTemplate oAuth2RestTemplate,
                                      RPCUserPoolService rpcUserPoolService,
                                      PerDiemService perDiemService, RuleService ruleService, ProjectService projectService, UserProjectService userProjectService, BillingCodeService billingCodeService, CostCodeService costCodeService) {
        this.mAccountService = mAccountService;
        this.mPasswordEncoder = mPasswordEncoder;
        this.mUserValidator = mUserValidator;
        this.mContentService = mContentService;
        this.notificationService = notificationService;
        this.employeeService = employeeService;
        this.oAuth2RestTemplate = oAuth2RestTemplate;
        this.rpcUserPoolService = rpcUserPoolService;
        this.perDiemService = perDiemService;
        // TM-233 add rule service
        this.ruleService = ruleService;
        this.projectService = projectService;
        this.userProjectService = userProjectService;
        this.billingCodeService = billingCodeService;
        this.costCodeService = costCodeService;
    }

    @InitBinder("user")
    protected void initUserBinder(WebDataBinder binder) {
        binder.addValidators(mUserValidator);
    }

    @PostMapping(value = "/access_token")
    public @ResponseBody
    String getAccessToken() {
        OAuth2AccessToken token = oAuth2RestTemplate.getAccessToken();
        if (token != null) {
            return token.getValue();
        }
        // Token in Authentication may expired
//        if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
//            accessToken = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
//        }
        return null;
    }

    /**
     * Show home page
     */
    @GetMapping(value = "/role-select")
    public ModelAndView roleSelect(Authentication authentication) {
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        ModelAndView modelAndView = new ModelAndView("role_select");
        modelAndView.addObject("roles", roles);
        return modelAndView;
    }

    /**
     * Show home page
     */
    @GetMapping(value = "/home")
    public String home(ModelMap model, Authentication authentication, @RequestParam(required = false) String role) {
        Long projectId = (Long) model.get("sessionProjectId");
        if (!StringUtils.isEmpty(role)) {
            removeOtherPageSpecificRoles(authentication, role);
            if (role.equalsIgnoreCase("ROLE_super_admin")) {
                return "redirect:home-super-admin";
            } else if (role.equalsIgnoreCase("ROLE_admin") || role.equalsIgnoreCase("ROLE_it_admin")) {
                return "redirect:home-admin";
            } else if (role.equalsIgnoreCase("ROLE_project_manager")) {
                if (projectId != null) {
                    return "redirect:project-dashboard";
                } else {
                    return "redirect:home-project-manager";
                }
            } else if (role.equalsIgnoreCase("ROLE_corporate_accountant")
                    || role.equalsIgnoreCase("ROLE_corporate_timekeeper")) {
                return "redirect:home-corporate-accountant";
            } else if (role.equalsIgnoreCase("ROLE_accountant")
                    || role.equalsIgnoreCase("ROLE_timekeeper")
                    || role.equalsIgnoreCase("ROLE_purchasing")
                    || role.equalsIgnoreCase("ROLE_accountant_foreman")) {
                if (projectId != null) {
                    return "redirect:project-dashboard";
                } else {
                    return "redirect:home-accountant";
                }
            } else if (role.equalsIgnoreCase("ROLE_foreman")) {
                return "redirect:home-foreman";
            }
        }
        if (multipleRoleBasedPages()) return "redirect:role-select";
        if (AuthenticationHelper.hasRole("ROLE_super_admin")) {
            return "redirect:home-super-admin";
        } else if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return "redirect:home-admin";
        } else if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            if (projectId != null) {
                return "redirect:project-dashboard";
            } else {
                return "redirect:home-project-manager";
            }
        } else if (AuthenticationHelper.hasRole("ROLE_corporate_accountant")
                || AuthenticationHelper.hasRole("ROLE_corporate_timekeeper")) {
            return "redirect:home-corporate-accountant";
        } else if (AuthenticationHelper.hasRole("ROLE_accountant")
                || AuthenticationHelper.hasRole("ROLE_timekeeper")
                || AuthenticationHelper.hasRole("ROLE_purchasing")
                || AuthenticationHelper.hasRole("ROLE_accountant_foreman")) {
            if (projectId != null) {
                return "redirect:project-dashboard";
            } else {
                return "redirect:home-accountant";
            }
        } else if (AuthenticationHelper.hasRole("ROLE_foreman")) {
            return "redirect:home-foreman";
        }
        return "redirect:index";
    }

    private boolean multipleRoleBasedPages() {
        int possiblePages = 0;
        if (AuthenticationHelper.hasRole("ROLE_super_admin")) {
            possiblePages++;
        }
        if (AuthenticationHelper.hasRole("ROLE_admin")) {
            possiblePages++;
        }
        if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            possiblePages++;
        } else if (AuthenticationHelper.hasRole("ROLE_accountant")
                || AuthenticationHelper.hasRole("ROLE_timekeeper")
                || AuthenticationHelper.hasRole("ROLE_purchasing")
                || AuthenticationHelper.hasRole("ROLE_accountant_foreman")) {
            possiblePages++;
        }
        if (AuthenticationHelper.hasRole("ROLE_corporate_accountant")
                || AuthenticationHelper.hasRole("ROLE_corporate_timekeeper")) {
            possiblePages++;
        }
        if (AuthenticationHelper.hasRole("ROLE_foreman")) {
            possiblePages++;
        }
        return possiblePages > 1;
    }

    private void removeOtherPageSpecificRoles(Authentication auth, String selectedRole) {
        List<String> pageRelatedRoles = Arrays.asList("ROLE_super_admin", "ROLE_admin", "ROLE_project_manager",
                "ROLE_accountant", "ROLE_timekeeper", "ROLE_accountant_foreman", "ROLE_corporate_accountant",
                "ROLE_corporate_timekeeper", "ROLE_foreman", "ROLE_purchasing");
        List<String> toBeRemoved = pageRelatedRoles.stream()
                .filter(r -> !r.equalsIgnoreCase(selectedRole))
                .collect(Collectors.toList());
        List<GrantedAuthority> updatedAuthorities = auth.getAuthorities().stream()
                .filter(r -> !toBeRemoved.contains(r.getAuthority()))
                .collect(Collectors.toList());
        OAuth2Request request = ((OAuth2Authentication) auth).getOAuth2Request();
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                auth.getPrincipal(), "N/A", updatedAuthorities);
        token.setDetails(auth.getDetails());


        OAuth2Authentication newAuth = new OAuth2Authentication(request, token);
        newAuth.setDetails(token.getDetails());
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }

    /**
     * Show the user adding page
     */
    @GetMapping(value = "/user")
    public ModelAndView user(@RequestParam(required = false) String username, ModelMap modelMap) {
        String currentUsername = getUsername();
        User user;
        ModelAndView modelAndView;
        boolean isSelf = false;
        if (username == null) {
            user = new User();
            modelAndView = new ModelAndView("add-user", "user", user);
        } else {
            user = mAccountService.loadUserByUsername(username, Set.of("roles"));
            modelAndView = new ModelAndView("update-user", "user", user);
            isSelf = currentUsername.equals(username);
            modelAndView.addObject("isSelf", isSelf);
        }

        if (username == null || !isSelf) {
            bindUserAttributes(modelMap, user);
        }
        return modelAndView;
    }

    private Map<Role, Long> getRoleIdMapForAdmin() {
        Set<Role> roles = mAccountService.getRolesForAdmin();
        return roles.stream().sorted(Comparator.comparingInt(Role::getDisplayOrder))
                .collect(LinkedHashMap::new, (map, item) -> map.put(item, item.getId()), Map::putAll);
    }

    private Map<Role, Long> getRoleIdMapForManager() {
        Set<Role> roles = mAccountService.getRolesForManager();
        return roles.stream().sorted(Comparator.comparingInt(Role::getDisplayOrder))
                .collect(LinkedHashMap::new, (map, item) -> map.put(item, item.getId()), Map::putAll);
    }

    private Map<Role, Long> getRoleIdMapForSuperAdmin() {
        Set<Role> roles = mAccountService.getRolesForSuperAdmin();
        return roles.stream().sorted(Comparator.comparingInt(Role::getDisplayOrder))
                .collect(LinkedHashMap::new, (map, item) -> map.put(item, item.getId()), Map::putAll);
    }

    @PostMapping(value = "/user")
    public String addUser(ModelMap model, @Validated @ModelAttribute("user") User user, BindingResult bindingResult) {
        Long projectId = (Long) model.get("sessionProjectId");
        if (bindingResult.hasErrors()) {
            bindUserAttributes(model, user);
            return "add-user";
        }
        user.setPassword(mPasswordEncoder.encode(user.getPassword()));
        if (null == projectId) {
            user = mAccountService.saveUser(user);
        } else {
            user = mAccountService.addUser(user, projectId);
        }
        if (!CollectionUtils.isEmpty(user.getTeams())
                && user.getId() != null
                && projectId != null) {
            mContentService.saveUserTeam(user, projectId);
        }
        if (AuthenticationHelper.hasRole("ROLE_super_admin") || AuthenticationHelper.hasRole("ROLE_project_manager")) {
            return rdHome();
        } else if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return rdUsers();
        }
        return "error";
    }

    private void bindUserAttributes(ModelMap model, User user) {
        Map<Role, Long> roleIdMap = new HashMap<>();
        if (AuthenticationHelper.hasRole("ROLE_project_manager")) {
            Long projectId = (Long) model.get("sessionProjectId");
            roleIdMap = getRoleIdMapForManager();
            model.put("teams", mContentService.getTeamNames(projectId, LocalDate.now()));
            if (user.getId() != null) {
                user.setTeams(mContentService.getTeamNamesByUserIdAndProjectId(user.getId(), projectId));
            }
        } else if (AuthenticationHelper.hasRole("ROLE_admin")) {
            roleIdMap = getRoleIdMapForAdmin();
        } else if (AuthenticationHelper.hasRole("ROLE_super_admin")) {
            roleIdMap = getRoleIdMapForSuperAdmin();
        }
        model.put("roleIdMap", roleIdMap);
    }

    @PutMapping(value = "/user")
    public String updateUser(@ModelAttribute("user") User user,
                             ModelMap modelMap) {
        if (!StringUtils.isEmpty(user.getPasswordConfirm())) {
            user.setPassword(mPasswordEncoder.encode(user.getPasswordConfirm()));
        }
        user = mAccountService.saveUser(user);
        Long projectId = null;
        if (modelMap.containsAttribute("sessionProjectId")) {
            projectId = (Long) modelMap.getAttribute("sessionProjectId");
        }
        if (!CollectionUtils.isEmpty(user.getTeams())
                && user.getId() != null
                && projectId != null) {
            mContentService.saveUserTeam(user, projectId);
        }
        if (AuthenticationHelper.hasRole("ROLE_super_admin")
                || AuthenticationHelper.hasRole("ROLE_project_manager")
                || AuthenticationHelper.hasRole("ROLE_accountant")
                || AuthenticationHelper.hasRole("ROLE_timekeeper")
                || AuthenticationHelper.hasRole("ROLE_purchasing")
                || AuthenticationHelper.hasRole("ROLE_accountant_foreman")
                || AuthenticationHelper.hasRole("ROLE_corporate_accountant")
                || AuthenticationHelper.hasRole("ROLE_corporate_timekeeper")
                || AuthenticationHelper.hasRole("ROLE_foreman")) {
            return rdHome();
        } else if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return rdUsers();
        }
        return "add-user";
    }

    @GetMapping(value = "/notifications")
    public ModelAndView getNotifications() {
        List<Notification> notifications = notificationService.getNotificationsByUserId();
        ModelAndView modelAndView = new ModelAndView("notifications");
        modelAndView.addObject("notifications", notifications);
        return modelAndView;
    }

    @PutMapping(value = "/user-validity")
    public String updateUserValidity(@RequestParam(value = "id") Long userId,
                                     @RequestParam(value = "active") boolean active) {
        mAccountService.changeUserValidity(userId, active);
        if (AuthenticationHelper.hasRole("ROLE_super_admin") || AuthenticationHelper.hasRole("ROLE_project_manager")) {
            return rdHome();
        } else if (AuthenticationHelper.hasRole("ROLE_admin")) {
            return rdUsers();
        }
        return "error";
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @GetMapping(value = "/employees")
    public ModelAndView listEmployees(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("employees");
        Long projectId = (Long) model.get("sessionProjectId");
        Set<String> tags = employeeService.getTagsByProjectId(projectId);
        modelAndView.addObject("tags", tags);
        List<PerDiem> perDiemList = perDiemService.getPerDiems(projectId);
        modelAndView.addObject("perDiems", perDiemList);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @GetMapping(value = "/employee-audit")
    public String listEmployeeAudit() {
        return "employee-audit";
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @GetMapping(value = "/employee")
    public ModelAndView employee(@RequestParam(value = "employeeId", required = false) Long id, ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        Employee employee;
        if (id == null) {
            employee = new Employee();
            employee.setProjectId(projectId);
        } else {
            employee = employeeService.findById(id);
        }
        ModelAndView modelAndView = new ModelAndView("employee", "employee", employee);
        ;
        List<PerDiem> perDiemList = perDiemService.getPerDiems(projectId);
        modelAndView.addObject("perDiems", perDiemList);

        // TM-233 add rule info
        Rule rule = (Rule) model.get("rule");
        if (rule == null) {
            rule = ruleService.getRule(projectId).orElse(null);
        }
        List<RPCUserPool> rpcUserPools = rpcUserPoolService.getUserPools();
        modelAndView.addObject("rpcUsers", rpcUserPools);
        modelAndView.addObject("rule", rule);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager','ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @RequestMapping(value = "/employee", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateEmployee(@ModelAttribute("employee") Employee employee) {
        employeeService.saveEmployee(employee);
        return "redirect:employees";
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-data-export")
    public ModelAndView dataExportReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-data-export");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-labor-time")
    public ModelAndView laborTimeReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-labor-time");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-owned-equipment")
    public ModelAndView ownedEquipmentReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-owned-equipment");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-third-equipment")
    public ModelAndView thirdEquipmentReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-third-equipment");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-equipment-billing")
    public ModelAndView equipmentBillingReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-equipment-billing");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-absentee")
    public ModelAndView absenteeReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-absentee");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-whoisin")
    public ModelAndView whoisinReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-whoisin");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-ot")
    public ModelAndView otReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-ot");
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            List<Project> projects = projectService.getAllProjects();
            modelAndView.addObject("projects", projects);
        }
        return modelAndView;
    }

    /**
     * TM-??? RPC profit detail
     * @param model
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = "/report-rpc-profit-detail")
    public ModelAndView rpcProfitDetailReport(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("report-rpc-profit-detail");
        List<Project> projects;
        if (AuthenticationHelper.hasRole("ROLE_corporate_accountant"))  {
            projects = projectService.getAllProjects();
        } else {
            projects = new ArrayList<>(userProjectService.getProjectsByUsername(getUsername())).stream().sorted().collect(Collectors.toList());
        }
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_corporate_accountant', 'ROLE_accountant', 'ROLE_accountant_foreman')")
    @GetMapping(value = {"/report-totals", "/report-classification-codes", "/report-employees", "/report-employee-hours", "/report-per-diem-expense", "/report-travel-pay-summary"})
    public ModelAndView hoursAndBillingReport(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("report-hours-billing");
        List<Project> projects;
        if (AuthenticationHelper.hasRole("ROLE_corporate_accountant"))  {
            projects = projectService.getAllProjects();
        } else {
            projects = new ArrayList<>(userProjectService.getProjectsByUsername(getUsername())).stream().sorted().collect(Collectors.toList());
        }
        modelAndView.addObject("projects", projects);
        List<BillingCodeType> billingCodeTypes = billingCodeService.findAllBillingCodeTypes()
                .stream().sorted(Comparator.comparing(BillingCodeType::getLevel, Comparator.nullsLast(Comparator.naturalOrder()))
                        .thenComparing(BillingCodeType::getCodeType, Comparator.nullsLast(Comparator.naturalOrder())))
                .collect(Collectors.toList());
        List<CostCodeTagType> tagTypes = costCodeService.findAllCostCodeTagTypes();
        List<String> categories = Stream.of(billingCodeTypes.stream()
                        .map(BillingCodeType::getCodeType)
                        .map(String::trim)
                        .map(t -> String.format("segment-%s", t))
                        .collect(Collectors.toList()),
                tagTypes.stream()
                        .map(CostCodeTagType::getTagType)
                        .map(String::trim)
                        .map(c -> String.format("tag-%s", c))
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList())
        ).flatMap(Collection::stream).collect(Collectors.toList());
        modelAndView.addObject("categories", categories);

        String servletPath = request.getServletPath();
        ReportHourAndBillingType type;
        String title;
        if (servletPath.startsWith("/report-totals")) {
            type = ReportHourAndBillingType.COST_BY_TOTALS;
            title = "Cost Reports by totals";
        } else if (servletPath.startsWith("/report-classification-codes")) {
            type = ReportHourAndBillingType.CLASSIFICATION_CODE;
            title = "Classification codes";
        } else if (servletPath.startsWith("/report-employees")) {
            type = ReportHourAndBillingType.EMPLOYEE_HOURS_BILLING;
            title = "Summary of Employee Hours and Billing";
        } else if (servletPath.startsWith("/report-employee-hours")) {
            type = ReportHourAndBillingType.EMPLOYEE_HOURS;
            title = "Weekly Employee Hours Summary";
        } else if (servletPath.startsWith("/report-per-diem-expense")) {
            type = ReportHourAndBillingType.PER_DIEM_EXPENSE;
            title = "Weekly Per Diem Expense Summary";
        } else if(servletPath.startsWith("/report-travel-pay-summary")){
            type = ReportHourAndBillingType.TRAVEL_PAY_SUMMARY;
            title = "Weekly Travel Pay Summary";
        } else {
            throw new IllegalArgumentException("Unknown report type");
        }
        modelAndView.addObject("type", type);
        modelAndView.addObject("title", title);
        return modelAndView;
    }

    private String rdHome() {
        return "redirect:home";
    }

    private String rdUsers() {
        return "redirect:users";
    }

    @GetMapping(value = "/report-labor-time-summary")
    public ModelAndView getReportLaborTimeSummary(){
        ModelAndView modelAndView = new ModelAndView("report-labor-time-summary");
        List<Project> projects;
        if (AuthenticationHelper.hasRole("ROLE_corporate_accountant"))  {
            projects = projectService.getAllProjects();
        } else {
            projects = new ArrayList<>(userProjectService.getProjectsByUsername(getUsername())).stream().sorted().collect(Collectors.toList());
        }
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }
}
