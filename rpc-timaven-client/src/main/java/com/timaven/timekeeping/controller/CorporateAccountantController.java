package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.DateRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@PreAuthorize("hasAnyRole('ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
public class CorporateAccountantController extends BaseController {

    private final ContentService mContentService;
    private final WeeklyProcessService weeklyProcessService;
    private final ReconcileService reconcileService;
    private final EmployeeService employeeService;
    private final AccountService accountService;
    private final RuleService ruleService;
    private final CostCodeService costCodeService;
    private final ActivityCodeService activityCodeService;

    @Autowired
    public CorporateAccountantController(ContentService contentService, WeeklyProcessService weeklyProcessService,
                                         ReconcileService reconcileService, EmployeeService employeeService,
                                         AccountService accountService, RuleService ruleService,
                                         CostCodeService costCodeService, ActivityCodeService activityCodeService) {
        mContentService = contentService;
        this.weeklyProcessService = weeklyProcessService;
        this.reconcileService = reconcileService;
        this.accountService = accountService;
        this.employeeService = employeeService;
        this.ruleService = ruleService;
        this.costCodeService = costCodeService;
        this.activityCodeService = activityCodeService;
    }

    @PreAuthorize("hasAnyRole('ROLE_corporate_accountant')")
    @GetMapping(value = "/reconcile")
    public ModelAndView reconcile(@RequestParam LocalDate weekEndDate) {
        ModelAndView modelAndView = new ModelAndView("reconcile");
        ReconcileDto reconcileDto = reconcileService.getReconcileData(weekEndDate);
        modelAndView.addObject("reconcileDto", reconcileDto);
        modelAndView.addObject("weekEndDate", weekEndDate);
        // TM-233 add rule info
        reconcileDto.getProjectIds().forEach(projectId -> {
            ruleService.getRule(projectId)
                    .ifPresentOrElse(r -> reconcileDto.getHideDtMap().put(projectId,r.isHideDt()),
                            () -> reconcileDto.getHideDtMap().put(projectId, false));
        });
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @GetMapping(value = "/corporate-weekly-process")
    public ModelAndView corporateWeeklyProcess(@RequestParam LocalDate weekEndDate, @RequestParam String teamName) {
        ModelAndView modelAndView = new ModelAndView("corporate-weekly-process");

        WeeklyProcess weeklyProcess = weeklyProcessService.findByDateAndTypeFetching(weekEndDate, teamName, WeeklyProcessType.NORMAL);

        CorporateWeeklyProcessDto dto = null;
        LocalDate weekStartDate = weekEndDate.minusDays(6);
        Map<String, EmpDateDto> idEmpDateMap = new HashMap<>();
        // TM-373 In order to ensure that everyone is in. Instead of only showing the saved personnel
        // First querying people who have saved. Then add unsaved personnel
        if (null != weeklyProcess) {
            if (weeklyProcess.getApproveUserId() != null) {
                weeklyProcess.setApproveUser(accountService.getUserById(weeklyProcess.getApproveUserId()));
            }
            if (weeklyProcess.getReviewUserId() != null) {
                weeklyProcess.setReviewUser(accountService.getUserById(weeklyProcess.getReviewUserId()));
            }
            Set<AllocationTime> allocationTimes = weeklyProcess.getAllocationTimes();
            if (!CollectionUtils.isEmpty(allocationTimes)) {
                for (AllocationTime allocationTime : allocationTimes) {
                    if (idEmpDateMap.containsKey(allocationTime.getEmpId())) {
                        EmpDateDto empDateDto  = idEmpDateMap.get(allocationTime.getEmpId());
                        empDateDto.addAllocationTime(allocationTime);
                    } else {
                        EmpDateDto empDateDto = new EmpDateDto(allocationTime);
                        idEmpDateMap.put(allocationTime.getEmpId(), empDateDto);
                    }
                }
            }
            dto = new CorporateWeeklyProcessDto(weeklyProcess, new ArrayList<>(idEmpDateMap.values()));
        }else{
            dto = new CorporateWeeklyProcessDto();
            dto.setWeekEndDate(weekEndDate);
            dto.setTeamName(teamName);
        }
        EmployeeFilter filter = new EmployeeFilter(null, teamName, null, null, null, weekStartDate, weekEndDate);
        List<Employee> employees = employeeService.getEmployees(filter);

        Map<String, Employee> idEmployeeMap = employees.stream()
                .collect(Collectors.toMap(Employee::getEmpId, Function.identity(), BinaryOperator
                        .maxBy(Comparator.comparing(Employee::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())))));

        // TM-373 add activate code
        List<ActivityCode> activityCodes = activityCodeService.getActivity();
        Map<Long, String> activityCodeMap = activityCodes
                .stream()
                .collect(Collectors.toMap(ActivityCode::getId,ActivityCode::getActivityCode));

        Map<String, Long> activityCodeIdMap = activityCodes
                .stream()
                .collect(Collectors.toMap(ActivityCode::getActivityCode, ActivityCode::getId, BinaryOperator.maxBy(Long::compareTo)));

        Set<String> idEmpDateMapKeys = idEmpDateMap.keySet();
        idEmployeeMap = idEmployeeMap.entrySet().stream()
                .filter(e -> !idEmpDateMapKeys.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Set<LocalDate> days = new TreeSet<>(new DateRange(weekStartDate, weekEndDate, false).toList());

        dto.getEmpDateDtos().addAll(idEmployeeMap.values().stream()
                .filter(e -> e.getTerminatedAt() == null
                        || e.getTerminatedAt().isAfter(weekEndDate)
                        || e.getTerminatedAt().isEqual(weekEndDate) )
                 .map(e -> new EmpDateDto(e, days, activityCodeIdMap))
                .collect(Collectors.toList()));
        dto.setEmpDateDtos(dto.getEmpDateDtos().stream().sorted().collect(Collectors.toList()));

        List<String> empActivityCodes =  idEmployeeMap.values().stream()
                .map(Employee::getActivityCode)
                .filter(StringUtils::hasText)
                .collect(Collectors.toList());
        dto.setActivityCodeMap(activityCodeMap);
        for (String code : empActivityCodes) {
            if (activityCodeIdMap.containsKey(code)) {
                dto.getActivityCodeMap().put(activityCodeIdMap.get(code), code);
            }
        }
        modelAndView.addObject("dto", dto);
        modelAndView.addObject("days", days);
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @PostMapping(value = "/corporate-weekly-process")
    public String saveCorporateWeeklyProcess(@ModelAttribute("dto") CorporateWeeklyProcessDto corporateWeeklyProcessDto,
                                             @RequestParam(required = false) String status) {
        WeeklyProcess weeklyProcess;
        Set<AllocationSubmission> allocationSubmissions = new HashSet<>();
        LocalDate weekEndDate = corporateWeeklyProcessDto.getWeekEndDate();
        List<EmpDateDto> empDateDtos = corporateWeeklyProcessDto.getEmpDateDtos();
        Map<LocalDate, AllocationSubmission> dateSubmissionMap = new HashMap<>();
        Map<Long, String> activityCodeMap = activityCodeService.getActivity()
                .stream()
                .collect(Collectors.toMap(ActivityCode::getId,ActivityCode::getActivityCode));

        String teamName = corporateWeeklyProcessDto.getTeamName();
        if (corporateWeeklyProcessDto.getId() != null) {
            weeklyProcess = weeklyProcessService.findWeeklyProcessByIdFetching(corporateWeeklyProcessDto.getId());
            Map<EmpDateKey, AllocationTime> allocationTimeMap = weeklyProcess.getAllocationTimes().stream()
                    .collect(Collectors.toMap(t -> new EmpDateKey(t.getEmpId(), t.getAllocationSubmission().getDateOfService())
                    , Function.identity()));

            Map<EmpDateActivityKey, ActivityCodePerc> activityCodePercMap = weeklyProcess.getAllocationTimes().stream()
                    .map(e -> e.getActivityCodePercs())
                    .flatMap(Set::stream)
                    .collect(Collectors.toMap(e -> new EmpDateActivityKey(e.getAllocationTime().getId(), e.getActivityId()), Function.identity()));

            empDateDtos.forEach(dto -> {
                Set<String> days =  dto.getStMap().values().stream()
                        .map(e -> e.keySet())
                        .flatMap(Set::stream)
                        .collect(Collectors.toSet());
                Map<Long, Map<String, EmpDateHourDto>> extraTimeMap = dto.getExtraTimeMap();
                Map<Long, Map<String, BigDecimal>> stMap = dto.getStMap();
                Set<Long> activateCodeIds = dto.getStMap().keySet();

                days.stream().forEach(day -> {

                    BigDecimal allocationTimeOtTotal = BigDecimal.ZERO;
                    BigDecimal allocationTimeStTotal = BigDecimal.ZERO;

                    LocalDate dateOfService = LocalDate.parse(day, DateTimeFormatter.ISO_LOCAL_DATE);
                    AllocationTime allocationTime = allocationTimeMap.get(new EmpDateKey(dto.getEmpId(), dateOfService));

                    AllocationSubmission submission = dateSubmissionMap.getOrDefault(dateOfService,
                            new AllocationSubmission());
                    if (!dateSubmissionMap.containsKey(dateOfService)) {
                        submission.setAllocationTimes(new HashSet<>());
                    }
                    submission.setDateOfService(dateOfService);
                    submission.setSubmitUserId(getUserId());
                    submission.setApproveUserId(getUserId());
                    submission.setStatus(AllocationSubmissionStatus.APPROVED);
                    submission.setApprovedAt(LocalDateTime.now());
                    submission.setPayrollDate(weekEndDate);
                    dateSubmissionMap.putIfAbsent(dateOfService, submission);

                    if (Objects.isNull(allocationTime)) {
                        allocationTime = new AllocationTime();
                        allocationTime.setEmpId(dto.getEmpId());
                        allocationTime.setLastName(dto.getLastName());
                        allocationTime.setFirstName(dto.getFirstName());
                        allocationTime.setTeamName(dto.getTeamName());
                        allocationTime.setJobNumber(dto.getJobNumber());
                    }

                    for(Long activityCodeId : activateCodeIds){
                        if(stMap.containsKey(activityCodeId) && stMap.get(activityCodeId).containsKey(day)){
                            ActivityCodePerc activityCodePerc = activityCodePercMap.get(new EmpDateActivityKey(allocationTime.getId(), activityCodeId));

                            if(Objects.isNull(activityCodePerc)){
                                activityCodePerc = new ActivityCodePerc();
                                activityCodePerc.setEmployeeId(dto.getEmpId());
                                activityCodePerc.setActivityId(activityCodeId);
                                activityCodePerc.setActivityCodeText(activityCodeMap.get(activityCodeId));

                                ActivityCode activityCode = new ActivityCode();
                                activityCode.setId(activityCodeId);
                                activityCode.setActivityCode(activityCodeMap.get(activityCodeId));
                                activityCodePerc.setActivityCode(activityCode);

                                allocationTime.setActivityCodePercs(new HashSet<>());
                            }else{
                                ActivityCode activityCode = new ActivityCode();
                                activityCode.setId(activityCodePerc.getActivityId());
                                activityCode.setActivityCode(activityCodeMap.get(activityCodePerc.getActivityId()));
                                activityCodePerc.setActivityCode(activityCode);
                            }

                            EmpDateHourDto empDateHourDto = extraTimeMap.get(activityCodeId).get(day);
                            BigDecimal stHour = stMap.get(activityCodeId).get(day) == null ? BigDecimal.ZERO : stMap.get(activityCodeId).get(day);
                            BigDecimal extraHour = empDateHourDto.getHour() == null ? BigDecimal.ZERO : empDateHourDto.getHour();


                            activityCodePerc.setOtHour(extraHour);
                            activityCodePerc.setStHour(stHour);
                            activityCodePerc.setExtraTimeType(AllocationTimeHourType.values()[empDateHourDto.getType()]);

                            BigDecimal allocatedHour = (extraHour == null ? BigDecimal.ZERO : extraHour)
                                    .add(stHour == null ? BigDecimal.ZERO : stHour);
                            activityCodePerc.setTotalHour(allocatedHour);

                            allocationTimeOtTotal = allocationTimeOtTotal.add(extraHour);
                            allocationTimeStTotal = allocationTimeStTotal.add(stHour);

                            allocationTime.getActivityCodePercs().add(activityCodePerc);
                        }
                    }
                    BigDecimal allocatedHour = (allocationTimeOtTotal == null ? BigDecimal.ZERO : allocationTimeOtTotal)
                            .add(allocationTimeStTotal == null ? BigDecimal.ZERO : allocationTimeStTotal);
                    allocationTime.setTotalHour(allocatedHour);
                    allocationTime.setAllocatedHour(allocatedHour);
                    allocationTime.setNetHour(allocatedHour);
                    allocationTime.setOtHour(allocationTimeOtTotal);
                    allocationTime.setStHour(allocationTimeStTotal);
                    allocationTime.setHasPerDiem(dto.isHasPerDiem());
                    allocationTime.setIsOffsite(dto.isOffsite()); // TM-307
                    submission.getAllocationTimes().add(allocationTime);
                });


            });

            Map<AllocationSubmission, List<AllocationTime>> submissionTimeMap = allocationTimeMap.values().stream()
                    .collect(Collectors.groupingBy(AllocationTime::getAllocationSubmission));
            submissionTimeMap.forEach((k, v) -> {
                k.setAllocationTimes(new HashSet<>(v));
                v.forEach(t -> t.setAllocationSubmission(null));
            });

            // TM-373 Judging whether there is a new number of days
            dateSubmissionMap.values().stream().forEach(k -> {
                boolean isNotAdd = true;
                for( AllocationSubmission submission : submissionTimeMap.keySet()){
                    // isNotAdd false Existence is added to the current allocation submission
                    if(k.getDateOfService().equals(submission.getDateOfService())){
                        submission.getAllocationTimes().addAll(k.getAllocationTimes());
                        submissionTimeMap.get(submission).addAll(k.getAllocationTimes());
                        isNotAdd = false;
                    }
                }
                // No join the currentallocationSubmissions
                if(isNotAdd){
                    allocationSubmissions.add(k);
                }
            });

            allocationSubmissions.addAll(submissionTimeMap.keySet());
        } else {
            // TM-373  corporateWeeklyProcess findByDateAndTypeFetching No inquiry. Suspected problem
            // Must pass parameters teamName
            // Result in regeneration weeklyProcess. Will lead to repeated storage. Violation of unique constraints
            weeklyProcess = weeklyProcessService.findOrGenerateWeeklyProcess(null, weekEndDate, WeeklyProcessType.NORMAL, teamName);

            // TM-373 foreach get EmpDateDto
            empDateDtos.forEach(dto -> {
                        Map<Long, Map<String, EmpDateHourDto>> extraTimeMap = dto.getExtraTimeMap();
                        Map<Long, Map<String, BigDecimal>> stMap = dto.getStMap();
                        Set<Long> activityCodeIds = dto.getStMap().keySet();
                        Set<String> days = stMap.values().stream()
                                .map(e -> e.keySet())
                                .flatMap(Set::stream)
                                .collect(Collectors.toSet());

                        days.stream().forEach(day -> {
                            BigDecimal allocationTimeOtTotal = BigDecimal.ZERO;
                            BigDecimal allocationTimeStTotal = BigDecimal.ZERO;
                            LocalDate dateOfService = LocalDate.parse(day, DateTimeFormatter.ISO_LOCAL_DATE);
                            AllocationSubmission submission = dateSubmissionMap.getOrDefault(dateOfService,
                                    new AllocationSubmission());

                            submission.setDateOfService(dateOfService);
                            submission.setSubmitUserId(getUserId());
                            submission.setApproveUserId(getUserId());
                            submission.setStatus(AllocationSubmissionStatus.APPROVED);
                            submission.setApprovedAt(LocalDateTime.now());
                            submission.setPayrollDate(weekEndDate);
                            dateSubmissionMap.putIfAbsent(dateOfService, submission);

                            AllocationTime allocationTime = new AllocationTime();
                            allocationTime.setEmpId(dto.getEmpId());
                            allocationTime.setLastName(dto.getLastName());
                            allocationTime.setFirstName(dto.getFirstName());
                            allocationTime.setTeamName(dto.getTeamName());
                            allocationTime.setJobNumber(dto.getJobNumber());

                            for(Long activityCodeId : activityCodeIds){
                                if (stMap.containsKey(activityCodeId) && stMap.get(activityCodeId).containsKey(day)) {
                                    ActivityCodePerc activityCodePerc = new ActivityCodePerc();

                                    ActivityCode activityCode = new ActivityCode();
                                    activityCode.setId(activityCodeId);
                                    activityCode.setActivityCode(activityCodeMap.get(activityCodeId));

                                    EmpDateHourDto empDateHourDto = extraTimeMap.get(activityCodeId).get(day);
                                    BigDecimal stHour = stMap.get(activityCodeId).get(day) == null ? BigDecimal.ZERO : stMap.get(activityCodeId).get(day);
                                    BigDecimal extraHour = empDateHourDto.getHour() == null ? BigDecimal.ZERO : empDateHourDto.getHour();

                                    activityCodePerc.setEmployeeId(dto.getEmpId());
                                    activityCodePerc.setActivityCode(activityCode);
                                    activityCodePerc.setActivityCodeText(activityCodeMap.get(activityCodeId));
                                    activityCodePerc.setOtHour(extraHour);
                                    activityCodePerc.setStHour(stHour);
                                    activityCodePerc.setExtraTimeType(AllocationTimeHourType.values()[empDateHourDto.getType()]);
                                    activityCodePerc.setActivityId(activityCodeId);

                                    BigDecimal allocatedHour = (extraHour == null ? BigDecimal.ZERO : extraHour)
                                            .add(stHour == null ? BigDecimal.ZERO : stHour);
                                    activityCodePerc.setTotalHour(allocatedHour);

                                    allocationTimeOtTotal = allocationTimeOtTotal.add(extraHour);
                                    allocationTimeStTotal = allocationTimeStTotal.add(stHour);

                                    allocationTime.getActivityCodePercs().add(activityCodePerc);
                                }
                            }

                            BigDecimal allocatedHour = (allocationTimeOtTotal == null ? BigDecimal.ZERO : allocationTimeOtTotal)
                                    .add(allocationTimeStTotal == null ? BigDecimal.ZERO : allocationTimeStTotal);
                            allocationTime.setTotalHour(allocatedHour);
                            allocationTime.setAllocatedHour(allocatedHour);
                            allocationTime.setNetHour(allocatedHour);
                            allocationTime.setOtHour(allocationTimeOtTotal);
                            allocationTime.setStHour(allocationTimeStTotal);
                            allocationTime.setHasPerDiem(dto.isHasPerDiem());
                            allocationTime.setIsOffsite(dto.isOffsite()); // TM-307
                            submission.getAllocationTimes().add(allocationTime);
                        });
                    });
            allocationSubmissions.addAll(dateSubmissionMap.values());
        }
        if (StringUtils.hasText(status)) {
            if (status.equalsIgnoreCase("approve")) {
                weeklyProcess.setApproveUserId(getUserId());
                weeklyProcess.setApprovedAt(LocalDateTime.now());
                weeklyProcess.setRejectAt(null);
                weeklyProcess.setRejectUserId(null);
            }
            if (status.equalsIgnoreCase("review")) {
                weeklyProcess.setReviewUserId(getUserId());
                weeklyProcess.setReviewedAt(LocalDateTime.now());
                weeklyProcess.setRejectAt(null);
                weeklyProcess.setRejectUserId(null);
            }
            Rule rule = ruleService.getRule(null).orElse(null);
            if (rule != null) {
                Set<AllocationTimeHourType> types = allocationSubmissions.stream()
                        .map(AllocationSubmission::getAllocationTimes)
                        .flatMap(Set::stream)
                        .map(AllocationTime::getActivityCodePercs)
                        .flatMap(Set::stream)
                        .map(ActivityCodePerc::getExtraTimeType)
                        .collect(Collectors.toSet());

                Set<String> costCodes = new HashSet<>();
                for (AllocationTimeHourType type : types) {
                    switch (type) {
                        case OT -> {
                            if (StringUtils.hasText(rule.getOtCostCode())) {
                                costCodes.add(rule.getOtCostCode());
                            }
                        }
                        case SICK -> {
                            if (StringUtils.hasText(rule.getSickCostCode())) {
                                costCodes.add(rule.getSickCostCode());
                            }
                        }
                        case HOLIDAY -> {
                            if (StringUtils.hasText(rule.getHolidayCostCode())) {
                                costCodes.add(rule.getHolidayCostCode());
                            }
                        }
                        case VACATION -> {
                            if (StringUtils.hasText(rule.getVacationCostCode())) {
                                costCodes.add(rule.getVacationCostCode());
                            }
                        }
                        case OTHER -> {
                            if (StringUtils.hasText(rule.getOtherCostCode())) {
                                costCodes.add(rule.getOtherCostCode());
                            }
                        }
                    }
                }
                if (!CollectionUtils.isEmpty(costCodes)) {
                    costCodeService.lockCostCodes(costCodes, null);
                }
            }
        }
        // TM-373 Here must set teamName.
        // Because weeklyProcessService.calculateCost findOrGenerateWeeklyProcess
        // Will find or build according to teamName
        weeklyProcess.setTeamName(teamName);
        weeklyProcess = weeklyProcessService.save(weeklyProcess);
        if (!CollectionUtils.isEmpty(allocationSubmissions)) {
            Long weeklyProcessId = weeklyProcess.getId();
            allocationSubmissions.stream()
                    .map(AllocationSubmission::getAllocationTimes)
                    .flatMap(Collection::stream)
                    .forEach(t -> t.setWeeklyProcessId(weeklyProcessId));
            mContentService.saveAllocationSubmissions(allocationSubmissions);
        }
        weeklyProcessService.calculateCost(null, teamName, weekEndDate, WeeklyProcessType.NORMAL);
        return "redirect:home";
    }

    @PreAuthorize("hasAnyRole('ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @PostMapping(value = "/corporate-weekly-process-reload-table")
    public ModelAndView corporateWeeklyProcessReloadTable(@RequestBody CorporateWeeklyProcessDto corporateWeeklyProcessDt,
                                                          @RequestParam("dateOfService")String dataOfService,
                                                          @RequestParam("employeeId")String employeeId,
                                                          @RequestParam("activityCodeId")Long activityCodeId) {
        ModelAndView modelAndView = new ModelAndView("corporate-weekly-process::div-table");

        LocalDate weekEndDate = corporateWeeklyProcessDt.getWeekEndDate();
        LocalDate weekStartDate = weekEndDate.minusDays(6);
        Set<LocalDate> days = new TreeSet<>(new DateRange(weekStartDate, weekEndDate, false).toList());

        if(days.contains(LocalDate.parse(dataOfService))){
            corporateWeeklyProcessDt.getEmpDateDtos().stream()
                    .filter(e -> e.getEmpId().equals(employeeId))
                    .forEach(e -> {
                        if(!e.getActivityCodeIds().contains(activityCodeId)){
                            e.getActivityCodeIds().add(activityCodeId);
                        }
                        Map<Long, Map<String, BigDecimal>> stMap = e.getStMap();
                        Map<Long, Map<String, EmpDateHourDto>> extraTimeMap = e.getExtraTimeMap();
                        if(!stMap.containsKey(activityCodeId)){
                            stMap.put(activityCodeId, new HashMap<>());
                        }
                        if(!extraTimeMap.containsKey(activityCodeId)){
                            extraTimeMap.put(activityCodeId, new HashMap<>());
                        }
                        stMap.get(activityCodeId).put(dataOfService, null);
                        extraTimeMap.get(activityCodeId).put(dataOfService, new EmpDateHourDto());
                    });
        }

        Map<Long, String> activityCodeMap = activityCodeService.getActivity()
                .stream()
                .collect(Collectors.toMap(ActivityCode::getId,ActivityCode::getActivityCode));

        corporateWeeklyProcessDt.setActivityCodeMap(activityCodeMap);
        modelAndView.addObject("dto", corporateWeeklyProcessDt);
        modelAndView.addObject("days", days);
        return modelAndView;
    }

    // TM-306
    @PreAuthorize("hasAnyRole('ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
    @PostMapping("/copy-previous-timesheet")
    public ModelAndView copyPreviousTimesheet(@RequestBody CorporateWeeklyProcessDto dto, LocalDate date) {
        ModelAndView modelAndView = new ModelAndView("corporate-weekly-process::div-table");
        modelAndView.addObject("dto", dto);
        LocalDate startDate = date.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate endDate = date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        Set<LocalDate> days = new TreeSet<>(new DateRange(startDate, endDate, false).toList());
        modelAndView.addObject("days", days);

        Map<String, Map<Long, Map<String, Object>[]>> data = getPreviousTimesheetData(date);
        if (data != null) {
            Map<Long, String> activityCodeMap = activityCodeService.getActivity()
                    .stream()
                    .collect(Collectors.toMap(ActivityCode::getId, ActivityCode::getActivityCode));
            dto.setActivityCodeMap(activityCodeMap);

            String[] dateStrings = days.stream().map(LocalDate::toString).toArray(String[]::new);
            dto.getEmpDateDtos().forEach(emp -> {
                String empId = emp.getEmpId();
                if (data.get(empId) == null) return;

                List<Long> activityCodeIds = new ArrayList<>();
                emp.setActivityCodeIds(activityCodeIds);
                emp.setStMap(new HashMap<>());
                emp.setExtraTimeMap(new EmpExtraTimeMap());

                Map<Long, Map<String, Object>[]> codeMap = data.get(empId);
                codeMap.forEach((codeId,v) -> {
                    activityCodeIds.add(codeId);

                    for (int i = 0; i < dateStrings.length; i++) {
                        if (v[i] != null) {
                            Map<String, BigDecimal> stNestedMap = emp.getStMap()
                                    .computeIfAbsent(codeId, id -> new HashMap<>());
                            Map<String, EmpDateHourDto> extraNestedMap = emp.getExtraTimeMap()
                                    .computeIfAbsent(codeId, id -> new HashMap<>());

                            BigDecimal stHour = BigDecimal.ZERO;
                            EmpDateHourDto extraHour = new EmpDateHourDto();
                            try {
                                String hourText = v[i].get("stHour").toString();
                                long hourLong = Long.parseLong(hourText);
                                stHour = BigDecimal.valueOf(hourLong);

                                String extraHourText = v[i].get("extraHour").toString();
                                String extraTimeType = v[i].get("extraTimeType").toString();
                                long extraHourLong = Long.parseLong(extraHourText);
                                extraHour.setHour(BigDecimal.valueOf(extraHourLong));
                                extraHour.setType(Integer.parseInt(extraTimeType));
                            } catch (Exception ignored) {}
                            stNestedMap.put(dateStrings[i], stHour);
                            extraNestedMap.put(dateStrings[i], extraHour);
                        }
                    }
                });

            });
        }

        return modelAndView;
    }


    // TM-306
    /**
     * @return the return data as map or {@code null} if no data,
     * the data's structure is follow:
     * <pre>
     * {
     *   "100544": {
     *     2: [
     *       {
     *         "stHour": 0,
     *         "extraHour": 0,
     *         "extraTimeType": 0
     *       }...
     *     ]
     *     3: [
     *       {
     *         "stHour": 0,
     *         "extraHour": 0,
     *         "extraTimeType": 0
     *       }
     *     ]
     *   }...
     * }
     * </pre>
     */
    @SuppressWarnings("unchecked")
    private Map<String, Map<Long, Map<String, Object>[]>> getPreviousTimesheetData(LocalDate date) {
        LocalDate weekEndDate = date.with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
        WeeklyProcess weeklyProcess = weeklyProcessService.findByDateAndTypeFetching(weekEndDate, null, WeeklyProcessType.NORMAL);
        if (weeklyProcess == null) return null;

        Map<String, Map<Long, Map<String, Object>[]>> rstMap = new HashMap<>();
        weeklyProcess.getAllocationTimes().stream()
                .map(AllocationTime::getActivityCodePercs)
                .flatMap(Collection::stream).forEach(code -> {
                    AllocationTime time = code.getAllocationTime();
                    String empId = time.getEmpId();

                    LocalDate dateOfService = time.getAllocationSubmission().getDateOfService();
                    int index = dateOfService.getDayOfWeek().getValue() - 1;

                    Map<Long, Map<String, Object>[]> codeMap = rstMap.computeIfAbsent(empId, id -> new HashMap<>());
                    Map<String, Object>[] array = codeMap.computeIfAbsent(code.getActivityId(), k -> new Map[7]);
                    if (array[index] == null) {
                        array[index] = new HashMap<>();
                    }
                    array[index].put("stHour", code.getStHour());
                    array[index].put("extraHour", code.getOtHour());
                    array[index].put("extraTimeType", code.getExtraTimeType().getValue());
                });

        return rstMap;
    }
}
