package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.ActivityCode;
import com.timaven.timekeeping.service.ActivityCodeService;
import com.timaven.timekeeping.service.WeeklyProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@SessionAttributes({"sessionProjectId"})
@PreAuthorize("hasAnyRole('ROLE_corporate_accountant')")
public class CorporateAccountantRestController extends BaseController {

    private final WeeklyProcessService weeklyProcessService;
    private final ActivityCodeService activityCodeService;

    @Autowired
    public CorporateAccountantRestController(WeeklyProcessService weeklyProcessService,
                                             ActivityCodeService activityCodeService) {
        this.weeklyProcessService = weeklyProcessService;
        this.activityCodeService = activityCodeService;
    }

    @PutMapping(value = "/per-diem-taxable")
    public String updatePerDiemTaxable(@RequestParam Long id, @RequestParam boolean taxablePerDiem) {
        weeklyProcessService.updatePerDiemTaxable(id, taxablePerDiem);
        return RESPONSE_SUCCESS;
    }

    @PutMapping(value = "/weekly-payroll-comment")
    public String updateWeeklyPayrollComment(@RequestParam Long id, @RequestParam String comment) {
        weeklyProcessService.updateWeeklyPayrollComment(id, comment);
        return RESPONSE_SUCCESS;
    }

    @PutMapping(value = "/activity-codes")
    public ActivityCode addActivateCode(@RequestBody ActivityCode activateCode) {
        return activityCodeService.saveActivityCode(activateCode);
    }

}
