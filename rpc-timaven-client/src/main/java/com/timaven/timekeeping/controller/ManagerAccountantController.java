package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.EmployeeDto;
import com.timaven.timekeeping.model.dto.ProjectsDashboardDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.util.AuthenticationHelper;
import com.timaven.timekeeping.util.DateRange;
import com.timaven.timekeeping.util.StreamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasAnyRole('ROLE_project_manager', 'ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper', 'ROLE_purchasing')")
public class ManagerAccountantController extends BaseController {

    private final ContentService contentService;
    private final RuleService ruleService;
    private final ProjectService projectService;
    private final CostCodeService costCodeService;
    private final UserProjectService userProjectService;
    private final WeeklyProcessService weeklyProcessService;

    @Autowired
    public ManagerAccountantController(ContentService contentService, RuleService ruleService,
                                       ProjectService projectService, CostCodeService costCodeService,
                                       UserProjectService userProjectService, WeeklyProcessService weeklyProcessService) {
        this.contentService = contentService;
        this.ruleService = ruleService;
        this.projectService = projectService;
        this.costCodeService = costCodeService;
        this.userProjectService = userProjectService;
        this.weeklyProcessService = weeklyProcessService;
    }


    @GetMapping(value = {"/home-accountant", "/home-project-manager", "/projects-dashboard"})
    public ModelAndView manageAccountantHome(ModelMap model) {
        ModelAndView modelAndView;
        model.clear();
        modelAndView = new ModelAndView("projects-dashboard");
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('ROLE_project_manager', 'ROLE_accountant', 'ROLE_timekeeper', 'ROLE_accountant_foreman', 'ROLE_purchasing')")
    @GetMapping(value = "/project-dashboard")
    public ModelAndView getProjectDashboard(ModelMap model,
                                            @RequestParam(required = false) Long projectId) {
        if (projectId != null) {
            model.addAttribute("sessionProjectId", projectId);
        } else {
            projectId = (Long) model.getAttribute("sessionProjectId");
        }
        ModelAndView modelAndView = new ModelAndView("project-dashboard");
        Rule rule = null;
        if (model.containsAttribute("rule")) {
            rule = (Rule) model.getAttribute("rule");
        } else {
            Optional<Rule> ruleOpt = ruleService.getRule(projectId);
            if (ruleOpt.isEmpty()) {
                if (!AuthenticationHelper.hasRole("ROLE_project_manager")) {
                    modelAndView = new ModelAndView("operation-error");
                    modelAndView.addObject("errorMsg", "Rule or default shift has not been set.");
                    modelAndView.addObject("solution", "Please ask your project manager to set up the rule and default shift");
                    return modelAndView;
                }
            } else {
                modelAndView.addObject("rule", ruleOpt.orElse(null));
                rule = ruleOpt.get();
            }
        }
        if (null != rule) {
            modelAndView.addObject("allowedDayOfWeek", (rule.getWeekEndDay().getValue()) % 7);
            LocalDate startOfWeek = rule.getStartOfWeek(LocalDate.now());
            LocalDate endOfWeek = rule.getEndOfWeek(LocalDate.now());
            List<AllocationSubmission> submissions = contentService
                    .getApprovedAllocationSubmissionsBetween(startOfWeek, endOfWeek, projectId);
            Map<String, Long> timesheetsProcessedMap = submissions.stream()
                    .map(AllocationSubmission::getDateOfService)
                    .collect(Collectors.groupingBy(LocalDate::toString, Collectors.counting()));
            List<LocalDate> days = new DateRange(startOfWeek, endOfWeek, false).toList();
            modelAndView.addObject("days", days);
            modelAndView.addObject("timesheetsProcessedMap", timesheetsProcessedMap);

            Function<AllocationTime, String> timeToCraftMapper = t ->
                    Optional.ofNullable(t.getEmployee().getCraft()).orElse("Unknown");

            Set<AllocationTime> allocationTimes = submissions.stream()
                    .map(AllocationSubmission::getAllocationTimes)
                    .flatMap(Collection::stream).collect(Collectors.toSet());
            Map<String, Long> craftCountMap = allocationTimes.stream()
                    .filter(StreamUtil.distinctByKeys(t -> t.getEmployee().getEmpId()))
                    .collect(Collectors.groupingBy(timeToCraftMapper, Collectors.counting()));

            Set<String> teams = contentService.getTeamNames(projectId, LocalDate.now());
            modelAndView.addObject("teamCount", teams.size());

            Map<String, Double> craftHourMap = allocationTimes.stream()
                    .collect(Collectors.groupingBy(timeToCraftMapper,
                        Collectors.summingDouble(t -> t.getAllocatedHour().doubleValue())));
            List<String> craftCodes = allocationTimes.stream()
                    .map(AllocationTime::getEmployee)
                    .map(e -> Optional.ofNullable(e.getCraft()).orElse("Unknown"))
                    .collect(toSet()).stream()
                    .sorted()
                    .collect(toList());
            Set<Craft> crafts = allocationTimes.stream()
                    .map(AllocationTime::getEmployee)
                    .map(Employee::getCraftEntity)
                    .filter(Objects::nonNull)
                    .collect(toSet());
            Map<String, String> craftDescriptionMap = crafts.stream()
                    .collect(Collectors.toMap(Craft::getCode, c -> Optional.ofNullable(c.getDescription()).orElse("")));
            modelAndView.addObject("craftCodes", craftCodes);
            modelAndView.addObject("craftCountMap", craftCountMap);
            modelAndView.addObject("craftHourMap", craftHourMap);
            modelAndView.addObject("craftDescriptionMap", craftDescriptionMap);

            Map<String, BigDecimal> costCodeHourMap = allocationTimes.stream()
                    .map(AllocationTime::getCostCodePercs)
                    .flatMap(Collection::stream)
                    .collect(Collectors.groupingBy(CostCodePerc::getCostCodeFull,
                            Collectors.reducing(BigDecimal.ZERO, CostCodePerc::getTotalHour, BigDecimal::add)));

            Map<String, BigDecimal> costCodeToDateHourMap = contentService.getToDateHours(projectId);
            List<String> costCodes = costCodeToDateHourMap.keySet().stream().sorted().collect(toList());
            for (String costCode : costCodes) {
                costCodeHourMap.putIfAbsent(costCode, BigDecimal.ZERO);
            }

            List<CostCodeLog> costCodeLogs = costCodeService
                    .getCostCodesByCodesAndProjectIdAndDate(projectId, costCodes, endOfWeek);
            Map<String, String> costCodeDescriptionMap = costCodeLogs.stream()
                    .collect(Collectors.toMap(CostCodeLog::getCostCodeFull,
                            cc -> cc.getDescription() == null ? "" : cc.getDescription()));
            modelAndView.addObject("costCodes", costCodes);
            modelAndView.addObject("costCodeDescriptionMap", costCodeDescriptionMap);
            modelAndView.addObject("costCodeHourMap", costCodeHourMap);

            Set<Long> costCodeIds = costCodeLogs.stream().map(CostCodeLog::getId).collect(toSet());
            List<CostCodeBillingCode> costCodeBillingCodes =  costCodeService.getCostCodeBillingCodes(costCodeIds);
            Map<String, List<CostCodeBillingCode>> codeBillingCodeMap = costCodeBillingCodes.stream()
                    .collect(Collectors.groupingBy(ccbc -> ccbc.getCostCodeLog().getCostCodeFull()));

            Map<String, BigDecimal> poHourMap = new HashMap<>();
            Map<String, String> poDescriptionMap = new HashMap<>();
            for (String costCode : costCodes) {
                if (codeBillingCodeMap.containsKey(costCode)) {
                    Optional<BillingCode> poCodeOptional = codeBillingCodeMap.get(costCode).stream()
                            .map(CostCodeBillingCode::getBillingCode)
                            .filter(billingCode -> billingCode
                                    .getBillingCodeType()
                                    .getCodeType()
                                    .equalsIgnoreCase("po"))
                            .findFirst();
                    Long finalProjectId = projectId;
                    poCodeOptional.ifPresent(billingCode -> {
                        poHourMap.put(billingCode.getClientAlias(),
                                poHourMap.getOrDefault(billingCode.getClientAlias(), BigDecimal.ZERO).add(costCodeHourMap.getOrDefault(costCode, BigDecimal.ZERO)));
                        if (!CollectionUtils.isEmpty(billingCode.getBillingCodeOverrides())) {
                            Optional<BillingCodeOverride> overrideOptional = billingCode.getBillingCodeOverrides()
                                    .stream().filter(c -> c.getProjectId().equals(finalProjectId)).findFirst();
                            if (overrideOptional.isPresent()) {
                                poDescriptionMap.put(billingCode.getClientAlias(), overrideOptional.get().getClientAlias());
                            } else {
                                poDescriptionMap.put(billingCode.getClientAlias(), billingCode.getDescription());
                            }
                        }
                    });
                }
            }
            List<String> poNumbers = poHourMap.keySet().stream().sorted().collect(toList());
            modelAndView.addObject("poNumbers", poNumbers);
            modelAndView.addObject("poDescriptionMap", poDescriptionMap);
            modelAndView.addObject("poHourMap", poHourMap);

            modelAndView.addObject("costCodeToDateHourMap", costCodeToDateHourMap);
        } else {
            modelAndView.addObject("allowedDayOfWeek", (DayOfWeek.SUNDAY.getValue()) % 7);
        }

        Project project = projectService.getProjectById(projectId);
        modelAndView.addObject("project", project);
        return modelAndView;
    }
}
