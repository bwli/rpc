package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.enums.PerDiemMode;
import com.timaven.timekeeping.service.*;
import com.timaven.timekeeping.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate"})
@PreAuthorize("hasRole('ROLE_project_manager')")
public class ProjectManagerController extends BaseController {

    private final AccountService mAccountService;
    private final UserProjectService mUserProjectService;
    private final UserValidator mUserValidator;
    private final EquipmentService equipmentService;
    private final RainOutService rainOutService;
    private final CostCodeService costCodeService;
    private final PerDiemService perDiemService;
    private final RuleService ruleService;

    @Autowired
    public ProjectManagerController(AccountService accountService, UserProjectService userProjectService,
                                    UserValidator userValidator, EquipmentService equipmentService,
                                    RainOutService rainOutService, CostCodeService costCodeService, PerDiemService perDiemService, RuleService ruleService) {
        mAccountService = accountService;
        mUserProjectService = userProjectService;
        mUserValidator = userValidator;
        this.equipmentService = equipmentService;
        this.rainOutService = rainOutService;
        this.costCodeService = costCodeService;
        this.perDiemService = perDiemService;
        this.ruleService = ruleService;
    }

    @InitBinder("user")
    protected void initUserBinder(WebDataBinder binder) {
        binder.addValidators(mUserValidator);
    }

    @DeleteMapping(value = "/equipment-usage")
    public String updateEquipmentValidity(@RequestParam(value = "id") Long id) {
        equipmentService.deactivateEquipmentUsage(id);
        return rdEquipmentList();
    }

    @GetMapping(value = "/rain-outs")
    public ModelAndView rainOuts(ModelMap model) {
        Long projectId = (Long) model.get("sessionProjectId");
        if (projectId == null) {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("errorMsg", "Project Id is null");
            modelAndView.addObject("cause", "Couldn't find the project");
            return modelAndView;
        }
        Set<RainOut> rainOuts = rainOutService.getAllRainOuts(projectId);
        return new ModelAndView("rain-outs", "rainOuts", rainOuts);
    }

    @GetMapping(value = "/rain-out")
    public ModelAndView rainOut(ModelMap model, @RequestParam(value = "id", required = false) Long id) {
        RainOut rainOut;
        if (null == id) {
            rainOut = new RainOut();
            Long projectId = (Long) model.get("sessionProjectId");
            rainOut.setProjectId(projectId);
        } else {
            rainOut = rainOutService.getRainOutById(id);
        }
        return new ModelAndView("rain-out", "rainOut", rainOut);
    }

    @RequestMapping(value = "/rain-out", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateShift(@ModelAttribute("rainOut") RainOut rainOut) {
        rainOutService.saveRainOut(rainOut);
        return "redirect:rain-outs";
    }

    @GetMapping(value = "/equipment-price-list")
    public ModelAndView equipmentPriceList() {
        return new ModelAndView("equipment-price-list");
    }

    @RequestMapping(value = "/equipment-price", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateEquipmentPrice(@ModelAttribute EquipmentPrice equipmentPrice,
                                            @ModelAttribute("sessionProjectId") Long projectId) {
        equipmentPrice.setProjectId(projectId);
        equipmentService.saveEquipmentPrice(equipmentPrice);
        return "redirect:equipment-price-list";
    }

    @GetMapping(value = "/equipment-price")
    public ModelAndView equipmentPrice(@RequestParam(required = false) Long equipmentPriceId) {
        EquipmentPrice price;
        if (equipmentPriceId != null) {
            price = equipmentService.getEquipmentPriceById(equipmentPriceId);
        } else {
            price = new EquipmentPrice();
        }
        return new ModelAndView("equipment-price", "equipmentPrice", price);
    }

    @DeleteMapping(value = "/equipment-price")
    public String deleteEquipmentPrice(@RequestParam Long id) {
        equipmentService.deleteEquipmentPriceById(id);
        return "redirect:equipment-price-list";
    }

    @GetMapping(value = {"/tag-types"})
    public String tagTypes() {
        return "tag-types";
    }

    @DeleteMapping(value = "/tag-types")
    public String deleteCostCodeTagType(@RequestParam Long id) {
        costCodeService.deleteCostCodeTagTypeById(id);
        return "redirect:tag-types";
    }

    @GetMapping(value = {"/tag-codes"})
    public ModelAndView tagCodes(@RequestParam Long typeId) {
        CostCodeTagType costCodeTagType = costCodeService.getCostCodeTagTypeById(typeId);
        return new ModelAndView("tag-codes", "costCodeTagType", costCodeTagType);
    }

    @DeleteMapping(value = "/tag-codes")
    public String deleteCostCodeTag(@RequestParam Long id, @RequestParam Long typeId) {
        costCodeService.deleteCostCodeTagById(id);
        return "redirect:tag-codes?typeId=" + typeId;
    }

    @GetMapping(value = {"/tag-type"})
    public ModelAndView tagType(@RequestParam(required = false) Long id,
                                @ModelAttribute("sessionProjectId") Long projectId ) {
        CostCodeTagType costCodeTagType;
        if (id != null) {
            costCodeTagType = costCodeService.getCostCodeTagTypeById(id);
        } else {
            costCodeTagType = new CostCodeTagType();
            costCodeTagType.setProjectId(projectId);
        }
        return new ModelAndView("tag-type", "tagType", costCodeTagType);
    }

    @RequestMapping(value = {"/tag-type"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public String saveOrUpdateTagType(@ModelAttribute("tagType") CostCodeTagType costCodeTagType) {
        costCodeService.saveTagType(costCodeTagType);
        return "redirect:tag-types";
    }

    @GetMapping(value = {"/tag-code"})
    public ModelAndView tagCode(@RequestParam(required = false) Long id,
                                @RequestParam(required = false) Long typeId) {
        if (id == null && typeId == null) return new ModelAndView("error");
        CostCodeTag costCodeTag;
        if (id != null) {
            costCodeTag = costCodeService.getCostCodeTagById(id);
        } else {
            costCodeTag = new CostCodeTag();
            costCodeTag.setTypeId(typeId);
        }
        return new ModelAndView("tag-code", "tag", costCodeTag);
    }

    @RequestMapping(value = {"/tag-code"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public String saveOrUpdateTagCode(@ModelAttribute("tag") CostCodeTag costCodeTag) {
        costCodeService.saveTagCode(costCodeTag);
        return "redirect:tag-codes?typeId=" + costCodeTag.getTypeId();
    }

    @GetMapping(value = {"/per-diem-list"})
    public ModelAndView perDiemList(ModelMap model) {
        ModelAndView modelAndView = new ModelAndView("per-diem-list");
        Long projectId = (Long) model.get("sessionProjectId");
        Optional<Rule> ruleOpt = ruleService.getRule(projectId);
        modelAndView.addObject("rule", ruleOpt.orElse(null));
        return modelAndView;
    }

    @GetMapping(value = {"/per-diem"})
    public ModelAndView perDiem(@ModelAttribute("sessionProjectId") Long projectId,
                                @RequestParam(required = false) Long id) {
        PerDiem perDiem;
        if (id == null) {
            perDiem = new PerDiem();
            perDiem.setProjectId(projectId);
            perDiem.setPerDiemMode(PerDiemMode.PAY_AS_YOU_GO);
        } else {
            perDiem = perDiemService.findById(id);
        }
        ModelAndView modelAndView = new ModelAndView("per-diem", "perDiem", perDiem);
        List<CostCodeLog> costCodeLogs = costCodeService.getAllCostCodeLogsByProjectIdAndDateOfService(projectId, LocalDate.now());
        modelAndView.addObject("costCodeLogs", costCodeLogs);
        return modelAndView;
    }

    @RequestMapping(value = {"/per-diems"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public String saveOrUpdatePerDiem(@ModelAttribute("perDiem") PerDiem perDiem) {
        perDiemService.savePerDiem(perDiem);
        return "redirect:per-diem-list";
    }

    @DeleteMapping(value = {"/per-diems"})
    public String deletePerDiem(@RequestParam Long id) {
        perDiemService.deleteById(id);
        return "redirect:per-diem-list";
    }

    private String rdEquipmentList() {
        return "redirect:equipment-list";
    }
}
