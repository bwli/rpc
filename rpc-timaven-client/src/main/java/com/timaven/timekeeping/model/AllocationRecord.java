package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.dto.AllocationRecordDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDateTime;
import java.util.Comparator;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationRecord.class)
@Getter
@Setter
@NoArgsConstructor
public class AllocationRecord implements Comparable<AllocationRecord> {
    private Long id;

    @JsonBackReference
    private AllocationTime allocationTime;

    private String doorName;

    private Boolean side;

    private boolean accessGranted = false;

    private boolean valid = false;

    private LocalDateTime adjustedTime;

    private LocalDateTime netEventTime;

    public AllocationRecord(AllocationRecordDto dto) {
        this.id = dto.getId();
        this.doorName = dto.getDoorName();
        this.side = dto.getSide();
        this.accessGranted = dto.isAccessGranted();
        this.valid = dto.isValid();
        this.adjustedTime = dto.getAdjustedTime();
        this.netEventTime = dto.getNetEventTime();
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationRecord)) return false;
        AllocationRecord that = (AllocationRecord) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(AllocationRecord allocationRecord) {
        return Comparator.comparing(AllocationRecord::getAdjustedTime, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, allocationRecord);
    }
}
