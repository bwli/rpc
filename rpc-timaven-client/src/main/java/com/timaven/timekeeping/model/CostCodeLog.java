package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeLog.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodeLog implements Comparable<CostCodeLog> {
    private Long id;

    @NotNull
    private String costCodeFull;

    private String description;

    private Long projectId;

    private LocalDate startDate;

    private LocalDate endDate;

    @NotNull
    private boolean locked = false;

    private LocalDateTime createdAt;

    private Set<CostCodeBillingCode> costCodeBillingCodes;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeFull);
        hcb.append(projectId);
        hcb.append(startDate);
        hcb.append(endDate);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeLog)) return false;
        CostCodeLog that = (CostCodeLog) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeFull, that.costCodeFull);
        eb.append(projectId, that.projectId);
        eb.append(startDate, that.startDate);
        eb.append(endDate, that.endDate);
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeLog costCodeLog) {
        if (projectId == null) return 1;
        if (costCodeLog.projectId == null) return -1;
        int result = projectId.compareTo(costCodeLog.projectId);
        if (result == 0) {
            result = costCodeFull.compareTo(costCodeLog.costCodeFull);
        }
        return result;
    }
}
