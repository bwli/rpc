package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.dto.CostCodeTagAssociationDto;
import com.timaven.timekeeping.model.enums.CostCodeType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeTagAssociation.class)
@Getter
@Setter
@EqualsAndHashCode(of = {"tagType", "codeName", "key", "costCodeType"})
@NoArgsConstructor
public class CostCodeTagAssociation {
    private Long id;
    private String tagType;
    private String typeDescription;
    private String codeName;
    private String codeDescription;
    private CostCodeType costCodeType;
    private Long key;

    public CostCodeTagAssociation(CostCodeTagAssociationDto dto, CostCodeType type) {
        this(dto, type, null);
    }

    public CostCodeTagAssociation(CostCodeTagAssociationDto dto, CostCodeType type, Long key) {
        this.id = dto.getId();
        this.tagType = dto.getTagType();
        this.typeDescription = dto.getTypeDescription();
        this.codeName = dto.getCodeName();
        this.codeDescription = dto.getCodeDescription();
        this.costCodeType = type;
        this.key = key;
    }
}
