package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeTagType.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodeTagType implements Comparable<CostCodeTagType> {

    private Long id;

    private String tagType;

    private String description;

    private Long projectId;

    private Map<String, Object> idType;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(tagType);
            hcb.append(projectId);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeTagType)) return false;
        CostCodeTagType that = (CostCodeTagType) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(tagType, that.tagType);
            eb.append(projectId, that.projectId);
        }
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeTagType o) {
        return Comparator.comparing(CostCodeTagType::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(CostCodeTagType::getTagType)
                .compare(this, o);
    }
}
