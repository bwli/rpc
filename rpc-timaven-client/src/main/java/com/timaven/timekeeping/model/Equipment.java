package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.EquipmentHourlyType;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Equipment.class)
@Getter
@Setter
@NoArgsConstructor
public class Equipment {

    private Long id;

    private String equipmentId;

    private String description;

    private String alias;

    private String serialNumber;

    private String department;

    @NotNull
    private String equipmentClass;

    private EquipmentHourlyType hourlyType = EquipmentHourlyType.DAILY;

    private EquipmentOwnershipType ownershipType = EquipmentOwnershipType.COMPANY_OWNED;

    private String empId;

    private String purchaseOrder;

    @NotNull
    private Boolean active = true;

    Set<EquipmentUsage> equipmentUsages;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Equipment)) return false;
        Equipment that = (Equipment) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentId, that.equipmentId);
        return eb.isEquals();
    }
}
