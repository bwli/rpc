package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.EquipmentAllocationSubmissionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentAllocationSubmission.class)
@Getter
@Setter
@NoArgsConstructor
public class EquipmentAllocationSubmission {

    private Long id;
    private Long approveUserId;
    private LocalDateTime approvedAt;

    private EquipmentAllocationSubmissionStatus status;

    private LocalDate dateOfService;

    @NotNull
    private Long projectId;

    private LocalDateTime createdAt;

    private Set<EquipmentAllocationTime> equipmentAllocationTimes;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(projectId);
        hcb.append(dateOfService);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentAllocationSubmission)) return false;
        EquipmentAllocationSubmission that = (EquipmentAllocationSubmission) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(projectId, that.projectId);
        eb.append(dateOfService, that.dateOfService);
        return eb.isEquals();
    }

}
