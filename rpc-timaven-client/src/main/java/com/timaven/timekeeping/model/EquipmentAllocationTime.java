package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.dto.EquipmentCostCodeDateDto;
import com.timaven.timekeeping.model.dto.EquipmentProcessAllocationTimeDto;
import com.timaven.timekeeping.model.enums.EquipmentHourlyType;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentAllocationTime.class)
@Getter
@Setter
@NoArgsConstructor
public class EquipmentAllocationTime {
    private Long id;

    private String equipmentId;

    private String description;

    private String alias;

    private String serialNumber;

    private String department;

    @NotNull
    private String equipmentClass;

    private EquipmentHourlyType hourlyType;

    private EquipmentOwnershipType ownershipType;

    private String empId;

    private BigDecimal totalHour;

    private BigDecimal totalCharge;

    private EquipmentAllocationSubmission equipmentAllocationSubmission;

    private Set<EquipmentCostCodePerc> equipmentCostCodePercs;

    private Long weeklyProcessId;

    private EquipmentWeeklyProcess equipmentWeeklyProcess;

    private LocalDate payrollDate;

    public EquipmentAllocationTime(EquipmentProcessAllocationTimeDto timeDto, EquipmentCostCodeDateDto dto) {
        this.equipmentId = timeDto.getEquipmentId();
        this.description = dto.getDescription();
        this.alias = dto.getAlias();
        this.equipmentClass = dto.getEquipmentClass();
        this.serialNumber = dto.getSerialNumber();
        this.department = dto.getDepartment();
        this.hourlyType = EquipmentHourlyType.fromDisplayValue(dto.getHourlyType());
        this.ownershipType = EquipmentOwnershipType.fromDisplayValue(dto.getOwnershipType());
        this.empId = dto.getEmpId();
        this.totalHour = timeDto.getTotalHour();
        this.totalCharge = timeDto.getTotalCharge();
        this.payrollDate = timeDto.getPayrollDate();
        this.equipmentCostCodePercs = timeDto.getEquipmentCostCodePercMap().values().stream()
                .map(EquipmentCostCodePerc::new).collect(Collectors.toSet());
    }

    public void addCostCodePerc(EquipmentCostCodePerc equipmentCostCodePerc) {
        if (null == equipmentCostCodePercs) {
            equipmentCostCodePercs = new HashSet<>();
        }
        equipmentCostCodePercs.add(equipmentCostCodePerc);
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? BigDecimal.ZERO : totalHour;
    }

    public BigDecimal getTotalCharge() {
        return totalCharge == null ? BigDecimal.ZERO : totalCharge;
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentAllocationTime)) return false;
        if (null != id) {
            EquipmentAllocationTime that = (EquipmentAllocationTime) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
