package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.dto.EquipmentProcessCostCodePercDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentCostCodePerc.class)
@Getter
@Setter
@NoArgsConstructor
public class EquipmentCostCodePerc {
    private Long id;

    @NotNull
    private String costCodeFull;

    private BigDecimal totalHour;

    private BigDecimal totalCharge;

    private EquipmentAllocationTime equipmentAllocationTime;

    private Long allocationTimeId;

    private LocalDateTime createdAt;

    public EquipmentCostCodePerc(EquipmentProcessCostCodePercDto dto) {
        this.costCodeFull = dto.getCostCodeFull();
        this.totalHour = dto.getTotalHour();
        this.totalCharge = dto.getTotalCharge();
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentCostCodePerc)) return false;
        if (null != id) {
            EquipmentCostCodePerc that = (EquipmentCostCodePerc) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
