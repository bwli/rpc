package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class EquipmentPrice {

    private Long id;

    @NotNull
    private String equipmentClass;

    private String description;

    @NotNull
    private Long projectId;

    private BigDecimal hourlyRate;

    private BigDecimal dailyRate;

    private BigDecimal weeklyRate;

    private BigDecimal monthlyRate;

    public BigDecimal getHourlyRate() {
        return hourlyRate == null ? BigDecimal.ZERO : hourlyRate;
    }

    public BigDecimal getDailyRate() {
        return dailyRate == null ? BigDecimal.ZERO : dailyRate;
    }

    public BigDecimal getWeeklyRate() {
        return weeklyRate == null ? BigDecimal.ZERO : weeklyRate;
    }

    public BigDecimal getMonthlyRate() {
        return monthlyRate == null ? BigDecimal.ZERO : monthlyRate;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentClass);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentPrice)) return false;
        EquipmentPrice that = (EquipmentPrice) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentClass, that.equipmentClass);
        return eb.isEquals();
    }
}
