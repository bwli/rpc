package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Notification {

    private Long id;

    private String from;

    private String summary;

    private String message;

    private boolean read;

    private Long userId;

    private LocalDateTime createdAt;

    public Notification(String from, String summary, String message, Long userId) {
        this.from = from;
        this.summary = summary;
        this.message = message;
        this.userId = userId;
        this.read = false;
    }
}
