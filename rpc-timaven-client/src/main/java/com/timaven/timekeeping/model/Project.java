package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Project implements Comparable<Project>, Serializable {
    private Long id;

    private String description;

    private String jobNumber;

    private String subJob;

    private String jobSubJob;

    @NotNull
    private Boolean active = true;

    @NotNull
    private boolean deleted = false;

    // TM-192 Start
    private String clientName;

    private String city;

    private String state;
    // TM-192 End

    private List<UserProject> userProjects;

    private Set<Long> userIds;

    private Long projectId;

    public Set<Long> getUserIds() {
        if (!CollectionUtils.isEmpty(userProjects)) {
            userIds = userProjects.stream().map(UserProject::getUserId).collect(Collectors.toSet());
        }
        return userIds == null ? new HashSet<>() : userIds;
    }

    public List<UserProject> getUserProjects() {
        return userProjects == null ? new ArrayList<>() : userProjects;
    }

    public String getJobSubJob() {
        if (StringUtils.hasText(subJob) && !StringUtils.hasText(jobNumber)) {
            return subJob;
        } else if (!StringUtils.hasText(subJob) && StringUtils.hasText(jobNumber)) {
            return jobNumber;
        } else if (StringUtils.hasText(subJob) && StringUtils.hasText(jobNumber)) {
            return String.format("%s-%s", jobNumber, subJob);
        }
        return description;
    }

    public String getJobSubJobClientNameDescription(){
        StringBuffer result = new StringBuffer();
        if(StringUtils.hasText(jobNumber)){
            result.append( result.isEmpty() ? jobNumber : "-"+jobNumber);
        }
        if(StringUtils.hasText(subJob)){
            result.append( result.isEmpty() ? subJob : "-"+subJob);
        }
        if(StringUtils.hasText(clientName)){
            result.append( result.isEmpty() ? clientName : "-"+clientName);
        }
        if(StringUtils.hasText(description)){
            result.append( result.isEmpty() ? description : "-"+description);
        }
        return result.toString();
    }

    @Override
    public int compareTo(Project project) {
        return Comparator.comparing(Project::getJobNumber, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Project::getSubJob,Comparator.nullsFirst(Comparator.naturalOrder()))
                .compare(this, project);
    }
}
