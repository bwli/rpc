package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class PurchaseOrder {
    private Long id;
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private Integer vendorNumber;
    private String vendorName;
    private String confirmTo;
    private String requestor;
    private String buyer;
    private String requisitionNumber;
    private String jobNumber;
    private String subJob;
    private String address1;
    private String deliveryPoint;
    private String terms;
    private String reference;
    private BigDecimal stateTax;
    private BigDecimal localTax;
    private String purchaseOrderLocation;
    private String poType;
    private Integer lineNumber;
    private String partNumber;
    private String partDescription;
    private LocalDate deliveryDate;
    private Integer quantity;
    private Integer adjustQuantityOrdered;
    private Integer quantityReceived;
    private BigDecimal unitCost;
    private BigDecimal extendedCost;
    private String priceCode;
    private BigDecimal originalPoDollarAmount;
    private BigDecimal currentPoDollarAmount;
    private BigDecimal openAmount;
    private String formattedCostDistribution;
    private String costType;
    private String glAccountNumberFormatted;
    private LocalDateTime createdAt;
    private Long submissionId;
    private PurchaseOrderSubmission purchaseOrderSubmission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public LocalDate getPurchaseOrderDate() {
        return purchaseOrderDate;
    }

    public void setPurchaseOrderDate(LocalDate purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    public Integer getVendorNumber() {
        return vendorNumber;
    }

    public void setVendorNumber(Integer vendorNumber) {
        this.vendorNumber = vendorNumber;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getConfirmTo() {
        return confirmTo;
    }

    public void setConfirmTo(String confirmTo) {
        this.confirmTo = confirmTo;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getSubJob() {
        return subJob;
    }

    public void setSubJob(String subJob) {
        this.subJob = subJob;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BigDecimal getStateTax() {
        return stateTax;
    }

    public void setStateTax(BigDecimal stateTax) {
        this.stateTax = stateTax;
    }

    public BigDecimal getLocalTax() {
        return localTax;
    }

    public void setLocalTax(BigDecimal localTax) {
        this.localTax = localTax;
    }

    public String getPurchaseOrderLocation() {
        return purchaseOrderLocation;
    }

    public void setPurchaseOrderLocation(String purchaseOrderLocation) {
        this.purchaseOrderLocation = purchaseOrderLocation;
    }

    public String getPoType() {
        return poType;
    }

    public void setPoType(String poType) {
        this.poType = poType;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDescription() {
        return partDescription;
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getAdjustQuantityOrdered() {
        return adjustQuantityOrdered;
    }

    public void setAdjustQuantityOrdered(Integer adjustQuantityOrdered) {
        this.adjustQuantityOrdered = adjustQuantityOrdered;
    }

    public Integer getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(Integer quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }

    public BigDecimal getExtendedCost() {
        return extendedCost;
    }

    public void setExtendedCost(BigDecimal extendedCost) {
        this.extendedCost = extendedCost;
    }

    public String getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(String priceCode) {
        this.priceCode = priceCode;
    }

    public BigDecimal getOriginalPoDollarAmount() {
        return originalPoDollarAmount;
    }

    public void setOriginalPoDollarAmount(BigDecimal originalPoDollarAmount) {
        this.originalPoDollarAmount = originalPoDollarAmount;
    }

    public BigDecimal getCurrentPoDollarAmount() {
        return currentPoDollarAmount;
    }

    public void setCurrentPoDollarAmount(BigDecimal currentPoDollarAmount) {
        this.currentPoDollarAmount = currentPoDollarAmount;
    }

    public BigDecimal getOpenAmount() {
        return openAmount;
    }

    public void setOpenAmount(BigDecimal openAmount) {
        this.openAmount = openAmount;
    }

    public String getFormattedCostDistribution() {
        return formattedCostDistribution;
    }

    public void setFormattedCostDistribution(String formattedCostDistribution) {
        this.formattedCostDistribution = formattedCostDistribution;
    }

    public String getCostType() {
        return costType;
    }

    public void setCostType(String costType) {
        this.costType = costType;
    }

    public String getGlAccountNumberFormatted() {
        return glAccountNumberFormatted;
    }

    public void setGlAccountNumberFormatted(String glAccountNumberFormatted) {
        this.glAccountNumberFormatted = glAccountNumberFormatted;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }


    public PurchaseOrderSubmission getPurchaseOrderSubmission() {
        return purchaseOrderSubmission;
    }

    public void setPurchaseOrderSubmission(PurchaseOrderSubmission purchaseOrderSubmission) {
        this.purchaseOrderSubmission = purchaseOrderSubmission;
    }

    public Long getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(Long submissionId) {
        this.submissionId = submissionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrder that = (PurchaseOrder) o;
        if (id != null && that.id != null) return id.equals(that.id);
        return false;
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        return super.hashCode();
    }
}
