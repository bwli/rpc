package com.timaven.timekeeping.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PurchaseOrderGroup {
    private Long id;
    private String purchaseOrderNumber;
    private String comment;
    private Boolean flag;
}
