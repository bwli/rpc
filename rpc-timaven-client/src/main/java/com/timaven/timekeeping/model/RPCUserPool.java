package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RPCUserPool implements Cloneable, Comparable<RPCUserPool> {

    private Long id;

    @NotNull
    private String empId;

    private String clientEmpId;

    private String craft;

    private Long perDiemId;

    private BigDecimal baseST;

    private BigDecimal baseOT;

    private BigDecimal baseDT;

    private BigDecimal holidayRate;

    private BigDecimal sickLeaveRate;

    private BigDecimal travelRate;

    private BigDecimal vacationRate;

    private String clientCraft;

    private String teamName;

    private String crew;

    private Long projectId;

    private String firstName;

    private String lastName;

    private String middleName;

    // Either F or M
    private String gender;

    private String company;

    private String badge;

    private String signInSheet;

    private String jobNumber;

    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate effectedOn;

    private LocalDate terminatedAt;

    private LocalDate rehiredAt;

    private LocalDate hiredAt;

    @NotNull
    private Boolean active = true;

    private Boolean hasPerDiem = false;

    private Boolean hasRigPay = false;

    private LocalDateTime createdAt;

    private Craft craftEntity;

    private String invalidReason;

    // TM-307
    private Boolean isOffsite = false;

    private String tag;

    private String shift;

    private LocalTime scheduleStart;

    private LocalTime scheduleEnd;

    private LocalTime lunchStart;

    private LocalTime lunchEnd;

    private String activityCode;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (id != null) {
            hcb.append(id);
        } else {
            hcb.append(empId);
            hcb.append(hiredAt);
            hcb.append(terminatedAt);
            hcb.append(projectId);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RPCUserPool)) return false;
        RPCUserPool that = (RPCUserPool) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (this.id != null && that.id != null) {
            eb.append(id, that.id);
        } else {
            eb.append(empId, that.empId);
            eb.append(projectId, that.projectId);
            eb.append(hiredAt, that.hiredAt);
            eb.append(terminatedAt, that.terminatedAt);
        }
        return eb.isEquals();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(RPCUserPool o) {
        return Comparator.comparing(RPCUserPool::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(RPCUserPool::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
