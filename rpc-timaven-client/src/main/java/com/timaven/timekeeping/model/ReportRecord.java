package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ReportRecord {
    private String costCode;
    private String purchaseOrder;
    private Integer percentage;

    public ReportRecord(String costCode, Integer percentage) {
        this.costCode = costCode;
        this.percentage = percentage;
    }
}
