package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class StageTransaction {
    private Long id;

    private String firstName;

    private String lastName;

    private String middleName;

    private Long contractId;

    private LocalDate dateOfService;

    private BigDecimal stRate;

    private BigDecimal otRate;

    private BigDecimal dtRate;

    private BigDecimal stHour;

    private BigDecimal otHour;

    private BigDecimal dtHour;

    private BigDecimal totalHour;

    private BigDecimal netHour;

    private Long areaId;

    private String workUnitId;

    private String empId;

    private String clientEmpId;

    private String badge;

    private String teamName;

    private BigDecimal perDiem;

    @JsonBackReference
    private TransSubmission transSubmission;

    private Long transSubmissionId;

    private Set<StageException> stageExceptions;

    private Set<StageRecord> stageRecords;

    public BigDecimal getStHour() {
        return stHour == null ? null : stHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getOtHour() {
        return otHour == null ? null : otHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? null : dtHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? null : totalHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getNetHour() {
        return netHour == null ? null : netHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }
}
