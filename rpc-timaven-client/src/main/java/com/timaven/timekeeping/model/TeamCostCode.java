package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"projectId", "teamName", "costCode", "startDate", "endDate"})
public class TeamCostCode {
    private Long id;

    private Long projectId;

    private String teamName;

    private String costCode;

    private LocalDate startDate;

    private LocalDate endDate;

    private LocalDateTime createdAt;
}
