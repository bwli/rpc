package com.timaven.timekeeping.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeamServiceDate implements Comparable<TeamServiceDate> {
    @NotNull
    private String team;
    @NotNull
    private LocalDate dateOfService;
    @NotNull
    private LocalDate payrollDate;

    @Override
    public int compareTo(TeamServiceDate teamServiceDate) {
        return Comparator.comparing(TeamServiceDate::getDateOfService, Comparator.nullsLast(Comparator.reverseOrder()))
                .thenComparing(TeamServiceDate::getTeam, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(TeamServiceDate::getPayrollDate, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, teamServiceDate);
    }
}
