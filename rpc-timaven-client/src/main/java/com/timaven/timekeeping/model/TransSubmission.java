package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class TransSubmission {
    private Long id;

    private Long userId;

    private Long projectId;

    private LocalDateTime createdAt;

    private Set<StageTransaction> stageTransactions;

    private Map<String, Set<LocalDate>> teamDateSet;

    private Set<LocalDate> localDateSet;

    public TransSubmission(Long userId, Long projectId) {
        this.userId = userId;
        this.projectId = projectId;
    }
}
