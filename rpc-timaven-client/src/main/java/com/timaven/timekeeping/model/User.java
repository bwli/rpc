package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity bean with JPA annotations Hibernate provides JPA implementation
 */
@Getter
@Setter
@EqualsAndHashCode(of = "username")
@NoArgsConstructor
public class User {


    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String displayName;

    private String ipAddress;

    private LocalDateTime loggedInAt;

    @NotNull
    private Boolean active = true;

    private String passwordConfirm;

    private Set<UserRole> userRoles;

    private Set<Long> roleIds;

    private Set<UserProject> userProjects;

    private Set<String> teams;

    public Set<String> getTeams() {
        if (teams == null) {
            teams = new HashSet<>();
        }
        return teams;
    }

    public User(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getRoleNamesStr() {
        if (!CollectionUtils.isEmpty(userRoles)) {
            List<String> roleNames =
                    userRoles.stream().sorted(Comparator.comparingInt(o -> o.getRole().getDisplayOrder()))
                            .map(e -> e.getRole().getName()).map(e -> e.substring(5)).collect(Collectors.toList());
            return StringUtils.join(roleNames, ",");
        }
        return "";
    }
}
