package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@NoArgsConstructor
public class UserRole {
    private Long id;

    private Long userId;

    private Long roleId;

    private User user;

    private Role role;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(userId);
        hcb.append(roleId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRole)) return false;
        UserRole that = (UserRole) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(userId, that.userId);
        eb.append(roleId, that.roleId);
        return eb.isEquals();
    }
}
