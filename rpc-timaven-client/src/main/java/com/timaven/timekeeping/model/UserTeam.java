package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@NoArgsConstructor
public class UserTeam {

    private Long id;
    private Long userProjectId;
    private String team;

    public UserTeam(Long userProjectId, String team) {
        this.userProjectId = userProjectId;
        this.team = team;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = null;
        if (null != id) {
            hcb = new HashCodeBuilder();
            hcb.append(id);
        } else if (null != userProjectId && null != team) {
            hcb = new HashCodeBuilder();
            hcb.append(userProjectId);
            hcb.append(team);
        }
        if (null != hcb) {
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserTeam)) return false;
        UserTeam that = (UserTeam) o;
        EqualsBuilder eb = null;
        if (null != id) {
            eb = new EqualsBuilder();
            eb.append(id, that.id);
        } else if (null != userProjectId && null != team) {
            eb = new EqualsBuilder();
            eb.append(userProjectId, that.userProjectId);
            eb.append(team, that.team);
        }
        if (null != eb) {
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
