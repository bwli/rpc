package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.PerDiem;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Data
@NoArgsConstructor
public class AllocationTimeDto implements Comparable<AllocationTimeDto> {
    private Long id;
    private String empId;
    private String clientEmpId;
    private String badge;
    private String teamName;
    private BigDecimal stHour;
    private BigDecimal otHour;
    private BigDecimal dtHour;
    private AllocationTimeHourType extraTimeType;
    private BigDecimal totalHour;
    private BigDecimal allocatedHour;
    private BigDecimal netHour;
    private boolean hasPerDiem;
    private boolean hasMob;
    private BigDecimal rigPay;
    private Set<CostCodePercDto> costCodePercs;
    private List<PunchExceptionDto> punchExceptions;
    private List<AllocationRecordDto> allocationRecords;
    private AllocationExceptionDto allocationException;
    private String firstName;
    private String lastName;
    private String jobNumber;
    private String maxTimeCostCode;
    private String perDiemCostCode;
    private String mobCostCode;
    private BigDecimal mobAmount;
    private BigDecimal mobMileageRate = BigDecimal.ZERO;
    private BigDecimal mobMileage = BigDecimal.ZERO;
    private Integer mobDisplayOrder;
    private BigDecimal perDiemAmount;
    private Long weeklyProcessId;
    private Map<Integer, CostCodePercDto> costCodePercMap = new HashMap<>();
    private List<CostCodeTagAssociationDto> perDiemCostCodeTagAssociations;
    private List<CostCodeTagAssociationDto> mobCostCodeTagAssociations;

    private Boolean employeeHasPerDiem = false;
    private Boolean employeeHasRigPay = false;
    private BigDecimal craftRigPay = BigDecimal.ZERO;
    private String employeeInvalidReason;
    private String employeeCrew;
    private String employeeCraft;
    private PerDiem perDiem;
    private String fullName;

    public CostCodePercDto getCostCodePerc(int key) {
        if (CollectionUtils.isEmpty(costCodePercMap) && !CollectionUtils.isEmpty(costCodePercs)) {
            costCodePercMap = costCodePercs.stream().collect(toMap(CostCodePercDto::hashCode, Function.identity()));
        }
        return costCodePercMap.get(key);
    }

    public BigDecimal getStHour() {
        return stHour == null ? BigDecimal.ZERO : stHour;
    }

    public BigDecimal getOtHour() {
        return otHour == null ? BigDecimal.ZERO : otHour;
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? BigDecimal.ZERO : dtHour;
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? BigDecimal.ZERO : totalHour;
    }

    public BigDecimal getAllocatedHour() {
        return allocatedHour == null ? BigDecimal.ZERO : allocatedHour;
    }

    public String getFullName() {
        if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
            return String.format("%s, %s", lastName, firstName);
        } else if (StringUtils.hasText(firstName)) {
            return firstName;
        } else if (StringUtils.hasText(lastName)) {
            return lastName;
        }
        return "Unknown";
    }

    public List<CostCodeTagAssociationDto> getPerDiemCostCodeTagAssociations() {
        return perDiemCostCodeTagAssociations == null ? new ArrayList<>() : perDiemCostCodeTagAssociations;
    }

    @Override
    public int compareTo(AllocationTimeDto o) {
        return Comparator.comparing(AllocationTimeDto::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AllocationTimeDto::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
