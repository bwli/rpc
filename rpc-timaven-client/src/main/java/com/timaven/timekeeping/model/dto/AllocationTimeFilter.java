package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AllocationTimeFilter {
    Set<LocalDate> dates;
    LocalDate startDate;
    LocalDate endDate;
    LocalDate payrollStartDate;
    LocalDate payrollEndDate;
    Boolean isAllProjects;
    Long projectId;
    String teamName;
    AllocationSubmissionStatus status;
    AllocationSubmissionStatus notStatus;
    List<String> includes;
    Set<String> empIds;
}
