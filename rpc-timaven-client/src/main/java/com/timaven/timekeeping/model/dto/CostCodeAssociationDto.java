package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class CostCodeAssociationDto {
    private Long id;
    private String costCode;
    private String description;
    private List<String> tags = new ArrayList<>();
    private String jobNumber;
    private String subJob;
    private Long projectId;
    private String level1;
    private String level2;
    private String level3;
    private String level4;
    private String level5;
    private String level6;
    private String level7;
    private String level8;
    private String level9;
    private String level10;
    private String level11;
    private String level12;
    private String level13;
    private String level14;
    private String level15;
    private String level16;
    private String level17;
    private String level18;
    private String level19;
    private String level20;
    /**
     * TM 389 Change cost code page description
     */
    private String costCodeSegment;
}
