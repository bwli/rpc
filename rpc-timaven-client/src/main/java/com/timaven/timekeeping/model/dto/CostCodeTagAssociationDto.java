package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.CostCodeTagAssociation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"tagType", "codeName"})
public class CostCodeTagAssociationDto implements Comparable<CostCodeTagAssociationDto> {
    private Long id;
    private String tagType;
    private String typeDescription;
    private String codeName;
    private String codeDescription;

    public CostCodeTagAssociationDto(CostCodeTagAssociation costCodeTagAssociation) {
        this.id = costCodeTagAssociation.getId();
        this.tagType = costCodeTagAssociation.getTagType();
        this.typeDescription = costCodeTagAssociation.getTypeDescription();
        this.codeName = costCodeTagAssociation.getCodeName();
        this.codeDescription = costCodeTagAssociation.getCodeDescription();
    }

    @Override
    public int compareTo(CostCodeTagAssociationDto o) {
        return Comparator.comparing(CostCodeTagAssociationDto::getTagType)
                .thenComparing(CostCodeTagAssociationDto::getCodeName)
                .compare(this, o);
    }
}
