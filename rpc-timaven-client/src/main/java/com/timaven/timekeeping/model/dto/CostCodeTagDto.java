package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Data
@NoArgsConstructor
public class CostCodeTagDto implements Comparable<CostCodeTagDto> {
    private String costCodeFull;
    private List<CostCodeTagAssociationDto> costCodeTagAssociationDtos = new ArrayList<>();
    private Integer displayOrder;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeFull);
        hcb.append(costCodeTagAssociationDtos);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeTagDto)) return false;
        CostCodeTagDto that = (CostCodeTagDto) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeFull, that.costCodeFull);
        eb.append(costCodeTagAssociationDtos, that.costCodeTagAssociationDtos);
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeTagDto o) {
        if (this.displayOrder != null) {
            return Comparator.comparing(CostCodeTagDto::getDisplayOrder, Comparator.nullsLast(Comparator.naturalOrder()))
                    .compare(this, o);
        }
        int result;
        result = Comparator.comparing(CostCodeTagDto::getCostCodeFull)
                .compare(this, o);
        if (result == 0) {
            if (!CollectionUtils.isEmpty(this.costCodeTagAssociationDtos) && !CollectionUtils.isEmpty(o.costCodeTagAssociationDtos)) {
                result = o.costCodeTagAssociationDtos.size() - costCodeTagAssociationDtos.size();
                if (result == 0) {
                    for (int i = 0; i < costCodeTagAssociationDtos.size(); i++) {
                        result = costCodeTagAssociationDtos.get(i).compareTo(o.costCodeTagAssociationDtos.get(i));
                        if (result != 0) return result;
                    }
                }
            } else if (!CollectionUtils.isEmpty(o.costCodeTagAssociationDtos)) {
                return 1;
            } else if (!CollectionUtils.isEmpty(costCodeTagAssociationDtos)) {
                return -1;
            } else {
                return 0;
            }
        }
        return result;
    }
}
