package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.PerDiem;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "empId")
public class EmpCostCodeDateDto implements Comparable<EmpCostCodeDateDto> {
    private String empId;
    private String firstName;
    private String lastName;
    private String fullName;
    private String team;
    private Set<String> costCodes;
    private Set<String> perDiemCostCodes;
    private Set<String> mobCostCodes;
    // Whether this employee qualified for per-diem
    private boolean hasPerDiem;
    private boolean hasAbsent;
    // Latest PerDiem Object
    private PerDiem perDiemEntity;

    public EmpCostCodeDateDto(String empId, String firstName, String lastName, String team, boolean hasPerDiem) {
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
        this.hasPerDiem = hasPerDiem;
    }

    private Map<String, WeeklyProcessAllocationTimeDto> empAllocationTimeMap;

    public Map<String, WeeklyProcessAllocationTimeDto> getEmpAllocationTimeMap() {
        if (empAllocationTimeMap == null) {
            empAllocationTimeMap = new HashMap<>();
        }
        return empAllocationTimeMap;
    }


    public Set<String> getCostCodes() {
        if (null == costCodes) {
            costCodes = new TreeSet<>();
        }
        return costCodes;
    }

    public Set<String> getPerDiemCostCodes() {
        if (null == perDiemCostCodes) {
            perDiemCostCodes = new TreeSet<>();
        }
        return perDiemCostCodes;
    }

    public Set<String> getMobCostCodes() {
        if (null == mobCostCodes) {
            mobCostCodes = new TreeSet<>();
        }
        return mobCostCodes;
    }

    public WeeklyProcessAllocationTimeDto getAllocationTime(LocalDate dateOfService) {
        return empAllocationTimeMap.get(dateOfService.toString());
    }

    public BigDecimal getTotalSt() {
        return new BigDecimal(getEmpAllocationTimeMap().values().stream()
                .map(WeeklyProcessAllocationTimeDto::getEmpCostCodePercMap)
                .map(Map::values)
                .flatMap(Collection::stream)
                .map(WeeklyProcessCostCodePercDto::getStHour)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).stripTrailingZeros().toPlainString());
    }

    public BigDecimal getTotalOt() {
        return new BigDecimal(getEmpAllocationTimeMap().values().stream()
                .map(WeeklyProcessAllocationTimeDto::getEmpCostCodePercMap)
                .map(Map::values)
                .flatMap(Collection::stream)
                .map(WeeklyProcessCostCodePercDto::getOtHour)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).stripTrailingZeros().toPlainString());
    }

    public BigDecimal getTotalDt() {
        return new BigDecimal(getEmpAllocationTimeMap().values().stream()
                .map(WeeklyProcessAllocationTimeDto::getEmpCostCodePercMap)
                .map(Map::values)
                .flatMap(Collection::stream)
                .map(WeeklyProcessCostCodePercDto::getDtHour)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).stripTrailingZeros().toPlainString());
    }

    @Override
    public int compareTo(EmpCostCodeDateDto o) {
        return Comparator.comparing(EmpCostCodeDateDto::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(EmpCostCodeDateDto::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(EmpCostCodeDateDto::getTeam, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }

    public boolean isHasAbsent() {
        if (getEmpAllocationTimeMap().size() == 0) return true;
        return getEmpAllocationTimeMap().values().stream()
                .anyMatch(WeeklyProcessAllocationTimeDto::isAbsent);
    }

    public String getFullName() {
        if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
            return String.format("%s, %s", lastName, firstName);
        } else if (StringUtils.hasText(firstName)) {
            return firstName;
        } else if (StringUtils.hasText(lastName)) {
            return lastName;
        }
        return "Unknown";
    }
}
