package com.timaven.timekeeping.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class EmpDateActivityKey {
    @NotNull
    private Long allocationTimeId;
    @NotNull
    private Long activityId;

    public EmpDateActivityKey(Long allocationTimeId, Long activityId){
        this.allocationTimeId = allocationTimeId;
        this.activityId = activityId;
    }
}
