package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.AllocationTime;
import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import com.timaven.timekeeping.util.DateRange;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class EmpDateDto implements Comparable<EmpDateDto> {
    private String teamName;
    private String jobNumber;
    private String empId;
    private String lastName;
    private String firstName;
    private BigDecimal stTotal;
    private BigDecimal otTotal;
    private BigDecimal holidayHour;
    private BigDecimal vacationHour;
    private BigDecimal sickHour;
    private BigDecimal other;
    private BigDecimal nonOt;
    private BigDecimal totalTime;
    // Used to decide whether the employee could have per diem or not
    private boolean hasPerDiem = false;
    private boolean isOffsite = false;

    /**
     * TM-373
     * stMap  Map<activityId, Map<date, BigDecimal>>
     * extraTimeMap Map<activityId, Map<date, EmpDateHourDto>>
     */
    private Map<Long, Map<String, BigDecimal>> stMap = new HashMap<>();
    private EmpExtraTimeMap extraTimeMap= new EmpExtraTimeMap();
    private List<Long> activityCodeIds = new ArrayList<>();

    public EmpDateDto(Employee employee, Collection<LocalDate> days, Map<String, Long> codeIdMap) {
        this.jobNumber = employee.getJobNumber();
        this.teamName = employee.getTeamName();
        this.empId = employee.getEmpId();
        this.lastName = employee.getLastName();
        this.firstName = employee.getFirstName();
        this.stMap = new HashMap<>();
        this.extraTimeMap = new EmpExtraTimeMap();
        this.activityCodeIds = new ArrayList<>();
        hasPerDiem = employee.getHasPerDiem();

        // TM-307
        if (Boolean.TRUE.equals(employee.getIsOffsite())) {
            this.isOffsite = true;
        }

        if (StringUtils.hasText(employee.getActivityCode()) && codeIdMap.containsKey(employee.getActivityCode())) {
            Long codeId = codeIdMap.get(employee.getActivityCode());
            activityCodeIds.add(codeId);
            stMap.put(codeId, new HashMap<>());
            extraTimeMap.put(codeId, new HashMap<>());
            days.forEach(d -> {
                stMap.get(codeId).put(d.toString(), null);
                extraTimeMap.get(codeId).put(d.toString(), new EmpDateHourDto());
            });
        }
    }

    public EmpDateDto(AllocationTime allocationTime) {
        this.jobNumber = allocationTime.getJobNumber();
        this.empId = allocationTime.getEmpId();
        this.lastName = allocationTime.getLastName();
        this.firstName = allocationTime.getFirstName();

        this.stMap = new HashMap<>();
        this.extraTimeMap = new EmpExtraTimeMap();
        this.activityCodeIds = new ArrayList<>();

        // TM-307
        if (Boolean.TRUE.equals(allocationTime.getIsOffsite())) {
            this.isOffsite = true;
        }

        addAllocationTime(allocationTime);
    }

    public void addAllocationTime(AllocationTime allocationTime) {
        String dateOfService = allocationTime.getAllocationSubmission().getDateOfService().toString();
        // TM-373 add activateId init or add stMap and extraTimeMap
        allocationTime.getActivityCodePercs().forEach(e -> {
            Long activateCodeId = e.getActivityId();
            if(!Objects.isNull(activateCodeId)){
                EmpDateHourDto empDateHourDto = new EmpDateHourDto(e.getExtraTimeType().getValue(),
                        e.getOtHour());
                if(!activityCodeIds.contains(activateCodeId)){
                    activityCodeIds.add(activateCodeId);
                    extraTimeMap.put(activateCodeId,new HashMap<>());
                    stMap.put(activateCodeId, new HashMap<>());
                }
                this.stMap.get(activateCodeId).put(dateOfService, e.getStHour());
                this.extraTimeMap.get(activateCodeId).put(dateOfService, empDateHourDto);
            }
        });
    }

    public BigDecimal getHolidayHour() {
        return extraTimeMap.values().stream()
                .flatMap(e -> e.entrySet().stream())
                .map(e -> e.getValue())
                .filter(e -> e.getType() == AllocationTimeHourType.HOLIDAY.getValue() && e.getHour() != null)
                .map(EmpDateHourDto::getHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getVacationHour() {
        return extraTimeMap.values().stream()
                .flatMap(e -> e.entrySet().stream())
                .map(e -> e.getValue())
                .filter(e -> e.getType() == AllocationTimeHourType.VACATION.getValue() && e.getHour() != null)
                .map(EmpDateHourDto::getHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getSickHour() {
        return extraTimeMap.values().stream()
                .flatMap(e -> e.entrySet().stream())
                .map(e -> e.getValue())
                .filter(e -> e.getType() == AllocationTimeHourType.SICK.getValue() && e.getHour() != null)
                .map(EmpDateHourDto::getHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getOther() {
        return extraTimeMap.values().stream()
                .flatMap(e -> e.entrySet().stream())
                .map(e -> e.getValue())
                .filter(e -> e.getType() == AllocationTimeHourType.OTHER.getValue() && e.getHour() != null)
                .map(EmpDateHourDto::getHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getStTotal() {
        return stMap.values().stream()
                .flatMap(e -> e.entrySet().stream())
                .map(e -> e.getValue())
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getOtTotal() {
        return extraTimeMap.values().stream()
                .flatMap(e -> e.entrySet().stream())
                .map(e -> e.getValue())
                .filter(e -> e.getType() == AllocationTimeHourType.OT.getValue() && e.getHour() != null)
                .map(EmpDateHourDto::getHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getNonOt() {
        return getNonNullHour(getStTotal()).add(getNonNullHour(getHolidayHour()))
                .add(getNonNullHour(getVacationHour())).add(getNonNullHour(getSickHour()))
                .add(getNonNullHour(getOther()));
    }

    public BigDecimal getTotalTime() {
        return getNonOt().add(getNonNullHour(getOtTotal()));
    }

    private BigDecimal getNonNullHour(BigDecimal hour) {
        return hour == null ? BigDecimal.ZERO : hour;
    }

    @Override
    public int compareTo(EmpDateDto o) {
        return Comparator
                .comparing(EmpDateDto::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(EmpDateDto::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
