package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class EmpDateHourDto {
    private int type;
    private BigDecimal hour;

    public EmpDateHourDto(int type, BigDecimal stHour) {
        this.type = type;
        this.hour = stHour;
    }
}
