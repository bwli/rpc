package com.timaven.timekeeping.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "empId")
public class EmpIdNameDto implements Comparable<EmpIdNameDto> {
    private String empId;
    private String name;

    @Override
    public int compareTo(EmpIdNameDto o) {
        return Comparator.comparing(EmpIdNameDto::getName)
                .compare(this, o);
    }
}
