package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class EmployeeDto {
    private Long id;
    private String empId;
    private String craft;
    private String perDiemName;
    private Long perDiemId;
    private String team;
    private String crew;
    private String name;
    private String company;
    private String badge;
    private String signInSheet;
    private LocalDate hiredAt;
    private LocalDate effectedOn;
    private LocalDate terminatedAt;
    private Boolean hasPerDiem = false;
    private Boolean hasRigPay = false;
    private String jobNumber;
    private List<String> tags = new ArrayList<>();
    // TM-307
    private Boolean isOffsite = false;
    private String activityCode;
}
