package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.EquipmentAllocationTime;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Data
@NoArgsConstructor
public class EquipmentProcessAllocationTimeDto {
    private Long id;
    private String equipmentId;
    private LocalDate dateOfService;
    private LocalDate payrollDate;

    private Map<String, EquipmentProcessCostCodePercDto> equipmentCostCodePercMap;

    public EquipmentProcessAllocationTimeDto(EquipmentAllocationTime equipmentAllocationTime, LocalDate dateOfService) {
        this.id = equipmentAllocationTime.getId();
        this.dateOfService = dateOfService;
        this.payrollDate = equipmentAllocationTime.getPayrollDate();
    }

    public Map<String, EquipmentProcessCostCodePercDto> getEquipmentCostCodePercMap() {
        if (null == equipmentCostCodePercMap) {
            equipmentCostCodePercMap = new HashMap<>();
        }
        return equipmentCostCodePercMap;
    }

    public EquipmentProcessCostCodePercDto getCostCodePerc(String costCode) {
        return getEquipmentCostCodePercMap().get(costCode);
    }

    public BigDecimal getTotalHour() {
        return getEquipmentCostCodePercMap().values().stream()
                .map(EquipmentProcessCostCodePercDto::getTotalHour)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getTotalCharge() {
        return getEquipmentCostCodePercMap().values().stream()
                .map(EquipmentProcessCostCodePercDto::getTotalCharge)
                .filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    // TM-275
    public static EquipmentProcessAllocationTimeDto cloneEquipmentProcessAllocationTimeDto(
            EquipmentProcessAllocationTimeDto dto) {
        EquipmentProcessAllocationTimeDto newDto = new EquipmentProcessAllocationTimeDto();
        newDto.setEquipmentId(dto.getEquipmentId());
        newDto.setDateOfService(dto.getDateOfService());
        newDto.setPayrollDate(dto.getPayrollDate());
        return newDto;
    }

}
