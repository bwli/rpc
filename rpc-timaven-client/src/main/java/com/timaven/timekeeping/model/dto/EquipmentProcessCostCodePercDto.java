package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.CostCodePerc;
import com.timaven.timekeeping.model.EquipmentCostCodePerc;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class EquipmentProcessCostCodePercDto {
    private Long id;
    private String costCodeFull;
    private BigDecimal totalHour;
    private BigDecimal totalCharge;

    public EquipmentProcessCostCodePercDto(EquipmentCostCodePerc equipmentCostCodePerc) {
        this.id = equipmentCostCodePerc.getId();
        this.costCodeFull = equipmentCostCodePerc.getCostCodeFull();
        this.totalHour = equipmentCostCodePerc.getTotalHour();
        this.totalCharge = equipmentCostCodePerc.getTotalCharge();
    }

    public EquipmentProcessCostCodePercDto(CostCodePerc costCodePerc) {
        this.costCodeFull = costCodePerc.getCostCodeFull();
        this.totalHour = costCodePerc.getTotalHour();
    }

    public BigDecimal getTotalCharge() {
        if (totalCharge != null) {
            return new BigDecimal(totalCharge.stripTrailingZeros().toPlainString());
        }
        return null;
    }

    // TM-275
    public static EquipmentProcessCostCodePercDto cloneCodeDtoWithSpecifiedCostCode(EquipmentProcessCostCodePercDto dto, String costCode) {
        EquipmentProcessCostCodePercDto newDto = new EquipmentProcessCostCodePercDto();
        newDto.setCostCodeFull(costCode);
        newDto.setTotalCharge(dto.getTotalCharge());
        newDto.setTotalHour(dto.getTotalHour());
        return newDto;
    }
}
