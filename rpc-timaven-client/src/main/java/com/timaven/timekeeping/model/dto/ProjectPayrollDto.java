package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class ProjectPayrollDto {
    private Long projectId;
    private String weekEndDate;
    private String employeeCount;
    private String totalHours;
    private Long timesheets;
    private String approvedAt;
    private String finalizedAt;
    private String payrollType;
    private Map<String, Object> weekEndDateStatus;
}
