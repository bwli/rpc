package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class PurchaseOrderBillingDto {
    private String purchaseOrderNumber;
    private String billingNumber;
    private String invoiceNumber;
    private BigDecimal freightShipping;
    private String otherCharge;
    private BigDecimal otherChargeAmount;
    private List<PurchaseOrderBillingDetailDto> purchaseOrderBillingDetailDtos;
}
