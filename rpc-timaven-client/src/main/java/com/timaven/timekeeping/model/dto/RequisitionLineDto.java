package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.Requisition;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class RequisitionLineDto {
    private Integer lineNumber;
    private String partNumber;
    private String partDescription;
    private Integer quantity;
    private BigDecimal unitCost;
    private BigDecimal extendedCost;
    private String formattedCostDistribution;
    private String costType;
}
