package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class WeeklyPayrollStatusDto {
    private List<Long> ids;
    private String status;
}
