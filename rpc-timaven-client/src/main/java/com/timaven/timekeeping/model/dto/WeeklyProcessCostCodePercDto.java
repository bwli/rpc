package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.CostCodePerc;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class WeeklyProcessCostCodePercDto {
    private Long id;
    private String costCodeFull;
    private BigDecimal stHour;
    private BigDecimal otHour;
    private BigDecimal dtHour;

    public WeeklyProcessCostCodePercDto(CostCodePerc costCodePerc) {
        this.id = costCodePerc.getId();
        this.costCodeFull = costCodePerc.getCostCodeTag();
        this.stHour = costCodePerc.getStHour();
        if (stHour != null) {
            stHour = new BigDecimal(stHour.stripTrailingZeros().toPlainString());
        }
        this.otHour = costCodePerc.getOtHour();
        if (otHour != null) {
            otHour = new BigDecimal(otHour.stripTrailingZeros().toPlainString());
        }
        this.dtHour = costCodePerc.getDtHour();
        if (dtHour != null) {
            dtHour = new BigDecimal(dtHour.stripTrailingZeros().toPlainString());
        }
    }

    public BigDecimal getStHour() {
        return stHour == null ? BigDecimal.ZERO : stHour;
    }

    public BigDecimal getOtHour() {
        return otHour == null ? BigDecimal.ZERO : otHour;
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? BigDecimal.ZERO : dtHour;
    }
}
