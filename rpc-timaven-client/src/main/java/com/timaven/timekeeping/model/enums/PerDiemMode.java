package com.timaven.timekeeping.model.enums;

public enum PerDiemMode {
    PAY_AS_YOU_GO("Pay as you go"),
    N_PLUS_ONE("N+1"),
    SEVEN_DAYS("7 days"),
    MANUAL("Manual");

    private final String displayValue;

    PerDiemMode(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
