package com.timaven.timekeeping.model.enums;

public enum WeeklyProcessStatus {
    REJECT,
    FINALIZE,
    UNFINALIZE;
}
