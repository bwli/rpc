package com.timaven.timekeeping.model.paging;

public enum Direction {
    asc,
    desc
}
