package com.timaven.timekeeping.security.extractor;

import com.timaven.timekeeping.model.dto.Principal;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;

@Component
public class ClientPrincipalExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Principal user = new Principal();
        user.setId(Long.valueOf((Integer) map.get("id")));
        user.setUsername((String)map.get("name"));
        user.setDisplayName((String)map.get("displayName"));
        user.setIpAddress((String)map.get("ipAddress"));
        user.setTenantId((String)map.get("tenantId"));
        if (map.containsKey("loggedInAt")) {
            user.setLoggedInAt(LocalDateTime.parse((CharSequence) map.get("loggedInAt")));
        }
        return user;
    }
}
