package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.BillingCode;
import com.timaven.timekeeping.model.BillingCodeOverride;
import com.timaven.timekeeping.model.BillingCodeType;
import com.timaven.timekeeping.model.dto.BillingCodeDto;
import com.timaven.timekeeping.model.dto.SaveCostCodeAssociationDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.util.List;

public interface BillingCodeService {
    List<BillingCodeType> findAllBillingCodeTypes();

    BillingCodeType findBillingCodeTypeById(Long id);

    Page<BillingCodeDto> findBillingCodesByTypeId(Long typeId, PagingRequest pagingRequest, Long projectId);

    BillingCode findBillingCodeById(Long billingCodeId);

    void deleteBillingCodeTypeById(Long id);

    void deleteBillingCodeById(Long id);

    void saveBillingCodeType(BillingCodeType billingCodeType);

    List<BillingCodeDto> findBillingCodeByCostCodeIdAndProjectId(Long id, Long projectId);

    List<BillingCodeDto> findBillingCodesByTypeId(Long id, Long projectId);

    void saveCostCodeBillingCode(Long projectId, SaveCostCodeAssociationDto dto);

    List<BillingCodeType> findBillingCodeTypesSortByLevel();

    void saveCostCodeStructure(List<Long> ids);

    BillingCodeOverride findBillingCodeOverride(Long billingCodeId, Long projectId);

    void saveBillingCodeOverride(BillingCodeOverride billingCodeOverride);

    void deleteBillingCodeOverrideByBillingCodeIdAndProjectId(Long id, Long projectId);

    void saveBillingCode(BillingCode billingCode);

    BillingCode findBillingCodeByTypeIdAndCodeName(Long typeId, String codeName);
}
