package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Craft;
import com.timaven.timekeeping.model.dto.CraftDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

public interface CraftService {
    Page<CraftDto> getCraftPage(PagingRequest pagingRequest, Long projectId);

    Craft findById(Long id);

    void saveCraft(Craft craft);

    void updateCraftPerDiem(Long craftId, Long perDiemId);
}
