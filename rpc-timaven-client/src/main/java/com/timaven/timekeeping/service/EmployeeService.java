package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.EmployeeFilter;
import com.timaven.timekeeping.model.dto.EmployeeAuditDto;
import com.timaven.timekeeping.model.dto.EmployeeDto;
import com.timaven.timekeeping.model.dto.EmployeeIdNameDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface EmployeeService {
    Page<EmployeeDto> getEmployees(PagingRequest pagingRequest, Long projectId);

    Employee findById(Long id);

    void saveEmployee(Employee employee);

    Page<EmployeeAuditDto> getEmployeeAudit(PagingRequest pagingRequest, Long projectId);

    List<Employee> getEmployees(EmployeeFilter filter);

    void saveEmployeeTag(Long projectId, String empId, String tag);

    Set<String> getTagsByProjectId(Long projectId);

    void deleteEmployeeTag(Long projectId, String empId, String tag);

    void updateEmployeePerDiem(Long employeeId, Long perDiemId);

    // TM-307
    void updateEmployeeIsOffsite(Long employeeId, Boolean isOffsite);

    Set<EmployeeIdNameDto> getEmployeeIdNames(Long projectId, LocalDate dateOfService);

    void saveEmployees(List<Employee> employees);
}
