package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.Notification;

import java.util.List;

public interface NotificationService {
    List<Notification> getNotificationsByUserId();

    void updateNotificationRead(Long id, boolean read);

    int getUnreadNotificationCountByUserId(Long id);

    List<Notification> saveNotifications(List<Notification> notifications);
}
