package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.RPCUserPool;

import java.util.List;

public interface RPCUserPoolService {
    /**
     * getUserPools
     * @param projectId
     * @return
     */
    List<RPCUserPool> getUserPools();
}
