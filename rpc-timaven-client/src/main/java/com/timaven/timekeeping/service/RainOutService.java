package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.RainOut;

import java.util.Set;

public interface RainOutService {
    Set<RainOut> getAllRainOuts(Long projectId);

    RainOut getRainOutById(Long id);

    void saveRainOut(RainOut rainOut);
}
