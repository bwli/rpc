package com.timaven.timekeeping.service;


import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;

import java.util.Set;

public interface UserProjectService {
    Set<Project> getProjectsByUsername(String principal);

    Page<Project> getProjectsByUsername(String principal, PagingRequest pagingRequest, boolean isAll);
}
