package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.provider.WebClientAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.CostCodeAssociationDto;
import com.timaven.timekeeping.model.dto.CostCodeBillingCodeFilter;
import com.timaven.timekeeping.model.enums.CostCodeType;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.*;

@Service
public class CostCodeServiceImpl implements CostCodeService {

    private final ProviderAPI providerAPI;
    private final WebClientAPI webClientAPI;

    @Autowired
    public CostCodeServiceImpl(ProviderAPI providerAPI, WebClientAPI webClientAPI) {
        this.providerAPI = providerAPI;
        this.webClientAPI = webClientAPI;
    }

    @Override
    public void deleteById(Long id) {
        providerAPI.deleteCostCodeLogById(id);
    }

    @Override
    public Page<CostCodeAssociationDto> findCostCodeAssociations(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getCostCodeAssociationDtoPage(projectId, pagingRequest);
    }

    @Override
    public List<CostCodeLog> getCostCodesByCodesAndProjectIdAndDate(Long projectId, List<String> costCodes,
                                                                    LocalDate dateOfService) {
        if (CollectionUtils.isEmpty(costCodes)) return new ArrayList<>();
        return providerAPI.getCostCodesByProjectIdAndDateOfServiceIncludeGlobal(projectId, dateOfService, costCodes);
    }

    @Override
    public CostCodeLog getCostCodeById(Long id) {
        return providerAPI.getCostCodeById(id);
    }

    @Override
    public List<CostCodeBillingCode> getCostCodeBillingCodes(Set<Long> costCodeIds) {
        if (CollectionUtils.isEmpty(costCodeIds)) return new ArrayList<>();
        CostCodeBillingCodeFilter filter = new CostCodeBillingCodeFilter(costCodeIds, Set.of("costCodeLog", "billingCodeType"));
        return providerAPI.getCostCodeBillingCodesByCostCodeIds(filter);
    }

    @Override
    public Map<String, String> findCostCodeColumns(Long projectId) {
        List<BillingCodeType> billingCodeTypes = providerAPI.getBillingCodeTypes(true);
        Map<String, String> result = new LinkedHashMap<>();
        result.put("id", "");
        // TM 389 Change cost code page description
        result.put("costCode", "Code ID");
        result.put("costCodeSegment", "Cost Code Distribution");
        result.put("description", "Description");
        result.put("tags", "Tags");
        if (projectId == null) {
            result.put("jobNumber", "Job #");
            result.put("subJob", "Sub Job #");
        }
        for (int i = 0; i < billingCodeTypes.size(); i++) {
            result.put("level" + (i + 1), billingCodeTypes.get(i).getCodeType());
        }
        return result;
    }

    @Override
    public void lockCostCodes(Set<String> costCodes, Long projectId) {
        providerAPI.lockCostCodes(costCodes, projectId);
    }

    @Override
    public Page<CostCodeTagType> getCostCodeTagTypePage(Long projectId, PagingRequest pagingRequest) {
        return providerAPI.getCostCodeTagTypePage(projectId, pagingRequest);
    }

    @Override
    public CostCodeTagType getCostCodeTagTypeById(Long id) {
        return providerAPI.getCostCodeTagTypeById(id);
    }

    @Override
    public CostCodeTag getCostCodeTagById(Long id) {
        return providerAPI.getCostCodeTagById(id);
    }

    @Override
    public Page<CostCodeTag> getCostCodeTagPage(Long typeId, PagingRequest pagingRequest) {
        return providerAPI.getCostCodeTagPage(typeId, pagingRequest);
    }

    @Override
    public void deleteCostCodeTagTypeById(Long id) {
        providerAPI.deleteCostCodeTagTypeById(id);
    }

    @Override
    public void deleteCostCodeTagById(Long id) {
        providerAPI.deleteCostCodeTagById(id);
    }

    @Override
    public void saveTagCode(CostCodeTag costCodeTag) {
        providerAPI.saveTagCode(costCodeTag);
    }

    @Override
    public void saveTagType(CostCodeTagType costCodeTagType) {
        providerAPI.saveTagType(costCodeTagType);
    }

    @Override
    public List<CostCodeTagType> getAllCostCodeTagTypesByProjectId(Long projectId) {
        return providerAPI.getAllCostCodeTagsByProjectId(projectId);
    }

    @Override
    public List<CostCodeTag> findCostCodeTagsByTypeId(Long typeId) {
        return providerAPI.findCostCodeTagsByTypeId(typeId);
    }

    @Override
    public void deleteCostCodeTagAssociationsByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type) {
        providerAPI.deleteCostCodeTagAssociationsByKeyInAndCostCodeType(keys, type);
    }

    @Override
    public void saveCostCodeTagAssociations(Set<CostCodeTagAssociation> costCodeTagAssociations) {
        providerAPI.saveCostCodeTagAssociations(costCodeTagAssociations);
    }

    @Override
    public List<CostCodeTagAssociation> getCostCodeTagAssociationsByKeysAndType(Set<Long> keys, CostCodeType type) {
        return providerAPI.getCostCodeTagAssociationsByKeysAndType(keys, type);
    }

    @Override
    public void saveCostCodeLogTag(Long projectId, String costCode, String tag) {
        providerAPI.saveCostCodeLogTag(projectId, costCode, tag);
    }

    @Override
    public void deleteCostCodeLogTag(Long projectId, String costCode, String tag) {
        providerAPI.deleteCostCodeLogTag(projectId, costCode, tag);
    }

    @Override
    public Set<String> getTagsByProjectId(Long projectId) {
        return providerAPI.getCostCodeLogTagStringsByProjectId(projectId);
    }

    @Override
    public List<CostCodeTagType> findAllCostCodeTagTypes() {
        return webClientAPI.findAllCostCodeTagTypes().block();
    }

    @Override
    public List<CostCodeLog> getAllCostCodeLogsByProjectIdAndDateOfService(Long projectId, LocalDate dateOfService) {
        return providerAPI.getCostCodesByProjectIdAndDateOfServiceIncludeGlobal(projectId, dateOfService, null);
    }

    @Override
    public List<CostCodeLog> getAllCostCodeLogsByProjectIdAndDateRange(Long projectId, List<LocalDate> days) {
        return providerAPI.getCostCodesByProjectIdAndDateRangeIncludeGlobal(projectId, days.get(0), days.get(days.size() - 1), null);
    }

    @Override
    public Set<CostCodeLog> getCostCodeLogsByTeamNameAndDateOfService(String teamName, Long projectId,
                                                                      LocalDate dateOfService) {
        return providerAPI.getCostCodeLogsByProjectIdAndTeamAndDateOfService(projectId, teamName, dateOfService);
    }
}
