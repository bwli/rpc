package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.provider.WebClientAPI;
import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.EmployeeFilter;
import com.timaven.timekeeping.model.dto.EmployeeAuditDto;
import com.timaven.timekeeping.model.dto.EmployeeDto;
import com.timaven.timekeeping.model.dto.EmployeeIdNameDto;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final ProviderAPI providerAPI;
    private final WebClientAPI webClientAPI;

    @Autowired
    public EmployeeServiceImpl(ProviderAPI providerAPI, WebClientAPI webClientAPI) {
        this.providerAPI = providerAPI;
        this.webClientAPI = webClientAPI;
    }

    @Override
    public Page<EmployeeDto> getEmployees(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getEmployees(pagingRequest, projectId);
    }

    @Override
    public Employee findById(Long id) {
        return providerAPI.getEmployeeById(id);
    }

    @Override
    public void saveEmployee(Employee employee) {
        providerAPI.saveEmployee(employee);
    }

    @Override
    public Page<EmployeeAuditDto> getEmployeeAudit(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getEmployeeAudit(projectId, pagingRequest);
    }

    @Override
    public List<Employee> getEmployees(EmployeeFilter filter) {
        return providerAPI.getEmployees(filter);
    }

    @Override
    public void saveEmployeeTag(Long projectId, String empId, String tag) {
        providerAPI.saveEmployeeTag(projectId, empId, tag);
    }

    @Override
    public Set<String> getTagsByProjectId(Long projectId) {
        return providerAPI.getEmployeeTagsByProjectId(projectId);
    }

    @Override
    public void deleteEmployeeTag(Long projectId, String empId, String tag) {
        providerAPI.deleteEmployeeTag(projectId, empId, tag);
    }

    @Override
    public void updateEmployeePerDiem(Long employeeId, Long perDiemId) {
        Employee employee = providerAPI.getEmployeeById(employeeId);
        employee.setPerDiemId(perDiemId);
        providerAPI.saveEmployee(employee);
    }

    // TM-307
    @Override
    public void updateEmployeeIsOffsite(Long employeeId, Boolean isOffsite) {
        Employee employee = providerAPI.getEmployeeById(employeeId);
        employee.setIsOffsite(isOffsite);
        providerAPI.saveEmployee(employee);
    }

    @Override
    public Set<EmployeeIdNameDto> getEmployeeIdNames(Long projectId, LocalDate dateOfService) {
        EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, dateOfService, null);
        return webClientAPI.getEmployeeIdNames(filter).defaultIfEmpty(new HashSet<>()).block();
    }

    @Override
    public void saveEmployees(List<Employee> employees) {
        webClientAPI.saveEmployees(employees).block();
    }
}
