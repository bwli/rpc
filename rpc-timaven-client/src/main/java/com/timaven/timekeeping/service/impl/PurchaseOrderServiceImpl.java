package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.paging.Page;
import com.timaven.timekeeping.model.paging.PagingRequest;
import com.timaven.timekeeping.service.PurchaseOrderService;
import com.timaven.timekeeping.util.AuthenticationHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final ProviderAPI providerAPI;

    @Autowired
    public PurchaseOrderServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Page<PurchaseOrderSummaryDto> getLatestPurchaseOrderSummaries(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getPurchaseOrderSummaryDtoPage(pagingRequest, projectId);
    }

    @Override
    public Page<PurchaseOrderDetailDto> getLatestPurchaseOrderDetails(PagingRequest pagingRequest,
                                                                      String purchaseOrderNumber) {
        return providerAPI.getPurchaseOrderDetailDtoPage(pagingRequest, purchaseOrderNumber);
    }

    @Override
    public RequisitionDto getRequisitionDto(String requisitionNumber) {
        return providerAPI.getRequisitionDto(requisitionNumber);
    }

    @Override
    public void deleteByRequisitionNumber(String requisitionNumber) {
        providerAPI.deleteRequisitionByRequisitionNumber(requisitionNumber);
    }

    @Override
    public void saveRequisition(RequisitionDto requisitionDto, Long projectId) {
        providerAPI.saveRequisitions(requisitionDto, projectId);
    }

    @Override
    public Page<RequisitionGroup> getRequisitionGroups(PagingRequest pagingRequest, Long projectId) {
        return providerAPI.getRequisitionGroups(pagingRequest, projectId, AuthenticationHelper.hasRole("ROLE_corporate_accountant"));
    }

    @Override
    public Set<String> getDistinctLatestPurchaseOrderNumbers() {
        return providerAPI.getDistinctLatestPurchaseOrderNumbers();
    }

    @Override
    public PurchaseOrderBilling savePurchaseOrderBilling(PurchaseOrderBillingDto dto) {
        return providerAPI.savePurchaseOrderBilling(dto);
    }

    @Override
    public Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderDetailHistory(PagingRequest pagingRequest, String purchaseOrderNumber) {
        return providerAPI.getPurchaseOrderDetailHistory(pagingRequest, purchaseOrderNumber);
    }

    @Override
    public void deletePurchaseOrderBillingById(Long id) {
        providerAPI.deletePurchaseOrderBillingById(id);
    }

    @Override
    public String generateRequisitionNumber(Project project) {
        String jobNumber = project.getJobNumber();
        String subJob = project.getSubJob();
        Set<Requisition> requisitions = providerAPI.getRequisitions(jobNumber, subJob);
        Integer[] nums = requisitions.stream()
                .map(Requisition::getRequisitionNumber)
                .map(r -> {
                    String numStr = StringUtils.stripStart(r, "0");
                    try {
                        return Integer.parseInt(numStr);
                    } catch (NumberFormatException ex) {
                        return -1;
                    }
                })
                .collect(Collectors.toSet())
                .toArray(Integer[]::new);
        if (nums.length > 0) {
            int size = nums.length;
            for (int i = 0; i < size; i++) {
                int target = nums[i];
                while (target < size && target != nums[target]) {
                    int newTarget = nums[target];
                    nums[target] = target;
                    target = newTarget;
                }
            }
            int result = size;
            for (int i = 0; i < size; i++) {
                if (nums[i] != i && i != 0) {
                    result = i;
                    break;
                }
            }
            return StringUtils.leftPad(String.valueOf(result), 3, '0');
        }
        return "001";
    }

    @Override
    public void updatePurchaseOrderGroupComment(String purchaseOrderNumber, String comment) {
        providerAPI.savePurchaseOrderGroupComment(purchaseOrderNumber, comment);
    }

    @Override
    public void updatePurchaseOrderGroupFlag(String purchaseOrderNumber, boolean flag) {
        providerAPI.savePurchaseOrderGroupFlag(purchaseOrderNumber, flag);
    }

    @Override
    public Page<PurchaseVendorRoster> getPurchaseVendorRoster(PagingRequest pagingRequest) {
        return providerAPI.getPurchaseVendorRoster(pagingRequest);
    }
}
