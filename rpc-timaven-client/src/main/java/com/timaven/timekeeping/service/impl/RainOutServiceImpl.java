package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.RainOut;
import com.timaven.timekeeping.service.RainOutService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class RainOutServiceImpl implements RainOutService {

    private final ProviderAPI providerAPI;

    public RainOutServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Set<RainOut> getAllRainOuts(Long projectId) {
        return providerAPI.getRainOuts(projectId);
    }

    @Override
    public RainOut getRainOutById(Long id) {
        return providerAPI.getRainOutById(id);
    }

    @Override
    public void saveRainOut(RainOut rainOut) {
        providerAPI.saveRainOut(rainOut);
    }
}
