package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.dto.ReconcileDto;
import com.timaven.timekeeping.service.ReconcileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class ReconcileServiceImpl implements ReconcileService {

    private final ProviderAPI providerAPI;

    @Autowired
    public ReconcileServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public ReconcileDto getReconcileData(LocalDate weekEndDate) {
        return providerAPI.getReconcileDto(weekEndDate);
    }
}
