package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.provider.WebClientAPI;
import com.timaven.timekeeping.model.Shift;
import com.timaven.timekeeping.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ShiftServiceImpl implements ShiftService {

    private final ProviderAPI providerAPI;
    private final WebClientAPI webClientAPI;

    @Autowired
    public ShiftServiceImpl(ProviderAPI providerAPI, WebClientAPI webClientAPI) {
        this.providerAPI = providerAPI;
        this.webClientAPI = webClientAPI;
    }

    @Override
    public Set<Shift> getShiftsByProjectId(Long projectId) {
        return providerAPI.getShiftsByProjectId(projectId);
    }

    @Override
    public Shift getShiftById(Long shiftId) {
        return providerAPI.getShiftById(shiftId);
    }

    @Override
    public void saveShift(Shift shift) {
        providerAPI.saveShift(shift);
    }

    @Override
    public void deleteShiftById(Long id) {
        providerAPI.deleteShiftById(id);
    }

    @Override
    public Set<String> getAllShiftNamesByProjectId(Long projectId) {
        return providerAPI.getAllShiftNamesByProjectId(projectId);
    }

    @Override
    public Shift getDefaultShiftByProjectId(Long projectId) {
        return providerAPI.getDefaultShiftByProjectId(projectId);
    }

    @Override
    public Shift getShiftByProjectIdAndName(Long projectId, String name) {
        return webClientAPI.findShiftByProjectIdAndName(projectId, name).block();
    }
}
