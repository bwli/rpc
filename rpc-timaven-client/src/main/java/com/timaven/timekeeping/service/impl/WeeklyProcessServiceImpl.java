package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.provider.WebClientAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.ProjectsDashboardDto;
import com.timaven.timekeeping.model.dto.ProjectsDashboardProfitDto;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.model.dto.WeeklyPayrollStatusDto;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import com.timaven.timekeeping.model.enums.WeeklyProcessStatus;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;
import com.timaven.timekeeping.service.RuleService;
import com.timaven.timekeeping.service.WeeklyProcessService;
import com.timaven.timekeeping.util.DateRange;
import com.timaven.timekeeping.util.LocalDateUtil;
import com.timaven.timekeeping.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WeeklyProcessServiceImpl implements WeeklyProcessService {

    private final RuleService ruleService;
    private final ProviderAPI providerAPI;
    private final WebClientAPI webClientAPI;

    @Autowired
    public WeeklyProcessServiceImpl(RuleService ruleService, ProviderAPI providerAPI, WebClientAPI webClientAPI) {
        this.ruleService = ruleService;
        this.providerAPI = providerAPI;
        this.webClientAPI = webClientAPI;
    }

    @Override
    public WeeklyProcess findByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, WeeklyProcessType type) {
        return providerAPI.findWeeklyProcessesByProjectIdAndDateAndType(weekEndDate, weekEndDate, type, projectId, null).stream().findFirst().orElse(null);
    }

    @Override
    public WeeklyProcess save(WeeklyProcess weeklyProcess) {
        return providerAPI.saveWeeklyProcess(weeklyProcess);
    }

    @Override
    public List<WeeklyPayrollDto> getWeeklyPayrollDtos(LocalDate endDate) {
        List<Project> projects = providerAPI.getAllProjects();
        List<Long> projectIds = projects.stream()
                .map(Project::getId).collect(Collectors.toList());
        // Add fake overhead project Id
        projectIds.add(0L);
        projectIds = projectIds.stream().sorted().collect(Collectors.toList());

        List<WeeklyPayrollDto> weeklyPayrollDtos = new ArrayList<>();
        for (Long projectId : projectIds) {
            Optional<Rule> ruleOpt = ruleService.getRule(projectId == 0 ? null : projectId);
            Rule rule = ruleOpt.orElse(null);
            if (projectId == 0) {
                if (rule == null) {
                    rule = new Rule();
                    rule.setWeekEndDay(DayOfWeek.SUNDAY);
                    rule.setTaxablePerDiem(false);
                }
            }
            if (rule != null) {

                LocalDate weekEndDate = endDate.with(rule.getWeekEndDay());
                if (weekEndDate.isAfter(endDate)) weekEndDate = weekEndDate.minusWeeks(1);

                Set<WeeklyProcess> weeklyProcesses = providerAPI.findWeeklyProcessesByProjectIdAndDateAndType(weekEndDate, weekEndDate, null, projectId == 0 ? null : projectId, Set.of("allocationTimes"));

                if (!CollectionUtils.isEmpty(weeklyProcesses)) {
                    for (WeeklyProcess weeklyProcess : weeklyProcesses) {
                        WeeklyPayrollDto weeklyPayrollDto = new WeeklyPayrollDto();
                        weeklyPayrollDto.setTimeLeftOff(weeklyProcess.getType() == WeeklyProcessType.TLO);
                        weeklyPayrollDto.setTaxablePerDiem(weeklyProcess.isTaxablePerDiem());
                        if (weeklyProcess.getApprovedAt() != null) {
                            weeklyPayrollDto.setApprovedAt(weeklyProcess.getApprovedAt());
                            // TM-268 modify corporate payroll process page add approver user info
                            weeklyPayrollDto.setApproveUser(providerAPI.getUserById(weeklyProcess.getApproveUserId()));
                        }
                        if (weeklyProcess.getReportGeneratedAt() != null) {
                            weeklyPayrollDto.setGeneratedAt(weeklyProcess.getReportGeneratedAt());
                        }
                        // TM-268 modify corporate payroll process page
                        if (weeklyProcess.getRejectAt() != null) {
                            weeklyPayrollDto.setRejectedAt(weeklyProcess.getRejectAt());
                            weeklyPayrollDto.setRejectUser(providerAPI.getUserById(weeklyProcess.getRejectUserId()));
                        }
                        if (weeklyProcess.getFinalizedAt() != null) {
                            weeklyPayrollDto.setFinalizedAt(weeklyProcess.getFinalizedAt());
                            weeklyPayrollDto.setFinalizeUser(providerAPI.getUserById(weeklyProcess.getFinalizeUserId()));
                        }
                        BigDecimal totalHours = weeklyProcess.getAllocationTimes().stream()
                                .map(AllocationTime::getAllocatedHour)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
                        weeklyPayrollDto.setTotalHours(totalHours);
                        int empCount = weeklyProcess.getAllocationTimes().stream()
                                .map(AllocationTime::getEmpId)
                                .collect(Collectors.toSet())
                                .size();
                        weeklyPayrollDto.setEmployeeCount(empCount);
                        weeklyPayrollDto.setId(weeklyProcess.getId());
                        if (projectId != 0) {
                            Project project = projects.stream()
                                    .filter(p -> p.getId().equals(projectId)).findFirst().orElseThrow(IllegalArgumentException::new);

                            // TM-368 project.getJobSubJob() to project.getJobSubJobClientNameDescription()
                            // Show JobNumber - SubJob - ClientName - Description
                            weeklyPayrollDto.setProject(project.getJobSubJobClientNameDescription());

                            weeklyPayrollDto.setTeamName("All");
                        } else {
                            weeklyPayrollDto.setProject("Corporate");
                            weeklyPayrollDto.setTeamName(weeklyProcess.getTeamName());
                        }
                        if (weeklyPayrollDto.isTimeLeftOff()) {
                            weeklyPayrollDto.setProject(weeklyPayrollDto.getProject().concat(" (Time Left Off)"));
                        }

                        // get project manager accountant info
                        Set<User> users = providerAPI.getUsersByProjectIdAndRoles(projectId, Set.of("ROLE_project_manager", "ROLE_accountant", "ROLE_accountant_foreman"));
                        List<String> projectManagers = users.stream()
                                .filter(u -> u.getUserRoles().stream()
                                        .map(UserRole::getRole)
                                        .map(Role::getName)
                                        .anyMatch(n -> n.equalsIgnoreCase("ROLE_project_manager")))
                                .map(User::getDisplayName)
                                .sorted()
                                .collect(Collectors.toList());
                        List<String> projectAccountants = users.stream()
                                .filter(u -> u.getUserRoles().stream()
                                        .map(UserRole::getRole)
                                        .map(Role::getName)
                                        .anyMatch(n -> n.equalsIgnoreCase("ROLE_accountant")
                                                || n.equalsIgnoreCase("ROLE_accountant_foreman")))
                                .map(User::getDisplayName)
                                .sorted()
                                .collect(Collectors.toList());

                        weeklyPayrollDto.setProjectManagers(projectManagers);
                        weeklyPayrollDto.setProjectAccountants(projectAccountants);
                        weeklyPayrollDto.setComment(weeklyProcess.getComment());

                        weeklyPayrollDtos.add(weeklyPayrollDto);
                    }
                }
            }
        }

        return weeklyPayrollDtos;
    }

    @Override
    public void approveWeeklyTimesheets(Long userId, LocalDate weekEndDate, Long projectId, int type) {
        WeeklyProcessType weeklyProcessType = WeeklyProcessType.fromValue(type);
        calculateCost(projectId, null, weekEndDate, weeklyProcessType, userId);
    }

    @Override
    public void calculateCost(Long projectId, @Nullable String teamName, LocalDate weekEndDate, WeeklyProcessType type) {
        calculateCost(projectId, teamName, weekEndDate, type, null);
    }

    private void calculateCost(Long projectId, String teamName, LocalDate weekEndDate, WeeklyProcessType type, Long userId) {
        if (weekEndDate == null) return;
        WeeklyProcess weeklyProcess = findOrGenerateWeeklyProcess(projectId, teamName, weekEndDate, type);
        LocalDate weekStartDate = weekEndDate.minusDays(6);
        Set<AllocationTime> allocationTimes;
        if (WeeklyProcessType.NORMAL == type) {
            allocationTimes = providerAPI.getAllocationTimes(weekStartDate, weekEndDate, weekEndDate, weekEndDate, projectId, teamName, AllocationSubmissionStatus.APPROVED, null, List.of("crafts"), null);
        } else if (WeeklyProcessType.TLO == type) {
            allocationTimes = providerAPI.getAllocationTimes(null, weekStartDate.minusDays(1), weekEndDate, weekEndDate, projectId, teamName, AllocationSubmissionStatus.APPROVED, null, List.of("crafts"), null);
        } else {
            allocationTimes = new HashSet<>();
        }
        if (userId != null) {
            weeklyProcess.setApprovedAt(LocalDateTime.now());
            weeklyProcess.setApproveUserId(userId);
            weeklyProcess.setRejectAt(null);
            weeklyProcess.setRejectUserId(null);
            Set<String> costCodes = allocationTimes.stream()
                    .map(AllocationTime::getCostCodePercs)
                    .flatMap(Collection::stream)
                    .map(CostCodePerc::getCostCodeFull)
                    .collect(Collectors.toSet());

            costCodes.addAll(allocationTimes.stream()
                    .map(AllocationTime::getMobCostCode)
                    .filter(StringUtils::hasText)
                    .collect(Collectors.toSet()));

            costCodes.addAll(allocationTimes.stream()
                    .map(AllocationTime::getPerDiemCostCode)
                    .filter(StringUtils::hasText)
                    .collect(Collectors.toSet()));

            providerAPI.lockCostCodes(costCodes, projectId);
        }
        calculateCost(weeklyProcess, allocationTimes);
    }

    @Override
    public void calculateCost(WeeklyProcess weeklyProcess, Set<AllocationTime> allocationTimes) {
        if (weeklyProcess.getWeekEndDate() == null) return;
        BigDecimal billableCost = allocationTimes.stream()
                .map(t -> {
                    Craft craft = t.getEmployee().getCraftEntity();
                    if (craft == null) return BigDecimal.ZERO;
                    BigDecimal billableST = craft.getBillableST() == null ? BigDecimal.ZERO : craft.getBillableST();
                    BigDecimal billableOT = craft.getBillableOT() == null ? BigDecimal.ZERO : craft.getBillableOT();
                    BigDecimal billableDT = craft.getBillableDT() == null ? BigDecimal.ZERO : craft.getBillableDT();
                    if (CollectionUtils.isEmpty(t.getCostCodePercs())) {
                        BigDecimal stHour = t.getStHour() == null ? BigDecimal.ZERO : t.getStHour();
                        BigDecimal otHour = BigDecimal.ZERO;
                        if (t.getExtraTimeType() != AllocationTimeHourType.OT) {
                            stHour = stHour.add(t.getOtHour());
                        } else {
                            otHour = t.getOtHour();
                        }
                        return stHour.multiply(billableST).add(otHour.multiply(billableOT));
                    } else {
                        return t.getCostCodePercs().stream()
                                .map(c -> {
                                    BigDecimal stHour = c.getStHour() == null ? BigDecimal.ZERO : c.getStHour();
                                    BigDecimal otHour = c.getOtHour() == null ? BigDecimal.ZERO : c.getOtHour();
                                    BigDecimal dtHour = c.getDtHour() == null ? BigDecimal.ZERO : c.getDtHour();
                                    return stHour.multiply(billableST).add(otHour.multiply(billableOT))
                                            .add(dtHour.multiply(billableDT));
                                }).reduce(BigDecimal.ZERO, BigDecimal::add);
                    }
                }).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal baseCost = allocationTimes.stream()
                .map(t -> {
                    if (CollectionUtils.isEmpty(t.getCostCodePercs())) return BigDecimal.ZERO;
                    Employee employee = t.getEmployee();
                    if (employee == null) return BigDecimal.ZERO;
                    BigDecimal baseST = employee.getBaseST() == null ? BigDecimal.ZERO : employee.getBaseST();
                    BigDecimal baseOT = employee.getBaseOT() == null ? BigDecimal.ZERO : employee.getBaseOT();
                    BigDecimal baseDT = employee.getBaseDT() == null ? BigDecimal.ZERO : employee.getBaseDT();
                    if (CollectionUtils.isEmpty(t.getCostCodePercs())) {
                        BigDecimal stHour = t.getStHour() == null ? BigDecimal.ZERO : t.getStHour();
                        BigDecimal otHour = BigDecimal.ZERO;
                        if (t.getExtraTimeType() != AllocationTimeHourType.OT) {
                            stHour = stHour.add(t.getOtHour());
                        } else {
                            otHour = t.getOtHour();
                        }
                        return stHour.multiply(baseST).add(otHour.multiply(baseOT));
                    } else {
                        return t.getCostCodePercs().stream()
                                .map(c -> {
                                    BigDecimal stHour = c.getStHour() == null ? BigDecimal.ZERO : c.getStHour();
                                    BigDecimal otHour = c.getOtHour() == null ? BigDecimal.ZERO : c.getOtHour();
                                    BigDecimal dtHour = c.getDtHour() == null ? BigDecimal.ZERO : c.getDtHour();
                                    return stHour.multiply(baseST).add(otHour.multiply(baseOT))
                                            .add(dtHour.multiply(baseDT));
                                }).reduce(BigDecimal.ZERO, BigDecimal::add);
                    }
                }).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalHours = allocationTimes.stream()
                .map(AllocationTime::getAllocatedHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        weeklyProcess.setBillableAmount(billableCost);
        weeklyProcess.setBaseAmount(baseCost);
        weeklyProcess.setTotalHours(totalHours);
        providerAPI.saveWeeklyProcess(weeklyProcess);
    }

    @Override
    public void updatePerDiemTaxable(Long weeklyProcessId, boolean taxablePerDiem) {
        WeeklyProcess weeklyProcess = providerAPI.findWeeklyProcessById(weeklyProcessId, null);
        if (weeklyProcess != null) {
            weeklyProcess.setTaxablePerDiem(taxablePerDiem);
            Rule rule = null;
            if (weeklyProcess.getProjectId() == null) {
                Optional<Rule> ruleOpt = providerAPI.getRuleByProjectId(null);
                rule = ruleOpt.orElseGet(Rule::new);
                rule.setTaxablePerDiem(taxablePerDiem);
            } else {
                Optional<Rule> ruleOpt = providerAPI.getRuleByProjectId(weeklyProcess.getProjectId());
                if (ruleOpt.isPresent()) {
                    rule = ruleOpt.get();
                    rule.setTaxablePerDiem(taxablePerDiem);
                }
            }
            if (null != rule) {
                providerAPI.saveRule(rule);
            }
            providerAPI.saveWeeklyProcess(weeklyProcess);
        }
    }

    @Override
    public WeeklyProcess findOrGenerateWeeklyProcess(Long projectId, String teamName, LocalDate weekEndDate, WeeklyProcessType type) {
        return providerAPI.findOrGenerateWeeklyProcessesByProjectIdAndDateAndType(weekEndDate, type, projectId, teamName);
    }

    @Override
    public WeeklyProcess findOrGenerateWeeklyProcess(Long projectId, LocalDate weekEndDate, WeeklyProcessType type) {
        return providerAPI.findOrGenerateWeeklyProcessesByProjectIdAndDateAndType(weekEndDate, type, projectId, null);
    }

    @Override
    public WeeklyProcess findOrGenerateWeeklyProcess(Long projectId, LocalDate weekEndDate, WeeklyProcessType type, String teamName) {
        return providerAPI.findOrGenerateWeeklyProcessesByProjectIdAndDateAndType(weekEndDate, type, projectId, teamName);
    }

    @Override
    public void updateWeeklyPayrollComment(Long id, String comment) {
        WeeklyProcess weeklyProcess = providerAPI.findWeeklyProcessById(id, null);
        if (weeklyProcess != null) {
            weeklyProcess.setComment(comment);
            providerAPI.saveWeeklyProcess(weeklyProcess);
        }
    }

    @Override
    public void addPerDiemAllocationTimes(Long projectId, LocalDate weekEndDate, Rule rule) {
        LocalDate weekStartDate = weekEndDate.minusDays(6);
        Set<AllocationSubmission> allocationSubmissions =
                providerAPI.getAllocationSubmissions(weekStartDate, weekEndDate, projectId, null, null, AllocationSubmissionStatus.DENIED, List.of("allocationTimes"));
        EmployeeFilter employeeFilter = new EmployeeFilter(projectId, null, null, null, null, weekStartDate, weekEndDate);
        List<Employee> employees = providerAPI.getEmployees(employeeFilter);
        List<Craft> crafts = providerAPI.getCraftsByProjectIdAndDateRange(projectId, weekStartDate, weekEndDate);
        Map<String, List<Craft>> codeCraftsMap = crafts.stream()
                .collect(Collectors.groupingBy(Craft::getCode));
        Map<String, List<Employee>> idEmployeesMap = employees.stream()
                .collect(Collectors.groupingBy(Employee::getEmpId));
        Map<Long, PerDiem> idPerDiemMap = new HashMap<>();
        PerDiem projectPerDiem = null;
        if (rule.getPerDiemId() != null) {
            projectPerDiem = providerAPI.findPerDiemById(rule.getPerDiemId());
            if (null != projectPerDiem) {
                idPerDiemMap.put(rule.getPerDiemId(), projectPerDiem);
            }
        }
        List<AllocationTime> allocationTimes = allocationSubmissions.stream()
                .map(AllocationSubmission::getAllocationTimes)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        Map<String, List<AllocationTime>> empIdAllocationTimeMap = allocationTimes.stream()
                .collect(Collectors.groupingBy(AllocationTime::getEmpId));

        Set<String> empIdsWithTime = allocationTimes.stream()
                .map(AllocationTime::getEmpId)
                .collect(Collectors.toSet());
        Map<String, Set<LocalDate>> empIdPerDiemDate = new HashMap<>();
        List<LocalDate> dates = new DateRange(weekStartDate, weekEndDate, false).toList();
        // Preprocess to confirm what date has perdiem
        for (String empId : empIdsWithTime) {
            Optional<Employee> employeeOpt = idEmployeesMap.getOrDefault(empId, new ArrayList<>())
                    .stream()
                    .filter(e -> e.getTerminatedAt() == null
                            || e.getTerminatedAt().isAfter(weekStartDate)
                            || e.getTerminatedAt().isEqual(weekStartDate))
                    .max(Comparator.comparing(Employee::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));
            if (employeeOpt.isPresent()) {
                Employee employee = employeeOpt.get();
                PerDiem perDiem = null;
                if (employee.getPerDiemId() != null) {
                    perDiem = idPerDiemMap.getOrDefault(employee.getPerDiemId(), providerAPI.findPerDiemById(employee.getPerDiemId()));

                }
                if (perDiem != null) {
                    idPerDiemMap.putIfAbsent(employee.getPerDiemId(), perDiem);
                } else {
                    List<Craft> craftList = codeCraftsMap.getOrDefault(employee.getCraft(), new ArrayList<>());
                    Optional<Craft> craftOpt = craftList.stream()
                            .filter(c -> c.getEndDate() == null
                                    || c.getEndDate().isAfter(weekStartDate)
                                    || c.getEndDate().isEqual(weekStartDate))
                            .max(Comparator.comparing(Craft::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));
                    if (craftOpt.isPresent()) {
                        Craft craft = craftOpt.get();
                        if (craft.getPerDiemId() != null) {
                            perDiem = idPerDiemMap.getOrDefault(craft.getPerDiemId(), providerAPI.findPerDiemById(craft.getPerDiemId()));
                            if (perDiem != null) {
                                idPerDiemMap.putIfAbsent(craft.getPerDiemId(), perDiem);
                            }
                        }
                    }
                }
                if (perDiem == null) {
                    perDiem = projectPerDiem;
                }
                if (perDiem != null && perDiem.getActive()) {
                    List<AllocationTime> allocationTimeList = empIdAllocationTimeMap.getOrDefault(empId, new ArrayList<>());
                    Set<LocalDate> dates1 = new HashSet<>();
                    if (!CollectionUtils.isEmpty(allocationTimeList)) {
                        dates1 = allocationTimeList.stream()
                                .filter(AllocationTime::isHasPerDiem)
                                .map(t -> t.getAllocationSubmission().getDateOfService())
                                .collect(Collectors.toSet());
                    }
                    switch (perDiem.getPerDiemMode()) {
                        case N_PLUS_ONE:
                            if (!CollectionUtils.isEmpty(allocationTimeList)) {
                                Integer weeklyThreshold = perDiem.getWeeklyThreshold();
                                weeklyThreshold = weeklyThreshold == null ? Integer.MAX_VALUE : weeklyThreshold;
                                if (dates1.size() >= weeklyThreshold) {
                                    LocalDate extraDate = LocalDateUtil.getFirstValidDate(weekStartDate, weekEndDate, dates1);
                                    if (null != extraDate) {
                                        dates1.add(extraDate);
                                        if (!CollectionUtils.isEmpty(dates1)) {
                                            empIdPerDiemDate.put(empId, dates1);
                                        }
                                    }
                                }
                            }
                            break;
                        case SEVEN_DAYS:
                            Integer weeklyThreshold = perDiem.getWeeklyThreshold();
                            weeklyThreshold = weeklyThreshold == null ? Integer.MAX_VALUE : weeklyThreshold;
                            if (dates1.size() >= weeklyThreshold) {
                                empIdPerDiemDate.put(empId, new HashSet<>(dates));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        Map<TeamServiceDate, AllocationSubmission> newAllocationSubmissionMap = new HashMap<>();
        for (Map.Entry<String, Set<LocalDate>> entry : empIdPerDiemDate.entrySet()) {
            String empId = entry.getKey();
            List<AllocationTime> allocationTimeList = empIdAllocationTimeMap.get(empId);
            if (CollectionUtils.isEmpty(allocationTimeList)) continue;
            AllocationTime lastAllocationTime = allocationTimeList.stream()
                    .max(Comparator.comparing(t -> t.getAllocationSubmission().getDateOfService())).orElse(new AllocationTime());
            AllocationSubmission lastAllocationSubmission = lastAllocationTime.getAllocationSubmission();
            Optional<Employee> employeeOpt = idEmployeesMap.getOrDefault(empId, new ArrayList<>())
                    .stream()
                    .max(Comparator.comparing(Employee::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));
            if (employeeOpt.isPresent()) {
                Employee employee = employeeOpt.get();

                if (!StringUtils.hasText(employee.getTeamName())) continue;

                Set<LocalDate> dateSet = entry.getValue();
                for (LocalDate dateOfService : dateSet) {
                    TeamServiceDate teamServiceDate = new TeamServiceDate(employee.getTeamName().trim(), dateOfService, weekEndDate);
                    Set<AllocationTime> allocationTimeSet = new HashSet<>();
                    AllocationSubmission submission = newAllocationSubmissionMap.get(teamServiceDate);

                    if (null == submission) {
                        submission = new AllocationSubmission();
                        submission.setSubmitUserId(lastAllocationSubmission.getSubmitUserId());
                        submission.setApproveUserId(lastAllocationSubmission.getApproveUserId());
                        submission.setFinalizeUserId(lastAllocationSubmission.getFinalizeUserId());
                        submission.setApprovedAt(lastAllocationSubmission.getApprovedAt());
                        submission.setFinalizedAt(lastAllocationSubmission.getFinalizedAt());
                        submission.setProjectId(projectId);
                        submission.setDateOfService(dateOfService);
                        submission.setTeamName(teamServiceDate.getTeam());
                        submission.setAllocationTimes(allocationTimeSet);
                        submission.setStatus(AllocationSubmissionStatus.APPROVED);
                        submission.setIsBySystem(true);
                        submission.setPayrollDate(weekEndDate);
                        newAllocationSubmissionMap.put(teamServiceDate, submission);
                    } else {
                        allocationTimeSet = submission.getAllocationTimes();
                    }
                    AllocationTime allocationTime = new AllocationTime();
                    allocationTime.setFirstName(Optional.ofNullable(employee.getFirstName()).orElse(""));
                    allocationTime.setLastName(Optional.ofNullable(employee.getLastName()).orElse(""));
                    allocationTime.setEmpId(empId);
                    allocationTime.setBadge(employee.getBadge());
                    allocationTime.setClientEmpId(employee.getClientEmpId());
                    allocationTime.setTeamName(teamServiceDate.getTeam());
                    allocationTime.setStHour(BigDecimal.ZERO);
                    allocationTime.setOtHour(BigDecimal.ZERO);
                    allocationTime.setDtHour(BigDecimal.ZERO);
                    allocationTime.setTotalHour(BigDecimal.ZERO);
                    allocationTime.setNetHour(BigDecimal.ZERO);
                    allocationTime.setPerDiemCostCode(lastAllocationTime.getPerDiemCostCode());
                    allocationTime.setPerDiemAmount(lastAllocationTime.getPerDiemAmount());
                    allocationTime.setHasPerDiem(true);
                    allocationTimeSet.add(allocationTime);
                }
            }
        }
        if (!CollectionUtils.isEmpty(newAllocationSubmissionMap)) {
            providerAPI.saveAllocationSubmissions(new HashSet<>(newAllocationSubmissionMap.values()));
        }
    }

    @Override
    public void updateWeeklyProcesses(Collection<Long> ids, WeeklyProcessStatus status) {
        webClientAPI.updateWeeklyProcesses(ids, status);
    }

    @Override
    public void getCorporateDashboard(List<ProjectsDashboardDto> projectsDashboardDtos,
                                      List<ProjectsDashboardProfitDto> projectsDashboardProfitDtos) {
        List<Project> projects = providerAPI.getAllProjects()
                .stream().sorted(Comparator.comparing(Project::getJobNumber, Comparator.nullsLast(Comparator.naturalOrder()))
                        .thenComparing(Project::getSubJob, Comparator.nullsLast(Comparator.naturalOrder())))
                .collect(Collectors.toList());
        getProjectsDashboard(projectsDashboardDtos, projectsDashboardProfitDtos, projects);
    }

    @Override
    public void getCorporateDashboard(List<ProjectsDashboardDto> projectsDashboardDtos,
                                      List<ProjectsDashboardProfitDto> projectsDashboardProfitDtos,
                                      List<Project> projectList) {
        getProjectsDashboard(projectsDashboardDtos, projectsDashboardProfitDtos, projectList);
    }

    @Override
    public void getProjectsDashboard(List<ProjectsDashboardDto> projectsDashboardDtos, Set<Project> projects) {
        getProjectsDashboard(projectsDashboardDtos, null, projects.stream()
                .sorted(Comparator.comparing(Project::getJobNumber,
                                Comparator.nullsLast(Comparator.naturalOrder()))
                        .thenComparing(Project::getSubJob,
                                Comparator.nullsLast(Comparator.naturalOrder())))
                .collect(Collectors.toList()));
    }

    @Override
    public WeeklyProcess findByDateAndTypeFetching(LocalDate weekEndDate, String teamName, WeeklyProcessType type) {
        Set<WeeklyProcess> weeklyProcesses = providerAPI.findWeeklyProcessesByProjectIdAndTeamAndDateAndType(weekEndDate, weekEndDate, type, null, teamName, Set.of("allocationTimes"));
        return weeklyProcesses.stream().findFirst().orElse(null);
    }

    @Override
    public WeeklyProcess findWeeklyProcessByIdFetching(Long id) {
        return providerAPI.findWeeklyProcessById(id, Set.of("allocationTimes"));
    }

    private void getProjectsDashboard(List<ProjectsDashboardDto> projectsDashboardDtos,
                                      List<ProjectsDashboardProfitDto> projectsDashboardProfitDtos,
                                      List<Project> projects) {
        for (Project project : projects) {
            Long projectId = project.getId();
            ProjectsDashboardDto dto = new ProjectsDashboardDto();
            dto.setProjectId(projectId);
            dto.setJobSubJob(project.getJobSubJob());
            dto.setDescription(project.getDescription());
            // TM-192 Start
            dto.setClientName(project.getClientName());
            dto.setCity(project.getCity());
            dto.setState(project.getState());
            // TM-192 End
            Optional<Rule> ruleOpt = providerAPI.getRuleByProjectId(projectId);
            if (ruleOpt.isPresent()) {
                ProjectsDashboardProfitDto profitDto = new ProjectsDashboardProfitDto();
                LocalDate weekEndDate = LocalDate.now().with(ruleOpt.get().getWeekEndDay());

                Set<WeeklyProcess> weeklyProcesses = providerAPI.findWeeklyProcessesByProjectIdAndDateAndType(null, weekEndDate, null, projectId, null);
                LocalDate startOfThisWeek = weekEndDate.minusDays(6);
//                LocalDate lastDateProcessed = weeklyProcesses.stream()
//                        .map(WeeklyProcess::getWeekEndDate)
//                        .max(LocalDate::compareTo).orElse(LocalDate.now().minusDays(1));
                Set<AllocationSubmission> allocationSubmissions = providerAPI.getAllocationSubmissions(startOfThisWeek, weekEndDate, projectId,
                        null, AllocationSubmissionStatus.DENIED, List.of("crafts"));

                List<AllocationTime> allocationTimes = allocationSubmissions.stream()
                        .map(AllocationSubmission::getAllocationTimes)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
                BigDecimal totalHours = BigDecimal.ZERO;
                //Finalized amount
                BigDecimal clientCost = weeklyProcesses.stream()
                        .filter(w -> w.getFinalizedAt() != null)
                        .map(WeeklyProcess::getBillableAmount)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                //Billable amount
                BigDecimal committedCost = weeklyProcesses.stream()
                        .map(WeeklyProcess::getBillableAmount)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                if (!CollectionUtils.isEmpty(allocationTimes)) {
                    totalHours = weeklyProcesses.stream()
                            .filter(w -> w.getApprovedAt() != null || w.getFinalizedAt() != null)
                            .map(WeeklyProcess::getTotalHours)
                            .reduce(BigDecimal.ZERO, BigDecimal::add)
                            .add(allocationTimes.stream()
                                    .map(AllocationTime::getAllocatedHour)
                                    .reduce(BigDecimal.ZERO, BigDecimal::add)
                            );
                    // All date before this week should have weeklyProcess because of the schedule task
//                    Set<AllocationTime> allocationTimeSet = new HashSet<>(allocationTimes);
//                    contentService.fetchEmployeeAndCraft(allocationTimeSet, projectId);
                    dto.setEmpCount(allocationTimes.stream().map(AllocationTime::getEmpId).collect(Collectors.toSet()).size());
                    committedCost = allocationTimes.stream()
                            .map(t -> {
                                Craft craft = t.getEmployee().getCraftEntity();
                                if (craft == null) return BigDecimal.ZERO;
                                BigDecimal billableST = craft.getBillableST() == null ? BigDecimal.ZERO : craft.getBillableST();
                                BigDecimal billableOT = craft.getBillableOT() == null ? BigDecimal.ZERO : craft.getBillableOT();
                                BigDecimal billableDT = craft.getBillableDT() == null ? BigDecimal.ZERO : craft.getBillableDT();
                                return t.getCostCodePercs().stream()
                                        .map(c -> {
                                            BigDecimal stHour = c.getStHour() == null ? BigDecimal.ZERO : c.getStHour();
                                            BigDecimal otHour = c.getOtHour() == null ? BigDecimal.ZERO : c.getOtHour();
                                            BigDecimal dtHour = c.getDtHour() == null ? BigDecimal.ZERO : c.getDtHour();
                                            return stHour.multiply(billableST).add(otHour.multiply(billableOT))
                                                    .add(dtHour.multiply(billableDT));
                                        }).reduce(BigDecimal.ZERO, BigDecimal::add);
                            }).reduce(committedCost, BigDecimal::add);
                }
                dto.setHours(NumberUtil.format(totalHours));

                LocalDate lastWeekEndDate = weekEndDate.minusDays(7);

                Optional<WeeklyProcess> lastWeeklyProcessOptional = weeklyProcesses.stream()
                        .filter(w -> w.getWeekEndDate().isEqual(lastWeekEndDate)).findFirst();
                boolean processed = lastWeeklyProcessOptional.isPresent()
                        && lastWeeklyProcessOptional.get().getApprovedAt() != null;
                dto.setPayrollProcessed(String.format("%s - %s", processed ? "Yes" : "No",
                        DateTimeFormatter.ofPattern("MM/dd/yyyy").format(lastWeekEndDate)));
                dto.setClientCost(NumberUtil.currencyFormat(clientCost));
                dto.setCommittedCost(NumberUtil.currencyFormat(committedCost));

                profitDto.setProjectId(projectId);
                profitDto.setJobSubJob(dto.getJobSubJob());
                profitDto.setDescription(dto.getDescription());
                profitDto.setEmpCount(dto.getEmpCount());
                profitDto.setCommittedValue(dto.getCommittedCost());

                if (null != projectsDashboardProfitDtos) {
                    profitDto.setCommittedCost(NumberUtil.currencyFormat(weeklyProcesses.stream()
                            .filter(w -> w.getFinalizedAt() != null)
                            .map(WeeklyProcess::getBaseAmount)
                            .reduce(BigDecimal.ZERO, BigDecimal::add)));
                    projectsDashboardProfitDtos.add(profitDto);
                }
            }

            // TM-412
            int rosterCount = providerAPI.countEmployeeByProjectId(projectId);
            dto.setRosterCount(rosterCount);

            projectsDashboardDtos.add(dto);
        }
    }
}
