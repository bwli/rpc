package com.timaven.timekeeping.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberUtil {
    public static String currencyFormat(BigDecimal n) {
        if (null == n) return "";
        return NumberFormat.getCurrencyInstance(Locale.US).format(n);
    }

    public static String format(BigDecimal n) {
        if (null == n) return "";
        return NumberFormat.getInstance(Locale.US).format(n);
    }

    public static String format(Long n) {
        if (null == n) return "";
        return NumberFormat.getInstance(Locale.US).format(n);
    }
}
