package com.timaven.timekeeping.validator;

import com.timaven.timekeeping.model.Equipment;
import com.timaven.timekeeping.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class EquipmentValidator implements Validator {
    private final EquipmentService equipmentService;

    @Autowired
    public EquipmentValidator(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Equipment.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Equipment equipment = (Equipment) o;

        if (equipment.getId() == null
                && equipmentService.getEquipmentByEquipmentId(equipment.getEquipmentId()) != null) {
            errors.rejectValue("equipmentId", "Duplicate.equipment.id", "Duplicate equipment id");
        }
    }
}
