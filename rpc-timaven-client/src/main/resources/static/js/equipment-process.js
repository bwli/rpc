let equipmentProcess = function () {

    function init() {
        $("#btn-ok").on("click", function () {
            let costCode= $("#costCodeModal").find("select[name='costCode']").first().val();
            let dateOfService = $('#datepicker-dos').val();
            let equipmentId = $('#select-equipment-id').val();

            if (costCode && dateOfService && equipmentId) {
                let splitDate = dateOfService.split(/\//);
                let isoDate = [splitDate[2],splitDate[0],splitDate[1]].join('-');
                if (days.includes(isoDate)) {
                    // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
                    submitModule.equipmentProcessUpdate(equipmentProcessDto, dateOfService, equipmentId, costCode, {
                        afterSuccess: processAfterProcessedTable
                    });
                } else {
                    alert("Selected date is invalid. It must be already existed in the list.");
                }
            } else {
                alert("All fields are required");
            }
        });

        // TM-275 Start
        $('#copy-cost-codes').on('click', copyCostCodes);
        $('#clear-cost-codes').on('click', clearCopiedCostCodes);
        equipmentProcessDto.equipmentCostCodeDateDtos.forEach(dto => {
            updateSelectsWithGroupUniqueValue(dto.equipmentId);
        });
        initTable();
        // TM-275 End
    }

    /* TM-275, function that should used when after process table (use init function
     * previously, which can cause global button(or other elements)
     * register multiple event function.
     */
    function initTable(){
        initTotalPerDay(); // TM-295
        initChargeDisplay();
        $('input[data-type="perc-total"]').on("change", chargeInputChanged);
        fireOnCostCodeSelectChange();
        $('button.remove-cost-code-in-equipment').on('click', removeCostCodeInEquipment);
    }

    // TM-295 Start
    function initTotalPerDay(){
        $('div[data-type=total-hour-day]').each(function(){
            calculateTotalPerDay($(this));
        });
    }

    function initTotalCharge(){
        const table = $('#equipment-table');
        const equipmentIds = equipmentProcessDto.equipmentCostCodeDateDtos
            .map(dto => dto.equipmentId);
        equipmentIds.forEach(equipmentId => {
            const totalValueInput = table.find('input[data-type=total-hour][data-equipment-id="'
                + equipmentId +'"]');
            const groupInputs = table.find('input[data-type=perc-total][data-equipment-id="' + equipmentId + '"]');
            let sum = 0;
            $(groupInputs).each(function (){
                if ($(this).val()) {
                    sum += Number($(this).val());
                    sum = parseFloat(sum.toFixed(2));
                }
            });
            totalValueInput.val(sum);
        });
    }

    function calculateTotalPerDay($this){
        let dateStr = $this.data("date");
        let table = $('#equipment-table');
        let totalPerDayDiv = table.find("div[data-type=total-hour-day][data-date=" + dateStr + "]");
        let groupPerDayInput = table.find("input[data-type=perc-total][data-date=" + dateStr + "]");
        let perDaySum = 0;
        groupPerDayInput.each(function(){
            if ($(this).val()) {
                perDaySum += Number($(this).val());
                perDaySum = parseFloat(perDaySum.toFixed(2));
            }
        });
        totalPerDayDiv.html(perDaySum);
    }
    // TM-295 End

    // TM-275 Start
    function initChargeDisplay(){
        let table = $('table#equipment-table');
        let costCodeSelects = table.find('select[data-type=cost-code]');
        costCodeSelects.each(function(){
            const _this = $(this);
            const equipmentId = _this.attr('data-equipment-id')
            const costCodeIndex = _this.attr('data-cost-code-index');
            let originCostCode = _this.attr('data-cost-code');
            originCostCode = originCostCode === 'unknown' ? '' : originCostCode;
            const value = _this.val();
            toggleDisplayAndChangeModel(equipmentId, costCodeIndex, originCostCode, value);
        });
    }

    function chargeInputChanged(){
        const _this = $(this);
        let equipmentId = _this.data("equipment-id");
        let type = _this.attr("data-type");
        let table = $('#equipment-table');
        let totalInput = table.find("input[data-type='total-hour'][data-equipment-id='" + equipmentId + "']");
        let groupInput = table.find("input[data-type='perc-total'][data-equipment-id='" + equipmentId + "']");
        let sum = 0;
        $(groupInput).each(function (){
            if ($(this).val()) {
                sum += Number($(this).val());
                sum = parseFloat(sum.toFixed(2));
            }
        });
        totalInput.val(sum);

        calculateTotalPerDay(_this); // TM-295
    }

    function fireOnCostCodeSelectChange(){
        $('select[data-type=cost-code]').on('change', function(){
            const _this = $(this);
            let equipmentId = _this.attr('data-equipment-id');
            let costCodeIndex = _this.attr('data-cost-code-index');
            let originCostCode = _this.attr('data-cost-code');
            originCostCode = originCostCode === 'unknown' ? '' : originCostCode;
            let value = _this.val();
            _this.attr('data-cost-code', value === '' ? 'unknown' : value);
            toggleDisplayAndChangeModel(equipmentId, costCodeIndex, originCostCode, value);
            updateSelectsWithGroupUniqueValue(equipmentId);
        });
    }

    /* Select elements that belong to same equipment are in one group.
     * Their value should be unique in one group.
     */
    function updateSelectsWithGroupUniqueValue(equipmentId) {
        let table = $('table#equipment-table');
        let allSelectWrapper = [];
        const selects = table.find('select[data-equipment-id="'+ equipmentId +'"]');
        selects.each(function(){
            let _this = $(this);
            let wrapper = {};
            wrapper.costCodeIndex = _this.attr('data-cost-code-index');
            wrapper.dom = this;
            allSelectWrapper.push(wrapper);
        });

        if (allSelectWrapper.length < 2) {
            selects.each(function(){
                $(this).find('option').show();
            });
            return;
        }

        for (let i = 0; i < allSelectWrapper.length; i++) {
            let others = [];
            for(let j = 0; j < allSelectWrapper.length; j++) {
                if (j !== i) {
                    others.push(allSelectWrapper[j])
                }
            }
            allSelectWrapper[i].others = others;
        }

        // update option elements: some should hide the others should show
        allSelectWrapper.forEach(wrapper => {
            let hideValues = [];
            wrapper.others.forEach(w => {
                let value = $(w.dom).val();
                if (value !== '' && value !== '-----') {
                    hideValues.push(value);
                }
            });
            $(wrapper.dom).find('option').each(function(){
                let _this = $(this);
                if (hideValues.includes(_this.val())) {
                    _this.hide();
                } else {
                    _this.show();
                }
            });
        });
    }

    /**
     * When cost code changed on select element,
     * td elements and nested charge inputs and hidden inputs that related to the select element
     * should change(e.g. display, change value and the global model should change too).
     * @param equipmentId
     * @param costCodeIndex
     * @param originCostCode cost code before change
     * @param costCode selected cost code
     */
    function toggleDisplayAndChangeModel(equipmentId, costCodeIndex, originCostCode, costCode){
        let table = $('table#equipment-table');
        let tds = table
            .find('td[data-type=equipment-charge][data-equipment-id="'
                + equipmentId +'"][data-cost-code-index="'+ costCodeIndex +'"]');
        tds.each(function(){
            let td = $(this);
            // change hidden inputs name attr
            td.find('input').each(function(){
                const _this = $(this);
                let currentName = _this.attr('name');
                currentName = currentName.replace(/(equipmentCostCodePercMap\[').*?('\])/, '$1'+ costCode +'$2');
                _this.attr('name', currentName);
            });

            td.attr('data-cost-code', costCode === '' ? 'unknown' : costCode);
            let chargeInput = td.find('input[data-type=perc-total]');
            if (costCode === '') {
                td.addClass('table-secondary');
                chargeInput.prop('readonly', true);
            } else {
                td.removeClass('table-secondary');
                chargeInput.prop('readonly', false);
            }

            let costCodeInput = td.find('input[data-type=cost-code]');
            costCodeInput.val(costCode);
        });

        // process js global model
        if (originCostCode !== costCode) {
            let dto = equipmentProcessDto.equipmentCostCodeDateDtos.find(
                dto => dto.equipmentId === equipmentId);
            let costCodes = dto.costCodes;
            costCodes[costCodeIndex] = costCode;

            let allocationTimeMap = dto.equipmentAllocationTimeMap;
            let findSelector = 'td[data-type=equipment-charge][data-equipment-id="'
                + equipmentId + '"][data-cost-code-index="' + costCodeIndex + '"]';
            table.find(findSelector).each(function () {
                const _this = $(this);
                _this.find('input[cost-code]').val(costCode);
                const dateStr = _this.attr('data-date');
                const costCodeMap = allocationTimeMap[dateStr].equipmentCostCodePercMap;
                costCodeMap[costCode] = costCodeMap[originCostCode] || { "costCodeFull": "" };
                costCodeMap[costCode].costCodeFull = costCode;
                delete costCodeMap[originCostCode];
            });
        }
    }

    function copyCostCodes(){
        let result = {};
        let table = $('table#equipment-table');
        let costCodeSelects = table.find('select[data-type=cost-code]');
        costCodeSelects.each(function(){
            let _this = $(this);
            let value = _this.val();
            if (value === '' || value === '-----') return;

            let equipmentId = _this.attr('data-equipment-id');
            let originCostCode = _this.attr('data-cost-code');
            if (result[equipmentId] === undefined) {
                result[equipmentId] = {};
            }
            let level2 = result[equipmentId];
            if (level2[value] === undefined) {
                level2[value] = [];
            }
            let level3Array = level2[value];
            let tdSelector = 'td[data-equipment-id="'
                + equipmentId +'"][data-cost-code="'
                + originCostCode +'"]';
            let tds = table.find(tdSelector);
            tds.each(function(){
                let _this = $(this);
                if (_this.attr('data-type') === 'equipment-charge') {
                    level3Array.push(0);
                } else {
                    level3Array.push(null);
                }
            });
        });

        if (Object.getOwnPropertyNames(result).length > 0) {
            $.ajax('copy-costcodes', {
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(result)
            }).done(function(data){
                if (data === 'success'){
                    iziToast.success({message: 'Copy cost codes successfully!'});
                }
            });
        }
    }

    function clearCopiedCostCodes(){
        $.ajax('clear-copied-costcodes', {
            method: 'GET'
        }).done(function(resp){
            if (resp === 'success') {
                iziToast.success({message: 'Clear copied cost codes successfully!'});
            }
        });
    }

    function processAfterProcessedTable(equipmentId){
        processModelAfterProcessedTable(equipmentId);
        updateSelectsWithGroupUniqueValue(equipmentId);
    }

    function processModelAfterProcessedTable(equipmentId){
        let table = $('table#equipment-table');
        const dto = equipmentProcessDto.equipmentCostCodeDateDtos
            .find(dto => dto.equipmentId = equipmentId);
        if (dto) {
            const costCodes = dto.costCodes;
            const allocationTimeMap = dto.equipmentAllocationTimeMap;

            Object.keys(allocationTimeMap).forEach(dateStr => {
                const time = allocationTimeMap[dateStr];
                const ccMap = time.equipmentCostCodePercMap;
                table.find('input[data-equipment-id="' +
                    equipmentId + '"][data-date="' +
                    dateStr + '"][data-type=perc-total]').each(function () {
                    const cc = $(this).parents('td').attr('data-cost-code');
                    if (ccMap[cc] === undefined) {
                        ccMap[cc] = {
                            costCodeFull: cc,
                            totalCharge: 0
                        };
                        if (!costCodes.includes(cc)) {
                            costCodes.push(cc);
                        }
                    }
                });
            });
        }
    }

    /* change cost code index
     * change rowspan
     * move rowspan td
     * move hidden input
     * change global js model
     */
    function removeCostCodeInEquipment(){
        const table = $('#equipment-table');
        const _this = $(this);
        const currentTd = _this.parents('td').eq(0);
        const select = currentTd.find('select');
        const costCode = select.val();
        const equipmentId = select.attr('data-equipment-id');
        const dto = equipmentProcessDto.equipmentCostCodeDateDtos
            .find(e => e.equipmentId === equipmentId);
        const dateStrings = Object.keys(dto.equipmentAllocationTimeMap);
        // Step 1: Remove current html element tr
        const tb = $('table#table-equipment');
        const trs = table.find('tr[name="'+ equipmentId +'"]');
        let tr = _this.parents('tr').eq(0);
        const equipmentIndex = tr.attr('data-equipment-index');
        if (trs.length === 1) {
            select.val('');
            select.attr('data-cost-code', 'unknown');
            select.attr('data-cost-code-index', 0);
            let nextTds = currentTd.nextAll('td');
            for (let i = 0; i < 5; i++) {
                nextTds.eq(i).replaceWith(createTd(equipmentIndex,
                    equipmentId, dateStrings[i], 0, ""));
            }
            nextTds = currentTd.nextAll('td');
            nextTds.each(function(){
                const _this = $(this);
                _this.addClass('table-secondary');
                const input = _this.find('input[data-type=perc-total]');
                input.prop('readonly', true);
                input.on('change', chargeInputChanged);
            });
            // process global js model
            dto.costCodes.push('');
            Object.values(dto.equipmentAllocationTimeMap).forEach(time => {
                time.equipmentCostCodePercMap = {
                    "": {
                        costCodeFull: "",
                        totalCharge: 0,
                        totalHour: 0
                    }
                };
            });
        } else if (trs.length > 1) {
            // Current tr is first tr in
            if (trs.get(0) === tr.get(0)) {
                const timeInputs = tr.find('input[data-type=allocation-time]');
                const secondTr = trs.eq(1);
                const rowspanTds = tr.find('td[data-type=rowspan]');
                addTimeInput(secondTr, timeInputs, dateStrings);
                replaceRowspanElements(secondTr.find('td[data-type=hidden]'), rowspanTds);
                tr.remove();
                let rowspan = rowspanTds.attr('rowspan');
                rowspanTds.attr('rowspan', rowspan - 1);
            } else {
                const rowspanTds = trs.eq(0).find('td[data-type=rowspan]');
                let rowspan = rowspanTds.attr('rowspan');
                rowspanTds.attr('rowspan', rowspan - 1);
                tr.remove();
            }
            // change select data-cost-code-index
            const selects = table.find('select[data-type=cost-code][data-equipment-id="'
                + equipmentId +'"]');
            selects.each(function(index){
                const _this = $(this);
                _this.attr('data-cost-code-index', index);
                const tds = _this.parents('td').eq(0).nextAll('td').slice(0, 5);
                tds.attr('data-cost-code-index', index);
            });
        }

        // Step 2: Change js global model
        const costCodes = dto.costCodes;
        costCodes.splice(costCodes.indexOf(costCode), 1);
        Object.values(dto.equipmentAllocationTimeMap)
            .map(t => t.equipmentCostCodePercMap)
            .forEach(e => {
            delete e[costCode]
        });

        initTotalPerDay();
        initTotalCharge();
        updateSelectsWithGroupUniqueValue(equipmentId);

        /**
         * Add inputs into tr at specified location.
         * @param tr jquery object that identify which tr should append inputs
         * @param inputs jquery objects those are input that should append to tr
         * @param dateStringArray array that contain date string that can
         * be used to determine inserted location
         */
        function addTimeInput(tr, inputs, dateStringArray){
            dateStringArray.forEach(dateStr => {
                let group = inputs.filter('input[data-date="'+ dateStr +'"]');
                let td = tr.find('td[data-date="'+ dateStr +'"]');
                td.before(group);
            });
        }
        function replaceRowspanElements(sources, targets){
            for (let i = 0; i < sources.length; i++) {
                sources.eq(i).replaceWith(targets.eq(i));
            }
        }
        function createTd(equipmentIndex, equipmentId,  dateStr, costCodeIndex, costCode){
            let tdHtml = '<td data-type="equipment-charge" ';
            tdHtml += 'data-equipment-id="'+ equipmentId +'" ';
            tdHtml += 'data-cost-code-index="'+ costCodeIndex +'" ';
            tdHtml += 'data-cost-code="'+ costCode +'"';
            tdHtml += 'data-date="'+ dateStr +'" ';
            tdHtml += '>\n';

            tdHtml += createInputHtml(equipmentIndex, dateStr, costCode, 'id', {
                "type": "hidden",
                "data-type": "id",
            }) + '\n';
            tdHtml += createInputHtml(equipmentIndex, dateStr, costCode, 'costCodeFull', {
                "type": "hidden",
                "data-type": "cost-code"
            }) + '\n';
            tdHtml += createInputHtml(equipmentIndex, dateStr, costCode, 'totalHour', {
                "type": "hidden",
                "data-type": "total-hour"
            }) + '\n';
            tdHtml += '<div class="input-group flex-nowrap">\n';
            tdHtml += '<span class="input-group-text">$</span>\n';
            tdHtml += createInputHtml(equipmentIndex, dateStr, costCode, 'totalCharge', {
                "class": "form-control",
                "type": "number",
                "step": "any",
                "data-type": "perc-total",
                "data-equipment-id": equipmentId,
                "data-date": dateStr,
                "value": 0
            }) + '\n';
            tdHtml += '</div>';

            tdHtml += '</td>';
            return tdHtml;

            function createInputHtml(equipmentIndex, dateStr, costCode, id, options){
                let nameStr = 'equipmentCostCodeDateDtos['+ equipmentIndex +']';
                nameStr += ".equipmentAllocationTimeMap['"+ dateStr +"']";
                nameStr += "?.equipmentCostCodePercMap['"+ costCode +"']";
                nameStr += '?.' + id;

                let inputHtml = '<input ';
                options = options || {};
                Object.keys(options).forEach(key => {
                    inputHtml += key + '="' + options[key] + '" ';
                });
                inputHtml += 'name="'+ nameStr +'" ';
                inputHtml += '>';
                return inputHtml;
            }
        }
    }
    // TM-275 End

    return {
        init: init,
        initTable: initTable // TM-275, total_hour is not function now in table time.equipment_cost_code_perc
    }
}();
