let logout = function () {
    function logout() {
        if (typeof event !== 'undefined') {
            event.preventDefault();
        }
        $.ajax({
            type: 'POST',
            url: `${logoutUrl}`,
            success: function () {
            },
            error: function (data) {
                console.error('Logged out Auth error');
                alert(data.statusText);
            },
            complete: function (data) {
                $.ajax({
                    type: 'POST',
                    url: 'logout',
                    success: function (data) {
                        // The response is actually the login html
                        // The redirect page will not be refresh
                        console.log('Logged out');
                        window.location.href = "home";
                        Cookies.remove("access_token", {path: 'timekeeping'});
                    },
                    error: function (data) {
                        console.error('Logged out Client error');
                        alert(data.statusText);
                    },
                    async: false
                });
            },
            async: false
        });
    }

    return {
        logout: logout
    }
}();
