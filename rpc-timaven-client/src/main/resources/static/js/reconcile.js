let reconcile = function () {

    function init() {

        $('#form-reconcile').validate({
            //Otherwise, validator will use title as error message
            ignoreTitle: true,
            onfocusout: false,
            invalidHandler: function (form, validator) {
                let errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            },
            errorPlacement: function (error, element ) {
                let div = $(element).closest('div');
                error.insertAfter(div);
            }
        });
    }

    $.validator.classRuleSettings.requireMatch = {
        requireMatch: true
    };

    $.validator.addMethod("requireMatch", function (value, element, params) {
        let val = $(element).val();
        let empId = $(element).data('emp-id');
        val = val === "" ? 0 : parseFloat(val);
        let otSpan = $('#reconcile-table').find('span[data-type="original-ot"][data-emp-id="' + empId +'"]');
        let assignOt = 0;
        otSpan.each(function () {
            assignOt += parseFloat($(this).text());
        });
        return assignOt === val;
    }, 'Assigned overtime does not match the total overtime.');

    function validateForm() {
        let form = $('#form-reconcile');
        form.valid();
    }

    return {
        init: init,
        validateForm: validateForm,
    }
}();
