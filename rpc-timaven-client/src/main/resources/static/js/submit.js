let submitModule = function () {
    let clicked = false;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;

    function changeDefShift(projectId, shiftId) {

        let projectShift = {
            projectId: projectId,
            shiftId: shiftId
        };

        $(function () {
            // noinspection JSUnresolvedFunction
            $.ajax({
                type: "PUT",
                url: "default-shift",
                data: JSON.stringify(projectShift),
                contentType: "application/json;charset=utf-8",
                dataType: "text",
                traditional: true,
                success: function (callback) {
                },
                error: function () {
                }
            });
        });
    }

    function saveTimeAllocation(allocationSubmission, url, successPage) {
        if (clicked) {
            return;
        }

        clicked = true;
        setTimeout(function () {
            clicked = false;
        }, 2000);

        $(function () {
            // noinspection JSUnresolvedFunction
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(allocationSubmission),
                contentType: "application/json;charset=utf-8",
                dataType: "text",
                traditional: true,
                success: function (callback) {
                    if (callback.lastIndexOf("success", 0) === 0) {
                        window.location = successPage;
                    } else {
                        $("html").html(callback);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (UNAUTHORIZED === xhr.status || FORBIDDEN === xhr.status) {
                        $("html").html(xhr.responseText);
                    }
                }
            });
        });
    }

    function submitReport(formData) {

        if (!clicked) {
            clicked = true;
            setTimeout(function () {
                clicked = false;
            }, 2000);

            $(function () {
                // noinspection JSUnresolvedFunction
                $.ajax({
                    processData: false,
                    contentType: false,
                    type: "POST",
                    url: 'report',
                    dataType: "text",
                    data: formData,
                    success: function (callback) {
                        if (callback.lastIndexOf("success", 0) === 0) {
                            window.location = "home";
                        } else {
                            const divFailed = $('#div-error');
                            divFailed.html(callback);
                            divFailed.fadeTo(2000, 500).slideUp(500, function () {
                                divFailed.slideUp(500);
                            });
                        }
                    },
                    error: function () {
                        const divFailed = $('#div-error');
                        divFailed.html('Submit failed');
                        divFailed.fadeTo(2000, 500).slideUp(500, function () {
                            divFailed.slideUp(500);
                        });
                    }
                });
            });
        }
    }

    function saveCodeAssociation(dto) {
        $.ajax({
            type: "POST",
            url: 'cost-code-association',
            data: JSON.stringify(dto),
            contentType: "application/json;charset=utf-8",
            dataType: "text",
            traditional: true,
            success: function (callback) {
                if (callback.lastIndexOf("success", 0) === 0) {
                    window.history.go(-1);
                } else {
                    $("html").html(callback);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                if (UNAUTHORIZED === xhr.status || FORBIDDEN === xhr.status) {
                    $("html").html(xhr.responseText);
                }
            }
        });

    }

    function saveCostCodeStructure(categories) {
        $.ajax({
            type: "POST",
            url: 'cost-code-structure',
            data: JSON.stringify(categories),
            contentType: "application/json;charset=utf-8",
            dataType: "text",
            traditional: true,
            success: function (callback) {
                if (callback.lastIndexOf("success", 0) === 0) {
                    window.history.go(-1);
                } else {
                    $("html").html(callback);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                if (UNAUTHORIZED === xhr.status || FORBIDDEN === xhr.status) {
                    $("html").html(xhr.responseText);
                }
            }
        });
    }

    // TM-275, add additional param 'options'
    function equipmentProcessUpdate(equipmentProcessDto, dateOfService, equipmentId, costCode, options) {
        options = options || {};
        $(function () {
            let params = ``;
            // noinspection JSUnresolvedFunction
            $.ajax({
                type: "post",
                url: `equipment-process-table?dateOfService=${dateOfService}&equipmentId=${equipmentId}&costCode=${costCode}`,
                data: JSON.stringify(equipmentProcessDto),
                contentType: "application/json;charset=utf-8",
                traditional: true,
                success: function (data) {
                    /*<![CDATA[*/
                    $('#div-table').html(data);
                    equipmentProcess.initTable(); // TM-275, using initTable instead of init
                    options.afterSuccess && options.afterSuccess.call(null, equipmentId); // TM-275, call callback function
                    /*]]>*/
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (UNAUTHORIZED === xhr.status || FORBIDDEN === xhr.status) {
                        $("html").html(xhr.responseText);
                    }
                }
            });
        });
    }

    return {
        changeDefShift: changeDefShift,
        saveTimeAllocation: saveTimeAllocation,
        submitReport: submitReport,
        saveCodeAssociation: saveCodeAssociation,
        saveCostCodeStructure: saveCostCodeStructure,
        equipmentProcessUpdate: equipmentProcessUpdate,
    }
}();
