function timavenDocument(method, version, url, isAsync, noContentMessage, params, body = null, contentType = 'application/x-www-form-urlencoded') {
    // noinspection DuplicatedCode
    token.getAccessToken(function (token) {
        let request = new XMLHttpRequest();
        let finalParams = params ? `?${params}` : '';
        request.open(method, `${parserUrl}/${version}/${url}${finalParams}`, isAsync);
        request.withCredentials = true;
        request.setRequestHeader('Content-type', contentType);
        request.setRequestHeader('Authorization', 'Bearer ' + token);
        request.responseType = 'blob';
        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (200 === this.status) {
                    let blob = this.response;
                    let filename = "";
                    let disposition = request.getResponseHeader('Content-Disposition');
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        let matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    }
                    if (window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        let downloadLink = window.document.createElement('a');
                        let contentTypeHeader = request.getResponseHeader("Content-Type");
                        downloadLink.href = window.URL.createObjectURL(new Blob([blob], {type: contentTypeHeader}));
                        downloadLink.download = filename;
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);
                    }
                } else if (204 === this.status) {
                    iziToast.show({
                        title: 'Error',
                        message: noContentMessage,
                        position: 'topRight',
                        timeout: 5000,
                        color: 'red'
                    });
                } else if (401 === this.status) {
                    window.location.href = 'silence-logout';
                } else if (400 === this.status) {
                    iziToast.show({
                        title: 'Error',
                        message: this.response.message,
                        position: 'topRight',
                        timeout: 5000,
                        color: 'red'
                    });
                }
            } else if (request.readyState === 2) {
                if (request.status === 200) {
                    request.responseType = 'blob';
                } else if (request.status === 400) {
                    request.responseType = 'json';
                } else {
                    request.responseType = 'text';
                }
            }
        }
        request.send(body);
    });
}