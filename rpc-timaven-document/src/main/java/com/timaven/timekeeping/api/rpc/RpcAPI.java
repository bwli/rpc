package com.timaven.timekeeping.api.rpc;

import com.timaven.timekeeping.api.ApiBinding;
import com.timaven.timekeeping.model.dto.RpcProfitDto;
import com.timaven.timekeeping.model.dto.RpcProfitFilter;
import lombok.Getter;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Getter
public class RpcAPI extends ApiBinding {

    private final WebClient webClient;

    private final String version = "/v1";

    public RpcAPI(String rpcApiUrl) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String accessToken = "";
        if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
            accessToken = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
        }
        String finalAccessToken = accessToken;
        this.webClient = WebClient.builder()
                .baseUrl(rpcApiUrl)
                .filter(((clientRequest, exchangeFunction) -> {
                    ClientRequest clientRequest1 = ClientRequest.from(clientRequest)
                            .header("Authorization", "Bearer " + finalAccessToken)
                            .build();
                    return exchangeFunction.exchange(clientRequest1);
                }))
                .exchangeStrategies(
                        ExchangeStrategies.builder()
                                .codecs(clientCodecConfigurer ->
                                        clientCodecConfigurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024))
                                .build())
                .build();
    }

    public RpcAPI(WebClient webClient) {
        this.webClient = webClient;
    }

    public Mono<List<RpcProfitDto>> getProfitDetails(RpcProfitFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/all-profit-details").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }
}
