package com.timaven.timekeeping.configuration.web;

import com.timaven.timekeeping.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Component
@Profile("!local")
public class HeaderTenantInterceptor implements WebRequestInterceptor {

    private final DocumentService documentService;

    @Autowired
    public HeaderTenantInterceptor(DocumentService documentService) {
        this.documentService = documentService;
    }

    @Override
    public void preHandle(WebRequest request) throws Exception {
        try {
            if (request instanceof ServletWebRequest) {
                ServletWebRequest servletWebRequest = (ServletWebRequest) request;
                if (servletWebRequest.getRequest() instanceof MultipartHttpServletRequest) {
                    MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) servletWebRequest.getRequest();
                    String uri = multipartHttpServletRequest.getRequestURI();
                    if (uri.endsWith("/roster")
                            || uri.endsWith("/billing-codes")
                            || uri.endsWith("/cost-codes")
                            || uri.endsWith("/tags")
                            || uri.endsWith("/equipment")
                            || uri.endsWith("/equipment-price")
                            || uri.endsWith("/equipment-log")
                            || uri.endsWith("/purchase-orders")
                            || uri.endsWith("/time-records")
                            || uri.endsWith("/report-rpc-profit-details")) {
                        documentService.dumpDocuments(multipartHttpServletRequest);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws Exception {
        ThreadTenantStorage.clear();
    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws Exception {

    }
}
