package com.timaven.timekeeping.configuration.web;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final WebRequestInterceptor webRequestInterceptor;

    public WebConfiguration(WebRequestInterceptor webRequestInterceptor) {
        this.webRequestInterceptor = webRequestInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addWebRequestInterceptor(webRequestInterceptor);
    }
}
