package com.timaven.timekeeping.controller;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.timaven.timekeeping.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@SessionAttributes({"sessionProjectId", "allowedDayOfWeek", "rule", "sessionTeamName", "sessionTimesheetsMode", "sessionTimesheetsDate", "idProjectMap"})
@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("isAuthenticated()")
public class DocumentRestController {

    @Autowired
    private DocumentService documentService;

    @GetMapping("/s3-object-summary")
    public List<S3ObjectSummary> templateList(@RequestParam boolean isTemplate) {
        return documentService.getAwsS3ListInfo(isTemplate);
    }

    @PreAuthorize("hasRole('ROLE_admin')")
    @PostMapping("/company-documents")
    public ResponseEntity<Object> uploadCompanyDocument(MultipartHttpServletRequest request){
        Map<String, MultipartFile> fileMap = request.getFileMap();
        MultipartFile multipartFile = fileMap.entrySet().iterator().next().getValue();
        try {
            return documentService.uploadCorporateFileByTenantId(multipartFile);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @PreAuthorize("hasRole('ROLE_admin')")
    @DeleteMapping(value = "/company-documents")
    public ResponseEntity<Object> deleteCompanyDocument(@RequestParam final String key){
        return documentService.removeCompanyDocumentByKey(key);
    }

    @GetMapping(value = "/download-template")
    public ResponseEntity<InputStreamResource> downloadTemplate(@RequestParam(value= "fileName") final String keyName,
                                                                @RequestParam boolean isTemplate){
        return documentService.downloadFile(keyName, isTemplate);
    }
}
