package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.service.ExportService;
import com.timaven.timekeeping.service.ReportCorporateWeeklyProcessService;
import com.timaven.timekeeping.service.ReportLaborTimeSummaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class ExcelController extends BaseController {

    private final ReportCorporateWeeklyProcessService reportCorporateWeeklyProcess;
    private final ReportLaborTimeSummaryService reportLaborTimeSummaryService;
    private final ExportService exportService;

    @Autowired
    public ExcelController(ReportCorporateWeeklyProcessService reportCorporateWeeklyProcess, ExportService exportService,
                           ReportLaborTimeSummaryService reportLaborTimeSummaryService) {
        this.reportCorporateWeeklyProcess = reportCorporateWeeklyProcess;
        this.exportService = exportService;
        this.reportLaborTimeSummaryService = reportLaborTimeSummaryService;
    }

    @GetMapping(value = "/report-corporate-weekly-process")
    public ResponseEntity<InputStreamResource> reportCorporateWeeklyProcess(@RequestParam String teamName, @RequestParam LocalDate weeklyEndDate) {
        // TM-373 report corporate weekly process to excel
        return reportCorporateWeeklyProcess.reportCorporateWeeklyProcess(weeklyEndDate, teamName);
    }

    @PostMapping(value = "/payroll-report")
    public ResponseEntity<InputStreamResource> exportPayrollReport(@RequestBody WeeklyPayrollDto weeklyPayrollDto) {
        return exportService.exportPayrollReport(weeklyPayrollDto);
    }

    @PostMapping(value = "/report-labor")
    public ResponseEntity<InputStreamResource> reportLabor(@RequestParam Long projectId,
                                                           @RequestParam String teamName,
                                                           @RequestParam LocalDate dateOfService,
                                                           @RequestParam LocalDate payrollDate) {
        return exportService.reportLabor(projectId, teamName, dateOfService, payrollDate);
    }

    @PostMapping(value = "/report-data-export")
    public ResponseEntity<InputStreamResource> reportDataExport(@RequestParam Long projectId,
                                                                @RequestParam LocalDate startDate,
                                                                @RequestParam LocalDate endDate,
                                                                @RequestParam(required = false) Boolean approvedOnly) {
        approvedOnly = approvedOnly != null && approvedOnly;
        return exportService.reportDataExport(projectId, startDate, endDate, approvedOnly);
    }

    @PostMapping(value = "/report-labor-time-summary")
    public ResponseEntity<InputStreamResource> reportLaborTimeSummary(@RequestParam Set<Long> projectIds,
                                                                      @RequestParam LocalDate weekEndDate,
                                                                      @RequestParam(required = false) Boolean approvedOnly) {
        return reportLaborTimeSummaryService.reportLaborTimeSummary(projectIds, weekEndDate, approvedOnly);
    }

    // TM-421
    @PostMapping(value = "/labor-rates-export")
    public ResponseEntity<InputStreamResource> laborRatesExport(@RequestParam(required = false) Long projectId) {
        return exportService.exportExcelCraftView(projectId);
    }

    @PostMapping(value = "/purchase-order-billings")
    public ResponseEntity<InputStreamResource> reportLabor(@RequestParam Long id) {
        return exportService.reportPurchaseOrderBillings(id);
    }

    @PostMapping(value = "/report-equipment", params = {"dateOfService"})
    public ResponseEntity<InputStreamResource> reportEquipemtByEquipmentOwnershipType(@RequestParam(required = false) Long projectId, @RequestParam LocalDate dateOfService,
                                @RequestParam LocalDate payrollDate, @RequestParam Integer type) {
        return exportService.reportEquipemtByEquipmentOwnershipType(projectId, dateOfService, payrollDate, type);
    }

    @PostMapping(value = "/report-equipment-billing")
    public ResponseEntity<InputStreamResource> reportEquipmentBilling(@RequestParam(required = false) Long projectId, @RequestParam LocalDate startDate,
                                       @RequestParam LocalDate endDate) {
        return exportService.reportEquipmentBilling(projectId, startDate, endDate);
    }

    @PostMapping(value = "/report-equipment-log")
    public ResponseEntity<InputStreamResource> reportEquipmentLog(@RequestParam(required = false) Long projectId) {
        return exportService.reportEquipmentLog(projectId);
    }

    @PostMapping(value = "/report-cost-code")
    public ResponseEntity<InputStreamResource> reportCostCode(@RequestParam(required = false) Long projectId) {
        return exportService.reportCostCode(projectId);
    }

    @PostMapping(value = "/report-employee")
    public ResponseEntity<InputStreamResource> reportEmployee(@RequestParam(required = false) Long projectId,
                                                              @RequestParam(required = false) Boolean showAll) {
        return exportService.reportEmployee(projectId, showAll);
    }

    @PostMapping(value = "/report-equipment", params = {"isAll"})
    public ResponseEntity<InputStreamResource> reportEquipemtByEquipmentOwnershipType(@RequestParam(required = false) Boolean isAll) {
        return exportService.reportEquipment(isAll);
    }

    @PostMapping(value = "/report-absentee")
    public ResponseEntity<InputStreamResource> reportAbsentee(@RequestParam LocalDate dateOfService,
                                                              @RequestBody Set<Long> employeeIds) {
        return exportService.reportAbsentee(dateOfService, employeeIds);
    }

    @PostMapping(value = "/report-whoisin")
    public ResponseEntity<InputStreamResource> reportWhoisin(@RequestParam(required = false) Long projectId,
                                                             @RequestParam LocalDate dateOfService,
                                                             @RequestBody Set<Long> employeeIds) {
        return exportService.reportWhoisin(projectId, dateOfService, employeeIds);
    }

    @PostMapping(value = "/report-rpc-profit-details")
    public ResponseEntity<InputStreamResource> reportRpcProfitDetails(@RequestParam(required = false) Long projectId,
                                                             @RequestParam LocalDate startDate,
                                                             @RequestParam LocalDate endDate) {
        return exportService.reportRpcProfitDetails(projectId, startDate, endDate);
    }

}
