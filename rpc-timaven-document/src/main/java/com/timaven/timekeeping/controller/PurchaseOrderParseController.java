package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.CommonResult;
import com.timaven.timekeeping.model.PurchaseOrder;
import com.timaven.timekeeping.model.PurchaseOrderSubmission;
import com.timaven.timekeeping.model.PurchaseVendorRoster;
import com.timaven.timekeeping.model.dto.ReceivedPurchaseOrderDto;
import com.timaven.timekeeping.service.PurchaseOrderService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.math.BigDecimal;
import java.util.*;

import static com.timaven.timekeeping.util.ExcelUtil.*;

@SuppressWarnings("Duplicates")
@RestController
@PreAuthorize("hasAnyRole('ROLE_corporate_accountant')")
public class PurchaseOrderParseController extends BaseController {

    private final static String KEY_FILE_TYPE = "fileType";
    private final static String TYPE_PURCHASE_ORDER = "purchase_order";
    private final static String TYPE_RECEIVED_PURCHASE = "received_purchase";
    private final static String TYPE_PURCHASE_VENDOR_ROSTER = "purchase_vendor_roster";

    private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public PurchaseOrderParseController(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @PostMapping(value = "/purchase-orders")
    public CommonResult uploadPurchaseOrders(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        CommonResult commonResult = new CommonResult();

        String fileType = request.getParameterMap().get(KEY_FILE_TYPE)[0];

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                Sheet sheet = workbook.getSheetAt(0);

                switch (fileType) {
                    case TYPE_PURCHASE_ORDER:
                        parsePurchaseOrders(sheet);
                        commonResult.addSuccess("Purchase Orders parse is complete");
                        break;
                    case TYPE_RECEIVED_PURCHASE:
                        PurchaseOrderSubmission submission = purchaseOrderService.getLatestPurchaseOrderSubmission();
                        if (null == submission) {
                            commonResult.setResult("failed");
                            commonResult.addError("Please upload purchase order template before uploading received report template");
                            return commonResult;
                        }
                        parseReceivedPurchase(sheet);
                        commonResult.addSuccess("Received Purchase parse is complete");
                        break;
                    case TYPE_PURCHASE_VENDOR_ROSTER:
                        parsePurchaseVendorRoster(sheet);
                        commonResult.addSuccess("Purchase Vendor Roster parse is complete");
                        break;
                    default:
                        break;
                }

                commonResult.setResult("success");
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    private void parsePurchaseOrders(Sheet sheet) {
        List<PurchaseOrder> purchaseOrders = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int trueRowNum = -1;
        int rowNum = 0;
        boolean foundTitle = false;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            trueRowNum++;
            if (!foundTitle && (isRowEmpty(row)
                    || !"Purchase Order Number".equalsIgnoreCase(getStringValue(row.getCell(0)).trim()))) continue;
            foundTitle = true;
            Row oneRowAhead = sheet.getRow(trueRowNum + 1);
            if (null == oneRowAhead || isRowEmpty(oneRowAhead)) {
                break;
            }
            PurchaseOrder purchaseOrder = null;
            if (rowNum != 0) {
                purchaseOrder = new PurchaseOrder();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "purchase order number":
                            purchaseOrder.setPurchaseOrderNumber(getStringValue(cell));
                            break;
                        case "purchase order date":
                            purchaseOrder.setPurchaseOrderDate(getLocalDate(cell));
                            break;
                        case "vendor number":
                            purchaseOrder.setVendorNumber(getIntValue(cell));
                            break;
                        case "vendor name":
                            purchaseOrder.setVendorName(getStringValue(cell));
                            break;
                        case "confirm to":
                            purchaseOrder.setConfirmTo(getStringValue(cell));
                            break;
                        case "requestor":
                            purchaseOrder.setRequestor(getStringValue(cell));
                            break;
                        case "buyer":
                            purchaseOrder.setBuyer(getStringValue(cell));
                            break;
                        case "requisition number":
                            purchaseOrder.setRequisitionNumber(getStringValue(cell));
                            break;
                        case "job number":
                            purchaseOrder.setJobNumber(getStringValue(cell));
                            break;
                        case "sub job":
                            purchaseOrder.setSubJob(getStringValue(cell));
                            break;
                        case "address 1":
                            purchaseOrder.setAddress1(getStringValue(cell));
                            break;
                        case "delivery point":
                            purchaseOrder.setDeliveryPoint(getStringValue(cell));
                            break;
                        case "terms":
                            purchaseOrder.setTerms(getStringValue(cell));
                            break;
                        case "reference":
                            purchaseOrder.setReference(getStringValue(cell));
                            break;
                        case "state tax":
                            Double stateTax = getDoubleValue(cell);
                            if (null != stateTax) {
                                purchaseOrder.setStateTax(BigDecimal.valueOf(stateTax));
                            }
                            break;
                        case "local tax":
                            Double localTax = getDoubleValue(cell);
                            if (null != localTax) {
                                purchaseOrder.setLocalTax(BigDecimal.valueOf(localTax));
                            }
                            break;
                        case "purchase order location":
                            purchaseOrder.setPurchaseOrderLocation(getStringValue(cell));
                            break;
                        case "p/o type":
                            purchaseOrder.setPoType(getStringValue(cell));
                            break;
                        case "line number":
                            purchaseOrder.setLineNumber(getIntValue(cell));
                            break;
                        case "part number":
                            purchaseOrder.setPartNumber(getStringValue(cell));
                            break;
                        case "part description":
                            purchaseOrder.setPartDescription(getStringValue(cell));
                            break;
                        case "delivery date":
                            purchaseOrder.setDeliveryDate(getLocalDate(cell));
                            break;
                        case "quantity":
                            Double quantity = getDoubleValue(cell);
                            if (null != quantity) {
                                purchaseOrder.setQuantity(BigDecimal.valueOf(quantity));
                            }
                            break;
                        case "adjusted quantity ordered":
                            Double adjustedQuantityOrdered = getDoubleValue(cell);
                            if (null != adjustedQuantityOrdered) {
                                purchaseOrder.setAdjustQuantityOrdered(BigDecimal.valueOf(adjustedQuantityOrdered));
                            }
                            break;
                        case "quantity received":
                            Double quantityReceived = getDoubleValue(cell);
                            if (null != quantityReceived) {
                                purchaseOrder.setQuantityReceived(BigDecimal.valueOf(quantityReceived));
                            }
                            break;
                        case "unit cost":
                            Double unitCost = getDoubleValue(cell);
                            if (null != unitCost) {
                                purchaseOrder.setUnitCost(BigDecimal.valueOf(unitCost));
                            }
                            break;
                        case "extended cost":
                            Double extendedCost = getDoubleValue(cell);
                            if (null != extendedCost) {
                                purchaseOrder.setExtendedCost(BigDecimal.valueOf(extendedCost));
                            }
                            break;
                        case "price code":
                            purchaseOrder.setPriceCode(getStringValue(cell));
                            break;
                        case "original po dollar amount":
                            Double originalDollarAmount = getDoubleValue(cell);
                            if (null != originalDollarAmount) {
                                purchaseOrder.setOriginalPoDollarAmount(BigDecimal.valueOf(originalDollarAmount));
                            }
                            break;
                        case "current po dollar amount":
                            Double currentDollarAmount = getDoubleValue(cell);
                            if (null != currentDollarAmount) {
                                purchaseOrder.setCurrentPoDollarAmount(BigDecimal.valueOf(currentDollarAmount));
                            }
                            break;
                        case "open amount":
                            Double openAmount = getDoubleValue(cell);
                            if (null != openAmount) {
                                purchaseOrder.setOpenAmount(BigDecimal.valueOf(openAmount));
                            }
                            break;
                        case "formatted cost distribution":
                            purchaseOrder.setFormattedCostDistribution(getStringValue(cell));
                            break;
                        case "cost type":
                            purchaseOrder.setCostType(getStringValue(cell));
                            break;
                        case "gl account number formatted":
                            purchaseOrder.setGlAccountNumberFormatted(getStringValue(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (!StringUtils.isEmpty(purchaseOrder.getPurchaseOrderNumber())) {
                    purchaseOrders.add(purchaseOrder);
                }
            }
            rowNum++;
        }
        purchaseOrderService.savePurchaseOrders(purchaseOrders, getUsername());
    }

    private void parseReceivedPurchase(Sheet sheet) {
        List<ReceivedPurchaseOrderDto> receivedPurchaseOrderDtos = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        boolean foundTitle = false;
        int trueRowNum = -1;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            trueRowNum++;
            if (!foundTitle && (isRowEmpty(row)
                    || !"Control Company".equalsIgnoreCase(getStringValue(row.getCell(0)).trim()))) continue;
            foundTitle = true;
            Row oneRowAhead = sheet.getRow(trueRowNum + 1);
            if (null == oneRowAhead || isRowEmpty(oneRowAhead)) {
                break;
            }
            ReceivedPurchaseOrderDto receivedPurchaseOrderDto = null;
            if (rowNum != 0) {
                receivedPurchaseOrderDto = new ReceivedPurchaseOrderDto();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "control company":
                            receivedPurchaseOrderDto.setControlCompany(getStringValue(cell));
                            break;
                        case "vendor number":
                            receivedPurchaseOrderDto.setVendorNumber(getIntValue(cell));
                            break;
                        case "vendor name":
                            receivedPurchaseOrderDto.setVendorName(getStringValue(cell));
                            break;
                        case "purchase order number":
                            receivedPurchaseOrderDto.setPurchaseOrderNumber(getStringValue(cell));
                            break;
                        case "po item":
                            receivedPurchaseOrderDto.setPoItem(getIntValue(cell));
                            break;
                        case "sequence no":
                            receivedPurchaseOrderDto.setSequenceNo(getIntValue(cell));
                            break;
                        case "part number":
                            receivedPurchaseOrderDto.setPartNumber(getStringValue(cell));
                            break;
                        case "description":
                            receivedPurchaseOrderDto.setDescription(getStringValue(cell));
                            break;
                        case "unit of measure":
                            receivedPurchaseOrderDto.setUnitOfMeasure(getStringValue(cell));
                            break;
                        case "quantity ordered":
                            Double quantityOrder = getDoubleValue(cell);
                            if (null != quantityOrder) {
                                receivedPurchaseOrderDto.setQuantityOrdered(BigDecimal.valueOf(quantityOrder));
                            }
                            break;
                        case "unit cost":
                            Double unitCost = getDoubleValue(cell);
                            if (null != unitCost) {
                                receivedPurchaseOrderDto.setUnitCost(BigDecimal.valueOf(unitCost));
                            }
                            break;
                        case "quantity received":
                            Double quantityReceived = getDoubleValue(cell);
                            if (null != quantityReceived) {
                                receivedPurchaseOrderDto.setQuantityReceived(BigDecimal.valueOf(quantityReceived));
                            }
                            break;
                        case "quantity rejected":
                            Double quantityRejected = getDoubleValue(cell);
                            if (null != quantityRejected) {
                                receivedPurchaseOrderDto.setQuantityRejected(BigDecimal.valueOf(quantityRejected));
                            }
                            break;
                        case "amount received":
                            Double amountReceived = getDoubleValue(cell);
                            if (null != amountReceived) {
                                receivedPurchaseOrderDto.setAmountReceived(BigDecimal.valueOf(amountReceived));
                            }
                            break;
                        case "amount accepted":
                            Double amountAccepted = getDoubleValue(cell);
                            if (null != amountAccepted) {
                                receivedPurchaseOrderDto.setAmountAccepted(BigDecimal.valueOf(amountAccepted));
                            }
                            break;
                        case "job number":
                            receivedPurchaseOrderDto.setJobNumber(getStringValue(cell));
                            break;
                        case "sub job number":
                            receivedPurchaseOrderDto.setSubJobNumber(getStringValue(cell));
                            break;
                        case "formatted cost distribution":
                            receivedPurchaseOrderDto.setFormattedCostDistribution(getStringValue(cell));
                            break;
                        case "cost type":
                            receivedPurchaseOrderDto.setCostType(getStringValue(cell));
                            break;
                        case "formatted g/l account":
                            receivedPurchaseOrderDto.setFormattedGlAccount(getStringValue(cell));
                            break;
                        case "date received":
                            receivedPurchaseOrderDto.setDateReceived(getLocalDate(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (!StringUtils.isEmpty(receivedPurchaseOrderDto.getPurchaseOrderNumber())) {
                    receivedPurchaseOrderDtos.add(receivedPurchaseOrderDto);
                }
            }
            rowNum++;
        }
        purchaseOrderService.saveReceivedPurchases(receivedPurchaseOrderDtos, getUsername());
    }

    public void parsePurchaseVendorRoster(Sheet sheet){
        List<PurchaseVendorRoster> purchaseVendorRosterList = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        boolean foundTitle = false;
        int trueRowNum = -1;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            trueRowNum++;
            if (!foundTitle && isRowEmpty(row)) continue;
            foundTitle = true;
            Row oneRowAhead = sheet.getRow(trueRowNum + 1);
            if (null == oneRowAhead || isRowEmpty(oneRowAhead)) {
                break;
            }
            PurchaseVendorRoster purchaseVendorRoster = null;
            if (rowNum != 0) {
                purchaseVendorRoster = new PurchaseVendorRoster();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "vendor no":
                            purchaseVendorRoster.setVendorId(getLongValue(cell));
                            break;
                        case "vendor name":
                            purchaseVendorRoster.setVendorName(getStringValue(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                purchaseVendorRosterList.add(purchaseVendorRoster);
            }
            rowNum++;
        }
        purchaseOrderService.savePurchaseVendorRoster(purchaseVendorRosterList);
    }
}
