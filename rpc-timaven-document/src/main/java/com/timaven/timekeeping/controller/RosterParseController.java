package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.IziToastOptions;
import com.timaven.timekeeping.service.ParserService;
import com.timaven.timekeeping.service.StageService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.timaven.timekeeping.util.ExcelUtil.*;

@SuppressWarnings("Duplicates")
@RestController
@PreAuthorize("hasAnyRole('ROLE_project_manager', 'ROLE_accountant_foreman', 'ROLE_accountant', 'ROLE_corporate_accountant', 'ROLE_corporate_timekeeper')")
public class RosterParseController extends BaseController {

    private final static String KEY_DATE = "date";
    private final static String KEY_PROJECT_Id = "projectId";

    private final static String SHEET_NAME_CRAFT = "crafts";
    private final static String SHEET_NAME_CRAFT_V2 = "crafts_v2";
    private final static String SHEET_NAME_ROSTER = "roster";
    private final static String SHEET_NAME_TEAM = "team";
    private final static String SHEET_NAME_TEAM_CC = "team-cc";

    private final ParserService mParserService;
    private final StageService mStageService;

    @Autowired
    public RosterParseController(ParserService parserService, StageService stageService) {
        mParserService = parserService;
        mStageService = stageService;
    }

    @PostMapping(value = "/roster")
    public CommonResult uploadRoster(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                String pickedDate = request.getParameterMap().get(KEY_DATE)[0];
                Long projectId = null;
                if (request.getParameterMap().get(KEY_PROJECT_Id) != null
                        && request.getParameterMap().get(KEY_PROJECT_Id)[0] != null) {
                    projectId = Long.valueOf(request.getParameterMap().get(KEY_PROJECT_Id)[0]);
                }
                LocalDate effectiveDate = parsePickedDate(pickedDate);
                List<StageTeam> stageTeams = new ArrayList<>();
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    if (SHEET_NAME_TEAM.equals(sheetName)) {
                        stageTeams = parseTeam(sheet);
                    }
                }

                Mono<List<StageRoster>> rosterMono = Mono.just(new ArrayList<>());
                Mono<List<StageTeamCostCode>> teamCostCodeMono = Mono.just(new ArrayList<>());
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    StageSubmission submission = new StageSubmission();
                    submission.setProjectId(projectId);
                    submission.setUsername(getUsername());
                    submission.setEffectiveOn(effectiveDate);
                    switch (sheetName) {
                        case SHEET_NAME_ROSTER:
                            submission.setType(StageSubmission.Type.Roster);
                            rosterMono = parseRoster(sheet, submission, stageTeams);
                            commonResult.addSuccess("Roaster parse is complete");
                            break;
                        case SHEET_NAME_TEAM_CC:
                            submission.setType(StageSubmission.Type.TeamCC);
                            teamCostCodeMono = parseTeamCostCode(sheet, submission);
                            commonResult.addSuccess("Team Cost Code parse is complete");
                            break;
                        default:
                            break;
                    }
                }
                Mono.zip(rosterMono, teamCostCodeMono).block();
                if (effectiveDate.isEqual(LocalDate.now()) || effectiveDate.isBefore(LocalDate.now())) {
                    CommonResult tempResult = mParserService.updateProjectSetUp();
                    commonResult.getIziToastOptions().addAll(tempResult.getIziToastOptions());
                }
                commonResult.setResult("success");
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    @PostMapping(value = "/labor-rates")
    public CommonResult uploadLaborRates(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                Long projectId = null;
                if (request.getParameterMap().get(KEY_PROJECT_Id) != null
                        && request.getParameterMap().get(KEY_PROJECT_Id)[0] != null) {
                    projectId = Long.valueOf(request.getParameterMap().get(KEY_PROJECT_Id)[0]);
                }

                Mono<List<StageCraft>> craftMono = Mono.just(new ArrayList<>());

                boolean sheetNameOutdated = false;
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    StageSubmission submission = new StageSubmission();
                    submission.setProjectId(projectId);
                    submission.setUsername(getUsername());
                    submission.setEffectiveOn(LocalDate.now());

                    if (SHEET_NAME_CRAFT.equals(sheetName)) {
                        sheetNameOutdated = true;
                        break;
                    }
                    if (SHEET_NAME_CRAFT_V2.equals(sheetName)) {
                        submission.setType(StageSubmission.Type.Crafts);
                        craftMono = parseCrafts(sheet, submission);
                        commonResult.addSuccess("Craft parse is complete");
                        break;
                    }
                }

                if (sheetNameOutdated) {
                    String msg = "The Labor rate template uploaded was outdated." +
                            "Please use the latest version by downloading from the project templates or using the exported template.";
                    IziToastOptions option = new IziToastOptions.IziToastOptionBuilder()
                            .title("Error")
                            .message(msg)
                            .position("topRight")
                            .timeout(false)
                            .color("red")
                            .build();
                    commonResult.addIziToastOption(option);
                    return commonResult;
                }

                craftMono.block();

                CommonResult tempResult = mParserService.updateCraftSubmission();
                commonResult.getIziToastOptions().addAll(tempResult.getIziToastOptions());
                commonResult.setResult("success");
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    private Mono<List<StageTeamCostCode>> parseTeamCostCode(Sheet sheet, StageSubmission submission) {
        List<StageTeamCostCode> stageTeamCostCodes = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();
        Map<String, String> titleCostCodeMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            String teamName = null;
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).trim();
                    indexTitleMap.put(cIndex, title);
                    if (cIndex != 0) {
                        titleCostCodeMap.put(title, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex) && cIndex != 0) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    String value = getStringValue(cell);

                    if (StringUtils.hasText(value)) {
                        if (cIndex == 0) {
                            teamName = value.trim();
                        } else {
                            StageTeamCostCode stageTeamCostCode = new StageTeamCostCode();
                            stageTeamCostCode.setTeam(teamName);
                            stageTeamCostCode.setCostCode(titleCostCodeMap.get(title));
                            stageTeamCostCodes.add(stageTeamCostCode);
                        }
                    }
                }
            }
            rowNum++;
        }
        return mStageService.saveTeamCostCode(stageTeamCostCodes, submission);
    }

    private Mono<List<StageCraft>> parseCrafts(Sheet sheet, StageSubmission submission) {
        List<StageCraft> stageCrafts = new ArrayList<>();

        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            StageCraft stageCraft = null;
            if (rowNum != 0) {
                stageCraft = new StageCraft();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (StringUtils.hasText(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "craft code":
                            stageCraft.setCraftCode(getStringValue(cell));
                            break;
                        case "craft":
                        case "description":
                            String description = getStringValue(cell);
                            if (StringUtils.hasText(description)) {
                                stageCraft.setDescription(description);
                            }
                            break;
                        case "billable st":
                            Double billableST = getDoubleValue(cell);
                            if (null != billableST) {
                                stageCraft.setBillableST(BigDecimal.valueOf(billableST));
                            }
                            break;
                        case "billable ot":
                            Double billableOT = getDoubleValue(cell);
                            if (null != billableOT) {
                                stageCraft.setBillableOT(BigDecimal.valueOf(billableOT));
                            }
                            break;
                        case "billable dt":
                            Double billableDT = getDoubleValue(cell);
                            if (null != billableDT) {
                                stageCraft.setBillableDT(BigDecimal.valueOf(billableDT));
                            }
                            break;
                        case "per diem amt":
                            Double perDiem = getDoubleValue(cell);
                            if (null != perDiem) {
                                stageCraft.setPerDiem(BigDecimal.valueOf(perDiem));
                            }
                            break;
                        case "rig pay":
                            Double rigPay = getDoubleValue(cell);
                            if (null != rigPay) {
                                stageCraft.setRigPay(BigDecimal.valueOf(rigPay));
                            }
                            break;
                        case "company":
                            // TM-90 add company column
                            String company = getStringValue(cell);
                            if (StringUtils.hasText(company)) {
                                stageCraft.setCompany(company);
                            }
                            break;
                        case "per diem rule":
                            String perDiemRule = getStringValue(cell);
                            if (StringUtils.hasText(perDiemRule)) {
                                stageCraft.setPerDiemRule(perDiemRule);
                            }
                            break;
                        case "start date":
                            stageCraft.setStartDate(getLocalDate(cell));
                            break;
                        case "end date":
                            stageCraft.setEndDate(getLocalDate(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (null != stageCraft.getCraftCode()) {
                    stageCrafts.add(stageCraft);
                } else {
                    System.out.println("Sheet: CRAFTS. Row: " + (rowNum + 1) + ". " +
                            "Craft name value is missing");
                }
            }
            rowNum++;
        }
        return mStageService.saveCrafts(stageCrafts, submission);
    }

    private LocalDate parsePickedDate(String effectiveDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        return LocalDate.parse(effectiveDate, formatter);
    }

    private Mono<List<StageRoster>> parseRoster(Sheet sheet, StageSubmission submission, List<StageTeam> stageTeams) {
        List<StageRoster> rosters = new ArrayList<>();

        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            StageRoster roster = null;
            if (rowNum != 0) {
                roster = new StageRoster();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (StringUtils.hasText(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "badge":
                        case "badge #":
                            roster.setBadge(getStringValue(cell));
                            break;
                        case "employee":
                            roster.setName(getStringValue(cell));
                            break;
                        case "job #.":
                        case "job number":
                            roster.setJobNumber(getStringValue(cell));
                            break;
                        case "employee id":
                        case "emp #":
                        case "emp id":
                            String empId = getStringValue(cell);
                            if (StringUtils.hasText(empId)) {
                                roster.setEmployeeId(empId);
                            }
                            break;
                        case "client employee id":
                            String clientEmpId = getStringValue(cell);
                            if (StringUtils.hasText(clientEmpId)) {
                                roster.setClientEmpId(clientEmpId);
                            }
                            break;
                        case "team name":
                        case "team":
                            roster.setTeam(getStringValue(cell));
                            break;
                        case "supervisor":
                        case "crew":
                        case "crew #":
                            roster.setCrew(getStringValue(cell));
                            break;
                        case "company":
                            roster.setCompany(getStringValue(cell));
                            break;
                        case "hire date":
                            roster.setHiredAt(getLocalDate(cell));
                            break;
                        case "term date":
                            roster.setTerminatedAt(getLocalDate(cell));
                            break;
                        case "shift":
                            roster.setShift(getStringValue(cell));
                            break;
                        case "schedule start":
                            roster.setScheduleStart(getLocalTime(cell));
                            break;
                        case "schedule end":
                            roster.setScheduleEnd(getLocalTime(cell));
                            break;
                        case "lunch start":
                            roster.setLunchStart(getLocalTime(cell));
                            break;
                        case "lunch end":
                            roster.setLunchEnd(getLocalTime(cell));
                            break;
                        case "craft code":
                        case "craft":
                            roster.setCraftCode(getStringValue(cell).trim());
                            break;
                        case "base st":
                            Double baseST = getDoubleValue(cell);
                            if (null != baseST) {
                                roster.setBaseST(BigDecimal.valueOf(baseST));
                            }
                            break;
                        case "base ot":
                            Double baseOT = getDoubleValue(cell);
                            if (null != baseOT) {
                                roster.setBaseOT(BigDecimal.valueOf(baseOT));
                            }
                            break;
                        case "base dt":
                            Double baseDT = getDoubleValue(cell);
                            if (null != baseDT) {
                                roster.setBaseDT(BigDecimal.valueOf(baseDT));
                            }
                            break;
                        case "holiday":
                        case "holiday rate":
                        case "hl":
                            Double holidayRate = getDoubleValue(cell);
                            if (null != holidayRate) {
                                roster.setHolidayRate(BigDecimal.valueOf(holidayRate));
                            }
                            break;
                        case "sick leave rate":
                        case "sick leave":
                        case "sl":
                            Double sickLeaveRate = getDoubleValue(cell);
                            if (null != sickLeaveRate) {
                                roster.setSickLeaveRate(BigDecimal.valueOf(sickLeaveRate));
                            }
                            break;
                        case "travel rate":
                        case "travel":
                        case "tv":
                            Double travelRate = getDoubleValue(cell);
                            if (null != travelRate) {
                                roster.setTravelRate(BigDecimal.valueOf(travelRate));
                            }
                            break;
                        case "vacation":
                        case "vacation rate":
                        case "va":
                            Double vacationRate = getDoubleValue(cell);
                            if (null != vacationRate) {
                                roster.setVacationRate(BigDecimal.valueOf(vacationRate));
                            }
                            break;
                        case "client craft code":
                            roster.setClientCraft(getStringValue(cell));
                            break;
                        case "per diem":
                            String strPerDiem = getStringValue(cell).trim();
                            roster.setHasPerDiem(StringUtils.hasText(strPerDiem));
                            break;
                        case "rig pay":
                            String strRigPay = getStringValue(cell).trim();
                            roster.setHasRigPay(StringUtils.hasText(strRigPay));
                            break;
                        case "sign in sheet":
                            String signInSheet = getStringValue(cell).trim();
                            roster.setSignInSheet(signInSheet);
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (null != roster.getEmployeeId()) {
                    rosters.add(roster);
                } else {
                    System.out.println("Sheet: Roster. Row: " + (rowNum + 1) +
                            ". misses values(employee id). ");
                }
            }
            rowNum++;
        }
        Map<String, StageTeam> empIdTeamMap = stageTeams.stream()
                .collect(Collectors.toMap(StageTeam::getEmployeeId, Function.identity(), (e1, e2) -> e2));
        rosters.forEach(r -> {
            StageTeam stageTeam = empIdTeamMap.get(r.getEmployeeId());
            if (stageTeam != null) {
                if (!StringUtils.hasText(r.getTeam())) {
                    r.setTeam(stageTeam.getTeam());
                }
                if (!StringUtils.hasText(r.getCrew())) {
                    r.setCrew(stageTeam.getCrew());
                }
            }
        });
        return mStageService.saveRoster(rosters, submission);
    }

    /**
     * @deprecated This will be removed once all users use the new template without team sheet.
     */
    @Deprecated
    private List<StageTeam> parseTeam(Sheet sheet) {
        List<StageTeam> stageTeams = new ArrayList<>();

        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            StageTeam stageTeam = null;
            if (rowNum != 0) {
                stageTeam = new StageTeam();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (StringUtils.hasText(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "employee id":
                        case "id":
                            String empId = getStringValue(cell);
                            if (StringUtils.hasText(empId)) {
                                stageTeam.setEmployeeId(empId);
                            }
                            break;
                        case "supervisor":
                        case "crew":
                        case "crew #":
                            stageTeam.setCrew(getStringValue(cell));
                            break;
                        case "team name":
                        case "team":
                            String tmName = getStringValue(cell).trim();
                            if (StringUtils.hasText(tmName)) {
                                stageTeam.setTeam(tmName);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (null != stageTeam.getEmployeeId() && StringUtils.hasText(stageTeam.getTeam())) {
                    stageTeams.add(stageTeam);
                } else {
                    System.out.println("Sheet: Team. Row: " + (rowNum + 1) +
                            ". misses values(employee id or team name). ");
                }
            }
            rowNum++;
        }
        return stageTeams;
    }
}
