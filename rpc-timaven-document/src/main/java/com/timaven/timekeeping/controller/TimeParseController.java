package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.lang.RosterNotFoundException;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.service.ParserService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static com.timaven.timekeeping.util.ExcelUtil.*;

@RestController
@PreAuthorize("hasAnyRole('ROLE_accountant', 'ROLE_accountant_foreman', 'ROLE_timekeeper')")
public class TimeParseController extends BaseController {

    private final static String KEY_PROJECT_Id = "projectId";
    private final static String KEY_RECORD_TYPE = "recordType";
    private final static String TYPE_DEFAULT_PUNCH_RECORD = "default_punch_record";
    private final static String TYPE_DEFAULT_TIMESHEET = "default_timesheet";
    private final static String TYPE_TRACK_PUNCH_RECORD = "track_punch_record";
    private final static String TYPE_TRACK_TIMESHEET = "track_timesheet";
    private final static String TYPE_SIGN_IN_SHEET = "sign_in_sheet";
    private final static String KEY_DATE_OF_SERVICE = "dateOfService";

    private final ParserService mParserService;

    @Autowired
    public TimeParseController(ParserService parserService) {
        mParserService = parserService;
    }

    /**
     * Analyze the time records submitted by a manager
     */
    @PostMapping(value = "time-records")
    public CommonResult timeRecords(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        Long projectId = Long.valueOf(request.getParameterMap().get(KEY_PROJECT_Id)[0]);
        CommonResult commonResult = new CommonResult();

        if (!mParserService.hasRuleAndShiftOfProject(projectId)) {
            commonResult.setResult("failed");
            commonResult.addError("Rule or default shift has not been set.");
            return commonResult;
        }

        String recordType = request.getParameterMap().get(KEY_RECORD_TYPE)[0];

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                Long transSubmissionId = null;
                boolean invalidFile = true;
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName();
                    LocalDate dateOfService = null;
                    if (recordType.equalsIgnoreCase(TYPE_SIGN_IN_SHEET)
                            || recordType.equalsIgnoreCase(TYPE_TRACK_TIMESHEET)) {
                        String[] parameters = request.getParameterMap().get(KEY_DATE_OF_SERVICE);
                        if (null != parameters && parameters.length > 0) {
                            String strDateOfService = parameters[0];
                            dateOfService = LocalDate.parse(strDateOfService,
                                    DateTimeFormatter.ofPattern("MM/dd/yyyy"));
                        }
                    }

                    switch (recordType) {
                        case TYPE_DEFAULT_PUNCH_RECORD:
                            if ("data".equalsIgnoreCase(sheetName)) {
                                transSubmissionId = parseDefaultPunchRecord(sheet, projectId);
                                invalidFile = false;
                                commonResult.addSuccess("Default Punch Record parse is complete");
                            }
                            break;
                        case TYPE_DEFAULT_TIMESHEET:
                        case TYPE_SIGN_IN_SHEET:
                            if ("data".equalsIgnoreCase(sheetName)) {
                                transSubmissionId = parseDefaultTimesheet(sheet, projectId, dateOfService);
                                invalidFile = false;
                                commonResult.addSuccess("Default Timesheet parse is complete");
                            }
                            break;
                        case TYPE_TRACK_PUNCH_RECORD:
                            if ("sheet1".equalsIgnoreCase(sheetName)) {
                                transSubmissionId = parseTrackPunchRecord(sheet, projectId);
                                invalidFile = false;
                                commonResult.addSuccess("Track Punch Record parse is complete");
                            }
                            break;
                        case TYPE_TRACK_TIMESHEET:
                            if ("sheet1".equalsIgnoreCase(sheetName)) {
                                transSubmissionId = parseTrackTimesheet(sheet, projectId, dateOfService);
                                invalidFile = false;
                                commonResult.addSuccess("Track Timesheet parse is complete");
                            }
                            break;
                        default:
                            invalidFile = true;
                            break;
                    }
                }
                if (null != transSubmissionId) {
                    commonResult.setResult("success");
                    return commonResult;
                }
                if (invalidFile) {
                    commonResult.setResult("failed");
                    commonResult.addError("Cannot parse this time record, please make sure the type and time record matches.");
                    return commonResult;
                } else {
                    commonResult.setResult("failed");
                    commonResult.addError("The file is valid but no valid time records");
                    return commonResult;
                }
            } catch (RosterNotFoundException ex) {
                ex.printStackTrace();
                commonResult.setResult("failed");
                commonResult.addError("Time Record upload failed due to no active roster for the date selected");
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    private Long parseDefaultPunchRecord(Sheet sheet, Long projectId) throws Exception {
        List<PunchRecord> punchRecordList = new ArrayList<>();
        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            PunchRecord punchRecord = null;
            PunchRecord.Event event = null;
            PunchRecord.Door door = null;
            if (rowNum != 0) {
                punchRecord = new PunchRecord();
                event = punchRecord.getEvent();
                door = punchRecord.getDoor();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "last name":
                        case "last":
                            punchRecord.setLastName(getStringValue(cell));
                            break;
                        case "first name":
                        case "first":
                            punchRecord.setFirstName(getStringValue(cell));
                            break;
                        case "emp id (cardholder)":
                        case "emp id":
                        case "id":
                            punchRecord.setEmpId(getStringValue(cell));
                            break;
                        case "badge":
                        case "badge id":
                        case "badge #":
                            punchRecord.setBadge(getStringValue(cell));
                            break;
                        case "client employee id":
                        case "client emp id":
                            punchRecord.setClientEmpId(getStringValue(cell));
                            break;
                        case "cardholder":
                            punchRecord.setCardHolder(getStringValue(cell));
                            break;
                        case "credential code":
                            punchRecord.setCredentialCode(getStringValue(cell));
                            break;
                        case "event":
                            String eventDetail = getStringValue(cell);
                            event.setDetail(eventDetail);
                            event.setAccessible("access granted".equalsIgnoreCase(eventDetail));
                            break;
                        case "door":
                            String doorStr = getStringValue(cell);
                            String[] doorArray = doorStr.split(" ");
                            door.setName(doorArray[0]);
                            door.setEntry("entry".equalsIgnoreCase(doorArray[1]));
                            break;
                        case "side":
                            punchRecord.setSide("reader - in".equalsIgnoreCase(getStringValue(cell)));
                            break;
                        case "event timestamp":
                            punchRecord.setEventTime(getLocalDateTime(cell));
                            punchRecord.setNetEventTime(getLocalDateTime(cell));
                            break;
                        case "crew":
                        case "crew #":
                        case "company (cardholder)":
                        case "company":
                            punchRecord.setCompany(getStringValue(cell));
                            break;
                        case "timesheet name":
                        case "team name":
                        case "team":
                            String tsName = getStringValue(cell).trim();
                            if (!StringUtils.isEmpty(tsName)) {
                                punchRecord.setTeamName(tsName);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0 && (StringUtils.hasText(punchRecord.getEmpId())
                    || StringUtils.hasText(punchRecord.getBadge())
                    || StringUtils.hasText(punchRecord.getClientEmpId()))) {
                punchRecordList.add(punchRecord);
            }
            rowNum++;
        }

        final Collection<EmpPunchRecords> empPunchRecords = getEmpPunchRecords(punchRecordList);

        filterInvalidPunchRecords(empPunchRecords);

        mParserService.parsePunchRecord(empPunchRecords, projectId);

        empPunchRecords.forEach(EmpPunchRecords::finalizeValidPunchRecords);
        return populateTime(empPunchRecords, projectId);
    }

    private Long parseDefaultTimesheet(Sheet sheet, Long projectId, LocalDate dateOfService) throws Exception {
        List<PunchRecord> punchRecordList = new ArrayList<>();
        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            PunchRecord clockIn = null;
            PunchRecord.Event clockInEvent = null;
            PunchRecord.Door clockInDoor = null;
            PunchRecord clockOut = null;
            PunchRecord.Event clockOutEvent = null;
            PunchRecord.Door clockOutDoor = null;
            PunchRecord lunchStart = null;
            PunchRecord.Event lunchStartEvent = null;
            PunchRecord.Door lunchStartDoor = null;
            PunchRecord lunchEnd = null;
            PunchRecord.Event lunchEndEvent = null;
            PunchRecord.Door lunchEndDoor = null;
            if (rowNum != 0) {
                clockIn = new PunchRecord();
                clockInEvent = clockIn.getEvent();
                clockInDoor = clockIn.getDoor();
                clockIn.setSide(true);
                clockInEvent.setDetail("Access granted");
                clockInEvent.setAccessible(true);

                clockOut = new PunchRecord();
                clockOutEvent = clockOut.getEvent();
                clockOutDoor = clockOut.getDoor();
                clockOut.setSide(false);
                clockOutEvent.setDetail("Access granted");
                clockOutEvent.setAccessible(true);

                lunchStart = new PunchRecord();
                lunchStartEvent = lunchStart.getEvent();
                lunchStartDoor = lunchStart.getDoor();
                lunchStart.setSide(false);
                lunchStartEvent.setDetail("Access granted");
                lunchStartEvent.setAccessible(true);

                lunchEnd = new PunchRecord();
                lunchEndEvent = lunchEnd.getEvent();
                lunchEndDoor = lunchEnd.getDoor();
                lunchEnd.setSide(true);
                lunchEndEvent.setDetail("Access granted");
                lunchEndEvent.setAccessible(true);
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "last name":
                        case "last":
                            clockIn.setLastName(getStringValue(cell));
                            clockOut.setLastName(getStringValue(cell));
                            lunchStart.setLastName(getStringValue(cell));
                            lunchEnd.setLastName(getStringValue(cell));
                            break;
                        case "first name":
                        case "first":
                            clockIn.setFirstName(getStringValue(cell));
                            clockOut.setFirstName(getStringValue(cell));
                            lunchStart.setFirstName(getStringValue(cell));
                            lunchEnd.setFirstName(getStringValue(cell));
                            break;
                        case "emp id (cardholder)":
                        case "emp id":
                        case "id":
                            clockIn.setEmpId(getStringValue(cell));
                            clockOut.setEmpId(getStringValue(cell));
                            lunchStart.setEmpId(getStringValue(cell));
                            lunchEnd.setEmpId(getStringValue(cell));
                            break;
                        case "badge":
                        case "badge id":
                        case "badge #":
                            clockIn.setBadge(getStringValue(cell));
                            clockOut.setBadge(getStringValue(cell));
                            lunchStart.setBadge(getStringValue(cell));
                            lunchEnd.setBadge(getStringValue(cell));
                            break;
                        case "client employee id":
                        case "client emp id":
                            clockIn.setClientEmpId(getStringValue(cell));
                            clockOut.setClientEmpId(getStringValue(cell));
                            lunchStart.setClientEmpId(getStringValue(cell));
                            lunchEnd.setClientEmpId(getStringValue(cell));
                            break;
                        case "company":
                            clockIn.setCompany(getStringValue(cell));
                            clockOut.setCompany(getStringValue(cell));
                            lunchStart.setCompany(getStringValue(cell));
                            lunchEnd.setCompany(getStringValue(cell));
                            break;
                        case "team":
                            String tsName = getStringValue(cell).trim();
                            if (!StringUtils.isEmpty(tsName)) {
                                clockIn.setTeamName(tsName);
                                clockOut.setTeamName(tsName);
                                lunchStart.setTeamName(tsName);
                                lunchEnd.setTeamName(tsName);
                            }
                            break;
                        case "clock in":
                            if (dateOfService == null) {
                                clockIn.setEventTime(getLocalDateTime(cell));
                                clockIn.setNetEventTime(getLocalDateTime(cell));
                            } else {
                                clockIn.setEventTime(getLocalDateTime(cell, dateOfService));
                                clockIn.setNetEventTime(getLocalDateTime(cell, dateOfService));
                            }
                            break;
                        case "clock out":
                            if (dateOfService == null) {
                                clockOut.setEventTime(getLocalDateTime(cell));
                                clockOut.setNetEventTime(getLocalDateTime(cell));
                            } else {
                                clockOut.setEventTime(getLocalDateTime(cell, dateOfService));
                                clockOut.setNetEventTime(getLocalDateTime(cell, dateOfService));
                            }
                            break;
                        case "lunch start":
                            if (dateOfService == null) {
                                lunchStart.setEventTime(getLocalDateTime(cell));
                                lunchStart.setNetEventTime(getLocalDateTime(cell));
                            } else {
                                lunchStart.setEventTime(getLocalDateTime(cell, dateOfService));
                                lunchStart.setNetEventTime(getLocalDateTime(cell, dateOfService));
                            }
                            break;
                        case "lunch end":
                            if (dateOfService == null) {
                                lunchEnd.setEventTime(getLocalDateTime(cell));
                                lunchEnd.setNetEventTime(getLocalDateTime(cell));
                            } else {
                                lunchEnd.setEventTime(getLocalDateTime(cell, dateOfService));
                                lunchEnd.setNetEventTime(getLocalDateTime(cell, dateOfService));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (StringUtils.hasText(clockIn.getEmpId())
                        || StringUtils.hasText(clockIn.getBadge())
                        || StringUtils.hasText(clockIn.getClientEmpId())) {
                    if (clockIn.getEventTime() != null && clockOut.getEventTime() != null) {
                        punchRecordList.add(clockIn);
                        punchRecordList.add(clockOut);
                        if (lunchStart.getEventTime() != null) {
                            punchRecordList.add(lunchStart);
                        }
                        if (lunchEnd.getEventTime() != null) {
                            punchRecordList.add(lunchEnd);
                        }
                    }
                }
            }
            rowNum++;
        }

        final Collection<EmpPunchRecords> empPunchRecords = getEmpPunchRecords(punchRecordList);

        mParserService.parsePunchRecord(empPunchRecords, projectId);

        empPunchRecords.forEach(EmpPunchRecords::finalizeValidPunchRecords);
        return populateTime(empPunchRecords, projectId);
    }

    private Long parseTrackPunchRecord(Sheet sheet, Long projectId) throws Exception {
        List<PunchRecord> punchRecordList = new ArrayList<>();
        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        String company = null;
        String lastName = null;
        String firstName = null;
        String middleName = null;
        String teamName = null;
        String employeeId = null;

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            if (isRowEmpty(row)) {
                break;
            }

            String description = getStringValue(row.getCell(0));

            if (rowNum == 0) {
                for (Cell cell : row) {
                    String title;
                    int cIndex = cell.getColumnIndex();
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                }
                rowNum++;
                continue;
            }

            if (rowNum == 1) {
                company = description;
                rowNum++;
                continue;
            }

            // Empty row is automatically ignored
//            if (rowNum == 2) {
//                rowNum++;
//                continue;
//            }

            if (isRowEmpty(row)) {
                break;
            }

            if (description.equalsIgnoreCase("In") || description.equalsIgnoreCase("Out")) {
                PunchRecord punchRecord = new PunchRecord();
                punchRecord.setLastName(lastName);
                punchRecord.setFirstName(firstName);
                punchRecord.setEmpId(employeeId);
                punchRecord.setCompany(company);
                punchRecord.setTeamName(teamName);
                PunchRecord.Event event = punchRecord.getEvent();
                event.setDetail("Access granted");
                event.setAccessible(true);
                punchRecord.setSide(description.equalsIgnoreCase("In"));
                LocalDate date = getLocalDate(row.getCell(1));
                LocalTime time = getLocalTime(row.getCell(2));
                if (null == date || null == time)  {
                    rowNum++;
                    continue;
                }
                LocalDateTime dateTime = LocalDateTime.of(date, time);
                punchRecord.setNetEventTime(dateTime);
                punchRecord.setEventTime(dateTime);
                if (employeeId != null) {
                    punchRecordList.add(punchRecord);
                }
            } else {
                try {
                    LocalDate date = getLocalDate(row.getCell(0));
                    if (date == null) throw new DateTimeParseException("Failed to parse date",
                            getStringValue(row.getCell(0)), 0);
                    rowNum++;
                    continue;
                } catch (DateTimeParseException ignore) {
                    lastName = null;
                    firstName = null;
                    middleName = null;
                    teamName = null;
                    employeeId = null;
                    for (Cell cell : row) {
                        String title;
                        int cIndex = cell.getColumnIndex();
                        if (cIndex == 0) {
                            String fullName = getStringValue(cell);
                            if (fullName.startsWith("Total Hours")) {
                                break;
                            }
                            if (fullName.contains(",")) {
                                String[] nameArray = Arrays.stream(fullName.split(","))
                                        .map(String::trim).toArray(String[]::new);
                                if (nameArray.length == 2) {
                                    lastName = nameArray[0];
                                    if (nameArray[1].contains(" ")) {
                                        String[] firstMiddleName = nameArray[1].split(" ");
                                        firstName = firstMiddleName[0];
                                        middleName = firstMiddleName[1];
                                    } else {
                                        firstName = nameArray[1];
                                    }
                                } else if (nameArray.length == 1) {
                                    lastName = nameArray[0];
                                } else {
                                    lastName = null;
                                    firstName = null;
                                }
                            }
                        } else if (cIndex == 1) {
                            teamName = getStringValue(cell);
                        } else {
                            if (!indexTitleMap.containsKey(cIndex)) {
                                continue;
                            }
                            title = indexTitleMap.get(cIndex);
                            switch (title) {
                                case "employee id":
                                    employeeId = getStringValue(cell);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            rowNum++;
        }

        final Collection<EmpPunchRecords> empPunchRecords = getEmpPunchRecords(punchRecordList);

        filterInvalidPunchRecords(empPunchRecords);

        mParserService.parsePunchRecord(empPunchRecords, projectId);

        empPunchRecords.forEach(EmpPunchRecords::finalizeValidPunchRecords);
        return populateTime(empPunchRecords, projectId);
    }

    private Long parseTrackTimesheet(Sheet sheet, Long projectId, LocalDate dateOfService) throws Exception {
        List<PunchRecord> punchRecordList = new ArrayList<>();
        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        String company = null;
        String teamName = null;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            String firstColumnString = getStringValue(row.getCell(0));
            if (rowNum == 1) {
                company = firstColumnString;
                teamName = getStringValue(rowIterator.next().getCell(0));
                rowNum += 2;
                continue;
            }
            if (firstColumnString.startsWith("Total in Group")) {
                teamName = getStringValue(rowIterator.next().getCell(0));
                if (teamName.startsWith("Print Date/Time")) break;
                rowNum += 2;
                continue;
            }
            String lastName = null;
            String firstName = null;
            String middleName = null;

            PunchRecord clockIn = null;
            PunchRecord.Event clockInEvent = null;
            PunchRecord.Door clockInDoor = null;

            PunchRecord clockOut = null;
            PunchRecord.Event clockOutEvent = null;
            PunchRecord.Door clockOutDoor = null;

            clockIn = new PunchRecord();
            clockInEvent = clockIn.getEvent();
            clockInDoor = clockIn.getDoor();
            clockOut = new PunchRecord();
            clockOutEvent = clockOut.getEvent();
            clockOutDoor = clockOut.getDoor();
            if (rowNum > 2) {
                clockIn.setCompany(company);
                clockIn.setTeamName(teamName);
                clockIn.setSide(true);
                clockInEvent.setDetail("Access granted");
                clockInEvent.setAccessible(true);

                clockOut.setCompany(company);
                clockOut.setTeamName(teamName);
                clockOut.setSide(false);
                clockOutEvent.setDetail("Access granted");
                clockOutEvent.setAccessible(true);
            }

            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "name":
                            String fullName = getStringValue(cell);
                            if (fullName.contains(",")) {
                                String[] nameArray = Arrays.stream(fullName.split(","))
                                        .map(String::trim).toArray(String[]::new);
                                if (nameArray.length == 2) {
                                    lastName = nameArray[0];
                                    if (nameArray[1].contains(" ")) {
                                        String[] firstMiddleName = nameArray[1].split(" ");
                                        firstName = firstMiddleName[0];
                                        middleName = firstMiddleName[1];
                                    } else {
                                        firstName = nameArray[1];
                                    }
                                } else if (nameArray.length == 1) {
                                    lastName = nameArray[0];
                                } else {
                                    lastName = null;
                                    firstName = null;
                                }
                            }
                            clockIn.setLastName(lastName);
                            clockIn.setFirstName(firstName);
                            clockOut.setLastName(lastName);
                            clockOut.setFirstName(firstName);
                            break;
                        case "badge":
                        case "badge id":
                        case "badge #":
                            clockIn.setBadge(getStringValue(cell));
                            clockOut.setBadge(getStringValue(cell));
                            break;
                        case "pid":
                            clockIn.setEmpId(getStringValue(cell));
                            clockOut.setEmpId(getStringValue(cell));
                            break;
                        case "in time":
                            LocalTime inTime = getLocalTime(cell, "HHmm");
                            if (null != inTime) {
                                clockIn.setEventTime(LocalDateTime.of(dateOfService, inTime));
                                clockIn.setNetEventTime(LocalDateTime.of(dateOfService, inTime));
                            }
                            break;
                        case "out time":
                            LocalTime outTime = getLocalTime(cell, "HHmm");
                            if (null != outTime) {
                                clockOut.setEventTime(LocalDateTime.of(dateOfService, outTime));
                                clockOut.setNetEventTime(LocalDateTime.of(dateOfService, outTime));
                            }
                            break;
                        case "time on site":
                            Double timeOnSite = getDoubleValue(cell);
                            if (null != timeOnSite) {
                                clockIn.setNetDuration(Duration.ofMinutes(Math.round(timeOnSite * 60)));
                            }
                            break;
                        case "net time":
                            Double netTime = getDoubleValue(cell);
                            if (null != netTime) {
                                clockIn.setPayDuration(Duration.ofMinutes(Math.round(netTime * 60)));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if ((clockIn.getEmpId() != null || clockIn.getBadge() != null) && clockIn.getEventTime() != null) {
                punchRecordList.add(clockIn);
                if (clockOut.getEventTime() != null && clockOut.getEventTime().isBefore(clockIn.getEventTime())) {
                    clockOut.setEventTime(clockOut.getEventTime().plusDays(1));
                    clockOut.setNetEventTime(clockOut.getEventTime());
                }
            }
            if ((clockOut.getEmpId() != null || clockOut.getBadge() != null) && clockOut.getEventTime() != null) {
                punchRecordList.add(clockOut);
            }
            rowNum++;
        }

        final Collection<EmpPunchRecords> empPunchRecords = getEmpPunchRecords(punchRecordList);

        mParserService.parsePunchRecord(empPunchRecords, projectId);

        empPunchRecords.forEach(EmpPunchRecords::finalizeValidPunchRecords);
        return populateTime(empPunchRecords, projectId);
    }

    private Collection<EmpPunchRecords> getEmpPunchRecords(List<PunchRecord> punchRecordList) {
        // Filter out "Not access granted" items
        final Map<String, EmpPunchRecords> idEmpPunchRecords = new LinkedHashMap<>();

        punchRecordList = punchRecordList.stream()
                .sorted((o1, o2) -> {
                    String id1 = "";
                    String id2 = "";
                    if (StringUtils.hasText(o1.getEmpId()) && StringUtils.hasText(o2.getEmpId())) {
                        id1 = o1.getEmpId();
                        id2 = o2.getEmpId();
                    } else if (StringUtils.hasText(o1.getBadge()) && StringUtils.hasText(o2.getBadge())) {
                        id1 = o1.getBadge();
                        id2 = o2.getBadge();
                    } else if (StringUtils.hasText(o1.getClientEmpId()) && StringUtils.hasText(o2.getClientEmpId())) {
                        id1 = o1.getClientEmpId();
                        id2 = o2.getClientEmpId();
                    }
                    int result = id1.compareTo(id2);
                    if (result == 0) {
                        return o1.getEventTime().compareTo(o2.getEventTime());
                    }
                    return result;
                }).collect(toList());

//        punchRecordList = punchRecordList.stream()
//                .sorted(Comparator.comparing(PunchRecord::getEmpId)
//                        .thenComparing(PunchRecord::getEventTime)).collect(toList());
        punchRecordList.forEach(
                e -> {
                    EmpPunchRecords empPunchRecords;
                    String id;

                    if (StringUtils.hasText(e.getEmpId())) {
                        id = e.getEmpId();
                    } else if (StringUtils.hasText(e.getBadge())) {
                        id = e.getBadge();
                    } else if (StringUtils.hasText(e.getClientEmpId())) {
                        id = e.getClientEmpId();
                    } else {
                        return;
                    }

                    if (idEmpPunchRecords.containsKey(id)) {
                        empPunchRecords = idEmpPunchRecords.get(id);
                    } else {
                        empPunchRecords = new EmpPunchRecords();
                        empPunchRecords.setEmpId(e.getEmpId());
                        empPunchRecords.setBadge(e.getBadge());
                        empPunchRecords.setClientEmpId(e.getClientEmpId());
                        empPunchRecords.setTeamName(e.getTeamName());
                        empPunchRecords.setLastName(e.getLastName());
                        empPunchRecords.setFirstName(e.getFirstName());
                        empPunchRecords.setCardHolder(e.getCardHolder());
                        empPunchRecords.setCompany(e.getCompany());
                        empPunchRecords.setCredentialCode(e.getCredentialCode());
                        // Temporary scheduleStart
                        empPunchRecords.setDate(e.getEventTime().toLocalDate());
                        idEmpPunchRecords.put(id, empPunchRecords);
                    }

                    if (e.getNetDuration() != null) {
                        empPunchRecords.setNetDuration(e.getNetDuration());
                    }
                    if (e.getPayDuration() != null) {
                        empPunchRecords.setPayDuration(e.getPayDuration());
                    }

                    // Monitor will keep last in first out when continuous in or out comes.
                    // But One could have multiple in and out pairs
                    empPunchRecords.addPunchRecord(e);
                }
        );
        return idEmpPunchRecords.values();
    }

    private void filterInvalidPunchRecords(Collection<EmpPunchRecords> empPunchRecords) {
        empPunchRecords.forEach(
                v -> {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy HH:mm");
                    List<PunchRecord> needToBeRemoved = new ArrayList<>();
                    final List<PunchRecord> punchRecords = v.getValidPunchRecordList();
                    int punchRecordListSize = punchRecords.size();
                    for (int i = 0; i < punchRecordListSize; ) {
                        PunchRecord punchRecord = punchRecords.get(i);
                        String eventTime = punchRecord.getNetEventTime().format(formatter);
                        if (!punchRecord.isSide()) {
                            needToBeRemoved.add(punchRecord);
                            v.addException(String.format("Punch in record missing for punch out %s.", eventTime), i, 3);
                            i++;
                        } else if (i == punchRecordListSize - 1 || punchRecords.get(i + 1).isSide()) {
                            needToBeRemoved.add(punchRecord);
                            v.addException(String.format("Punch out record missing for punch in %s.", eventTime), i, 2);
                            i++;
                        } else {
                            i = i + 2;
                        }
                    }
                    v.removePunchRecords(needToBeRemoved);
                }
        );
    }

    private Long populateTime(Collection<EmpPunchRecords> empPunchRecordsCollection, Long projectId) {
        TransSubmission transSubmission = new TransSubmission(getUserId(), projectId);
        final LinkedHashSet<StageTransaction> stageTransactionsSet = new LinkedHashSet<>();
        for (EmpPunchRecords empPunchRecords : empPunchRecordsCollection) {
            StageTransaction stageTransaction = new StageTransaction();
            stageTransaction.setTransSubmission(transSubmission);
            stageTransaction.setFirstName(empPunchRecords.getFirstName());
            stageTransaction.setLastName(empPunchRecords.getLastName());
            stageTransaction.setDateOfService(empPunchRecords.getDate());
            stageTransaction.setTotalHour(empPunchRecords.getTotalHours());
            stageTransaction.setNetHour(empPunchRecords.getNetHours());
            stageTransaction.setEmpId(empPunchRecords.getEmpId());
            stageTransaction.setClientEmpId(empPunchRecords.getClientEmpId());
            stageTransaction.setBadge(empPunchRecords.getBadge());
            stageTransaction.setCompany(empPunchRecords.getCompany());
            stageTransaction.setTeamName(empPunchRecords.getTeamName());
            stageTransaction.setByUpload(true);
            stageTransactionsSet.add(stageTransaction);
            List<EmpPunchRecords.EmpPunchException> exceptionList = empPunchRecords.getExceptionList();
            if (!CollectionUtils.isEmpty(exceptionList)) {
                Set<StageException> stageExceptions = new HashSet<>();
                for (EmpPunchRecords.EmpPunchException empPunchException : exceptionList) {
                    StageException stageException = new StageException();
                    stageException.setException(empPunchException.getException());
                    stageException.setOrder(empPunchException.getOrder());
                    stageException.setExceptionType(empPunchException.getExceptionType());
                    stageExceptions.add(stageException);
                }
                stageTransaction.setStageExceptions(stageExceptions);
            }
            List<PunchRecord> originalPunchRecordList = empPunchRecords.getAllPunchRecordList();
            if (!CollectionUtils.isEmpty(originalPunchRecordList)) {
                Set<StageRecord> stageRecords = new HashSet<>();
                for (PunchRecord punchRecord : originalPunchRecordList) {
                    StageRecord stageRecord = new StageRecord();
                    stageRecord.setDoorName(punchRecord.getDoor().getName());
                    stageRecord.setSide(punchRecord.isSide());
                    stageRecord.setAccessGranted(punchRecord.getEvent().isAccessible());
                    stageRecord.setValid(punchRecord.isValid());
                    stageRecord.setAdjustedTime(punchRecord.getEventTime());
                    stageRecord.setNetEventTime(punchRecord.getNetEventTime());
                    stageRecords.add(stageRecord);
                }
                stageTransaction.setStageRecords(stageRecords);
            }
        }
        transSubmission.setStageTransactions(stageTransactionsSet);
        transSubmission = mParserService.saveTransSubmission(transSubmission);
        return transSubmission == null ? null : transSubmission.getId();
    }
}
