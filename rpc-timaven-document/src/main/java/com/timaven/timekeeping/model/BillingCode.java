package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingCode.class)
@Getter
@Setter
@NoArgsConstructor
public class BillingCode {

    private Long id;

    private BillingCodeType billingCodeType;

    private Set<CostCodeBillingCode> costCodeBillingCodes;

    private Long typeId;

    private String codeName;

    private String clientAlias;

    private String description;

    private Set<BillingCodeOverride> billingCodeOverrides;


    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCode)) return false;
        BillingCode that = (BillingCode) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}
