package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.BillingCodeTypeParsedBy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope =
        BillingCodeType.class)
@Getter
@Setter
@NoArgsConstructor
public class BillingCodeType {

    private Long id;

    private String codeType;

    private Integer level;

    @NotNull
    private Boolean required = false;

    @NotNull
    private boolean manageableByProject;

    private BillingCodeTypeParsedBy billingCodeTypeParsedBy;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(codeType);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCodeType)) return false;
        BillingCodeType that = (BillingCodeType) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(codeType, that.codeType);
        }
        return eb.isEquals();
    }
}
