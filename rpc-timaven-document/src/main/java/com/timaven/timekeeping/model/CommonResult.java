package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.timaven.timekeeping.model.dto.IziToastOptions;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Data
public class CommonResult {
    String result;
    List<IziToastOptions> iziToastOptions = new ArrayList<>();

    public void addIziToastOption(IziToastOptions options) {
        this.iziToastOptions.add(options);
    }

    public void addInfo(String message) {
        iziToastOptions.add(new IziToastOptions.IziToastOptionBuilder()
                .title("Info")
                .message(message)
                .position("topRight")
                .color("blue")
                .timeout(5000)
                .build()
        );
    }

    public void addSuccess(String message) {
        iziToastOptions.add(new IziToastOptions.IziToastOptionBuilder()
                .title("Success")
                .message(message)
                .position("topRight")
                .color("green")
                .timeout(5000)
                .build()
        );
    }

    public void addWarning(String message) {
        iziToastOptions.add(new IziToastOptions.IziToastOptionBuilder()
                .title("Warning")
                .message(message)
                .position("topRight")
                .color("yellow")
                .timeout(10000)
                .build()
        );
    }

    public void addError(String message) {
        iziToastOptions.add(new IziToastOptions.IziToastOptionBuilder()
                .title("Error")
                .message(message)
                .position("topRight")
                .timeout(10000)
                .color("red")
                .build()
        );
    }
}
