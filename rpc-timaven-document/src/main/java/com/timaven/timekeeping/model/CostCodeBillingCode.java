package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope =
        CostCodeBillingCode.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodeBillingCode {

    private Long id;

    private Long costCodeId;

    private Long billingCodeId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private CostCodeLog costCodeLog;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private BillingCode billingCode;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeId);
        hcb.append(billingCodeId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeBillingCode)) return false;
        CostCodeBillingCode that = (CostCodeBillingCode) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeId, that.costCodeId);
        eb.append(billingCodeId, that.billingCodeId);
        return eb.isEquals();
    }
}
