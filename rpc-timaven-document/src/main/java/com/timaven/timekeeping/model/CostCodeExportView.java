package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
public class CostCodeExportView {
    private Long rowNumber;
    private Long costCodeId;
    private String costCodeFull;
    private String description;
    private String costCode;
    private String jobNumber;
    private String subJob;
    private Long projectId;
    private Long billingCodeTypeId;
    LocalDate startDate;
    LocalDate endDate;
    private LocalDateTime createdAt;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeId);
        hcb.append(billingCodeTypeId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeExportView)) return false;
        CostCodeExportView that = (CostCodeExportView) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeId, that.costCodeId);
        eb.append(billingCodeTypeId, that.billingCodeTypeId);
        return eb.isEquals();
    }

}
