package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeLog.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodeLog implements Comparable<CostCodeLog> {
    private Long id;

    @NotNull
    private String costCodeFull;

    private String description;

    private Long projectId;

    private LocalDate startDate;

    private LocalDate endDate;

    @NotNull
    private boolean locked = false;

    private LocalDateTime createdAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeLog)) return false;
        CostCodeLog that = (CostCodeLog) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id && null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(costCodeFull, that.costCodeFull);
            eb.append(projectId, that.projectId);
            eb.append(startDate, that.startDate);
            eb.append(endDate, that.endDate);
        }
        return eb.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
        } else {
            hcb.append(costCodeFull);
            hcb.append(projectId);
            hcb.append(startDate);
            hcb.append(endDate);
        }
        return hcb.toHashCode();
    }

    @Override
    public int compareTo(CostCodeLog costCodeLog) {
        return Comparator.comparing(CostCodeLog::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(CostCodeLog::getCostCodeFull, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(CostCodeLog::getDescription, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, costCodeLog);
    }
}
