package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodePerc.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodePerc implements Comparable<CostCodePerc> {
    private Long id;

    @NotNull
    private String costCodeFull;

    private String purchaseOrder;

    private BigDecimal stHour;

    private BigDecimal otHour;

    private BigDecimal dtHour;

    private BigDecimal totalHour;

    @NotNull
    private Integer percentage;

    private AllocationTime allocationTime;

    private LocalDateTime createdAt;

    private List<CostCodeTagAssociation> costCodeTagAssociations;

    public String getCostCodeTag() {
        String tags = "";
        if (!CollectionUtils.isEmpty(costCodeTagAssociations)) {
            tags = costCodeTagAssociations.stream()
                    .map(t -> String.format("%s : %s", t.getTagType(), t.getCodeName()))
                    .collect(Collectors.joining(","));
        }
        if (StringUtils.hasText(tags)) return String.format("%s,%s", costCodeFull, tags);
        return costCodeFull;
    }

    public BigDecimal getStHour() {
        return stHour == null ? BigDecimal.ZERO : stHour;
    }

    public BigDecimal getOtHour() {
        return otHour == null ? BigDecimal.ZERO : otHour;
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? BigDecimal.ZERO : dtHour;
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? BigDecimal.ZERO : totalHour;
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodePerc)) return false;
        if (null != id) {
            CostCodePerc that = (CostCodePerc) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(CostCodePerc o) {
        return Comparator.comparing(CostCodePerc::getCostCodeFull, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(CostCodePerc::getCostCodeTag, Comparator.nullsFirst(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
