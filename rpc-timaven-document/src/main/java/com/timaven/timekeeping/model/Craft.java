package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
public class Craft implements Comparable<Craft>, Cloneable {

    private Long id;

    private String code;

    private Long projectId;

    private BigDecimal billableST;

    private BigDecimal billableOT;

    private BigDecimal billableDT;

    private String description;

    private BigDecimal perDiem;

    private Long perDiemId;

    private BigDecimal rigPay;

    private LocalDate startDate;

    private LocalDate endDate;

    private LocalDateTime createdAt;

    private String company;

    public BigDecimal getBillableST() {
        return billableST == null ? BigDecimal.ZERO : billableST;
    }

    public BigDecimal getBillableOT() {
        return billableOT == null ? BigDecimal.ZERO : billableOT;
    }

    public BigDecimal getBillableDT() {
        return billableDT == null ? BigDecimal.ZERO : billableDT;
    }

    public BigDecimal getPerDiem() {
        return perDiem == null ? BigDecimal.ZERO : perDiem;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(projectId);
            hcb.append(code);
            hcb.append(startDate);
            hcb.append(endDate);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Craft)) return false;
        Craft that = (Craft) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(projectId, that.projectId);
            eb.append(code, that.code);
            eb.append(startDate, that.startDate);
            eb.append(endDate, that.endDate);
        }
        return eb.isEquals();
    }

    @Override
    public int compareTo(Craft o) {
        return Comparator.comparing(Craft::getCode)
                .thenComparing(Craft::getDescription, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableST, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableOT, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableDT, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getPerDiem, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getRigPay, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getCompany, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
