package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeView {
    private Long id;
    private String empId;
    private String clientEmpId;
    private String craft;
    private BigDecimal baseST;
    private BigDecimal baseOT;
    private BigDecimal baseDT;
    private BigDecimal holidayRate;
    private BigDecimal sickLeaveRate;
    private BigDecimal travelRate;
    private BigDecimal vacationRate;
    private Long perDiemId;
    private String perDiemName;
    private String clientCraft;
    private String teamName;
    private String crew;
    private Long projectId;
    private String firstName;
    private String lastName;
    private String middleName;
    private String gender;
    private String company;
    private String badge;
    private String signInSheet;
    private String department;
    private String jobNumber;
    private LocalDate hiredAt;
    private LocalDate effectedOn;
    private LocalDate terminatedAt;
    private Boolean hasPerDiem = false;
    private Boolean hasRigPay = false;
    private String tags;
    private String shift;
    private LocalTime scheduleStart;
    private LocalTime scheduleEnd;
    private LocalTime lunchStart;
    private LocalTime lunchEnd;
    private Boolean isOffsite = false;
    private String activityCodes;

    public String getFullName() {
        if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
            return String.format("%s %s", firstName, lastName);
        } else if (StringUtils.hasText(firstName)) {
            return firstName;
        } else if (StringUtils.hasText(lastName)) {
            return lastName;
        }
        return "Unknown";
    }

}
