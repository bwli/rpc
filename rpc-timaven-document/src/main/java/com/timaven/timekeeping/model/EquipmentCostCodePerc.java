package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentCostCodePerc.class)
@Getter
@Setter
@NoArgsConstructor
public class EquipmentCostCodePerc implements Comparable<EquipmentCostCodePerc> {
    private Long id;

    private String costCodeFull;

    private BigDecimal totalHour;

    private BigDecimal totalCharge;

    private EquipmentAllocationTime equipmentAllocationTime;

    private Long allocationTimeId;

    private LocalDateTime createdAt;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentCostCodePerc)) return false;
        if (null != id) {
            EquipmentCostCodePerc that = (EquipmentCostCodePerc) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(EquipmentCostCodePerc o) {
        return Comparator.comparing(EquipmentCostCodePerc::getCostCodeFull, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(EquipmentCostCodePerc::getId)
                .compare(this, o);
    }
}
