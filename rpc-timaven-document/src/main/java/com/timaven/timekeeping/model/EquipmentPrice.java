package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class EquipmentPrice {

    private Long id;

    @NotNull
    private String equipmentClass;

    private String description;

    @NotNull
    private Long projectId;

    private BigDecimal hourlyRate;

    private BigDecimal dailyRate;

    private BigDecimal weeklyRate;

    private BigDecimal monthlyRate;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(projectId);
        hcb.append(equipmentClass);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentPrice)) return false;
        EquipmentPrice that = (EquipmentPrice) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(projectId, that.projectId);
        eb.append(equipmentClass, that.equipmentClass);
        return eb.isEquals();
    }
}
