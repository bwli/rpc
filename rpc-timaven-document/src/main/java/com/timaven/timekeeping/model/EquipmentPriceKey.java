package com.timaven.timekeeping.model;

import lombok.Data;

@Data
public class EquipmentPriceKey {
    private Long projectId;
    private String equipmentClass;

    public EquipmentPriceKey(Long projectId, String equipmentClass) {
        this.projectId = projectId;
        this.equipmentClass = equipmentClass;
    }
}
