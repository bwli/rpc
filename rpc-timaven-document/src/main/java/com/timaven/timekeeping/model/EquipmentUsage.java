package com.timaven.timekeeping.model;

import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class EquipmentUsage {

    private Long id;

    private Long equipmentId;

    private Equipment equipment;

    private LocalDate startDate;

    private LocalDate endDate;

    private Long startedBy;

    private Long endedBy;

    private Long projectId;

    private Project project;

    private BigDecimal dailyRate;

    private BigDecimal weeklyRate;

    private BigDecimal monthlyRate;

    private boolean active = true;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentUsage)) return false;
        EquipmentUsage that = (EquipmentUsage) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentId, that.equipmentId);
        return eb.isEquals();
    }
}
