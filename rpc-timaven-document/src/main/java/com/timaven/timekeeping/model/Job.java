package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Job {
    private Long id;

    @NotNull
    private Long jobId;

    @NotNull
    private Long projectId;

    private String address;

    private Boolean isLs;

    public void merge(Job job) {
        if (job == null) return;
        this.address = job.address;
        this.isLs = job.isLs;
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(jobId);
            hcb.append(projectId);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Job)) return false;
        if (null != id) {
            Job that = (Job) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(jobId, that.jobId);
            eb.append(projectId, that.projectId);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
