package com.timaven.timekeeping.model;

import com.timaven.timekeeping.model.enums.PerDiemMode;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@NoArgsConstructor
public class PerDiem {

    private Long id;

    private String name;

    private PerDiemMode perDiemMode = PerDiemMode.PAY_AS_YOU_GO;

    private String costCode;

    private Integer dailyThreshold;

    private Integer weeklyThreshold;

    private Long projectId;

    private Boolean active = true;

    private BigDecimal overwriteAmount;
}
