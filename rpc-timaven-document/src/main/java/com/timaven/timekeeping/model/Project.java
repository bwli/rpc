package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Project implements Comparable<Project>, Serializable {
    private Long id;

    private String description;

    private String jobNumber;

    private String subJob;

    private String clientName;

    private String jobSubJob;

    @NotNull
    private Boolean active = true;

    @NotNull
    private boolean deleted = false;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonManagedReference("project")
    private List<UserProject> userProjects;

    private Set<Long> userIds;

    public Set<Long> getUserIds() {
        if (!CollectionUtils.isEmpty(userProjects)) {
            userIds = userProjects.stream().map(UserProject::getUserId).collect(Collectors.toSet());
        }
        return userIds == null ? new HashSet<>() : userIds;
    }

    public List<UserProject> getUserProjects() {
        return userProjects == null ? new ArrayList<>() : userProjects;
    }

    public String getJobSubJob() {
        if (StringUtils.hasText(subJob) && !StringUtils.hasText(jobNumber)) {
            return subJob;
        } else if (!StringUtils.hasText(subJob) && StringUtils.hasText(jobNumber)) {
            return jobNumber;
        } else if (StringUtils.hasText(subJob) && StringUtils.hasText(jobNumber)) {
            return String.format("%s-%s", jobNumber, subJob);
        }
        return description;
    }

    @Override
    public int compareTo(Project project) {
        return Comparator.comparing(Project::getJobNumber, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Project::getSubJob,Comparator.nullsFirst(Comparator.naturalOrder()))
                .compare(this, project);
    }
}
