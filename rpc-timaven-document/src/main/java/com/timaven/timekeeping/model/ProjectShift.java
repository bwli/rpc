package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@EqualsAndHashCode(of = {"projectId", "shiftId"})
@NoArgsConstructor
public class ProjectShift {

    private Long id;

    private Long projectId;

    private Long shiftId;
}
