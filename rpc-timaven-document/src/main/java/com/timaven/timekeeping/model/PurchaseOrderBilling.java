package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class PurchaseOrderBilling {
    private Long id;
    private String purchaseOrderNumber;
    private String billingNumber;
    private String invoiceNumber;
    private BigDecimal freightShipping;
    private String otherCharge;
    private BigDecimal otherChargeAmount;
    private Set<PurchaseOrderBillingDetail> purchaseOrderBillingDetails = new HashSet<>();
}
