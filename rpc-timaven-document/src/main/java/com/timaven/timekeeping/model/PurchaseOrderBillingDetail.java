package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class PurchaseOrderBillingDetail {
    private Long id;
    private String purchaseOrderNumber;
    private BigDecimal unitCost;
    private String part;
    private Integer lineNumber;
    private BigDecimal billingQuantity;
    private BigDecimal markupQuantity;
    private BigDecimal markupAmount;
    private Long billingId;
    private BigDecimal tax;
    // TODO
//    private PurchaseOrderBilling purchaseOrderBilling;
}
