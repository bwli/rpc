package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
public class PurchaseOrderGroup {
    private Long id;
    @NotNull
    private String purchaseOrderNumber;
    private String comment;
    private Boolean flag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrderGroup that = (PurchaseOrderGroup) o;
        if (purchaseOrderNumber != null && that.purchaseOrderNumber != null)
            return purchaseOrderNumber.equals(that.getPurchaseOrderNumber());
        return false;
    }

    @Override
    public int hashCode() {
        if (purchaseOrderNumber != null) return Objects.hash(purchaseOrderNumber);
        return super.hashCode();
    }
}
