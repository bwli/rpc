package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;


@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Rule.class)
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Rule implements Serializable {

    private Long id;

    @NotNull
    private int roundingMinutes;

    @NotNull
    private String roundingMode = "ROUNDING_TO_NEAREST_NTH_MINUTE";

    @NotNull
    private int graceIn;

    @NotNull
    private int graceOut;

    private String description;

    private Long projectId;

    private Integer rigPayThreshold;

    private DayOfWeek weekEndDay = DayOfWeek.SUNDAY;

    private BigDecimal equipmentWorkdayHour = BigDecimal.valueOf(8);

    private boolean equipmentExcludeWeekend = true;

    private String equipmentCostCode;

    private String stCostCode;

    private String otCostCode;

    private String dtCostCode;

    private String sickCostCode;

    private String holidayCostCode;

    private String vacationCostCode;

    private String otherCostCode;

    private int dayOffThreshold = 13;

    private Long perDiemId;

    @NotNull
    private boolean taxablePerDiem = false;

    public LocalDate getStartOfWeek(LocalDate dateOfService) {
        LocalDate startOfWeek = dateOfService.with(weekEndDay.plus(1));
        if (startOfWeek.isAfter(dateOfService)) startOfWeek = startOfWeek.minusWeeks(1);
        return startOfWeek;
    }

    public LocalDate getEndOfWeek(LocalDate dateOfService) {
        LocalDate endOfWeek = dateOfService.with(weekEndDay);
        if (endOfWeek.isBefore(dateOfService)) endOfWeek = endOfWeek.plusWeeks(1);
        return endOfWeek;
    }

    public LocalDate getPreviousEndOfWeek(LocalDate dateOfService) {
        LocalDate endOfWeek = dateOfService.with(weekEndDay);
        if (endOfWeek.isAfter(dateOfService)) endOfWeek = endOfWeek.minusWeeks(1);
        return endOfWeek;
    }
}
