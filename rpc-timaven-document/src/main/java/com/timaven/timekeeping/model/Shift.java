package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "projectId"})
public class Shift {

    private Long id;

    private String name;

    private LocalTime scheduleStart;

    private LocalTime scheduleEnd;

    private LocalTime breakStart;

    private LocalTime breakEnd;

    private int paidBreakDuration;

    private Long projectId;
}
