package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class StageRoster {

    private Long id;

    private Long submissionId;

    private String badge;

    private String name;

    private String employeeId;

    private String clientEmpId;

    private String company;

    private String team;

    private String crew;

    private String craftCode;

    private BigDecimal baseST;

    private BigDecimal baseOT;

    private BigDecimal baseDT;

    private BigDecimal holidayRate;

    private BigDecimal sickLeaveRate;

    private BigDecimal travelRate;

    private BigDecimal vacationRate;

    private String clientCraft;

    private Boolean hasPerDiem = false;

    private Boolean hasRigPay = false;

    private LocalDate hiredAt;

    private LocalDate terminatedAt;

    private String shift;

    private LocalTime scheduleStart;

    private LocalTime scheduleEnd;

    private LocalTime lunchStart;

    private LocalTime lunchEnd;

    private String jobNumber;

    private String signInSheet;
}
