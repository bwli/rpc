package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class StageSubmission {

    private Long id;
    private Long projectId;
    @NotNull
    private String username;
    @NotNull
    private LocalDate effectiveOn;
    private Type type;
    private Status status;
    private LocalDateTime processedAt;
    private LocalDateTime createdAt;

    public enum Type {
        Roster,
        Team,
        CostCode,
        TeamCC,
        Crafts,
        Unknown,
    }

    public enum Status {
        Pending,
        Imported,
        Ignored,
    }
}
