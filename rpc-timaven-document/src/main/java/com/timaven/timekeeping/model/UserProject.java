package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Comparator;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
public class UserProject implements Comparable<UserProject> {

    private Long id;

    private Long userId;

    @JsonBackReference("userProjects")
    private User user;

    private Long projectId;

    @JsonBackReference("project")
    private Project project;

    public UserProject(Long userId, Long projectId) {
        this.userId = userId;
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProject that = (UserProject) o;
        if (id != null && that.id != null) {
            return Objects.equals(id, that.id);
        } else if (userId != null && that.userId != null
                && projectId != null && that.projectId != null) {
            return Objects.equals(userId, that.userId) &&
                    Objects.equals(projectId, that.projectId);
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        if (userId != null && projectId != null) return Objects.hash(userId, projectId);
        return super.hashCode();
    }

    @Override
    public int compareTo(UserProject userProject) {
        return Comparator.comparing(UserProject::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(UserProject::getUserId, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, userProject);
    }
}
