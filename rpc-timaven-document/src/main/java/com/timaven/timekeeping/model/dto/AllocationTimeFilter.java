package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
public class AllocationTimeFilter {
    private final Set<LocalDate> dates;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final LocalDate payrollStartDate;
    private final LocalDate payrollEndDate;
    private final Boolean isAllProjects;
    private final String teamName;
    private final Long projectId;
    private final AllocationSubmissionStatus status;
    private final AllocationSubmissionStatus notStatus;
    private final List<String> includes;
    private final Set<String> empIds;
    private final String shift;
    private final Boolean excludePerDiem;

    public AllocationTimeFilter(AllocationTimeFilterBuilder builder) {
        this.dates = builder.dates;
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
        this.payrollStartDate = builder.payrollStartDate;
        this.payrollEndDate = builder.payrollEndDate;
        this.isAllProjects = builder.isAllProjects;
        this.projectId = builder.projectId;
        this.status = builder.status;
        this.notStatus = builder.notStatus;
        this.includes = builder.includes;
        this.empIds = builder.empIds;
        this.shift = builder.shift;
        this.teamName = builder.teamName;
        this.excludePerDiem = builder.excludePerDiem;
    }

    public static class AllocationTimeFilterBuilder {
        private Set<LocalDate> dates;
        private LocalDate startDate;
        private LocalDate endDate;
        private LocalDate payrollStartDate;
        private LocalDate payrollEndDate;
        private Boolean isAllProjects;
        private Long projectId;
        private AllocationSubmissionStatus status;
        private AllocationSubmissionStatus notStatus;
        private List<String> includes;
        private Set<String> empIds;
        private String shift;
        private String teamName;
        private Boolean excludePerDiem;

        public AllocationTimeFilterBuilder dates(Set<LocalDate> dates) {
            this.dates = dates;
            return this;
        }

        public AllocationTimeFilterBuilder startDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public AllocationTimeFilterBuilder endDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public AllocationTimeFilterBuilder payrollStartDate(LocalDate payrollStartDate) {
            this.payrollStartDate = payrollStartDate;
            return this;
        }

        public AllocationTimeFilterBuilder payrollEndDate(LocalDate payrollEndDate) {
            this.payrollEndDate = payrollEndDate;
            return this;
        }

        public AllocationTimeFilterBuilder isAllProject(Boolean isAllProjects) {
            this.isAllProjects = isAllProjects;
            return this;
        }

        public AllocationTimeFilterBuilder projectId(Long projectId) {
            this.projectId = projectId;
            return this;
        }

        public AllocationTimeFilterBuilder status(AllocationSubmissionStatus status) {
            this.status = status;
            return this;
        }

        public AllocationTimeFilterBuilder notStatus(AllocationSubmissionStatus notStatus) {
            this.notStatus = notStatus;
            return this;
        }

        public AllocationTimeFilterBuilder includes(List<String> includes) {
            this.includes = includes;
            return this;
        }

        public AllocationTimeFilterBuilder empIds(Set<String> empIds) {
            this.empIds = empIds;
            return this;
        }

        public AllocationTimeFilterBuilder shift(String shift) {
            this.shift = shift;
            return this;
        }

        public AllocationTimeFilterBuilder teamName(String teamName) {
            this.teamName = teamName;
            return this;
        }

        public AllocationTimeFilterBuilder excludePerDiem(Boolean excludePerDiem) {
            this.excludePerDiem = excludePerDiem;
            return this;
        }

        public AllocationTimeFilter build() {
            return new AllocationTimeFilter(this);
        }

    }
}
