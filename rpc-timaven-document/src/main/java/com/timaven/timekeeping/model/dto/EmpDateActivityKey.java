package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class EmpDateActivityKey {
    @NotNull
    private Long allocationTimeId;
    @NotNull
    private Long activityId;

    public EmpDateActivityKey(Long allocationTimeId, Long activityId){
        this.allocationTimeId = allocationTimeId;
        this.activityId = activityId;
    }
}
