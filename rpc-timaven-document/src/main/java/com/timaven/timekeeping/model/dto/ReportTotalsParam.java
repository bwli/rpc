package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ReportTotalsParam {
    private Set<Long> projectIds;
    private List<String> groupingBy;
    private String startDate;
    private String endDate;
    private String shift;
    Boolean approvedOnly;
    Boolean hideDetail;
}
