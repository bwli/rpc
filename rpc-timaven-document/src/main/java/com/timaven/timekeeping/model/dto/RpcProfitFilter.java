package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RpcProfitFilter {
    private Long projectId;
    private LocalDate startDate;
    private LocalDate endDate;

    public RpcProfitFilter(){ }

    public RpcProfitFilter(Long projectId,LocalDate startDate,LocalDate endDate){
        this.projectId = projectId;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
