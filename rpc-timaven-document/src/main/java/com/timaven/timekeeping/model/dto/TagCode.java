package com.timaven.timekeeping.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TagCode {
    private String tag;
    private String tagDescription;
    private String code;
    private String codeDescription;
}
