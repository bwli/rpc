package com.timaven.timekeeping.model.dto;

import lombok.Data;

@Data
public class TenantDto {
    private String name;
    private String host;
    private String s3Logo;
    private String companyName;
    private String companyAddress;
    private String buckName;
    private String dumpBuckName;
    private Long maxUploadSize;
}
