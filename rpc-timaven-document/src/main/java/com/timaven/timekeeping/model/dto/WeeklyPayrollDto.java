package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class WeeklyPayrollDto {
    private List<Long> ids;
    private LocalDate weekEndDate;
}
