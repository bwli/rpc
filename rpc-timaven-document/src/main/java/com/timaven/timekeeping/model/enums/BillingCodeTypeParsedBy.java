package com.timaven.timekeeping.model.enums;

public enum BillingCodeTypeParsedBy {
    CODE_NAME,
    CLIENT_ALIAS;
}
