package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.Set;

public interface ExportService {
    ResponseEntity<InputStreamResource> exportSignInSheet(Long projectId, String signInSheet, LocalDate dateOfService);

    ResponseEntity<InputStreamResource> exportTimesheet(Long projectId, String team, LocalDate dateOfService);

    ResponseEntity<InputStreamResource> generateOtReport(Long projectId, LocalDate weekEndDate, LocalDate payrollDate);

    ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type);

    ResponseEntity<InputStreamResource> exportPayrollReport(WeeklyPayrollDto weeklyPayrollDto);

    ResponseEntity<InputStreamResource> reportLabor(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate);

    ResponseEntity<InputStreamResource> reportDataExport(Long projectId, LocalDate startDate, LocalDate endDate, Boolean approvedOnly);

    // TM-421
    ResponseEntity<InputStreamResource> exportExcelCraftView(Long projectId);

    ResponseEntity<InputStreamResource> reportPurchaseOrderBillings(Long id);

    ResponseEntity<InputStreamResource> reportEquipemtByEquipmentOwnershipType(Long projectId, LocalDate dateOfService, LocalDate payrollDate, Integer type);

    ResponseEntity<InputStreamResource> reportEquipmentBilling(Long projectId, LocalDate startDate, LocalDate endDate);

    ResponseEntity<InputStreamResource> reportEquipmentLog(Long projectId);

    ResponseEntity<InputStreamResource> reportCostCode(Long projectId);

    ResponseEntity<InputStreamResource> reportEmployee(Long projectId, Boolean showAll);

    ResponseEntity<InputStreamResource> reportEquipment(Boolean isAll);

    ResponseEntity<InputStreamResource> reportAbsentee(LocalDate dateOfService, Set<Long> employeeIds);

    ResponseEntity<InputStreamResource> reportWhoisin(Long projectId, LocalDate dateOfService, Set<Long> employeeIds);

    ResponseEntity<InputStreamResource> reportRpcProfitDetails(Long projectId, LocalDate startDate, LocalDate endDate);
}
