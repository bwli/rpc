package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface ReportCostCodeService {
    ResponseEntity<InputStreamResource> reportCostCode(Long projectId);
}
