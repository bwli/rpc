package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface ReportEmployeeService {
    ResponseEntity<InputStreamResource> reportEmployee(Long projectId, Boolean showAll);
}
