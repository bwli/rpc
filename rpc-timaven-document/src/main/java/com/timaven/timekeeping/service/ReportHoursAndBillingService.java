package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface ReportHoursAndBillingService {
    ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type);
}
