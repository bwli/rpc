package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Set;

public interface ReportLaborTimeSummaryService {
    ResponseEntity<InputStreamResource> reportLaborTimeSummary(Set<Long> projectIds, LocalDate weekEndDate, Boolean approvedOnly);
}
