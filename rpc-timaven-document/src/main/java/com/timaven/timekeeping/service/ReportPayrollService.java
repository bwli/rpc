package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface ReportPayrollService {
    ResponseEntity<InputStreamResource> exportPayrollReport(WeeklyPayrollDto weeklyPayrollDto);
}
