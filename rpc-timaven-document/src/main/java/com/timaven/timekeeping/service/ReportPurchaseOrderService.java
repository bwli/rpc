package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface ReportPurchaseOrderService {
    ResponseEntity<InputStreamResource> reportPurchaseOrderBillings(Long id);
}
