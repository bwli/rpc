package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Set;

public interface ReportWhoisinService {
    ResponseEntity<InputStreamResource> reportWhoisin(Long projectId, LocalDate dateOfService, Set<Long> empIds);
}
