package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface SignInSheetService {
    ResponseEntity<InputStreamResource> exportSignInSheet1(Long projectId, String signInSheet, LocalDate dateOfService);
    ResponseEntity<InputStreamResource> exportSignInSheet2(Long projectId, String signInSheet, LocalDate dateOfService);
}
