package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.StageCraft;
import com.timaven.timekeeping.model.StageRoster;
import com.timaven.timekeeping.model.StageSubmission;
import com.timaven.timekeeping.model.StageTeamCostCode;
import reactor.core.publisher.Mono;

import java.util.List;

public interface StageService {
    Mono<List<StageRoster>> saveRoster(List<StageRoster> rosters, StageSubmission submission);

    Mono<List<StageTeamCostCode>> saveTeamCostCode(List<StageTeamCostCode> stageTeamCostCodes, StageSubmission submission);

    Mono<List<StageCraft>> saveCrafts(List<StageCraft> stageCrafts, StageSubmission submission);
}
