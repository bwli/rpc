package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface TimesheetService {
    ResponseEntity<InputStreamResource> exportTimesheet1(Long projectId, String team, LocalDate dateOfService);
}
