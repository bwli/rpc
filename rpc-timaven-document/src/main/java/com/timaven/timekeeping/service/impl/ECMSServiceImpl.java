package com.timaven.timekeeping.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.model.dto.ECMSMapping;
import com.timaven.timekeeping.service.ECMSService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.timaven.timekeeping.util.ExcelUtil.getStringValue;
import static com.timaven.timekeeping.util.ExcelUtil.isRowEmpty;

@Profile("!dev")
@Service
public class ECMSServiceImpl implements ECMSService {

    @Value("${aws.s3.access-key}")
    protected String s3_access_key;

    @Value("${aws.s3.secret-key}")
    protected String s3_secret_key;

    @Value("${aws.s3.buck-name}")
    protected String buck_name;

    private ECMSMapping ecmsMapping;

    @Autowired
    public ECMSServiceImpl() {
    }

    @PostConstruct
    public void initMethod() {
        AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
        String ecmsEmplClassTypeKey = "eCMS Employee Class and Type table.xlsx";
        try {
            Workbook workbook = new XSSFWorkbook(s3client.getObject(buck_name, ecmsEmplClassTypeKey).getObjectContent());
            Sheet sheet = workbook.getSheetAt(0);

            HashMap<Integer, String> indexTitleMap = new HashMap<>();
            Iterator<Row> rowIterator = sheet.rowIterator();
            Map<String, String> craftCodeEmplClassMap = new HashMap<>();
            Map<String, String> craftCodeEmplTypeMap = new HashMap<>();
            String[] values = new String[3];
            int rowNum = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (isRowEmpty(row)) {
                    break;
                }
                for (Cell cell : row) {
                    String title;
                    int cIndex = cell.getColumnIndex();
                    if (rowNum == 0) {
                        // Header
                        title = getStringValue(cell).toLowerCase().trim();
                        if (StringUtils.hasText(title)) {
                            indexTitleMap.put(cIndex, title);
                        }
                    } else {
                        if (!indexTitleMap.containsKey(cIndex)) {
                            continue;
                        }
                        title = indexTitleMap.get(cIndex);
                        switch (title) {
                            case "classdesc":
                                values[0] = getStringValue(cell);
                                break;
                            case "emplclass":
                                values[1] = getStringValue(cell);
                                break;
                            case "empltype":
                                values[2] = getStringValue(cell);
                                break;
                            default:
                                break;
                        }
                    }
                }
                if (rowNum != 0) {
                    if (StringUtils.hasText(values[0]) && StringUtils.hasText(values[1]) && StringUtils.hasText(values[2])) {
                        craftCodeEmplClassMap.put(values[0], values[1]);
                        craftCodeEmplTypeMap.put(values[0], values[2]);
                    } else {
                        System.out.println("Sheet: Roster. Row: " + (rowNum + 1) +
                                ". misses values. ");
                    }
                }
                rowNum++;
            }
            this.ecmsMapping = new ECMSMapping();
            this.ecmsMapping.setCraftCodeEmplClassMap(craftCodeEmplClassMap);
            this.ecmsMapping.setCraftCodeEmplTypeMap(craftCodeEmplTypeMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ECMSMapping getECMSMapping() {
        return ecmsMapping;
    }
}
