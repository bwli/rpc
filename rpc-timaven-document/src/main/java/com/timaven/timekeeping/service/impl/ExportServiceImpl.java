package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.dto.CraftDto;
import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import com.timaven.timekeeping.service.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExportServiceImpl implements ExportService {

    Map<String, MultipleTenantService> multipleTenantServiceMap;
    private final ReportPayrollService reportPayrollService;
    private final ReportDataExportService reportDataExportService;
    private final ProviderAPI providerAPI;
    private final ExcelService excelService;
    private final ReportPurchaseOrderService reportPurchaseOrderService;
    private final ReportEquipmentService reportEquipmentService;
    private final ReportEquipmentBillingService reportEquipmentBillingService;
    private final ReportEquipmentLogService reportEquipmentLogService;
    private final ReportEmployeeService reportEmployeeService;
    private final ReportCostCodeService reportCostCodeService;
    private final ReportAbsenteeService reportAbsenteeService;
    private final ReportWhoisinService reportWhoisinService;
    private final ReportRpcProfitDetailService reportRpcProfitDetailService;

    @Autowired
    public ExportServiceImpl(List<MultipleTenantService> multipleTenantServices, ReportPayrollService reportPayrollService,
                             ProviderAPI providerAPI, ExcelService excelService,
                             ReportDataExportService reportDataExportService, ReportPurchaseOrderService reportPurchaseOrderService,
                             ReportEquipmentService reportEquipmentService, ReportEquipmentBillingService reportEquipmentBillingService,
                             ReportEquipmentLogService reportEquipmentLogService, ReportEmployeeService reportEmployeeService,
                             ReportCostCodeService reportCostCodeService, ReportAbsenteeService reportAbsenteeService,
                             ReportWhoisinService reportWhoisinService,ReportRpcProfitDetailService reportRpcProfitDetailService) {
        this.multipleTenantServiceMap = multipleTenantServices.stream()
                .collect(Collectors.toMap(MultipleTenantService::getTenantName, Function.identity()));
        this.reportPayrollService = reportPayrollService;
        this.reportDataExportService = reportDataExportService;
        this.providerAPI = providerAPI;
        this.excelService = excelService;
        this.reportPurchaseOrderService = reportPurchaseOrderService;
        this.reportEquipmentService = reportEquipmentService;
        this.reportEquipmentBillingService = reportEquipmentBillingService;
        this.reportEquipmentLogService = reportEquipmentLogService;
        this.reportEmployeeService = reportEmployeeService;
        this.reportCostCodeService = reportCostCodeService;
        this.reportAbsenteeService = reportAbsenteeService;
        this.reportWhoisinService = reportWhoisinService;
        this.reportRpcProfitDetailService = reportRpcProfitDetailService;
    }

    @Override
    public ResponseEntity<InputStreamResource> exportSignInSheet(Long projectId, String signInSheet, LocalDate dateOfService) {
        return multipleTenantServiceMap.get(ThreadTenantStorage.getTenantId()).exportSignInSheet(projectId, signInSheet, dateOfService);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportTimesheet(Long projectId, String team, LocalDate dateOfService) {
        return multipleTenantServiceMap.get(ThreadTenantStorage.getTenantId()).exportTimesheet(projectId, team, dateOfService);
    }

    @Override
    public ResponseEntity<InputStreamResource> generateOtReport(Long projectId, LocalDate weekEndDate, LocalDate payrollDate) {
        return multipleTenantServiceMap.get(ThreadTenantStorage.getTenantId()).generateOtReport(projectId, weekEndDate, payrollDate);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type) {
        return multipleTenantServiceMap.get(ThreadTenantStorage.getTenantId()).reportHourAndBilling(param, type);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportPayrollReport(WeeklyPayrollDto weeklyPayrollDto) {
        return reportPayrollService.exportPayrollReport(weeklyPayrollDto);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportLabor(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate) {
        return multipleTenantServiceMap.get(ThreadTenantStorage.getTenantId()).reportLabor(projectId, teamName, dateOfService, payrollDate);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportDataExport(Long projectId, LocalDate startDate, LocalDate endDate, Boolean approvedOnly) {
        return reportDataExportService.reportDataExport(projectId, startDate, endDate, approvedOnly);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportExcelCraftView(Long projectId) {
        List<CraftDto> craftList = providerAPI.findCraftsByProjectId(projectId, LocalDate.now());
        if (craftList == null)
            return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();

        XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Crafts_v2");

        XSSFCreationHelper creationHelper = workbook.getCreationHelper();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-MM-dd"));

        AtomicInteger counter = new AtomicInteger();
        Map<Integer, Map<String, String>> guideMap = Arrays.stream(new String[][]{
            { "Craft Code",    "code"        },
            { "Description",   "description" },
            { "Billable ST",   "billableST"  },
            { "Billable OT",   "billableOT"  },
            { "Billable DT",   "billableDT"  },
            { "Per Diem Rule", "perDiemName" },
            { "Per Diem Amt",  "perDiemAmt"  },
            { "Rig Pay",       "rigPay"      },
            { "Company",       "company"     },
            { "Start Date",    "startDate"   },
            { "End Date",      "endDate"     }
        }).collect(
            LinkedHashMap::new,
            (map, arr) -> {
                Integer k = counter.getAndIncrement();
                Map<String, String> nested = map.computeIfAbsent(k, key -> new HashMap<>());
                nested.put("columnName", arr[0]);
                nested.put("propertyName", arr[1]);
            },
            (map, otherMap) -> {}
        );

        // Fill title
        Row titleRow = sheet.createRow(0);
        for (Integer idx : guideMap.keySet()) {
            Cell cell = titleRow.createCell(idx);
            cell.setCellValue(guideMap.get(idx).get("columnName"));
        }

        // Fill body
        for (int i = 0; i < craftList.size(); i++) {
            int rowIndex = i + 1;
            Row row = sheet.createRow(rowIndex);
            BeanWrapper bw = new BeanWrapperImpl(craftList.get(i));
            for (Integer index : guideMap.keySet()) {
                Cell cell = row.createCell(index);
                String propertyName = guideMap.get(index).get("propertyName");
                Object value = bw.getPropertyValue(propertyName);
                if (value instanceof LocalDate) {
                    cell.setCellValue((LocalDate) value);
                    cell.setCellStyle(dateCellStyle);
                } else if (value != null) {
                    cell.setCellValue(value.toString());
                }
            }
        }

        return excelService.downloadExcel("Labor Rates", workbook);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportPurchaseOrderBillings(Long id) {
        return reportPurchaseOrderService.reportPurchaseOrderBillings(id);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipemtByEquipmentOwnershipType(Long projectId, LocalDate dateOfService, LocalDate payrollDate, Integer type) {
        return reportEquipmentService.reportEquipemtByEquipmentOwnershipType(projectId, dateOfService, payrollDate, type);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipmentBilling(Long projectId, LocalDate startDate, LocalDate endDate) {
        return reportEquipmentBillingService.reportEquipemtBilling(projectId, startDate, endDate);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipmentLog(Long projectId) {
        return reportEquipmentLogService.reportEquipemtTemplateLog(projectId);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportCostCode(Long projectId) {
        return reportCostCodeService.reportCostCode(projectId);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEmployee(Long projectId, Boolean showAll) {
        return reportEmployeeService.reportEmployee(projectId, showAll);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipment(Boolean isAll) {
        isAll = isAll == null ? false : isAll;
        return reportEquipmentService.reportEquipemt(isAll);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportAbsentee(LocalDate dateOfService, Set<Long> employeeIds) {
        return reportAbsenteeService.reportAbsentee(dateOfService, employeeIds);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportWhoisin(Long projectId, LocalDate dateOfService, Set<Long> employeeIds) {
        return reportWhoisinService.reportWhoisin(projectId, dateOfService, employeeIds);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportRpcProfitDetails(Long projectId, LocalDate startDate, LocalDate endDate) {
        return reportRpcProfitDetailService.reportRpcProfitDetails(projectId, startDate, endDate);
    }
}
