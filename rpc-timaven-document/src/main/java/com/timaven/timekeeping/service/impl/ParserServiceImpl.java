package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.lang.RosterNotFoundException;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.service.ParserService;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.stream.Collectors.toMap;

@SuppressWarnings("Duplicates")
@Service
public class ParserServiceImpl implements ParserService {

    private final KieContainer mKieContainer;

    private final ProviderAPI providerAPI;

    @Autowired
    public ParserServiceImpl(KieContainer kieContainer,
                             ProviderAPI providerAPI) {
        mKieContainer = kieContainer;
        this.providerAPI = providerAPI;
    }

    private static Duration overlap(LocalDateTime scheduleStart, LocalDateTime scheduleEnd, LocalDateTime eventStart,
                                    LocalDateTime eventEnd) {
        if (eventEnd.isBefore(scheduleStart) || scheduleEnd.isBefore(eventStart)) {
            // no overlap
            return Duration.ZERO;
        }
        LocalDateTime overlapStart = eventStart.isBefore(scheduleStart)
                ? scheduleStart : eventStart;
        LocalDateTime overlapEnd = eventEnd.isBefore(scheduleEnd)
                ? eventEnd : scheduleEnd;
        return Duration.between(overlapStart, overlapEnd);
    }

    @Override
    public TransSubmission saveTransSubmission(TransSubmission transSubmission) {
        return providerAPI.saveTransSubmission(transSubmission);
    }

    @Override
    public void parsePunchRecord(Collection<EmpPunchRecords> empPunchRecords, Long projectId) throws Exception {

        Mono<Rule> ruleMono = providerAPI.findRuleByProjectId(projectId);
        Mono<Shift> shiftMono = providerAPI.findProjectShiftByProjectId(projectId)
                .flatMap(projectShift -> Mono.deferContextual(Mono::just)
                        .flatMap(ctx -> {
                            ProviderAPI api = ctx.get(ProviderAPI.class);
                            return api.findShiftById(projectShift.getShiftId())
                                    .switchIfEmpty(Mono.empty());
                        }))
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())));
        Rule rule = null;
        Shift shift = null;
        Tuple2<Rule, Shift> tuple = Mono.zip(ruleMono, shiftMono).block();
        if (tuple != null) {
            rule = tuple.getT1();
            shift = tuple.getT2();
        }

        final int roundingMinutes;
        final String roundingMode;
        final int graceIn;
        final int graceOut;
        final Duration paidBreakDuration;

        assert (rule != null);
        roundingMinutes = rule.getRoundingMinutes();
        roundingMode = rule.getRoundingMode();
        graceIn = rule.getGraceIn();
        graceOut = rule.getGraceOut();
        paidBreakDuration = Duration.ofMinutes(shift.getPaidBreakDuration());


        int minutesToBePlus = roundingMode.equals("ROUNDING_TO_NEAREST_NTH_MINUTE") ?
                roundingMinutes / 2 : roundingMode.equals("ROUNDING_UP") ? roundingMinutes : 0;

//        Set<EmpPunchRecords> empPunchRecords = new HashSet<>(empPunchRecords.values());

        Map<LocalDate, List<EmpPunchRecords>> dateEmpPunchRecordMap = empPunchRecords.stream()
                .collect(Collectors.groupingBy(EmpPunchRecords::getDate));

        Set<Employee> validEmployees = new HashSet<>();
        for (Map.Entry<LocalDate, List<EmpPunchRecords>> entry : dateEmpPunchRecordMap.entrySet()) {
            LocalDate dateOfService = entry.getKey();
            Map<String, EmpPunchRecords> empIdEmpPunchRecordsMap = entry.getValue().stream()
                    .filter(e -> StringUtils.hasText(e.getEmpId()))
                    .collect(Collectors.toMap(EmpPunchRecords::getEmpId, Function.identity()));
            if (!CollectionUtils.isEmpty(empIdEmpPunchRecordsMap)) {
                Set<String> empIds = empIdEmpPunchRecordsMap.keySet();
                EmployeeFilter filter = new EmployeeFilter();
                filter.setEmpIds(empIds);
                filter.setProjectId(projectId);
                filter.setStartDate(dateOfService);
                List<Employee> employees = providerAPI.getEmployees(filter).block();
                if (employees != null) {
                    for (Employee employee : employees) {
                        EmpPunchRecords punchRecords = empIdEmpPunchRecordsMap.get(employee.getEmpId());
                        punchRecords.setTeamName(employee.getTeamName());
                        punchRecords.setFirstName(employee.getFirstName());
                        punchRecords.setLastName(employee.getLastName());
                    }
                    validEmployees.addAll(employees);
                }
            }
            Map<String, EmpPunchRecords> badgeEmpPunchRecordsMap = entry.getValue().stream()
                    .filter(e -> StringUtils.hasText(e.getBadge()))
                    .collect(Collectors.toMap(EmpPunchRecords::getBadge, Function.identity()));
            if (!CollectionUtils.isEmpty(badgeEmpPunchRecordsMap)) {
                Set<String> badges = badgeEmpPunchRecordsMap.keySet();
                EmployeeFilter filter = new EmployeeFilter();
                filter.setBadges(badges);
                filter.setProjectId(projectId);
                filter.setStartDate(dateOfService);
                List<Employee> employees = providerAPI.getEmployees(filter).block();
                if (employees != null) {
                    for (Employee employee : employees) {
                        EmpPunchRecords punchRecords = badgeEmpPunchRecordsMap.get(employee.getBadge());
                        punchRecords.setTeamName(employee.getTeamName());
                        punchRecords.setEmpId(employee.getEmpId());
                        punchRecords.setFirstName(employee.getFirstName());
                        punchRecords.setLastName(employee.getLastName());
                    }
                    validEmployees.addAll(employees);
                }
            }
            Map<String, EmpPunchRecords> clientEmpIdEmpPunchRecordsMap = entry.getValue().stream()
                    .filter(e -> StringUtils.hasText(e.getClientEmpId()))
                    .collect(Collectors.toMap(EmpPunchRecords::getClientEmpId, Function.identity()));
            if (!CollectionUtils.isEmpty(clientEmpIdEmpPunchRecordsMap)) {
                Set<String> clientEmpIds = clientEmpIdEmpPunchRecordsMap.keySet();
                EmployeeFilter filter = new EmployeeFilter();
                filter.setClientEmpIds(clientEmpIds);
                filter.setProjectId(projectId);
                filter.setStartDate(dateOfService);
                List<Employee> employees = providerAPI.getEmployees(filter).block();
                if (employees != null) {
                    for (Employee employee : employees) {
                        EmpPunchRecords punchRecords = clientEmpIdEmpPunchRecordsMap.get(employee.getClientEmpId());
                        punchRecords.setTeamName(employee.getTeamName());
                        punchRecords.setEmpId(employee.getEmpId());
                        punchRecords.setFirstName(employee.getFirstName());
                        punchRecords.setLastName(employee.getLastName());
                    }
                    validEmployees.addAll(employees);
                }
            }
        }

        if (CollectionUtils.isEmpty(validEmployees)) {
            throw new RosterNotFoundException("No effective roster found for the date selected");
        }
        Map<String, Employee> idEmpMap = validEmployees.stream()
                .collect(Collectors.toMap(Employee::getEmpId,
                        Function.identity(), (e1, e2) -> e1.getCreatedAt().isBefore(e2.getCreatedAt()) ? e2 : e1));

        KieSession kieSession = mKieContainer.newKieSession();
        kieSession.setGlobal("graceIn", graceIn);
        kieSession.setGlobal("graceOut", graceOut);
        kieSession.setGlobal("roundingMinutes", roundingMinutes);
        kieSession.setGlobal("minutesToBePlus", minutesToBePlus);
        kieSession.getAgenda().getAgendaGroup("adjust time").setFocus();
        List<FactHandle> factHandleList = new ArrayList<>();

        for (EmpPunchRecords item : empPunchRecords) {

            String k = item.getEmpId() == null ? "" : item.getEmpId();
            List<PunchRecord> recordList = item.getValidPunchRecordList();

            if (CollectionUtils.isEmpty(recordList)) {
                continue;
            }

            final LocalTime scheduleStart;
            if (idEmpMap.containsKey(k) && idEmpMap.get(k).getScheduleStart() != null) {
                scheduleStart = idEmpMap.get(k).getScheduleStart();
            } else {
                scheduleStart = shift.getScheduleStart();
            }

            final LocalTime scheduleEnd;
            if (idEmpMap.containsKey(k) && idEmpMap.get(k).getScheduleEnd() != null) {
                scheduleEnd = idEmpMap.get(k).getScheduleEnd();
            } else {
                scheduleEnd = shift.getScheduleEnd();
            }

            for (PunchRecord punchRecord : recordList) {
                punchRecord.setScheduleStart(scheduleStart);
                punchRecord.setScheduleEnd(scheduleEnd);
                factHandleList.add(kieSession.insert(punchRecord));
            }
        }
//            for (FactHandle factHandle : factHandleList) {
//                kieSession.delete(factHandle);
//            }
        kieSession.fireAllRules();
        kieSession.dispose();

        for (EmpPunchRecords item : empPunchRecords) {
            List<PunchRecord> recordList = item.getValidPunchRecordList();
            if (CollectionUtils.isEmpty(recordList)) {
                continue;
            }
            if (item.getPayDuration() != null) {
                item.setDate(LocalDate.from(item.getFirstPunchRecord().getEventTime()));
                continue;
            }
            String k = item.getEmpId() == null ? "" : item.getEmpId();

            final LocalTime scheduleStart;
            if (idEmpMap.containsKey(k) && idEmpMap.get(k).getScheduleStart() != null) {
                scheduleStart = idEmpMap.get(k).getScheduleStart();
            } else {
                scheduleStart = shift.getScheduleStart();
            }
            final LocalTime scheduleEnd;
            if (idEmpMap.containsKey(k) && idEmpMap.get(k).getScheduleEnd() != null) {
                scheduleEnd = idEmpMap.get(k).getScheduleEnd();
            } else {
                scheduleEnd = shift.getScheduleEnd();
            }
            final LocalTime lunchStart;
            if (idEmpMap.containsKey(k) && idEmpMap.get(k).getLunchStart() != null) {
                lunchStart = idEmpMap.get(k).getLunchStart();
            } else {
                lunchStart = shift.getBreakStart();
            }
            final LocalTime lunchEnd;
            if (idEmpMap.containsKey(k) && idEmpMap.get(k).getLunchEnd() != null) {
                lunchEnd = idEmpMap.get(k).getLunchEnd();
            } else {
                lunchEnd = shift.getBreakEnd();
            }

            final LocalDateTime firstInDateTime = item.getFirstPunchRecord().getEventTime();
            final boolean eventOverNight = DAYS.between(firstInDateTime,
                    item.getLastPunchRecord().getEventTime()) != 0;
            final boolean scheduleOverNight = scheduleEnd.isBefore(scheduleStart);
            final LocalDate firstInDate = firstInDateTime.toLocalDate();
            final LocalDate firstInYesterdayDate = firstInDate.minusDays(1);
            final LocalDate firstInNextdayDate = firstInDate.plusDays(1);
            final LocalDateTime scheduleStartWithDate;
            final LocalDateTime scheduleEndWithDate;
            final LocalDateTime lunchStartWithDate;
            final LocalDateTime lunchEndWithDate;
            LocalDateTime scheduleStartAnotherPossibleDate = null;
            LocalDateTime scheduleEndAnotherPossibleDate = null;
            LocalDateTime lunchStartAnotherPossibleDate = null;
            LocalDateTime lunchEndAnotherPossibleDate = null;
            final boolean twoCases;
            if (scheduleOverNight && !eventOverNight) {
                twoCases = true;
                scheduleStartWithDate = LocalDateTime.of(firstInDate, scheduleStart);
                scheduleEndWithDate = LocalDateTime.of(firstInNextdayDate, scheduleEnd);
                scheduleStartAnotherPossibleDate = LocalDateTime.of(firstInYesterdayDate, scheduleStart);
                scheduleEndAnotherPossibleDate = LocalDateTime.of(firstInDate, scheduleEnd);
            } else if (!scheduleOverNight && eventOverNight) {
                twoCases = true;
                scheduleStartWithDate = LocalDateTime.of(firstInDate, scheduleStart);
                scheduleEndWithDate = LocalDateTime.of(firstInDate, scheduleEnd);
                scheduleStartAnotherPossibleDate = LocalDateTime.of(firstInNextdayDate, scheduleStart);
                scheduleEndAnotherPossibleDate = LocalDateTime.of(firstInNextdayDate, scheduleEnd);
            } else {
                twoCases = false;
                scheduleStartWithDate = LocalDateTime.of(firstInDate, scheduleStart);
                scheduleEndWithDate = LocalDateTime.of(firstInDate, scheduleEnd);
            }

            if (lunchStart.isAfter(scheduleStart)) {
                lunchStartWithDate = LocalDateTime.of(scheduleStartWithDate.toLocalDate(), lunchStart);
            } else {
                lunchStartWithDate = LocalDateTime.of(scheduleStartWithDate.toLocalDate().plusDays(1),
                        lunchStart);
            }
            if (lunchEnd.isAfter(lunchStart)) {
                lunchEndWithDate = LocalDateTime.of(lunchStartWithDate.toLocalDate(), lunchEnd);
            } else {
                lunchEndWithDate = LocalDateTime.of(lunchStartWithDate.toLocalDate().plusDays(1), lunchEnd);
            }
            final Duration maxDuration =
                    Duration.between(scheduleStartWithDate, lunchStartWithDate)
                            .plus(Duration.between(lunchEndWithDate, scheduleEndWithDate))
                            .plus(paidBreakDuration);

            if (twoCases) {
                if (lunchStart.isAfter(scheduleStart)) {
                    lunchStartAnotherPossibleDate =
                            LocalDateTime.of(scheduleStartAnotherPossibleDate.toLocalDate(), lunchStart);
                } else {
                    lunchStartAnotherPossibleDate =
                            LocalDateTime.of(scheduleStartAnotherPossibleDate.toLocalDate().plusDays(1),
                                    lunchStart);
                }
                if (lunchEnd.isAfter(lunchStart)) {
                    lunchEndAnotherPossibleDate =
                            LocalDateTime.of(lunchStartAnotherPossibleDate.toLocalDate(), lunchEnd);
                } else {
                    lunchEndAnotherPossibleDate =
                            LocalDateTime.of(lunchStartAnotherPossibleDate.toLocalDate().plusDays(1), lunchEnd);
                }
            }

            Duration netDuration = Duration.ZERO;
            Duration duration = Duration.ZERO;
            Duration breakDuration = Duration.ZERO;
            Duration anotherPossibleDuration = Duration.ZERO;
            Duration anotherPossibleBreakDuration = Duration.ZERO;

            int size = recordList.size();
            for (int i = 0; i < size; i++) {
                PunchRecord punchRecord = recordList.get(i);
                LocalDateTime netEventTime = punchRecord.getNetEventTime();
                LocalDateTime eventTime = punchRecord.getEventTime();
                if (punchRecord.isSide()) {
                    if (i > 0) {
                        // Not first in. The out and in period might be between break time
                        PunchRecord previousOut = recordList.get(i - 1);
                        LocalDateTime outTime = previousOut.getEventTime();
                        breakDuration = breakDuration.plus(overlap(lunchStartWithDate, lunchEndWithDate,
                                outTime, eventTime));
                        if (twoCases) {
                            anotherPossibleBreakDuration =
                                    anotherPossibleBreakDuration.plus(overlap(lunchStartAnotherPossibleDate,
                                            lunchEndAnotherPossibleDate, outTime, eventTime));
                        }
                    }
                } else {
                    PunchRecord previousIn = recordList.get(i - 1);

                    LocalDateTime inTime = previousIn.getEventTime();
                    duration = duration.plus(overlap(scheduleStartWithDate, lunchStartWithDate, inTime,
                            eventTime));
                    duration = duration.plus(overlap(lunchEndWithDate, scheduleEndWithDate, inTime, eventTime));
                    breakDuration = breakDuration.plus(overlap(lunchStartWithDate, lunchEndWithDate,
                            inTime, eventTime));

                    LocalDateTime netInTime = previousIn.getNetEventTime();

                    netDuration = netDuration.plus(Duration.between(netInTime, netEventTime));
                    if (twoCases) {
                        anotherPossibleDuration =
                                anotherPossibleDuration.plus(overlap(scheduleStartAnotherPossibleDate,
                                        lunchStartAnotherPossibleDate, inTime, eventTime));
                        anotherPossibleDuration =
                                anotherPossibleDuration.plus(overlap(lunchEndAnotherPossibleDate,
                                        scheduleEndAnotherPossibleDate, inTime, eventTime));
                        anotherPossibleBreakDuration =
                                anotherPossibleBreakDuration.plus(overlap(lunchStartAnotherPossibleDate,
                                        lunchEndAnotherPossibleDate, inTime, eventTime));
                    }
                }
            }
            Duration totalPayDuration;
            if (twoCases && anotherPossibleDuration.compareTo(duration) > 0) {
                // Another case has better pay duration. This should be the right schedule start/end
                anotherPossibleBreakDuration = anotherPossibleBreakDuration.compareTo(paidBreakDuration) > 0 ?
                        paidBreakDuration : anotherPossibleBreakDuration;
                totalPayDuration = anotherPossibleDuration.plus(anotherPossibleBreakDuration);
                totalPayDuration = totalPayDuration.compareTo(maxDuration) > 0 ? maxDuration : totalPayDuration;
                item.setPayDuration(totalPayDuration);
                item.setDate(scheduleStartAnotherPossibleDate.toLocalDate());
            } else {
                breakDuration = breakDuration.compareTo(paidBreakDuration) > 0 ? paidBreakDuration : breakDuration;
                totalPayDuration = duration.plus(breakDuration);
                totalPayDuration = totalPayDuration.compareTo(maxDuration) > 0 ? maxDuration : totalPayDuration;
                item.setPayDuration(totalPayDuration);
                item.setDate(scheduleStartWithDate.toLocalDate());
            }
            item.setNetDuration(netDuration);
        }

//        idEmpPunchRecords.forEach((k, v) -> {
//                    v.getValidPunchRecordList().forEach(e -> System.out.println("" + e.getEmpId() + ": " + e
//                    .getLastName()
//                            + " " + e.getFirstName() + (e.isSide() ? " in " : " out ") + e.getEvent().getDetail() +
//                            " at " + e.getAdjustedTime()));
////                    double payHours = v.getPayDuration().toMinutes() / 60d;
////                    DecimalFormat df = new DecimalFormat("#.#");
////                    df.setRoundingMode(RoundingMode.HALF_UP);
////                    System.out.println("" + v.getEmpId() + " pay duration : " + df.format(payHours));
//                    v.getExceptionList().forEach(
//                            e -> System.out.println(k + " has exception \"" + e + "\"")
//                    );
//                    System.out.println("" + v.getEmpId() + " pay hours : " + v.getTotalHours());
//                    System.out.println("" + v.getEmpId() + " net hours : " + v.getNetHours());
//                }
//        );
    }

    @Override
    public boolean hasRuleAndShiftOfProject(Long projectId) {
        Mono<Rule> ruleMono = providerAPI.findRuleByProjectId(projectId);
        Mono<Shift> shiftMono = providerAPI.findProjectShiftByProjectId(projectId)
                .flatMap(projectShift -> Mono.deferContextual(Mono::just)
                        .flatMap(ctx -> {
                            ProviderAPI api = ctx.get(ProviderAPI.class);
                            return api.findShiftById(projectShift.getShiftId())
                                    .switchIfEmpty(Mono.empty());
                        }))
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())));
        Tuple2<Rule, Shift> tuple = Mono.zip(ruleMono, shiftMono).block();
        return tuple != null;
    }

    @Override
    public CommonResult updateProjectSetUp() {
        return providerAPI.updateProjectSetUp().block();
    }

    @Override
    public CommonResult updateCraftSubmission() {
        return providerAPI.updateCraftSubmission().block();
    }

    @Override
    public void parseEquipments(List<Equipment> equipmentList) {
        if (CollectionUtils.isEmpty(equipmentList)) return;
        Set<String> equipmentIds = equipmentList.stream()
                .map(Equipment::getEquipmentId).collect(Collectors.toSet());
        List<Equipment> existingEquipments = providerAPI.findEquipmentByEquipmentIdIn(equipmentIds).block();
        if (CollectionUtils.isEmpty(existingEquipments)) return;
        Map<String, Equipment> equipmentMap = existingEquipments.stream()
                .collect(Collectors.toMap(Equipment::getEquipmentId, Function.identity()));
        for (Equipment equipment : equipmentList) {
            if (equipmentMap.containsKey(equipment.getEquipmentId())) {
                equipment.setId(equipmentMap.get(equipment.getEquipmentId()).getId());
            }
        }
        providerAPI.saveEquipmentList(equipmentList);
    }

    @Override
    public void parseEquipmentPrices(List<EquipmentPrice> equipmentPriceList, Long projectId) {
        if (CollectionUtils.isEmpty(equipmentPriceList)) return;
        List<EquipmentPrice> existingPrice = providerAPI.findEquipmentPricesByProjectId(projectId);
        Map<EquipmentPriceKey, EquipmentPrice> keyPriceMap = existingPrice.stream()
                .collect(toMap(e -> new EquipmentPriceKey(e.getProjectId(), e.getEquipmentClass()), Function.identity()));
        for (EquipmentPrice equipmentPrice : equipmentPriceList) {
            EquipmentPriceKey key = new EquipmentPriceKey(equipmentPrice.getProjectId(), equipmentPrice.getEquipmentClass());
            if (keyPriceMap.containsKey(key)) {
                equipmentPrice.setId(keyPriceMap.get(key).getId());
            }
        }

        providerAPI.saveEquipmentPrices(equipmentPriceList);
    }

    @Override
    public void parseEquipmentLog(Map<String, Equipment> equipmentMap, List<EquipmentUsage> equipmentUsageList) {
        // TM-300 improt equipment and equipment log
        if (CollectionUtils.isEmpty(equipmentMap) && CollectionUtils.isEmpty(equipmentUsageList)) return;
        Set<String> equipmentIds = equipmentMap.keySet();
        List<Equipment> equipmentList = new ArrayList<>(equipmentMap.size());
        List<Equipment> existingEquipments = providerAPI.findEquipmentByEquipmentIdIn(equipmentIds).block();
        Map<String, Equipment> equipmentMapInfo = existingEquipments != null ? existingEquipments.stream()
                .collect(Collectors.toMap(Equipment::getEquipmentId, Function.identity())) : new HashMap<>();
        // save equipment id
        for (String equipmentId : equipmentIds) {
            if (equipmentMapInfo.containsKey(equipmentId)) {
                equipmentMap.get(equipmentId).setId(equipmentMapInfo.get(equipmentId).getId());
            }
            equipmentList.add(equipmentMap.get(equipmentId));
        }
        providerAPI.saveEquipmentList(equipmentList);
        providerAPI.saveImportEquipmentUsageAll(equipmentUsageList);
    }
}
