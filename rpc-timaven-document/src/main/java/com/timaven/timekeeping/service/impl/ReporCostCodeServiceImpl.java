package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.BillingCodeType;
import com.timaven.timekeeping.model.CostCodeExportView;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportCostCodeService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReporCostCodeServiceImpl implements ReportCostCodeService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReporCostCodeServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportCostCode(Long projectId) {
        List<CostCodeExportView> costCodeExportViewList = providerAPI.findCostCodeExportViewByProjectId(projectId).block();
        // Map<id , Map<100L, CostCodeDto> and Map<btc_id , cost_code>>
        Map<Long , Map<Long , Object>> costCodeAllData = new HashMap<>();
        costCodeExportViewList.stream().forEach(costCodeView -> {
            if(costCodeAllData.containsKey(costCodeView.getCostCodeId())){
                if(null != costCodeView.getBillingCodeTypeId()){
                    costCodeAllData.get(costCodeView.getCostCodeId()).put(costCodeView.getBillingCodeTypeId(),costCodeView.getCostCode());
                }
            }else{
                Map<Long , Object> data = new HashMap<>();
                data.put(100L,costCodeView);
                if(null != costCodeView.getBillingCodeTypeId()){
                    data.put(costCodeView.getBillingCodeTypeId(),costCodeView.getCostCode());
                }
                costCodeAllData.put(costCodeView.getCostCodeId(),data);
            }
        });

        List<BillingCodeType> billingCodeTypeList = providerAPI.findBillingCodeTypeSort("level").block();
        Map<Integer , Long> billingCodeTypeMap = new HashMap<>(billingCodeTypeList.size());

        List<String> defaultBillingCode = List.of("Invoice","Job#","Sub Job");

        try{
            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Data");
            Row header = sheet.createRow(0);
            XSSFCellStyle headerStyle = workbook.createCellStyle();

            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font);

            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            int headCellIndex = 0;
            Cell headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Cost Code");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Description");
            headerCell.setCellStyle(headerStyle);

            int billingCodeIndex = 0;
            for (int i = 0; i < billingCodeTypeList.size(); i++) {
                // not default billing code type
                if(!defaultBillingCode.contains(billingCodeTypeList.get(i).getCodeType())){
                    headerCell = header.createCell(headCellIndex++);
                    headerCell.setCellValue(billingCodeTypeList.get(i).getCodeType());
                    headerCell.setCellStyle(headerStyle);
                    billingCodeTypeMap.put(billingCodeIndex++, billingCodeTypeList.get(i).getId());
                }
            }

            CellStyle style = workbook.createCellStyle();
            XSSFFont bodyFont = workbook.createFont();
            bodyFont.setFontName("Arial");
            bodyFont.setFontHeightInPoints((short) 10);
            style.setFont(bodyFont);

            Integer rowIndex = 0;

            for (Long costCodeId : costCodeAllData.keySet() ) {
                CostCodeExportView costCodeDto = (CostCodeExportView) costCodeAllData.get(costCodeId).get(100L);

                Row row = sheet.createRow(++rowIndex);

                int cellIndex = 0;

                Cell cell = row.createCell(cellIndex++);
                cell.setCellValue(costCodeDto.getCostCodeFull());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(costCodeDto.getDescription());
                cell.setCellStyle(style);

                for(int k = 0; k < billingCodeTypeMap.size(); k++){
                    cell = row.createCell(cellIndex++);
                    cell.setCellValue(costCodeAllData.get(costCodeId).get(billingCodeTypeMap.get(k)) == null ?
                            "" : costCodeAllData.get(costCodeId).get(billingCodeTypeMap.get(k)).toString());
                    cell.setCellStyle(style);
                }
            }
            return excelService.downloadExcel( "Cost Code", workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}
