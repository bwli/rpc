package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Equipment;
import com.timaven.timekeeping.model.EquipmentUsage;
import com.timaven.timekeeping.model.dto.EquipmentDto;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportEquipmentLogService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReporEquipmentLogServiceImpl implements ReportEquipmentLogService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReporEquipmentLogServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipemtTemplateLog(Long projectId) {
        List<Equipment> equipmentList;
        if(null == projectId){
            equipmentList = providerAPI.
                    findEquipmentByOwnershipType(EquipmentOwnershipType.COMPANY_OWNED).block();
        }else{
            equipmentList = providerAPI.
                    findEquipmentRentalAndBeingUsedEquipments(projectId).block();
        }
        List<EquipmentUsage> equipmentUsages = providerAPI.findEquipmentUsagesByIdsAndDates(null, null, LocalDate.now(), null).block();
        Map<Long, List<EquipmentUsage>> equipmentUsagesMap = equipmentUsages.stream()
                .collect(Collectors.groupingBy(EquipmentUsage::getEquipmentId,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(),
                                        e -> e.stream()
                                                .sorted(Comparator.comparing(EquipmentUsage::getStartDate))
                                                .collect(Collectors.toList())))));
        List<EquipmentDto> equipmentDtos = new ArrayList<>();
        for (Equipment equipment : equipmentList) {
            EquipmentDto equipmentDto = new EquipmentDto(equipment);
            if (equipmentUsagesMap.containsKey(equipment.getId())) {
                List<EquipmentUsage> usages = equipmentUsagesMap.get(equipment.getId()).stream()
                        .filter(equipmentUsage -> equipmentUsage.getProjectId().equals(projectId)).collect(Collectors.toList());
                equipmentDto.setEquipmentUsages(usages);
            }
            equipmentDtos.add(equipmentDto);
        }
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Equipment_Log");
            Row header = sheet.createRow(0);

            XSSFFont bodyFont = workbook.createFont();
            bodyFont.setFontName("Arial");
            bodyFont.setFontHeightInPoints((short) 10);

            XSSFCellStyle cellStyleMDYYYY = workbook.createCellStyle();
            XSSFDataFormat formatMDYYYY= workbook.createDataFormat();
            cellStyleMDYYYY.setDataFormat(formatMDYYYY.getFormat("m/d/yyyy"));
            cellStyleMDYYYY.setFont(bodyFont);

            CellStyle style = workbook.createCellStyle();
            style.setFont(bodyFont);
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setVerticalAlignment(VerticalAlignment.CENTER);

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font);

            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            int headCellIndex = 0;
            Cell headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("ID");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Description");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Alias");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Serial Number");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Department");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Class");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Hourly Type");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Start Date");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("End Date");
            headerCell.setCellStyle(headerStyle);

            int startIndex = 0;

            for (int i = 0; i < equipmentDtos.size(); i++) {
                EquipmentDto equipmentDto = equipmentDtos.get(i);

                Row row = sheet.createRow(++startIndex);

                // set equipment usage info
                List<EquipmentUsage> equipmentUsagesListInfo = equipmentDto.getEquipmentUsages();

                if((startIndex + (equipmentUsagesListInfo.size() - 1)) > startIndex){
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1), 0, 0));
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1),1,1));
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1),2,2));
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1),3,3));
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1),4,4));
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1),5,5));
                    sheet.addMergedRegion(new CellRangeAddress(startIndex,startIndex + (equipmentUsagesListInfo.size() - 1),6,6));
                }

                Cell cell = row.createCell(0);
                cell.setCellValue(equipmentDto.getEquipment().getEquipmentId());
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(equipmentDto.getEquipment().getDescription());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(equipmentDto.getEquipment().getAlias());
                cell.setCellStyle(style);

                cell = row.createCell(3);
                cell.setCellValue(equipmentDto.getEquipment().getSerialNumber());
                cell.setCellStyle(style);

                cell = row.createCell(4);
                cell.setCellValue(equipmentDto.getEquipment().getDepartment());
                cell.setCellStyle(style);

                cell = row.createCell(5);
                cell.setCellValue(equipmentDto.getEquipment().getEquipmentClass());
                cell.setCellStyle(style);

                cell = row.createCell(6);
                cell.setCellValue(equipmentDto.getEquipment().getHourlyType().toString());
                cell.setCellStyle(style);

                if(equipmentUsagesListInfo.size() > 0){
                    int startDateCellIndex = 7;
                    int endDateCellIndex = 8;
                    for(int k = 0; k < equipmentUsagesListInfo.size(); k++){
                        cell = row.createCell(startDateCellIndex);
                        cell.setCellValue(equipmentUsagesListInfo.get(k).getStartDate()  == null ? "" :
                                equipmentUsagesListInfo.get(k).getStartDate().format(DateTimeFormatter.ofPattern("M/d/yyyy")));
                        cell.setCellStyle(cellStyleMDYYYY);

                        cell = row.createCell(endDateCellIndex);
                        cell.setCellValue(equipmentUsagesListInfo.get(k).getEndDate()  == null ? "" :
                                equipmentUsagesListInfo.get(k).getEndDate().format(DateTimeFormatter.ofPattern("M/d/yyyy")));
                        cell.setCellStyle(cellStyleMDYYYY);

                        if((k + 1) < equipmentUsagesListInfo.size()){
                            row = sheet.createRow(++startIndex);
                        }
                    }
                }
            }

            return excelService.downloadExcel( "EquipmentLog", workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}
