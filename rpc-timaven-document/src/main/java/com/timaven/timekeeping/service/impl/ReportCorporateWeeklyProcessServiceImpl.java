package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;
import com.timaven.timekeeping.service.ReportCorporateWeeklyProcessService;
import com.timaven.timekeeping.util.DateRange;
import com.timaven.timekeeping.util.LocalDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReportCorporateWeeklyProcessServiceImpl extends PdfBaseService implements ReportCorporateWeeklyProcessService {

    protected ReportCorporateWeeklyProcessServiceImpl(ProviderAPI providerAPI, TenantProperties tenantProperties) {
        super(providerAPI, tenantProperties);
    }


    @Override
    public ResponseEntity<InputStreamResource> reportCorporateWeeklyProcess(LocalDate weeklyEndDate, String teamName) {
        LocalDate weeklyStartDate = weeklyEndDate.minusDays(6);
        Mono<WeeklyProcess[]> weeklyProcessMono = providerAPI.findWeeklyProcessesByProjectIdAndTeamAndDateAndType(weeklyEndDate, weeklyEndDate,
                WeeklyProcessType.NORMAL, null, teamName, Set.of("allocationTimes"));

        WeeklyProcess[] weeklyProcessArray = weeklyProcessMono.block();
        WeeklyProcess weeklyProcess  = weeklyProcessArray == null || weeklyProcessArray.length == 0 ? null : weeklyProcessArray[0];

        EmployeeFilter filter = new EmployeeFilter(null, teamName, null, null, null, weeklyStartDate, weeklyEndDate);
        Mono<List<Employee>> employeesListMono = providerAPI.getEmployees(filter);
        List<Employee> employeesList = employeesListMono.block();

        Map<String, Set<Long>> activityCodeIdsByEmpId = new HashMap<>();
        Map<Long, String> activityCodeMap = new HashMap<>();
        Map<EmpDateKey, AllocationTime> allocationTimeMap = new HashMap<>();
        Map<EmpDateActivityKey, ActivityCodePerc> activityCodePercMap = new HashMap<>();
        if(!Objects.isNull(weeklyProcess)){
            activityCodeIdsByEmpId = weeklyProcess.getAllocationTimes().stream()
                    .map(AllocationTime::getActivityCodePercs)
                    .flatMap(Set::stream)
                    .collect(Collectors.groupingBy(ActivityCodePerc::getEmployeeId,
                            Collectors.mapping(ActivityCodePerc::getActivityId, Collectors.toSet())));

            Set<Long> allActivityCode = new HashSet<>();
            activityCodeMap = weeklyProcess.getAllocationTimes().stream()
                    .map(AllocationTime::getActivityCodePercs)
                    .flatMap(Set::stream)
                    .filter(e -> allActivityCode.add(e.getActivityId()))
                    .collect(Collectors.toMap(ActivityCodePerc::getActivityId, ActivityCodePerc::getActivityCodeText));

            allocationTimeMap = weeklyProcess.getAllocationTimes().stream()
                    .collect(Collectors.toMap(t -> new EmpDateKey(t.getEmpId(), t.getAllocationSubmission().getDateOfService())
                            , Function.identity()));

            activityCodePercMap = weeklyProcess.getAllocationTimes().stream()
                    .map(AllocationTime::getActivityCodePercs)
                    .flatMap(Set::stream)
                    .collect(Collectors.toMap(e -> new EmpDateActivityKey(e.getAllocationTime().getId(), e.getActivityId()), Function.identity()));
        }

        if (CollectionUtils.isEmpty(employeesList)) employeesList = new ArrayList<>();
        Set<String> empIds = employeesList.stream()
                .map(Employee::getEmpId)
                .collect(Collectors.toSet());

        Map<String, Employee> employeeMap = employeesList.stream()
                .collect(Collectors.toMap(Employee::getEmpId,
                        Function.identity(),
                        BinaryOperator.maxBy(Comparator
                                .comparing(Employee::getCreatedAt, Comparator.nullsLast(Comparator.naturalOrder())))));

        Set<LocalDate> days = new TreeSet<>(new DateRange(weeklyStartDate, weeklyEndDate, false).toList());

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Table");

            org.apache.poi.ss.usermodel.Row header = sheet.createRow(0);
            XSSFCellStyle headerStyle = workbook.createCellStyle();
            XSSFCellStyle dayHeaderStyle = workbook.createCellStyle();
            XSSFCellStyle centerStyle = workbook.createCellStyle();
            XSSFCellStyle style = workbook.createCellStyle();

            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);

            centerStyle.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
            centerStyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);

            headerStyle.setFont(font);
            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            dayHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
            dayHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            dayHeaderStyle.setFont(font);

            int headCellIndex = 0;
            Cell headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Name");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("EE No.");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Timesheet. (Department)");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Dept. Code (Job #)");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Activity Code");
            headerCell.setCellStyle(headerStyle);

            for(LocalDate day : days){
                int startIndex = headCellIndex++;
                int endIndex = headCellIndex++;
                CellRangeAddress cellAddresses = new CellRangeAddress(0, 0, startIndex, endIndex);
                sheet.addMergedRegion(cellAddresses);
                headerCell = header.createCell(startIndex);
                headerCell.setCellValue(day.toString());
                headerCell.setCellStyle(dayHeaderStyle);
                RegionUtil.setBorderTop(BorderStyle.MEDIUM, cellAddresses, sheet);
                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, cellAddresses, sheet);
                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, cellAddresses, sheet);
                RegionUtil.setBorderRight(BorderStyle.MEDIUM, cellAddresses, sheet);
            }

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("ST.");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("OT.");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Hol.");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Vac.");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Sick");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Other");
            headerCell.setCellStyle(headerStyle);

            int rowIndex = 1;
            BigDecimal totalStraightTime = BigDecimal.ZERO;
            BigDecimal totalOverTime = BigDecimal.ZERO;
            BigDecimal totalNonOverTime = BigDecimal.ZERO;
            BigDecimal totalTime = BigDecimal.ZERO;

            for(String empId : empIds){
                BigDecimal stTotalTime = BigDecimal.ZERO;
                BigDecimal otTotalTime = BigDecimal.ZERO;
                BigDecimal holTotalTime = BigDecimal.ZERO;
                BigDecimal vacTotalTime = BigDecimal.ZERO;
                BigDecimal sickTotalTime = BigDecimal.ZERO;
                BigDecimal otherTotalTIme = BigDecimal.ZERO;
                BigDecimal allocationTotalTime = BigDecimal.ZERO;
                BigDecimal allocationNonOverTimeTotalTime = BigDecimal.ZERO;

                // all employee Some do not exist data
                // for distinguishing not exist data new HashSet<>(){{add(-1L);}}
                Set<Long> activityCodeIds = activityCodeIdsByEmpId.get(empId) == null ?
                        new HashSet<>(){{add(-1L);}} : activityCodeIdsByEmpId.get(empId);

                org.apache.poi.ss.usermodel.Row totalStTimeRow = null;
                int totalStTimeCellIndex = 0;

                org.apache.poi.ss.usermodel.Row totalOtTimeRow = null;
                int totalOtTimeCellIndex = 0;
                int totalOtTimeRowIndex = 0;

                Cell cell;
                boolean isOnce = true;

                int mergeIndex = activityCodeIds.size() * 2 - 1;
                for(Long acitvityCodeId :activityCodeIds){

                    int deptCodeRowIndex = rowIndex++;
                    org.apache.poi.ss.usermodel.Row stRow = sheet.createRow(deptCodeRowIndex);
                    int stCellIndex = 0;

                    int otRowIndex = rowIndex++;
                    org.apache.poi.ss.usermodel.Row otRow = sheet.createRow(otRowIndex);
                    int otCellIndex = 0;

                    if(isOnce){
                        Employee employee = employeeMap.get(empId);
                        cell = stRow.createCell(stCellIndex++);
                        cell.setCellValue(employee.getFullName());
                        cell.setCellStyle(style);
                        cell = stRow.createCell(stCellIndex++);
                        cell.setCellValue(empId);
                        cell.setCellStyle(style);

//                        int deptCodeCellIndex = stCellIndex++;
//                        int deptCodeEndIndex = deptCodeRowIndex + mergeIndex;
//                        CellRangeAddress cellAddresses = new CellRangeAddress(deptCodeRowIndex,deptCodeEndIndex,deptCodeCellIndex, deptCodeCellIndex);
//                        sheet.addMergedRegion(cellAddresses);

                        cell = stRow.createCell(stCellIndex++);
                        cell.setCellValue(Optional.ofNullable(employee.getTeamName()).orElse(""));
                        cell.setCellStyle(centerStyle);

                        cell = stRow.createCell(stCellIndex++);
                        cell.setCellValue(Optional.ofNullable(employee.getJobNumber()).orElse(""));
                        cell.setCellStyle(centerStyle);

                        cell = stRow.createCell(stCellIndex++);
                        cell.setCellValue(Optional.ofNullable(employee.getActivityCode()).orElse(""));
                        cell.setCellStyle(centerStyle);

                        if(acitvityCodeId.equals(-1L)){
                            totalStTimeRow = stRow;
                            totalStTimeCellIndex = 18;

                            totalOtTimeRow = otRow;
                            totalOtTimeRowIndex = otRowIndex;
                            totalOtTimeCellIndex = 18;
                            break;
                        }
                    }else{
                        stCellIndex = stCellIndex + 3;
                    }

                    otCellIndex = otCellIndex + 4;

                    // activity code merge two rows
                    int activityCodeCellIndex = stCellIndex;
                    CellRangeAddress cellAddresses = new CellRangeAddress(deptCodeRowIndex, otRowIndex, activityCodeCellIndex, activityCodeCellIndex);
                    sheet.addMergedRegion(cellAddresses);

                    cell = stRow.createCell(activityCodeCellIndex);
                    cell.setCellValue(activityCodeMap.get(acitvityCodeId));
                    cell.setCellStyle(centerStyle);

                    for(LocalDate day : days){
                        AllocationTime allocationTime = allocationTimeMap.get(new EmpDateKey(empId, day));
                        if(Objects.isNull(allocationTime) || !activityCodePercMap.containsKey(new EmpDateActivityKey(allocationTime.getId(), acitvityCodeId))){
                            stCellIndex = stCellIndex + 2;
                            otCellIndex = otCellIndex + 2;
                        }else{
                            ActivityCodePerc activityCodePerc = activityCodePercMap.get(new EmpDateActivityKey(allocationTime.getId(), acitvityCodeId));
                            BigDecimal stHour = activityCodePerc.getStHour();
                            BigDecimal otHour = activityCodePerc.getOtHour();
                            AllocationTimeHourType extraTimeType = activityCodePerc.getExtraTimeType();
                            String extraTimeTypeText = "";

                            switch (extraTimeType.getValue()) {
                                case 0:
                                    otTotalTime = otTotalTime.add(otHour);
                                    extraTimeTypeText = "OT:";
                                    allocationNonOverTimeTotalTime = allocationNonOverTimeTotalTime.add(stHour);
                                    break;
                                case 1:
                                    sickTotalTime = sickTotalTime.add(otHour);
                                    allocationNonOverTimeTotalTime = allocationNonOverTimeTotalTime.add(otHour).add(stHour);
                                    extraTimeTypeText = "SICK:";
                                    break;
                                case 2:
                                    holTotalTime = holTotalTime.add(otHour);
                                    allocationNonOverTimeTotalTime = allocationNonOverTimeTotalTime.add(otHour).add(stHour);
                                    extraTimeTypeText = "HOLIDAY:";
                                    break;
                                case 3:
                                    vacTotalTime = vacTotalTime.add(otHour);
                                    allocationNonOverTimeTotalTime = allocationNonOverTimeTotalTime.add(otHour).add(stHour);
                                    extraTimeTypeText = "VACATION:";
                                    break;
                                case 4:
                                    otherTotalTIme = otherTotalTIme.add(otHour);
                                    allocationNonOverTimeTotalTime = allocationNonOverTimeTotalTime.add(otHour).add(stHour);
                                    extraTimeTypeText = "OTHER:";
                                    break;
                            }

                            stTotalTime = stTotalTime.add(stHour);
                            allocationTotalTime = allocationTotalTime.add(stHour).add(otHour);

                            cell = stRow.createCell(stCellIndex++);
                            cell.setCellValue("ST:");
                            cell.setCellStyle(centerStyle);
                            cell = stRow.createCell(stCellIndex++);
                            cell.setCellValue(stHour.toString());
                            cell.setCellStyle(centerStyle);

                            cell = otRow.createCell(otCellIndex++);
                            cell.setCellValue(extraTimeTypeText);
                            cell.setCellStyle(centerStyle);
                            cell = otRow.createCell(otCellIndex++);
                            cell.setCellValue(otHour.toString());
                            cell.setCellStyle(centerStyle);
                        }
                    }

                    if(isOnce){
                        totalStTimeRow = stRow;
                        totalStTimeCellIndex = stCellIndex;

                        totalOtTimeRow = otRow;
                        totalOtTimeRowIndex = otRowIndex;
                        totalOtTimeCellIndex = otCellIndex;
                        isOnce = false;
                    }
                }

                if(Objects.isNull(totalStTimeRow) || Objects.isNull(totalOtTimeRow)){
                    totalStTimeRow = sheet.createRow(rowIndex++);
                    totalOtTimeRow = sheet.createRow(rowIndex++);
                    totalStTimeCellIndex = 18;
                    totalOtTimeCellIndex = 18;
                }

                cell = totalStTimeRow.createCell(totalStTimeCellIndex++);
                cell.setCellValue(stTotalTime.toString());
                cell.setCellStyle(centerStyle);
                cell = totalStTimeRow.createCell(totalStTimeCellIndex++);
                cell.setCellValue(otTotalTime.toString());
                cell.setCellStyle(centerStyle);
                cell = totalStTimeRow.createCell(totalStTimeCellIndex++);
                cell.setCellValue(holTotalTime.toString());
                cell.setCellStyle(centerStyle);
                cell = totalStTimeRow.createCell(totalStTimeCellIndex++);
                cell.setCellValue(vacTotalTime.toString());
                cell.setCellStyle(centerStyle);
                cell = totalStTimeRow.createCell(totalStTimeCellIndex++);
                cell.setCellValue(sickTotalTime.toString());
                cell.setCellStyle(centerStyle);
                cell = totalStTimeRow.createCell(totalStTimeCellIndex++);
                cell.setCellValue(otherTotalTIme.toString());
                cell.setCellStyle(centerStyle);

                int startIndex = totalOtTimeCellIndex++;
                int endIndex = totalOtTimeCellIndex++;
                // Because st row exist, Ot merge - 1
                int endMergeRowIndex = totalOtTimeRowIndex + mergeIndex - 1;
                CellRangeAddress cellAddresses = new CellRangeAddress(totalOtTimeRowIndex, endMergeRowIndex, startIndex, endIndex);
                sheet.addMergedRegion(cellAddresses);
                cell = totalOtTimeRow.createCell(startIndex);
                cell.setCellValue("Non Over-Time:");
                cell.setCellStyle(centerStyle);

                startIndex = totalOtTimeCellIndex++;
                if(totalOtTimeRowIndex != endMergeRowIndex){
                    cellAddresses = new CellRangeAddress(totalOtTimeRowIndex, endMergeRowIndex, startIndex, startIndex);
                    sheet.addMergedRegion(cellAddresses);
                }
                cell = totalOtTimeRow.createCell(startIndex);
                cell.setCellValue(allocationNonOverTimeTotalTime.toString());
                cell.setCellStyle(centerStyle);

                startIndex = totalOtTimeCellIndex++;
                endIndex = totalOtTimeCellIndex++;
                cellAddresses = new CellRangeAddress(totalOtTimeRowIndex, endMergeRowIndex, startIndex, endIndex);
                sheet.addMergedRegion(cellAddresses);
                cell = totalOtTimeRow.createCell(startIndex);
                cell.setCellValue("Total Time:");
                cell.setCellStyle(centerStyle);

                startIndex = totalOtTimeCellIndex++;
                if(totalOtTimeRowIndex != endMergeRowIndex){
                    cellAddresses = new CellRangeAddress(totalOtTimeRowIndex, endMergeRowIndex, startIndex, startIndex);
                    sheet.addMergedRegion(cellAddresses);
                }
                cell = totalOtTimeRow.createCell(startIndex);
                cell.setCellValue(allocationTotalTime.toString());
                cell.setCellStyle(centerStyle);

                totalTime = totalTime.add(allocationTotalTime);
                totalStraightTime = totalStraightTime.add(stTotalTime);
                totalOverTime = totalOverTime.add(otTotalTime);
                totalNonOverTime = totalNonOverTime.add(allocationNonOverTimeTotalTime);
            }

            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            Row totalRows = sheet.createRow(rowIndex + 1);
            Cell cell = totalRows.createCell(0);
            cell.setCellValue("From Date: " + String.format("%s [%s]", dateFormatter.format(weeklyStartDate), LocalDateUtil.getDayOfWeekName(weeklyStartDate)));
            cell.setCellStyle(style);

            cell = totalRows.createCell(1);
            cell.setCellValue("From Date: " + String.format("%s [%s]", dateFormatter.format(weeklyEndDate), LocalDateUtil.getDayOfWeekName(weeklyEndDate)));
            cell.setCellStyle(style);

            cell = totalRows.createCell(2);
            cell.setCellValue("Team: " + teamName);
            cell.setCellStyle(style);

            totalRows = sheet.createRow(rowIndex + 2);
            cell = totalRows.createCell(0);
            cell.setCellValue("Total Straight Time: " + totalStraightTime);
            cell.setCellStyle(style);

            cell = totalRows.createCell(1);
            cell.setCellValue("Total Overtime: " + totalOverTime );
            cell.setCellStyle(style);

            cell = totalRows.createCell(2);
            cell.setCellValue("Total Non-Overtime: " + totalNonOverTime );
            cell.setCellStyle(style);

            cell = totalRows.createCell(3);
            cell.setCellValue("Total Time: " + totalTime );
            cell.setCellStyle(style);

            // set first four columns auto width
            for(int i = 0 ; i < 4; i++){
                sheet.autoSizeColumn(i);
            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            headers.add("Content-Disposition", "attachment; filename=Corporate Weekly Process.xlsx");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

}
