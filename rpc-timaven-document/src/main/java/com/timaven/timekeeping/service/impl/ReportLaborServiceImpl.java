package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.AllocationTimeFilter;
import com.timaven.timekeeping.model.dto.TenantDto;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.CostCodeType;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportLaborService;
import com.timaven.timekeeping.util.LocalDateUtil;
import com.timaven.timekeeping.util.NumberUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuple4;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@Service
public class ReportLaborServiceImpl implements ReportLaborService {

    private final ProviderAPI providerAPI;

    private final TenantProperties tenantProperties;

    private final ExcelService excelService;

    @Autowired
    public ReportLaborServiceImpl(ProviderAPI providerAPI, TenantProperties tenantProperties, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.tenantProperties = tenantProperties;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportLabor(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate) {
        try {

            AllocationTimeFilter allocationTimeFilter = new AllocationTimeFilter.AllocationTimeFilterBuilder()
                    .projectId(projectId)
                    .teamName(teamName)
                    .startDate(dateOfService)
                    .endDate(dateOfService)
                    .payrollStartDate(payrollDate)
                    .payrollEndDate(payrollDate)
                    .status(AllocationSubmissionStatus.APPROVED)
                    .includes(List.of("crafts"))
                    .excludePerDiem(true)
                    .build();

            Tuple3<Project, List<BillingCodeType>, Tuple4<List<AllocationTime>, List<User>, List<CostCodeTagAssociation>, List<CostCodeAssociation>>> result =
                    Mono.zip(providerAPI.findProjectById(projectId).defaultIfEmpty(new Project()),
                            providerAPI.findBillingCodeTypes(true).defaultIfEmpty(new ArrayList<>()),
                            providerAPI.getAllocationTimes(allocationTimeFilter)
                                    .defaultIfEmpty(new ArrayList<>())
                                    .flatMap(allocationTimes -> {
                                        Set<Long> approvedUserIds = allocationTimes.stream()
                                                .map(AllocationTime::getAllocationSubmission)
                                                .map(AllocationSubmission::getApproveUserId)
                                                .filter(Objects::nonNull)
                                                .collect(toSet());
                                        Set<Long> laborKeys = allocationTimes.stream()
                                                .map(AllocationTime::getCostCodePercs)
                                                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                                                .flatMap(Collection::stream)
                                                .map(CostCodePerc::getId)
                                                .collect(toSet());
                                        Set<String> costCodeSet = allocationTimes.stream()
                                                .map(AllocationTime::getCostCodePercs)
                                                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                                                .flatMap(Collection::stream)
                                                .map(CostCodePerc::getCostCodeFull)
                                                .collect(Collectors.toSet());
                                        return Mono.deferContextual(Mono::just).flatMap(
                                                ctx -> {
                                                    ProviderAPI api = ctx.get(ProviderAPI.class);
                                                    Mono<List<User>> usersMono = api.findUsersById(approvedUserIds);
                                                    Mono<List<CostCodeTagAssociation>> laborAssociationMono = api.findCostCodeTagAssociationsByKeysAndType(laborKeys, CostCodeType.DEFAULT).defaultIfEmpty(new ArrayList<>());
                                                    Mono<List<CostCodeAssociation>> costCodeAssociationSetMono = CollectionUtils.isEmpty(costCodeSet) ? Mono.just(new ArrayList<>()) : api.findCostCodeAssociationsByProjectIdAndDateOfService(projectId, dateOfService, costCodeSet).defaultIfEmpty(new ArrayList<>());
                                                    return Mono.zip(Mono.just(allocationTimes), usersMono, laborAssociationMono, costCodeAssociationSetMono);
                                                });
                                    })
                                    .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                    ).block();

            if (result == null) {
                return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();
            }
            Project project = result.getT1();
            List<BillingCodeType> billingCodeTypes = result.getT2();
            List<AllocationTime> allocationTimes = result.getT3().getT1();
            List<User> users = result.getT3().getT2();
            Map<Long, User> idUserMap = users.stream()
                    .collect(Collectors.toMap(User::getId, Function.identity()));
            List<CostCodeTagAssociation> laborAssociations = result.getT3().getT3();
            List<CostCodeAssociation> associations = result.getT3().getT4();
            if (CollectionUtils.isEmpty(allocationTimes)) {
                return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();
            }
            Map<Long, List<CostCodeTagAssociation>> laborIdAssociationsMap = laborAssociations.stream()
                    .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
            Map<String, CostCodeAssociation> associationMap = associations.stream()
                    .collect(Collectors.toMap(CostCodeAssociation::getCostCode, Function.identity()));


            List<String> costCodes = allocationTimes.stream()
                    .map(AllocationTime::getCostCodePercs)
                    .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                    .flatMap(Collection::stream)
                    .map(CostCodePerc::getCostCodeFull)
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());

            // 8 fix column and dynamic cost code
            int costCodeSize = costCodes.size();

            XSSFWorkbook workbook = new XSSFWorkbook();

            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            Sheet sheet = workbook.createSheet("data");
            sheet.setDisplayGridlines(false);

            int totalColumns = 8 + costCodeSize;
            int currentRowIndex = -1;
            int currentColumnIndex = -1;

            TenantDto tenantDto = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto());
            Row row = sheet.createRow(++currentRowIndex);

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Cell cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));
            cell.setCellValue(Optional.ofNullable(tenantDto.getCompanyName()).orElse(""));
            cell.setCellStyle(headerStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));

            XSSFCellStyle rightStyle = workbook.createCellStyle();
            rightStyle.setAlignment(HorizontalAlignment.RIGHT);
            XSSFCellStyle centerStyle = workbook.createCellStyle();
            centerStyle.setAlignment(HorizontalAlignment.CENTER);
            cell.setCellValue(Optional.ofNullable(tenantDto.getCompanyAddress()).orElse(""));
            cell.setCellStyle(centerStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));
            XSSFCellStyle italicStyle = workbook.createCellStyle();
            italicStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 12);
            font.setItalic(true);
            italicStyle.setFont(font);
            cell.setCellValue("Time Sheet");
            cell.setCellStyle(italicStyle);

            XSSFCellStyle boldStyle = workbook.createCellStyle();
            XSSFFont boldFont = workbook.createFont();
            boldFont.setBold(true);
            boldStyle.setFont(boldFont);

            XSSFCellStyle boldRightStyle = workbook.createCellStyle();
            boldRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            boldRightStyle.setFont(boldFont);
            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Time Sheet Name");
            cell.setCellStyle(boldRightStyle);

            AllocationSubmission allocationSubmission = allocationTimes.iterator().next().getAllocationSubmission();
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(allocationSubmission.getTeamName());

            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1 + costCodeSize));

            currentColumnIndex += (2 + costCodeSize);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Date - Shift");
            cell.setCellStyle(boldRightStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(String.format("%s [%s] - %s", dateFormatter.format(dateOfService),
                    LocalDateUtil.getDayOfWeekName(dateOfService), "All"));

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Job");
            cell.setCellStyle(boldRightStyle);

            String jobSubJob = project.getJobSubJob();
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(jobSubJob);

            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1 + costCodeSize));

            currentColumnIndex += (2 + costCodeSize);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Foreman");
            cell.setCellStyle(boldRightStyle);

            String submitUserName = "Unknown";
            if (allocationSubmission.getSubmitUserId() != null && idUserMap.containsKey(allocationSubmission.getSubmitUserId())) {
                User submitUser = idUserMap.get(allocationSubmission.getSubmitUserId());
                submitUserName = submitUser.getDisplayName();
            }
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(StringUtils.hasText(submitUserName) ? submitUserName : "Unknown");

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Notes");
            cell.setCellStyle(boldRightStyle);

            XSSFCellStyle codeTypeStyle = workbook.createCellStyle();
            codeTypeStyle.setAlignment(HorizontalAlignment.RIGHT);

            XSSFCellStyle blueBorderedStyle = workbook.createCellStyle();
            blueBorderedStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.cyan, null));
            blueBorderedStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            blueBorderedStyle.setBorderBottom(BorderStyle.THIN);
            blueBorderedStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            blueBorderedStyle.setBorderLeft(BorderStyle.THIN);
            blueBorderedStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            blueBorderedStyle.setBorderRight(BorderStyle.THIN);
            blueBorderedStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            blueBorderedStyle.setBorderTop(BorderStyle.THIN);
            blueBorderedStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            XSSFCellStyle borderedRightStyle = workbook.createCellStyle();
            borderedRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            borderedRightStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
            borderedRightStyle.setBorderBottom(BorderStyle.THIN);
            borderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderLeft(BorderStyle.THIN);
            borderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderRight(BorderStyle.THIN);
            borderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderTop(BorderStyle.THIN);
            borderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            XSSFCellStyle borderedStyle = workbook.createCellStyle();
            borderedRightStyle.setBorderBottom(BorderStyle.THIN);
            borderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderLeft(BorderStyle.THIN);
            borderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderRight(BorderStyle.THIN);
            borderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderTop(BorderStyle.THIN);
            borderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            for (int i = 0; i < billingCodeTypes.size(); i++) {
                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = 2;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(billingCodeTypes.get(i).getCodeType());
                cell.setCellStyle(codeTypeStyle);

                boolean evenColumn = i % 2 == 0;

                for (String costCode : costCodes) {
                    CostCodeAssociation association = associationMap.getOrDefault(costCode, new CostCodeAssociation());
                    String levelX = association.getLevel(i + 1);
                    cell = row.createCell(++currentColumnIndex);
                    cell.setCellValue(levelX);
                    cell.setCellStyle(evenColumn ? blueBorderedStyle : borderedStyle);
                }
            }

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = 2;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Rep#");
            cell.setCellStyle(codeTypeStyle);

            boolean evenColumn = billingCodeTypes.size() % 2 == 0;

            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(costCode);
                cell.setCellStyle(evenColumn ? blueBorderedStyle : borderedStyle);
            }

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Number");
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Badge");

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Classifications(s)");
            cell.setCellStyle(centerStyle);
            cell = row.createCell(totalColumns - 1);
            cell.setCellValue("Totals");
            cell.setCellStyle(rightStyle);

            Map<String, BigDecimal> costCodeStTotal = new HashMap<>();
            Map<String, BigDecimal> costCodeOtTotal = new HashMap<>();
            Map<String, BigDecimal> costCodeDtTotal = new HashMap<>();
            Map<String, BigDecimal> employeeStTotal = new HashMap<>();
            Map<String, BigDecimal> employeeOtTotal = new HashMap<>();
            Map<String, BigDecimal> employeeDtTotal = new HashMap<>();
            Map<String, BigDecimal> costCodeStBillingTotal = new HashMap<>();
            Map<String, BigDecimal> costCodeOtBillingTotal = new HashMap<>();
            Map<String, BigDecimal> costCodeDtBillingTotal = new HashMap<>();
            XSSFCellStyle pinkStyle = workbook.createCellStyle();
            pinkStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.PINK, null));
            pinkStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            XSSFCellStyle greenBorderedRightStyle = workbook.createCellStyle();
            greenBorderedRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            greenBorderedRightStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.GREEN, null));
            greenBorderedRightStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            greenBorderedRightStyle.setBorderBottom(BorderStyle.THIN);
            greenBorderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            greenBorderedRightStyle.setBorderLeft(BorderStyle.THIN);
            greenBorderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            greenBorderedRightStyle.setBorderRight(BorderStyle.THIN);
            greenBorderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            greenBorderedRightStyle.setBorderTop(BorderStyle.THIN);
            greenBorderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            XSSFCellStyle yellowBorderedRightStyle = workbook.createCellStyle();
            yellowBorderedRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            yellowBorderedRightStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.YELLOW, null));
            yellowBorderedRightStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            yellowBorderedRightStyle.setBorderBottom(BorderStyle.THIN);
            yellowBorderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            yellowBorderedRightStyle.setBorderLeft(BorderStyle.THIN);
            yellowBorderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            yellowBorderedRightStyle.setBorderRight(BorderStyle.THIN);
            yellowBorderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            yellowBorderedRightStyle.setBorderTop(BorderStyle.THIN);
            yellowBorderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            for (AllocationTime allocationTime : allocationTimes) {
                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 2));
                currentColumnIndex += 2;
                cell.setCellValue(allocationTime.getEmpId());
                cell.setCellStyle(pinkStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("DT");
                cell.setCellStyle(greenBorderedRightStyle);
                Set<CostCodePerc> costCodePercs = allocationTime.getCostCodePercs();
                if (!CollectionUtils.isEmpty(costCodePercs)) {
                    costCodePercs.forEach(ccp -> ccp.setCostCodeTagAssociations(laborIdAssociationsMap.getOrDefault(ccp.getId(), new ArrayList<>())));
                }

                Map<String, List<CostCodePerc>> costCodePercMap = costCodePercs.stream()
                        .collect(Collectors.groupingBy(CostCodePerc::getCostCodeFull));
                BigDecimal dtTotal = BigDecimal.ZERO;
                Employee employee = allocationTime.getEmployee();
                Craft craft = employee.getCraftEntity();
                if (craft == null) craft = new Craft();
                BigDecimal billableSt = craft.getBillableST();
                BigDecimal billableOt = craft.getBillableOT();
                BigDecimal billableDt = craft.getBillableDT();

                for (String costCode : costCodes) {
                    cell = row.createCell(++currentColumnIndex);

                    List<CostCodePerc> costCodePercList = costCodePercMap.getOrDefault(costCode, new ArrayList<>());
                    BigDecimal hour = costCodePercList.stream()
                            .map(CostCodePerc::getDtHour)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    dtTotal = dtTotal.add(hour);
                    cell.setCellValue(hour.compareTo(BigDecimal.ZERO) == 0 ? "" :
                            hour.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                    cell.setCellStyle(greenBorderedRightStyle);

                    BigDecimal codeTotal = costCodeDtTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeTotal = codeTotal.add(hour);
                    costCodeDtTotal.put(costCode, codeTotal);

                    BigDecimal codeDtBillingTotal = costCodeDtBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeDtBillingTotal = codeDtBillingTotal.add(hour.multiply(billableDt));
                    costCodeDtBillingTotal.put(costCode, codeDtBillingTotal);
                }
                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1));
                currentColumnIndex += 1;
                cell.setCellStyle(greenBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellStyle(greenBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(dtTotal.compareTo(BigDecimal.ZERO) == 0 ? "" :
                        dtTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(greenBorderedRightStyle);

                BigDecimal tTotal = employeeDtTotal.getOrDefault(allocationTime.getEmpId(), BigDecimal.ZERO);
                tTotal = tTotal.add(dtTotal);
                employeeDtTotal.put(allocationTime.getEmpId(), tTotal);

                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(String.format("%s, %s", employee.getLastName(), employee.getFirstName()));

                currentColumnIndex += 2;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("OT");
                cell.setCellStyle(yellowBorderedRightStyle);

                BigDecimal otTotal = BigDecimal.ZERO;
                for (String costCode : costCodes) {
                    cell = row.createCell(++currentColumnIndex);

                    List<CostCodePerc> costCodePercList = costCodePercMap.getOrDefault(costCode, new ArrayList<>());
                    BigDecimal hour = costCodePercList.stream()
                            .map(CostCodePerc::getOtHour)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    otTotal = otTotal.add(hour);
                    cell.setCellValue(hour.compareTo(BigDecimal.ZERO) == 0 ? "" :
                            hour.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                    cell.setCellStyle(yellowBorderedRightStyle);

                    BigDecimal codeTotal = costCodeOtTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeTotal = codeTotal.add(hour);
                    costCodeOtTotal.put(costCode, codeTotal);

                    BigDecimal codeOtBillingTotal = costCodeOtBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeOtBillingTotal = codeOtBillingTotal.add(hour.multiply(billableOt));
                    costCodeOtBillingTotal.put(costCode, codeOtBillingTotal);
                }
                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1));
                currentColumnIndex += 1;
                cell.setCellStyle(yellowBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellStyle(yellowBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(otTotal.compareTo(BigDecimal.ZERO) == 0 ? "" :
                        otTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(yellowBorderedRightStyle);

                tTotal = employeeOtTotal.getOrDefault(allocationTime.getEmpId(), BigDecimal.ZERO);
                tTotal = tTotal.add(otTotal);
                employeeOtTotal.put(allocationTime.getEmpId(), tTotal);

                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                currentColumnIndex += 1;
                cell = row.createCell(++currentColumnIndex);
                if (!StringUtils.hasText(employee.getBadge())) {
                    cell.setBlank();
                } else {
                    cell.setCellValue(employee.getBadge());
                }

                currentColumnIndex += 1;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("ST");
                cell.setCellStyle(borderedRightStyle);

                BigDecimal stTotal = BigDecimal.ZERO;
                for (String costCode : costCodes) {
                    cell = row.createCell(++currentColumnIndex);

                    List<CostCodePerc> costCodePercList = costCodePercMap.getOrDefault(costCode, new ArrayList<>());
                    BigDecimal hour = costCodePercList.stream()
                            .map(CostCodePerc::getStHour)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    stTotal = stTotal.add(hour);
                    cell.setCellValue(hour.compareTo(BigDecimal.ZERO) == 0 ? "" :
                            hour.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                    cell.setCellStyle(borderedRightStyle);

                    BigDecimal codeTotal = costCodeStTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeTotal = codeTotal.add(hour);
                    costCodeStTotal.put(costCode, codeTotal);

                    BigDecimal codeStBillingTotal = costCodeStBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeStBillingTotal = codeStBillingTotal.add(hour.multiply(billableSt));
                    costCodeStBillingTotal.put(costCode, codeStBillingTotal);
                }
                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1));
                currentColumnIndex += 1;
                cell.setCellStyle(borderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellStyle(borderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(stTotal.compareTo(BigDecimal.ZERO) == 0 ? "" :
                        stTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(borderedRightStyle);

                tTotal = employeeStTotal.getOrDefault(allocationTime.getEmpId(), BigDecimal.ZERO);
                tTotal = tTotal.add(stTotal);
                employeeStTotal.put(allocationTime.getEmpId(), tTotal);
            }

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Hours Summary");
            cell.setCellStyle(boldRightStyle);
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Straightime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeStTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : codeTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            BigDecimal totalStraightime = employeeStTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(totalStraightime.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Overtime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeOtTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : codeTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            BigDecimal totalOvertime = employeeOtTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(totalOvertime.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Doubletime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeDtTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : codeTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            BigDecimal totalDoubletime = employeeDtTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(totalDoubletime.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Totals:");
            cell.setCellStyle(rightStyle);
            BigDecimal total = BigDecimal.ZERO;
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeStTotal.getOrDefault(costCode, BigDecimal.ZERO)
                        .add(costCodeOtTotal.getOrDefault(costCode, BigDecimal.ZERO))
                        .add(costCodeDtTotal.getOrDefault(costCode, BigDecimal.ZERO));
                total = codeTotal.add(total);

                cell.setCellValue(codeTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(total.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
            cell.setCellStyle(boldRightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Billing Summary");
            cell.setCellStyle(boldRightStyle);
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Straightime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);
                BigDecimal codeTotal = costCodeStBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : NumberUtil.currencyFormat(codeTotal.setScale(2, RoundingMode.HALF_DOWN)));
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            totalStraightime = costCodeStBillingTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(NumberUtil.currencyFormat(totalStraightime.setScale(2, RoundingMode.HALF_DOWN)));
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Overtime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeOtBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : NumberUtil.currencyFormat(codeTotal.setScale(2, RoundingMode.HALF_DOWN)));
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            totalOvertime = costCodeOtBillingTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(NumberUtil.currencyFormat(totalOvertime.setScale(2, RoundingMode.HALF_DOWN)));
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Doubletime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeDtBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : NumberUtil.currencyFormat(codeTotal.setScale(2, RoundingMode.HALF_DOWN)));
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            totalDoubletime = costCodeDtBillingTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(NumberUtil.currencyFormat(totalDoubletime.setScale(2, RoundingMode.HALF_DOWN)));
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Totals:");
            cell.setCellStyle(rightStyle);
            total = BigDecimal.ZERO;
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeStBillingTotal.getOrDefault(costCode, BigDecimal.ZERO)
                        .add(costCodeOtBillingTotal.getOrDefault(costCode, BigDecimal.ZERO))
                        .add(costCodeDtBillingTotal.getOrDefault(costCode, BigDecimal.ZERO));
                total = codeTotal.add(total);

                cell.setCellValue(NumberUtil.currencyFormat(codeTotal.setScale(2, RoundingMode.HALF_DOWN)));
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(NumberUtil.currencyFormat(total.setScale(2, RoundingMode.HALF_DOWN)));
            cell.setCellStyle(rightStyle);

            currentRowIndex += 1;
            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);

            cell.setCellValue("Contractor:");
            cell.setCellStyle(boldRightStyle);

            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns / 2 - 1));
            cell.setCellValue("____________________________________________________");

            currentColumnIndex = totalColumns / 2 - 1;
            currentColumnIndex += 2;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Client:");
            cell.setCellStyle(boldRightStyle);

            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));
            cell.setCellValue("____________________________________________________");

            return excelService.downloadExcel("Labor Time Sheet", workbook);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}
