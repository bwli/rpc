package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.rpc.RpcAPI;
import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.dto.RpcProfitDto;
import com.timaven.timekeeping.model.dto.RpcProfitFilter;
import com.timaven.timekeeping.model.dto.employeeVo;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportRpcProfitDetailService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;

@Service
public class ReportRpcProfitDetailServiceImpl implements ReportRpcProfitDetailService {

    private final ProviderAPI providerAPI;
    private final RpcAPI rpcAPI;
    private final ExcelService excelService;

    @Autowired
    public ReportRpcProfitDetailServiceImpl(ProviderAPI providerAPI, ExcelService excelService, RpcAPI rpcAPI) {
        this.providerAPI = providerAPI;
        this.rpcAPI = rpcAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportRpcProfitDetails(Long projectId, LocalDate startDate, LocalDate endDate) {
        List<RpcProfitDto> rpcProfitDetails = rpcAPI.getProfitDetails(new RpcProfitFilter(projectId,startDate,endDate)).block();
        Project project = providerAPI.findProjectById(projectId).block();
        if (CollectionUtils.isEmpty(rpcProfitDetails) || ObjectUtils.isEmpty(project)) {
            return excelService.downloadEmpty();
        }
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Sheet sheet = workbook.createSheet("data");

            int colIndex = 0;
            int rowIndex = 0;

            Row row = sheet.createRow(rowIndex++);
            Cell cell = row.createCell(colIndex++);
            cell.setCellValue("Project #");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(colIndex++);
            String projectDescription = project.getDescription();
            if (!StringUtils.hasText(project.getDescription())) projectDescription = "";
            if (StringUtils.hasText(project.getJobSubJob())) {
                cell.setCellValue(project.getJobSubJob() + " " + projectDescription);
            } else {
                cell.setBlank();
            }

            colIndex = 0;
            row = sheet.createRow(rowIndex++);
            cell = row.createCell(colIndex++);

            for (RpcProfitDto rpcProfitDto : rpcProfitDetails) {
                colIndex = 0;
                row = sheet.createRow(rowIndex++);
                cell = row.createCell(colIndex++);

                // create costCode title
                cell.setCellValue("Cost Code");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Cost");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Billed");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Profit");
                cell.setCellStyle(headerStyle);

                colIndex = 0;
                row = sheet.createRow(rowIndex++);
                cell = row.createCell(colIndex++);

                // create costCode value
                if (rpcProfitDto.getCostCode() != null) {
                    cell.setCellValue(rpcProfitDto.getCostCode());
                }

                cell = row.createCell(colIndex++);
                if (rpcProfitDto.getCostCodeCost() != null) {
                    cell.setCellValue(rpcProfitDto.getCostCodeCost().doubleValue());
                }

                cell = row.createCell(colIndex++);
                if (rpcProfitDto.getCostCodeBilled() != null) {
                    cell.setCellValue(rpcProfitDto.getCostCodeBilled().doubleValue());
                }

                cell = row.createCell(colIndex++);
                if (rpcProfitDto.getCostCodeProfit() != null) {
                    cell.setCellValue(rpcProfitDto.getCostCodeProfit().doubleValue());
                }

                colIndex = 0;
                row = sheet.createRow(rowIndex++);
                cell = row.createCell(colIndex++);

                // create employee details
                cell.setCellValue("Details");
                cell.setCellStyle(headerStyle);

                colIndex = 0;
                row = sheet.createRow(rowIndex++);
                cell = row.createCell(colIndex++);

                // create employee title
                cell.setCellValue("Name");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Cost");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Billed");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Profit");
                cell.setCellStyle(headerStyle);

                // create employee value
                for (employeeVo employee : rpcProfitDto.getEmployeeVOS()) {

                    colIndex = 0;
                    row = sheet.createRow(rowIndex++);
                    cell = row.createCell(colIndex++);

                    if (employee.getFirstName() != null && employee.getLastName() != null) {
                        cell.setCellValue(employee.getFirstName() + " " + employee.getLastName());
                    }

                    cell = row.createCell(colIndex++);
                    if (employee.getCost() != null) {
                        cell.setCellValue(employee.getCost().doubleValue());
                    }

                    cell = row.createCell(colIndex++);
                    if (employee.getBilled() != null) {
                        cell.setCellValue(employee.getBilled().doubleValue());
                    }

                    cell = row.createCell(colIndex++);
                    if (employee.getProfit() != null) {
                        cell.setCellValue(employee.getProfit().doubleValue());
                    }
                }

                // create costCode separator
                colIndex = 0;
                row = sheet.createRow(rowIndex++);
                cell = row.createCell(colIndex++);
            }

            return excelService.downloadExcel(String.format("PO Project report for %s", project.getJobSubJob() + " " + projectDescription), workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}
