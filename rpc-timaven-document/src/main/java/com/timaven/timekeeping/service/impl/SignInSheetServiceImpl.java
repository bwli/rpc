package com.timaven.timekeeping.service.impl;

import be.quodlibet.boxable.*;
import be.quodlibet.boxable.image.Image;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.controller.BaseController;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.EmpWorkdays;
import com.timaven.timekeeping.model.dto.TenantDto;
import com.timaven.timekeeping.service.SignInSheetService;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple4;
import reactor.util.function.Tuple5;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.timaven.timekeeping.util.PdfUtility.hex2Rgb;

@Service
public class SignInSheetServiceImpl extends PdfBaseService implements SignInSheetService {

    protected SignInSheetServiceImpl(ProviderAPI providerAPI, TenantProperties tenantProperties) {
        super(providerAPI, tenantProperties);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportSignInSheet1(Long projectId, String signInSheet, LocalDate dateOfService) {
        Mono<Rule> ruleMono = providerAPI.findRuleByProjectId(projectId);
        Mono<Project> projectMono = providerAPI.findProjectById(projectId);
        AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
        String s3LogoKey = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getS3Logo();

        Image image = null;
        if (StringUtils.hasText(s3LogoKey)) {
            try {
                image = new Image(ImageIO.read(s3client.getObject(buck_name, s3LogoKey).getObjectContent()));
                float imageWidth = 150f;
                image = image.scaleByWidth(imageWidth);
            } catch (Exception ignore) {
            }
        }

        EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, dateOfService, dateOfService);
        Mono<List<Employee>> employeesMono = providerAPI.getEmployees(filter);
        Tuple4<Rule, Project, List<Employee>, List<EmpWorkdays>> tuple4 =
                Mono.zip(ruleMono, projectMono, employeesMono)
                        .flatMap(tuple -> {
                            Rule rule = tuple.getT1();
                            Project project = tuple.getT2();
                            List<Employee> employees = tuple.getT3();
                            if (employees.isEmpty()) return Mono.empty();
                            long days = rule.getDayOffThreshold();
                            if (StringUtils.hasText(signInSheet)) {
                                employees = employees.stream()
                                        .filter(e -> e.getSignInSheet() != null && e.getSignInSheet().equals(signInSheet))
                                        .collect(Collectors.toList());
                            }
                            Set<String> employeeIds = employees.stream().map(Employee::getEmpId).collect(Collectors.toSet());
                            return Mono.zip(Mono.just(rule), Mono.just(project), Mono.just(employees), Mono.deferContextual(Mono::just)
                                    .flatMap(ctx -> {
                                        ProviderAPI api = ctx.get(ProviderAPI.class);
                                        return api.findDayOffEmployees(employeeIds, dateOfService, days);
                                    }));
                        })
                        .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                        .block();

        if (tuple4 == null) return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();

        Rule rule = tuple4.getT1();
        Project project = tuple4.getT2();
        List<Employee> employees = tuple4.getT3();
        List<EmpWorkdays> empCountList = tuple4.getT4();
        Map<String, List<Employee>> sheetEmployeeMap = employees.stream()
                .peek(e -> {
                    if (e.getSignInSheet() == null) {
                        e.setSignInSheet("");
                    }
                })
                .collect(Collectors.groupingBy(Employee::getSignInSheet, TreeMap::new,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(), e -> e.stream()
                                        .sorted(Comparator.comparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                                                .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder())))
                                        .collect(Collectors.toList())))));
        Map<String, Long> empDays = employees.stream()
                .collect(Collectors.toMap(Employee::getEmpId, employee -> 0L));
        if (!CollectionUtils.isEmpty(empCountList)) {
            for (EmpWorkdays empWorkdays : empCountList) {
                empDays.put(empWorkdays.getEmpId(), empWorkdays.getConsecutiveDays());
            }
        }

        List<PDDocument> documents = new ArrayList<>();
        for (Map.Entry<String, List<Employee>> entry : sheetEmployeeMap.entrySet()) {
            List<Employee> employeeList = entry.getValue();
            String timesheet = entry.getKey();
            if (CollectionUtils.isEmpty(employeeList)) continue;

            PDDocument document = new PDDocument();
            PDPage page = new PDPage(PDRectangle.LETTER);
            document.addPage(page);

            float height = page.getMediaBox().getHeight();
            float width = page.getMediaBox().getWidth();
            float topMargin = 10f;
            float bottomMargin = 50f;
            float leftMargin = 36f;
            float rightMargin = 16f;

            // starting y position is whole page height subtracted by top and bottom margin
            float yStartNewPage = height - (topMargin + bottomMargin);
            // we want table across whole page width (subtracted by left and right margin of course)
            float tableWidth = width - (rightMargin + leftMargin);

            // y position is your coordinate of top left corner of the table
            float yPosition = height - topMargin;

            // Add header
            try {
                PDPageContentStream contentStream = new PDPageContentStream(document, page,
                        PDPageContentStream.AppendMode.APPEND, true);
                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, false, true);

                // set default line spacing for entire table
                table.setLineSpacing(0.6f);

                Row<PDPage> row = table.createRow(54f);

                Cell<PDPage> cell = row.createCell(50f, "Sign-In", HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM);
                cell.setFontSize(25f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(hex2Rgb("#c55910"));
                if (image != null) {
                    row.createImageCell(50f,
                            image, HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE);
                } else {
                    row.createCell(50f, "", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
                }

                yPosition = table.draw();

                yPosition -= 10;

                contentStream.moveTo(36f, yPosition);
                contentStream.lineTo(width - 34f, yPosition);
                contentStream.setLineWidth(4f);
                contentStream.stroke();

                yPosition -= 10;

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, true, true);

                row = table.createRow(20f);

                cell = row.createCell(23f, "Job - Sub Job Number", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(10f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(48f, Optional.ofNullable(project.getJobSubJob()).orElse(""), HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(10f);
                cell.setFont(PDType1Font.HELVETICA);

                row = table.createRow(20f);

                cell = row.createCell(23f, "Timesheet Name", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(10f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(48f, timesheet, HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(10f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.BLACK);

                yPosition = table.draw();

                yPosition -= 40;

                String weekday = dateOfService.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.US);

                contentStream.beginText();
                contentStream.newLineAtOffset(36f, yPosition);
                contentStream.setNonStrokingColor(hex2Rgb("#c55910"));
                contentStream.setFont(PDType1Font.HELVETICA_BOLD, 20);
                contentStream.showText(weekday);
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(166f, yPosition);
                contentStream.setNonStrokingColor(hex2Rgb("#595959"));
                contentStream.setFont(PDType1Font.HELVETICA_BOLD, 20);
                contentStream.showText(dateOfService.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
                contentStream.endText();

                yPosition -= 15;

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, true, true);
                table.setLineSpacing(1.5f);

                row = table.createRow(20f);
                cell = row.createCell(4f, "#",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(11f, "Emp. ID",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(7f, "Craft",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(25f, "Name",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(25f, "Signature",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(10f, "In",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(10f, "Out",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell = row.createCell(8f, "Days Worked",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setTextColor(Color.white);
                cell.setLeftPadding(0);
                cell.setRightPadding(0);

                table.addHeaderRow(row);

                for (int i = 0; i < employeeList.size() || i < 25; i++) {
                    Employee employee = i < employeeList.size() ? employeeList.get(i) : new Employee();
                    if (!StringUtils.hasText(employee.getEmpId())) employee.setEmpId("");

                    Color bgColor = empDays.getOrDefault(employee.getEmpId(), 0L) < rule.getDayOffThreshold() ? i % 2 == 0 ? Color.lightGray : Color.white : Color.gray;

                    row = table.createRow(20f);
                    cell = row.createCell(4f, String.valueOf(i + 1),
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(11f, employee.getEmpId(),
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(7f, Optional.ofNullable(employee.getCraft()).orElse(""),
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    String name = StringUtils.hasText(employee.getEmpId()) ? String.format("%s, %s", Optional.ofNullable(employee.getLastName()).orElse(""),
                            Optional.ofNullable(employee.getFirstName()).orElse("")) : "";
                    cell = row.createCell(25f, name,
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(25f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(10f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(10f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    String daysWorked = empDays.containsKey(employee.getEmpId()) ? String.valueOf(empDays.getOrDefault(employee.getEmpId(), 0L)) : "";
                    cell = row.createCell(8f, daysWorked,
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                }
                yPosition = table.draw();

                yPosition -= 25;

                if (table.getCurrentPage() != page) {
                    contentStream.close();
                    page = table.getCurrentPage();
                    contentStream = new PDPageContentStream(document, page,
                            PDPageContentStream.AppendMode.APPEND, true);
                }

                table = new BaseTable(yPosition, yPosition, 0f, 330f, leftMargin,
                        document, page, true, true);

                row = table.createRow(20f);

                cell = row.createCell(33f, "Authorization (if required)", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));
                cell.setLeftPadding(0f);
                cell.setRightPadding(0f);

                row.createCell(66f, "", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);

                table.draw();

                table = new BaseTable(yPosition, yPosition, 0f, 180f, 416f,
                        document, page, true, true);

                row = table.createRow(20f);

                cell = row.createCell(40f, "Date", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                row.createCell(60f, "", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);

                table.draw();

                contentStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            documents.add(document);
        }

        try {
            PDDocument finalDocument = new PDDocument();
            PDFMergerUtility utility = new PDFMergerUtility();
            for (PDDocument doc : documents) {
                utility.appendDocument(finalDocument, doc);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            finalDocument.save(outputStream);
            finalDocument.close();

            for (PDDocument doc : documents) {
                doc.close();
            }
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            String teamStr = StringUtils.hasText(signInSheet) ? "_" + signInSheet.replace(",", "").replace(" ", "_") : "";
            headers.add("Content-Disposition", String.format("attachment; filename=SignIn_Sheet_%s%s.pdf", dateOfService, teamStr));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(inputStream));
        } catch (IOException ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
        }
    }

    @Override
    public ResponseEntity<InputStreamResource> exportSignInSheet2(Long projectId, String signInSheet, LocalDate dateOfService) {
        Mono<Rule> ruleMono = providerAPI.findRuleByProjectId(projectId);
        Mono<Project> projectMono = providerAPI.findProjectById(projectId);
        Mono<Shift> shiftMono = providerAPI.findDefaultShiftByProjectId(projectId);
        AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
        String s3LogoKey = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getS3Logo();
        String username = BaseController.getUsername();

        Image image = null;
        if (StringUtils.hasText(s3LogoKey)) {
            try {
                image = new Image(ImageIO.read(s3client.getObject(buck_name, s3LogoKey).getObjectContent()));
                float imageWidth = 150f;
                image = image.scaleByWidth(imageWidth);
            } catch (Exception ignore) {
            }
        }

        EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, dateOfService, dateOfService);
        Mono<List<Employee>> employeesMono = providerAPI.getEmployees(filter);
        Tuple5<Rule, Project, Shift, List<Employee>, List<EmpWorkdays>> tuple5 =
                Mono.zip(ruleMono, projectMono, shiftMono, employeesMono)
                        .flatMap(tuple -> {
                            Rule rule = tuple.getT1();
                            Project project = tuple.getT2();
                            Shift shift = tuple.getT3();
                            List<Employee> employees = tuple.getT4();
                            long days = rule.getDayOffThreshold();
                            Set<String> employeeIds = employees.stream().map(Employee::getEmpId).collect(Collectors.toSet());
                            Map<String, List<Employee>> teamEmployeeMap = employees.stream()
                                    .filter(e -> e.getSignInSheet() != null && e.getSignInSheet().equals(signInSheet))
                                    .filter(e -> StringUtils.hasText(e.getTeamName()))
                                    .collect(Collectors.groupingBy(Employee::getTeamName, TreeMap::new,
                                            Collectors.mapping(Function.identity(),
                                                    Collectors.collectingAndThen(Collectors.toList(), e -> e.stream()
                                                            .sorted(Comparator.comparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                                                                    .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder())))
                                                            .collect(Collectors.toList())))));
                            return Mono.zip(Mono.just(rule), Mono.just(project), Mono.just(shift), Mono.just(employees), Mono.deferContextual(Mono::just)
                                    .flatMap(ctx -> {
                                        ProviderAPI api = ctx.get(ProviderAPI.class);
                                        return api.findDayOffEmployees(employeeIds, dateOfService, days);
                                    }));
                        })
                        .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                        .block();

        if (tuple5 == null) return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();

        Rule rule = tuple5.getT1();
        Project project = tuple5.getT2();
        Shift shift = tuple5.getT3();
        List<Employee> employees = tuple5.getT4();
        List<EmpWorkdays> empCountList = tuple5.getT5();
        Map<String, List<Employee>> teamEmployeeMap = employees.stream()
                .filter(e -> StringUtils.hasText(e.getTeamName()))
                .collect(Collectors.groupingBy(Employee::getTeamName, TreeMap::new,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(), e -> e.stream()
                                        .sorted(Comparator.comparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                                                .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder())))
                                        .collect(Collectors.toList())))));
        Map<String, Long> empDays = employees.stream()
                .collect(Collectors.toMap(Employee::getEmpId, employee -> 0L));
        if (!CollectionUtils.isEmpty(empCountList)) {
            for (EmpWorkdays empWorkdays : empCountList) {
                empDays.put(empWorkdays.getEmpId(), empWorkdays.getConsecutiveDays());
            }
        }

        List<PDDocument> documents = new ArrayList<>();
        for (Map.Entry<String, List<Employee>> entry : teamEmployeeMap.entrySet()) {
            List<Employee> employeeList = entry.getValue();
            String timesheet = entry.getKey();
            if (CollectionUtils.isEmpty(employeeList)) continue;
            String teamShift = employeeList.stream()
                    .map(Employee::getShift)
                    .filter(StringUtils::hasText)
                    .collect(Collectors.joining(", "));

            PDDocument document = new PDDocument();
            PDPage page = new PDPage(PDRectangle.LETTER);
            document.addPage(page);

            float height = page.getMediaBox().getHeight();
            float width = page.getMediaBox().getWidth();
            float topMargin = 50f;
            float bottomMargin = 50f;
            float leftMargin = 50f;
            float rightMargin = 50f;

            // starting y position is whole page height subtracted by top and bottom margin
            float yStartNewPage = height - (topMargin + bottomMargin);
            // we want table across whole page width (subtracted by left and right margin of course)
            float tableWidth = width - (rightMargin + leftMargin);

            // y position is your coordinate of top left corner of the table
            float yPosition = height - topMargin;

            // Add header
            try {
                PDPageContentStream contentStream = new PDPageContentStream(document, page,
                        PDPageContentStream.AppendMode.APPEND, true);
                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, false, true);

                // set default line spacing for entire table
                table.setLineSpacing(0.6f);

                Row<PDPage> row = table.createRow(50f);

                String s1 = String.format("" +
                        "<p></p>" +
                        "<p>%s</p>" +
                        "%s" +
                        "<p></p>" +
                        "<p></p>" +
                        "<p>%s</p>" +
                        "<p>%s</p>", "Shift:", "Job #", "Supervisor:", "Sign In Sheet:");
                String s2 = String.format("" +
                                "<p>%s</p>" +
                                "<p>%s</p>" +
                                "%s" +
                                "<p></p>" +
                                "<p></p>" +
                                "<p>%s</p>" +
                                "<p>%s</p>", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(dateOfService),
                        StringUtils.hasText(teamShift) ? teamShift :  shift.getName(),
                        Optional.ofNullable(project.getJobSubJob()).orElse(""),
                        username, timesheet);
                row.createCell(15f, s1, HorizontalAlignment.CENTER, VerticalAlignment.TOP);
                row.createCell(22f, s2, HorizontalAlignment.CENTER, VerticalAlignment.TOP);
                if (image != null) {
                    row.createImageCell(30f,
                            image, HorizontalAlignment.CENTER, VerticalAlignment.TOP);
                } else {
                    row.createCell(30f, "", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
                }
                row.createCell(33f, String.format("<p></p>%s%s", "Jobsite: ",
                        Optional.ofNullable(project.getDescription()).orElse("")),
                        HorizontalAlignment.RIGHT, VerticalAlignment.TOP);

                yPosition = table.draw();

                yPosition -= 10;

                contentStream.beginText();
                contentStream.newLineAtOffset(78f, yPosition);
                contentStream.setNonStrokingColor(Color.BLACK);
                contentStream.setFont(PDType1Font.HELVETICA, 8);
                contentStream.showText("Topic:");
                contentStream.endText();

                yPosition -= 5;

                contentStream.moveTo(136f, yPosition);
                contentStream.lineTo(width - 50f, yPosition);
                contentStream.stroke();

                yPosition -= 15;

                contentStream.moveTo(78f, yPosition);
                contentStream.lineTo(width - 50f, yPosition);
                contentStream.stroke();

                yPosition -= 15;

                contentStream.moveTo(78f, yPosition);
                contentStream.lineTo(width - 50f, yPosition);
                contentStream.stroke();

                yPosition -= 15;

                contentStream.moveTo(78f, yPosition);
                contentStream.lineTo(width - 50f, yPosition);
                contentStream.stroke();

                yPosition -= 20;

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, true, true);
                table.setLineSpacing(1.5f);

                row = table.createRow(20f);
                row.createCell(6f, "***",
                        HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                Cell<PDPage> cell = row.createCell(4f, "No.",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setLeftPadding(0f);
                cell.setRightPadding(0f);
                cell = row.createCell(8f, "Emp No.",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setLeftPadding(0f);
                cell.setRightPadding(0f);
                row.createCell(28f, "Name",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                row.createCell(8f, "Craft",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                row.createCell(28f, "Signature",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                row.createCell(9f, "Time in",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                row.createCell(9f, "Time out",
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                table.addHeaderRow(row);

                for (int i = 0; i < employeeList.size() || i < 20 ; i++) {
                    Employee employee = i < employeeList.size() ? employeeList.get(i) : new Employee();
                    if (!StringUtils.hasText(employee.getEmpId())) employee.setEmpId("");

                    Color bgColor = empDays.getOrDefault(employee.getEmpId(), 0L) < rule.getDayOffThreshold() ? Color.white : Color.gray;

                    row = table.createRow(20f);
                    cell = row.createCell(6f, "",
                            HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(4f, String.valueOf(i + 1),
                            HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(8f, employee.getEmpId(),
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    String name = StringUtils.hasText(employee.getEmpId()) ? String.format("%s, %s", Optional.ofNullable(employee.getLastName()).orElse(""),
                            Optional.ofNullable(employee.getFirstName()).orElse("")) : "";
                    cell = row.createCell(28f, name,
                            HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(8f, Optional.ofNullable(employee.getCraft()).orElse(""),
                            HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(28f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(9f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                    cell = row.createCell(9f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    cell.setFillColor(bgColor);
                }
                table.draw();
                contentStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            documents.add(document);
        }

        try {
            PDDocument finalDocument = new PDDocument();
            PDFMergerUtility utility = new PDFMergerUtility();
            for (PDDocument doc : documents) {
                utility.appendDocument(finalDocument, doc);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            finalDocument.save(outputStream);
            finalDocument.close();

            for (PDDocument doc : documents) {
                doc.close();
            }
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            String teamStr =  StringUtils.hasText(signInSheet) ? "_" + signInSheet.replace(",", "").replace(" ", "_") : "";
            headers.add("Content-Disposition", String.format("attachment; filename=SignIn_Sheet_%s%s.pdf", dateOfService, teamStr));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(inputStream));
        } catch (IOException ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
        }
    }
}
