package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.StageCraft;
import com.timaven.timekeeping.model.StageRoster;
import com.timaven.timekeeping.model.StageSubmission;
import com.timaven.timekeeping.model.StageTeamCostCode;
import com.timaven.timekeeping.service.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@Transactional
public class StageServiceImpl implements StageService {

    private final ProviderAPI providerAPI;

    @Autowired
    public StageServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public Mono<List<StageRoster>> saveRoster(List<StageRoster> rosters, StageSubmission submission) {
        return providerAPI.saveStageSubmission(submission)
                .flatMap(s -> {
                    rosters.forEach(r -> r.setSubmissionId(s.getId()));
                    return Mono.deferContextual(Mono::just)
                            .flatMap(ctx -> {
                                ProviderAPI api = ctx.get(ProviderAPI.class);
                                return api.saveStageRosters(rosters);
                            });
                })
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())));
    }

    @Override
    public Mono<List<StageTeamCostCode>> saveTeamCostCode(List<StageTeamCostCode> stageTeamCostCodes, StageSubmission submission) {
        return providerAPI.saveStageSubmission(submission)
                .flatMap(s -> {
                    stageTeamCostCodes.forEach(t -> t.setSubmissionId(s.getId()));
                    return Mono.deferContextual(Mono::just)
                            .flatMap(ctx -> {
                                ProviderAPI api = ctx.get(ProviderAPI.class);
                                return api.saveStageTeamCostCodes(stageTeamCostCodes);
                            });
                })
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())));
    }

    @Override
    public Mono<List<StageCraft>> saveCrafts(List<StageCraft> stageCrafts, StageSubmission submission) {
        return providerAPI.saveStageSubmission(submission)
                .flatMap(s -> {
                    stageCrafts.forEach(c -> c.setSubmissionId(s.getId()));
                    return Mono.deferContextual(Mono::just)
                            .flatMap(ctx -> {
                                ProviderAPI api = ctx.get(ProviderAPI.class);
                                return api.saveStageCrafts(stageCrafts);
                            });
                })
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())));
    }
}
