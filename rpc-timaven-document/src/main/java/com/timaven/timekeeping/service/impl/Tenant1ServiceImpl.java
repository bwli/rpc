package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import com.timaven.timekeeping.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional
public class Tenant1ServiceImpl implements MultipleTenantService {

    private final SignInSheetService signInSheetService;
    private final TimesheetService timesheetService;
    private final ReportOtService reportOtService;
    private final ReportHoursAndBillingService reportHoursAndBillingService;
    private final ReportLaborService reportLaborService;

    @Autowired
    public Tenant1ServiceImpl(SignInSheetService signInSheetService, TimesheetService timesheetService,
                              ReportOtService reportOtService,
                              ReportHoursAndBillingService reportHoursAndBillingService, ReportLaborService reportLaborService) {
        this.signInSheetService = signInSheetService;
        this.timesheetService = timesheetService;
        this.reportOtService = reportOtService;
        this.reportHoursAndBillingService = reportHoursAndBillingService;
        this.reportLaborService = reportLaborService;
    }

    @Override
    public String getTenantName() {
        return "tenant1";
    }

    @Override
    public ResponseEntity<InputStreamResource> exportSignInSheet(Long projectId, String signInSheet, LocalDate dateOfService) {
        return signInSheetService.exportSignInSheet1(projectId, signInSheet, dateOfService);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportTimesheet(Long projectId, String team, LocalDate dateOfService) {
        return timesheetService.exportTimesheet1(projectId, team, dateOfService);
    }

    @Override
    public ResponseEntity<InputStreamResource> generateOtReport(Long projectId, LocalDate weekEndDate, LocalDate payrollDate) {
        return reportOtService.generateOtReport1(projectId, weekEndDate, payrollDate);
    }
    @Override
    public ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type) {
        return reportHoursAndBillingService.reportHourAndBilling(param, type);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportLabor(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate) {
        return reportLaborService.reportLabor(projectId, teamName, dateOfService, payrollDate);
    }
}
