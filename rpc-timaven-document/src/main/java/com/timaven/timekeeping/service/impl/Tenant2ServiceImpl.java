package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import com.timaven.timekeeping.service.MultipleTenantService;
import com.timaven.timekeeping.service.SignInSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class Tenant2ServiceImpl implements MultipleTenantService {

    private final SignInSheetService signInSheetService;

    @Autowired
    public Tenant2ServiceImpl(SignInSheetService signInSheetService) {
        this.signInSheetService = signInSheetService;
    }

    @Override
    public String getTenantName() {
        return "tenant2";
    }

    @Override
    public ResponseEntity<InputStreamResource> exportSignInSheet(Long projectId, String signInSheet, LocalDate dateOfService) {
        return signInSheetService.exportSignInSheet2(projectId, signInSheet, dateOfService);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportTimesheet(Long projectId, String team, LocalDate dateOfService) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<InputStreamResource> generateOtReport(Long projectId, LocalDate weekEndDate, LocalDate payrollDate) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<InputStreamResource> reportLabor(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate) {
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}
