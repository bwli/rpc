package com.timaven.timekeeping.service.impl;

import be.quodlibet.boxable.*;
import be.quodlibet.boxable.image.Image;
import be.quodlibet.boxable.line.LineStyle;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.CellAttributes;
import com.timaven.timekeeping.model.dto.TenantDto;
import com.timaven.timekeeping.service.TimesheetService;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple4;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.timaven.timekeeping.util.PdfUtility.generateCells;
import static com.timaven.timekeeping.util.PdfUtility.hex2Rgb;

@Service
public class TimesheetServiceImpl extends PdfBaseService implements TimesheetService {

    public TimesheetServiceImpl(ProviderAPI providerAPI, TenantProperties tenantProperties) {
        super(providerAPI, tenantProperties);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportTimesheet1(Long projectId, String team, LocalDate dateOfService) {
        Mono<Project> projectMono = providerAPI.findProjectById(projectId);
        Mono<List<CostCodeLog>> costCodeLogMono = providerAPI.findCostCodesByProjectAndDate(projectId, dateOfService, null, true, null);
        AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
        String s3LogoKey = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getS3Logo();

        Image image = null;
        if (StringUtils.hasText(s3LogoKey)) {
            try {
                image = new Image(ImageIO.read(s3client.getObject(buck_name, s3LogoKey).getObjectContent()));
                float imageWidth = 150f;
                image = image.scaleByWidth(imageWidth);
            } catch (Exception ignore) {
            }
        }

        EmployeeFilter filter = new EmployeeFilter(projectId, team, null, null, null, dateOfService, dateOfService);
        Mono<List<Employee>> employeesMono = providerAPI.getEmployees(filter);
        Tuple4<Project, List<Employee>, List<CostCodeLog>, List<TeamCostCode>> tuple4 =
                Mono.zip(projectMono, employeesMono, costCodeLogMono)
                        .flatMap(tuple -> {
                            Project project = tuple.getT1();
                            List<Employee> employees = tuple.getT2();
                            if (employees.isEmpty()) return Mono.empty();
                            Set<String> teams = employees.stream()
                                    .map(Employee::getTeamName)
                                    .filter(StringUtils::hasText)
                                    .collect(Collectors.toSet());
                            List<CostCodeLog> costCodeLogs = tuple.getT3();
                            return Mono.zip(Mono.just(project), Mono.just(employees), Mono.just(costCodeLogs), Mono.deferContextual(Mono::just)
                                    .flatMap(ctx -> {
                                        ProviderAPI api = ctx.get(ProviderAPI.class);
                                        return api.findTeamCostCodesByProjectIdAndTeamsAndDateOfService(project.getId(), teams, dateOfService, null);
                                    }));
                        })
                        .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                        .block();

        if (tuple4 == null) return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();

        Project project = tuple4.getT1();
        List<Employee> employees = tuple4.getT2();
        List<CostCodeLog> costCodeLogs = tuple4.getT3();
        List<TeamCostCode> teamCostCodes = tuple4.getT4();
        Map<String, CostCodeLog> costCodeLogMap = costCodeLogs.stream()
                .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, Function.identity()));
        Map<String, List<TeamCostCode>> teamCostCodeMap = teamCostCodes.stream()
                .collect(Collectors.groupingBy(TeamCostCode::getTeamName));
        Map<String, List<Employee>> teamEmployeeMap = employees.stream()
                .filter(e -> StringUtils.hasText(e.getTeamName()))
                .collect(Collectors.groupingBy(Employee::getTeamName, TreeMap::new,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(), e -> e.stream()
                                        .sorted(Comparator.comparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                                                .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder())))
                                        .collect(Collectors.toList())))));

        List<PDDocument> documents = new ArrayList<>();
        for (Map.Entry<String, List<Employee>> entry : teamEmployeeMap.entrySet()) {
            List<Employee> employeeList = entry.getValue();
            String timesheet = entry.getKey();
            if (CollectionUtils.isEmpty(employeeList)) continue;
            List<TeamCostCode> teamCostCodeList = teamCostCodeMap.getOrDefault(timesheet, new ArrayList<>());

            PDDocument document = new PDDocument();
            PDPage page = new PDPage(new PDRectangle(PDRectangle.LETTER.getHeight(), PDRectangle.LETTER.getWidth()));
            document.addPage(page);

            float height = page.getMediaBox().getHeight();
            float width = page.getMediaBox().getWidth();
            float topMargin = 10f;
            float bottomMargin = 50f;
            float leftMargin = 36f;
            float rightMargin = 36f;

            // starting y position is whole page height subtracted by top and bottom margin
            float yStartNewPage = height - (topMargin + bottomMargin);
            // we want table across whole page width (subtracted by left and right margin of course)
            float tableWidth = width - (rightMargin + leftMargin);

            // y position is your coordinate of top left corner of the table
            float yPosition = height - topMargin;

            // Add header
            try {
                PDPageContentStream contentStream = new PDPageContentStream(document, page,
                        PDPageContentStream.AppendMode.APPEND, true);
                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, false, true);

                // set default line spacing for entire table
                table.setLineSpacing(0.6f);

                Row<PDPage> row = table.createRow(44f);

                Cell<PDPage> cell = row.createCell(50f, "Timesheet", HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM);
                cell.setFontSize(20f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(hex2Rgb("#c55910"));
                if (image != null) {
                    row.createImageCell(50f,
                            image, HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE);
                } else {
                    row.createCell(50f, "", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
                }

                yPosition = table.draw();

                yPosition -= 5;

                contentStream.moveTo(36f, yPosition);
                contentStream.lineTo(width - 34f, yPosition);
                contentStream.setLineWidth(4f);
                contentStream.stroke();

                yPosition -= 10;

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, 328f, leftMargin,
                        document, page, true, true);

                row = table.createRow(10f);
                cell = row.createCell(41f, "Job - Sub Job Number", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(59f, Optional.ofNullable(project.getJobSubJob()).orElse(""), HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA);

                row = table.createRow(10f);

                cell = row.createCell(41f, "Timesheet Name", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(59f, timesheet, HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.BLACK);

                table.draw();

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, 328f, 428f,
                        document, page, true, true);

                row = table.createRow(10f);

                cell = row.createCell(41f, "Client", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(59f, Optional.ofNullable(project.getClientName()).orElse(""), HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA);

                row = table.createRow(10f);

                cell = row.createCell(41f, "Project", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(59f, project.getDescription(), HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.BLACK);

                yPosition = table.draw();

                yPosition -= 30;

                String weekday = dateOfService.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.US);

                contentStream.beginText();
                contentStream.newLineAtOffset(36f, yPosition);
                contentStream.setNonStrokingColor(hex2Rgb("#c55910"));
                contentStream.setFont(PDType1Font.HELVETICA_BOLD, 20);
                contentStream.showText(weekday);
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(166f, yPosition);
                contentStream.setNonStrokingColor(hex2Rgb("#595959"));
                contentStream.setFont(PDType1Font.HELVETICA_BOLD, 20);
                contentStream.showText(dateOfService.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
                contentStream.endText();

                yPosition -= 10;

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, true, true);
                table.setLineSpacing(1.5f);

                LineStyle headerWhiteLine = new LineStyle(Color.white, 0f);
                LineStyle headerGreyLine = new LineStyle(hex2Rgb("#767070"), 0f);
                LineStyle headerBlackLine = new LineStyle(Color.black, 0.5f);

                int[] points = new int[]{36, 58, 108, 152, 280, 320, 360, 400, 440, 480, 520, 560, 600, 650, 756};
                CellAttributes[] firstLineAttributes = new CellAttributes[points.length - 1];
                for (int i = 0; i < points.length - 1; i++) {
                    firstLineAttributes[i] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                            .borderStyle(headerWhiteLine)
                            .build();
                }
                for (int i = 0; i < 8; i++) {
                    firstLineAttributes[i + 4] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                            .fillColor(hex2Rgb("#767070"))
                            .topBorderStyle(headerBlackLine)
                            .bottomBorderStyle(headerGreyLine)
                            .build();
                }
                CellAttributes[] secondLineAttributes = new CellAttributes[points.length - 1];
                for (int i = 0; i < points.length - 1; i++) {
                    secondLineAttributes[i] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                            .borderStyle(headerWhiteLine)
                            .build();
                }
                for (int i = 0; i < 8; i++) {
                    secondLineAttributes[i + 4] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                            .fillColor(hex2Rgb("#767070"))
                            .topBorderStyle(headerGreyLine)
                            .bottomBorderStyle(headerGreyLine)
                            .build();
                }
                CellAttributes[] headLineAttributes = new CellAttributes[points.length - 1];
                headLineAttributes[0] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                .value("#")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build();
                headLineAttributes[1] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                .value("Emp. ID")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build();
                headLineAttributes[2] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                .value("Craft")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build();
                headLineAttributes[3] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                .value("Name")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build();
                for (int i = 0; i < 8; i++) {
                    String cellText = "";
                    if (i < teamCostCodeList.size()) {
                        CostCodeLog costCodeLog = costCodeLogMap.get(teamCostCodeList.get(i).getCostCode());
                        cellText = costCodeLog == null ? "" : String.format("%s<br/>%s",
                                Optional.ofNullable(costCodeLog.getCostCodeFull()).orElse(""),
                                Optional.ofNullable(costCodeLog.getDescription()).orElse(""));
                    }
                    headLineAttributes[i + 4] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                            .value(cellText)
                            .fillColor(hex2Rgb("#767070"))
                            .textColor(Color.white)
                            .topBorderStyle(headerGreyLine)
                            .build();
                }
                headLineAttributes[12] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                        .value("Totals")
                        .fillColor(hex2Rgb("#767070"))
                        .textColor(Color.white)
                        .build();
                headLineAttributes[13] = new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                        .value("Nodes")
                        .fillColor(hex2Rgb("#767070"))
                        .textColor(Color.white)
                        .build();

                row = table.createRow(20f);
                generateCells(row, points, firstLineAttributes);
                table.addHeaderRow(row);
                row = table.createRow(20f);
                generateCells(row, points, secondLineAttributes);
                table.addHeaderRow(row);
                row = table.createRow(20f);
                generateCells(row, points, secondLineAttributes);
                table.addHeaderRow(row);
                row = table.createRow(20f);
                generateCells(row, points, headLineAttributes);
                table.addHeaderRow(row);

                for (int i = 0; i < employeeList.size() || i < 16; i++) {
                    Employee employee = i < employeeList.size() ? employeeList.get(i) : new Employee();
                    if (!StringUtils.hasText(employee.getEmpId())) employee.setEmpId("");

                    Color bgColor = i % 2 == 0 ? Color.lightGray : Color.white;

                    row = table.createRow(15f);
                    cell = row.createCell(3f, String.valueOf(i + 1),
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);
                    cell = row.createCell(7f, employee.getEmpId(),
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);
                    cell = row.createCell(6f, Optional.ofNullable(employee.getCraft()).orElse(""),
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);
                    String name = StringUtils.hasText(employee.getEmpId()) ? String.format("%s, %s", Optional.ofNullable(employee.getLastName()).orElse(""),
                            Optional.ofNullable(employee.getFirstName()).orElse("")) : "";
                    cell = row.createCell(18f, name,
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(5.5f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);

                    cell = row.createCell(7f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);
                    cell = row.createCell(15f, "",
                            HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
                    cell.setFillColor(bgColor);
                    cell.setHeight(cell.getTextHeight() + 0.5f);
                }
                yPosition = table.draw();

                yPosition -= 10;

                if (table.getCurrentPage() != page) {
                    contentStream.close();
                    page = table.getCurrentPage();
                    contentStream = new PDPageContentStream(document, page,
                            PDPageContentStream.AppendMode.APPEND, true);
                }

                table = new BaseTable(yPosition, yStartNewPage, 0f, tableWidth, leftMargin,
                        document, page, true, true);

                row = table.createRow(10f);

                cell = row.createCell(20f, "Scope Work Description", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(80f, "", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA);

                row = table.createRow(10f);

                cell = row.createCell(20f, "FCO Work Description", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.white);
                cell.setFillColor(hex2Rgb("#767070"));

                cell = row.createCell(80f, "", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
                cell.setFontSize(8f);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setTextColor(Color.BLACK);

                yPosition = table.draw();

                if (table.getCurrentPage() != page) {
                    contentStream.close();
                    page = table.getCurrentPage();
                    contentStream = new PDPageContentStream(document, page,
                            PDPageContentStream.AppendMode.APPEND, true);
                }

                yPosition -= 10f;

                table = new BaseTable(yPosition, yStartNewPage, 0f, tableWidth, leftMargin,
                        document, page, true, true);

                row = table.createRow(15f);

                LineStyle noLine = new LineStyle(Color.white, 0);
                generateCells(row, new int[]{36, 146, 364, 416, 488, 598, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                        .value("Authorization (if required)")
                                        .fillColor(hex2Rgb("#767070"))
                                        .fontSize(8f)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .textColor(Color.white)
                                        .leftPadding(0f)
                                        .rightPadding(0f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                        .borderStyle(noLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                        .value("Date")
                                        .fillColor(hex2Rgb("#767070"))
                                        .fontSize(8f)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .textColor(Color.white)
                                        .leftPadding(0f)
                                        .rightPadding(0f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                        .borderStyle(noLine)
                                        .build(),
                        });

//                cell = row.createCell(33f, "Authorization (if required)", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
//                cell.setFontSize(8f);
//                cell.setFont(PDType1Font.HELVETICA_BOLD);
//                cell.setTextColor(Color.white);
//                cell.setFillColor(hex2Rgb("#767070"));
//                cell.setLeftPadding(0f);
//                cell.setRightPadding(0f);
//
//                row.createCell(66f, "", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
//
//                table.draw();
//
//                if (table.getCurrentPage() != page) {
//                    contentStream.close();
//                    page = table.getCurrentPage();
//                    contentStream = new PDPageContentStream(document, page,
//                            PDPageContentStream.AppendMode.APPEND, true);
//                }
//
//                table = new BaseTable(yPosition, yStartNewPage, 0f, 180f, 416f,
//                        document, page, true, true);
//
//                row = table.createRow(15f);
//
//                cell = row.createCell(40f, "Date", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
//                cell.setFontSize(8f);
//                cell.setFont(PDType1Font.HELVETICA_BOLD);
//                cell.setTextColor(Color.white);
//                cell.setFillColor(hex2Rgb("#767070"));
//
//                row.createCell(60f, "", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);

                table.draw();

                contentStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            documents.add(document);
        }

        try {
            PDDocument finalDocument = new PDDocument();
            PDFMergerUtility utility = new PDFMergerUtility();
            for (PDDocument doc : documents) {
                utility.appendDocument(finalDocument, doc);
            }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            finalDocument.save(outputStream);
            finalDocument.close();

            for (PDDocument doc : documents) {
                doc.close();
            }
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            String teamStr = StringUtils.hasText(team) ? "_" + team.replace(",", "").replace(" ", "_") : "";
            headers.add("Content-Disposition", String.format("attachment; filename=Timesheet_%s%s.pdf", dateOfService, teamStr));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(inputStream));
        } catch (IOException ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
        }
    }
}
