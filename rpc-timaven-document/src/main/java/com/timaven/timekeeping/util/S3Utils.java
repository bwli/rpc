package com.timaven.timekeeping.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.dto.TenantDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
public class S3Utils {

    @Value("${aws.s3.access-key}")
    private String s3AccessKey;

    @Value("${aws.s3.secret-key}")
    private String s3SecretKey;

    @Value("${aws.s3.template-buck-name}")
    private String timavenBuckName;

    private AWSCredentials credentials;

    private AmazonS3 s3client;

    private final TenantProperties tenantProperties;

    public S3Utils(TenantProperties tenantProperties) {
        this.tenantProperties = tenantProperties;
    }

    public AWSCredentials getCredentials() {
        if (Objects.isNull(credentials)) {
            credentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey);
        }
        return credentials;
    }

    public AmazonS3 getS3client() {
        if (Objects.isNull(s3client)) {
            s3client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(getCredentials()))
                    .withRegion(Regions.US_EAST_2)
                    .build();
        }
        return s3client;
    }

    /**
     * get timaven buck name or tenant buck name
     * isDefault false is timaven buck name. true is tenant buck name
     */
    public String getTemplateBuckName() {
        return timavenBuckName;
    }

    public String getTenantBuckName() {
        TenantDto tenantDto = tenantProperties.getTenants().get(ThreadTenantStorage.getTenantId());
        return tenantDto == null ? "" : Optional.ofNullable(tenantDto.getBuckName()).orElse("");
    }

    /**
     * get max upload size by tenantId
     */
    public Long getMaxUploadSize() {
        TenantDto tenantDto = tenantProperties.getTenants().get(ThreadTenantStorage.getTenantId());
        return tenantDto == null ? 0 : tenantDto.getMaxUploadSize() == null ? 0 : tenantDto.getMaxUploadSize();
    }

    public String getDumpBuckName() {
        TenantDto tenantDto = tenantProperties.getTenants().get(ThreadTenantStorage.getTenantId());
        return tenantDto == null ? "" : Optional.ofNullable(tenantDto.getDumpBuckName()).orElse("");
    }
}
