package com.timaven.timekeeping;

import com.timaven.timekeeping.model.PunchRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@ActiveProfiles("dev")
@SpringBootTest(classes = {KieContainer.class})
@ContextConfiguration(classes = RuleConfiguration.class)
public class RuleConfigurationTest {

    @Autowired
    private KieContainer kieContainer;

    @Test
    public void checkKieWorksProperly() {
        KieSession kieSession = kieContainer.newKieSession();
        kieSession.setGlobal("graceIn", 10);
        kieSession.setGlobal("graceOut", 10);
        kieSession.setGlobal("roundingMinutes", 15);
        kieSession.setGlobal("minutesToBePlus", 7);
        kieSession.getAgenda().getAgendaGroup("adjust time").setFocus();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        PunchRecord punchInGrace = new PunchRecord();
        punchInGrace.setSide(true);
        LocalDateTime dateTime = LocalDateTime.parse("03/29/2021 06:51", dateTimeFormatter);
        punchInGrace.setEventTime(dateTime);
        punchInGrace.setNetEventTime(dateTime);
        LocalTime time = LocalTime.parse("07:00", timeFormatter);
        punchInGrace.setScheduleStart(time);
        time = LocalTime.parse("17:30", timeFormatter);
        punchInGrace.setScheduleEnd(time);
        kieSession.insert(punchInGrace);
        PunchRecord punchInRound = new PunchRecord();
        punchInRound.setSide(true);
        dateTime = LocalDateTime.parse("03/29/2021 07:20", dateTimeFormatter);
        punchInRound.setEventTime(dateTime);
        punchInRound.setNetEventTime(dateTime);
        time = LocalTime.parse("07:00", timeFormatter);
        punchInRound.setScheduleStart(time);
        time = LocalTime.parse("17:30", timeFormatter);
        punchInRound.setScheduleEnd(time);
        kieSession.insert(punchInRound);
        PunchRecord punchOutGrace = new PunchRecord();
        punchOutGrace.setSide(false);
        dateTime = LocalDateTime.parse("03/29/2021 17:21", dateTimeFormatter);
        punchOutGrace.setEventTime(dateTime);
        punchOutGrace.setNetEventTime(dateTime);
        time = LocalTime.parse("07:00", timeFormatter);
        punchOutGrace.setScheduleStart(time);
        time = LocalTime.parse("17:30", timeFormatter);
        punchOutGrace.setScheduleEnd(time);
        kieSession.insert(punchOutGrace);
        PunchRecord punchOutRound = new PunchRecord();
        punchOutRound.setSide(false);
        dateTime = LocalDateTime.parse("03/29/2021 16:44", dateTimeFormatter);
        punchOutRound.setEventTime(dateTime);
        punchOutRound.setNetEventTime(dateTime);
        time = LocalTime.parse("07:00", timeFormatter);
        punchOutRound.setScheduleStart(time);
        time = LocalTime.parse("17:30", timeFormatter);
        punchOutRound.setScheduleEnd(time);
        kieSession.insert(punchOutRound);
        kieSession.fireAllRules();
        kieSession.dispose();
        dateTime = LocalDateTime.parse("03/29/2021 07:00", dateTimeFormatter);
        Assertions.assertEquals(punchInGrace.getEventTime(), dateTime);
        dateTime = LocalDateTime.parse("03/29/2021 07:15", dateTimeFormatter);
        Assertions.assertEquals(punchInRound.getEventTime(), dateTime);
        dateTime = LocalDateTime.parse("03/29/2021 17:30", dateTimeFormatter);
        Assertions.assertEquals(punchOutGrace.getEventTime(), dateTime);
        dateTime = LocalDateTime.parse("03/29/2021 16:45", dateTimeFormatter);
        Assertions.assertEquals(punchOutRound.getEventTime(), dateTime);
    }
}
