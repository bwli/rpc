package com.timaven.provider.configuration.datasource;

import com.timaven.provider.configuration.tenant.TenantProperties;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;

@Configuration
@EnableTransactionManagement
public class DataSourceConfiguration {

    private final TenantProperties tenantProperties;
    private final Environment env;
    private final MasterDataSourceProperties masterDataSourceProperties;

    @Autowired
    public DataSourceConfiguration(TenantProperties tenantProperties, Environment env,
                                   MasterDataSourceProperties masterDataSourceProperties) {
        this.tenantProperties = tenantProperties;
        this.env = env;
        this.masterDataSourceProperties = masterDataSourceProperties;
    }

    @Bean
    public DataSource dataSource() {
        TenantRoutingDataSource customDataSource = new TenantRoutingDataSource();
        customDataSource.setTargetDataSources(tenantProperties.getDatasources());
        customDataSource.setDefaultTargetDataSource(tenantProperties.getDatasources().get("tenant1"));
        return customDataSource;
    }

    @PostConstruct
    public void migrate() {
        // close flyway init old version is dev
        if (Arrays.asList(env.getActiveProfiles()).contains("test") || Arrays.asList(env.getActiveProfiles()).contains("rpc")) {
            return;
        }

        ArrayList<String> flywayArray = new ArrayList<>();
        flywayArray.add("classpath:/db/migration/postgresql/RPCMaster");
        tenantProperties
                .getDatasources()
                .values()
                .stream()
                .map(dataSource -> (DataSource) dataSource)
                .forEach(d -> migrate(d, flywayArray.toArray(new String[flywayArray.size()])));
    }

    private void migrate(DataSource dataSource, String[] locations) {
        Flyway flyway = Flyway.configure()
                .dataSource(dataSource)
                .locations(locations)
                .baselineOnMigrate(true)
                .schemas("flyway_history_schema")
                .initSql("create schema if not exists flyway_history_schema")
                .table("client_history")
                .load();
        flyway.migrate();
    }
}
