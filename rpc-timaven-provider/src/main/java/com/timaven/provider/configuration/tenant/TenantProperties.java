package com.timaven.provider.configuration.tenant;

import com.timaven.provider.model.dto.TenantDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@ConfigurationProperties(prefix = "multi-tenants")
public class TenantProperties {

    private final Map<Object, Object> datasources = new LinkedHashMap<>();

    private final Map<String, TenantDto> tenants = new LinkedHashMap<>();

    private Map<String, String> hosts = new HashMap<>();

    public void setTenants(Map<String, Map<String, String>> tenants) {
        tenants.forEach((key, value) -> this.tenants.put(key, parseTenant(key, value)));
        hosts = this.tenants.values().stream()
                .collect(Collectors.toMap(TenantDto::getHost, TenantDto::getName));
    }

    private TenantDto parseTenant(String key, Map<String, String> source) {
        TenantDto tenantDto = new TenantDto();
        tenantDto.setName(key);
        tenantDto.setHost(source.get("host"));
        return tenantDto;
    }

    public Map<String, String> getHosts() {
        return hosts;
    }

    public Map<Object, Object> getDatasources() {
        return datasources;
    }

    // Map application.yml tenants -> datasources
    public void setDatasources(Map<String, Map<String, String>> datasources) {
        datasources
                .forEach((key, value) -> this.datasources.put(key, convert(value)));
    }

    public DataSource convert(Map<String, String> source) {
        return DataSourceBuilder.create()
                .url(source.get("url"))
                .driverClassName(source.get("driver-class-name"))
                .username(source.get("username"))
                .password(source.get("password"))
                .build();
    }
}
