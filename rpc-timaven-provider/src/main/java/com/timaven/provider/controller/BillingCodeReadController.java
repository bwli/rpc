package com.timaven.provider.controller;

import com.timaven.provider.model.BillingCode;
import com.timaven.provider.model.BillingCodeOverride;
import com.timaven.provider.model.BillingCodeType;
import com.timaven.provider.model.dto.BillingCodeDto;
import com.timaven.provider.model.dto.CostCodeAssociationDto;
import com.timaven.provider.model.enums.BillingCodeTypeParsedBy;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.BillingCodeService;
import com.timaven.provider.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class BillingCodeReadController extends BaseController {

    private final BillingCodeService billingCodeService;
    private final CostCodeService costCodeService;

    @Autowired
    public BillingCodeReadController(BillingCodeService billingCodeService, CostCodeService costCodeService) {
        this.billingCodeService = billingCodeService;
        this.costCodeService = costCodeService;
    }

    @GetMapping(path = "/billing-code-types")
    public BillingCodeType[] getBillingCodeTypes(@RequestParam(required = false) Boolean levelNotNull) {
        List<BillingCodeType> billingCodeTypes;
        if (levelNotNull != null && levelNotNull) {
            billingCodeTypes = billingCodeService.findBillingCodeTypesSortByLevel();
        } else {
            billingCodeTypes = billingCodeService.findAllBillingCodeTypes();
        }
        return billingCodeTypes.toArray(BillingCodeType[]::new);
    }

    @GetMapping(path = "/billing-code-types/{id}")
    public BillingCodeType getBillingCodeTypesById(@PathVariable Long id) {
        return billingCodeService.findBillingCodeTypeById(id);
    }

    @PostMapping(value = "/cost-code-associations")
    public Page<CostCodeAssociationDto> getCostCodeAssociations(@RequestParam(required = false) Long projectId,
                                                                @RequestBody PagingRequest pagingRequest) {
        return costCodeService.findCostCodeAssociations(pagingRequest, projectId);
    }

    @GetMapping(path = "/billing-code-dtos", params = {"costCodeId"})
    public BillingCodeDto[] getBillingCodesByProjectIdAndCostCodeId(@RequestParam(required = false) Long projectId,
                                                                    @RequestParam(required = false) Long costCodeId) {
        if (null == costCodeId) return new BillingCodeDto[0];
        return billingCodeService.findBillingCodeByCostCodeIdAndProjectId(costCodeId, projectId);
    }

    @GetMapping(path = "/billing-code-dtos", params = {"typeId"})
    public BillingCodeDto[] getBillingCodesByProjectIdAndTypeId(@RequestParam(required = false) Long projectId,
                                                                @RequestParam Long typeId) {
        return billingCodeService.findBillingCodesByTypeIdAndProjectId(typeId, projectId);
    }

    @PostMapping(value = "/billing-code-dtos")
    public Page<BillingCodeDto> getBillingCodesByTypeId(@RequestParam(required = false) Long projectId,
                                                        @RequestParam Long typeId,
                                                        @RequestBody PagingRequest pagingRequest) {
        return billingCodeService.findBillingCodesByTypeIdAndProjectId(typeId, pagingRequest, projectId);
    }

    @GetMapping(path = "/billing-codes/{id}")
    public BillingCode getBillingCodeByIdAndTypeId(@PathVariable Long id) {
        return billingCodeService.findBillingCodeById(id);
    }

    @GetMapping(path = "/billing-codes")
    public BillingCode getBillingCodeByIdAndTypeId(@RequestParam Long typeId, @RequestParam String codeName) {
        return billingCodeService.findByTypeIdAndCodeName(typeId, codeName);
    }

    @PostMapping(path = "/billing-codes-filtered", params = "typeId")
    public BillingCode[] getBillingCodesByTypeIdAndCodeNames(@RequestParam Long typeId, @RequestBody Collection<String> codeNames) {
        return billingCodeService.findByTypeIdAndCodeNameIn(typeId, codeNames);
    }

    @GetMapping(path = "/billing-code-overrides")
    public BillingCodeOverride getBillingCodeOverrideByIdAndTypeId(@RequestParam Long projectId, @RequestParam Long billingCodeId) {
        return billingCodeService.findBillingCodeOverride(billingCodeId, projectId);
    }

    @PostMapping(path = "/billing-code-overrides-filtered", params = {"projectId", "typeId"})
    public BillingCodeOverride[] getBillingCodeOverridesByProjectIdAndTypeIdAndCodeNames(@RequestParam Long projectId,
                                                                                         @RequestParam Long typeId,
                                                                                         @RequestBody Collection<String> codeNames) {
        return billingCodeService.findBillingCodeOverridesByProjectIdAndTypeIdAndCodeNames(projectId, typeId, codeNames);
    }

    @PostMapping(path = "/billing-code-overrides-filtered", params = {"projectId"})
    public BillingCodeOverride[] getBillingCodeOverridesByProjectIdAndBillingCodeIds(@RequestParam Long projectId,
                                                                                     @RequestBody Collection<Long> billingCodeIds) {
        return billingCodeService.findBillingCodeOverridesByProjectIdAndBillingCodeIds(projectId, billingCodeIds);
    }

    @GetMapping(path = "/billing-code-override-collection")
    public BillingCodeOverride[] getBillingCodeOverridesByProjectId(@RequestParam Long projectId) {
        return billingCodeService.findBillingCodeOverrides(projectId);
    }

    @PostMapping(path = "/billing-codes-filtered", params = {"typeId", "parsedBy"})
    public List<BillingCode> getBillingCodesByTypeIdAndValues(@RequestBody Collection<String> values,
                                                              @RequestParam Long typeId,
                                                              @RequestParam(required = false) Long projectId,
                                                              @RequestParam BillingCodeTypeParsedBy parsedBy) {
        return billingCodeService.findByTypeIdAndValues(typeId, projectId, values, parsedBy);
    }

    @PostMapping(path = "/billing-code-type-sort")
    public List<BillingCodeType> getBillingCodeTypeSort(@RequestParam String column) {
        return billingCodeService.findBillingCodeTypeSort(column);
    }
}
