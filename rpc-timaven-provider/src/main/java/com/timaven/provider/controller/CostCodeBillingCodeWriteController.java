package com.timaven.provider.controller;

import com.timaven.provider.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class CostCodeBillingCodeWriteController extends BaseController {

    private final CostCodeService costCodeService;

    @Autowired
    public CostCodeBillingCodeWriteController(CostCodeService costCodeService) {
        this.costCodeService = costCodeService;
    }

    @PostMapping(path = "/cost-code-billing-codes", params = {"costCodeId", "billingCodeId"})
    public void saveCostCodeBillingCodeById(@RequestParam Long costCodeId,
                                                           @RequestParam Long billingCodeId) {
        costCodeService.saveCostCodeBillingCodeById(costCodeId, billingCodeId);
    }
}
