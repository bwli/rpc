package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.enums.CostCodeType;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class CostCodeLogReadController extends BaseController {

    private final CostCodeService costCodeService;

    @Autowired
    public CostCodeLogReadController(CostCodeService costCodeService) {
        this.costCodeService = costCodeService;
    }

    @PostMapping(path = "/cost-code-log-filtered", params = {"startDate"})
    public CostCodeLog[] findCostCodeLogsByProjectIdAndDate(@RequestParam(required = false) Long projectId,
                                                           @RequestParam LocalDate startDate,
                                                           @RequestParam(required = false) Boolean includeGlobal,
                                                           @RequestParam(value = "endDate", required = false) LocalDate endDate,
                                                           @RequestBody(required = false) Collection<String> costCodes) {
        if (null == startDate) return null;
        if (includeGlobal == null) includeGlobal = true;
        return costCodeService.findCostCodesByProjectAndDate(projectId, startDate, endDate, costCodes, includeGlobal);
    }

    @GetMapping(path = "/cost-codes", params = {"teamName", "projectId", "dateOfService"})
    public CostCodeLog[] getCostCodeLogsByProjectIdAndTeamAndDateOfService(@RequestParam Long projectId,
                                                                           @RequestParam LocalDate dateOfService,
                                                                           @RequestParam String teamName) {
        return costCodeService.getCostCodeLogsByProjectIdAndTeamNameAndDateOfService(projectId, teamName, dateOfService);
    }

    @GetMapping(path = "/cost-codes/{id}")
    public CostCodeLog getCostCodeLogsById(@PathVariable Long id) {
        return costCodeService.getCostCodeById(id);
    }

    @PostMapping(path = "/cost-code-tag-type-page")
    public Page<CostCodeTagType> getCostCodeTagTypesByProjectId(@RequestParam Long projectId,
                                                                @RequestBody PagingRequest pagingRequest) {
        return costCodeService.getCostCodeTagTypesByProjectId(projectId, pagingRequest);
    }

    @GetMapping(path = "/cost-code-tag-types/{id}")
    public CostCodeTagType getCostCodeTagTypeById(@PathVariable Long id) {
        return costCodeService.getCostCodeTagTypeById(id);
    }

    @GetMapping(path = "/cost-code-tag-types")
    public CostCodeTagType[] getAllCostCodeTagType() {
        return costCodeService.getAllCostCodeTagTypes();
    }

    @GetMapping(path = "/cost-code-tag-types", params = "projectId")
    public CostCodeTagType[] getCostCodeTagTypeByProjectId(@RequestParam Long projectId) {
        return costCodeService.getAllCostCodeTagTypeByProjectId(projectId);
    }

    @GetMapping(path = "/cost-code-tag-types", params = {"projectId", "tagType"})
    public CostCodeTagType getCostCodeTagTypeByProjectIdAndTagType(@RequestParam Long projectId,
                                                                   @RequestParam String tagType) {
        return costCodeService.getCostCodeTagByProjectIdAndTagType(projectId, tagType);
    }

    @GetMapping(path = "/cost-code-tags/{id}")
    public CostCodeTag getCostCodeTagById(@PathVariable Long id) {
        return costCodeService.getCostCodeTagById(id);
    }

    @GetMapping(path = "/cost-code-tags", params = {"typeId"})
    public CostCodeTag[] getCostCodeTagsByTypeId(@RequestParam Long typeId) {
        return costCodeService.getCostCodeTagsByTypeId(typeId);
    }

    @GetMapping(path = "/cost-code-tags", params = {"typeId", "codeName"})
    public CostCodeTag getCostCodeTagByTypeIdAndCodeName(@RequestParam Long typeId,
                                                         @RequestParam String codeName) {
        return costCodeService.getCostCodeTagByTypeIdAndCodeName(typeId, codeName);
    }

    @PostMapping(path = "/cost-code-tag-page")
    public Page<CostCodeTag> getCostCodeTagsByProjectId(@RequestParam Long typeId,
                                                            @RequestBody PagingRequest pagingRequest) {
        return costCodeService.getCostCodeTagsByProjectId(typeId, pagingRequest);
    }

    @PostMapping(path = "/cost-code-tag-association-collection")
    public CostCodeTagAssociation[] getCostCodeTagAssociationsByKeysAndType(@RequestBody Set<Long> keys,
                                                                          @RequestParam CostCodeType costCodeType) {
        return costCodeService.getCostCodeTagAssociationsByKeysAndType(keys, costCodeType);
    }

    @GetMapping(path = "/cost-code-log-tag-string-collection")
    public String[] getCostCodeLogTagStrings(@RequestParam(required = false) Long projectId) {
        return costCodeService.findCostCodeLogTagStringsByProjectId(projectId);
    }

    @GetMapping(path = "/cost-code-log-tag-collection")
    public CostCodeLogTag[] getCostCodeLogTags(@RequestParam(required = false) Long projectId) {
        return costCodeService.findCostCodeLogTagsByProjectId(projectId);
    }

    @PostMapping(path = "/team-cost-codes-filtered")
    public TeamCostCode[] getTeamCostCodes(@RequestParam(required = false) Long projectId,
                                           @RequestParam LocalDate startDate,
                                           @RequestParam(required = false) LocalDate endDate,
                                           @RequestBody Collection<String> teamNames) {
        return costCodeService.getTeamCostCodesByProjectIdAndTeamNames(projectId, teamNames, startDate, endDate);
    }

    @PostMapping(path = "/cost-code-association-collection", params = {"dateOfService"})
    public List<CostCodeAssociation> getCostCodeAssociationsByProjectAndDateOfService(@RequestBody Collection<String> costCodes ,
                                                                                      @RequestParam(required = false) Long projectId,
                                                                                      @RequestParam LocalDate dateOfService) {
        return costCodeService.findCostCodeAssociations(projectId, dateOfService, costCodes);
    }

    @PostMapping(path = "/cost-code-association-collection", params = {"startDate", "endDate"})
    public List<CostCodeAssociation> getCostCodeAssociationsByProjectAndDateOfService(@RequestBody Collection<String> costCodes ,
                                                                                      @RequestParam(required = false) Long projectId,
                                                                                      @RequestParam LocalDate startDate,
                                                                                      @RequestParam LocalDate endDate) {
        return costCodeService.findCostCodeAssociations(projectId, startDate, endDate, costCodes);
    }

    @PostMapping(path = "/cost-code-association-collection", params = {"startDateOfService"})
    public Set<CostCodeAssociation> getCostCodeAssociationByProjectIdAndCodes(@RequestParam Long projectId,
                                                                              @RequestBody Set<String> codes,
                                                                              @RequestParam LocalDate startDateOfService) {
        return costCodeService.findCostCodeAssociationsByProjectIdAndCodes(projectId, codes, startDateOfService);
    }

    @PostMapping(path = "/cost-code-export-view-collection")
    public List<CostCodeExportView> getCostCodeExportViewByProjectId(@RequestParam(required = false) Long projectId) {
        return costCodeService.findCostCodeExportViewByProjectId(projectId);
    }
}
