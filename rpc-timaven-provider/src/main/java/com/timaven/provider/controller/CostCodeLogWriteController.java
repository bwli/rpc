package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.enums.CostCodeType;
import com.timaven.provider.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class CostCodeLogWriteController extends BaseController {

    private final CostCodeService costCodeService;

    @Autowired
    public CostCodeLogWriteController(CostCodeService costCodeService) {
        this.costCodeService = costCodeService;
    }

    @PutMapping(path = "/cost-codes/is-lock")
    public String updateCostCodesLock(@RequestParam(required = false) Long projectId,
                                      @RequestBody Set<String> costCodes) {
        if (CollectionUtils.isEmpty(costCodes)) return RESPONSE_FAILED;
        costCodeService.lockCostCodes(costCodes, projectId);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(value = "/cost-codes/{id}")
    public String deleteCostCodeLogById(@PathVariable Long id) {
        if (id == null) return RESPONSE_FAILED;
        costCodeService.deleteById(id);
        return RESPONSE_SUCCESS;
    }

    @RequestMapping(value = "/cost-code-tag-types", method = {RequestMethod.POST, RequestMethod.PUT})
    public CostCodeTagType saveCostCodeTagType(@RequestBody CostCodeTagType costCodeTagType) {
        return costCodeService.saveCostCodeTagType(costCodeTagType);
    }

    @RequestMapping(value = "/cost-code-tags", method = {RequestMethod.POST, RequestMethod.PUT})
    public CostCodeTag saveCostCodeTag(@RequestBody CostCodeTag costCodeTag) {
        return costCodeService.saveCostCodeTag(costCodeTag);
    }

    @DeleteMapping(value = "/cost-code-tag-types/{id}")
    public String deleteCostCodeTagType(@PathVariable Long id) {
        costCodeService.deleteCostCodeTagTypeById(id);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(value = "/cost-code-tags/{id}")
    public String deleteCostCodeTagById(@PathVariable Long id) {
        if (id == null) return RESPONSE_FAILED;
        costCodeService.deleteCostCodeTagById(id);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(value = "/cost-code-tag-associations")
    public String deleteCostCodeTagAssociationsByKeyInAndCostCodeType(@RequestBody Set<Long> keys,
                                                                      @RequestParam CostCodeType costCodeType) {
        if (CollectionUtils.isEmpty(keys)) return RESPONSE_SUCCESS;
        costCodeService.deleteCostCodeTagAssociationsByKeyInAndCostCodeType(keys, costCodeType);
        return RESPONSE_SUCCESS;
    }

    @PatchMapping(path = "/cost-code-tag-association-collection")
    public String saveCostCodeTagAssociations(@RequestBody Set<CostCodeTagAssociation> costCodeTagAssociations) {
        costCodeService.saveCostCodeTagAssociations(costCodeTagAssociations);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(path = "/cost-code-log-tags")
    public String saveCostCodeLogTag(@RequestParam(required = false) Long projectId,
                                     @RequestParam String costCode,
                                     @RequestParam String tag) {
        costCodeService.saveCostCodeLogTag(projectId, costCode, tag);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/cost-code-log-tags")
    public String deleteCostCodeLogTag(@RequestParam(required = false) Long projectId,
                                       @RequestParam String costCode,
                                       @RequestParam String tag) {
        costCodeService.deleteCostCodeLogTag(projectId, costCode, tag);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(path = "/team-cost-code-collection")
    public void saveTeamCostCodes(@RequestBody Set<TeamCostCode> teamCostCodes) {
        if (CollectionUtils.isEmpty(teamCostCodes)) return;
        costCodeService.saveTeamCostCodes(teamCostCodes);
    }

    @PostMapping(path = "/cost-code-log-collection")
    public CostCodeLog[] saveCostCodeLogs(@RequestBody Collection<CostCodeLog> costCodeLogs) {
        if (CollectionUtils.isEmpty(costCodeLogs)) return new CostCodeLog[0];
        return costCodeService.saveCostCodeLogs(costCodeLogs);
    }
}
