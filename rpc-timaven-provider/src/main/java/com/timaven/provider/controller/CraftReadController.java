package com.timaven.provider.controller;

import com.timaven.provider.model.Craft;
import com.timaven.provider.model.dto.CraftDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.CraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class CraftReadController extends BaseController {

    private final CraftService craftService;

    @Autowired
    public CraftReadController(CraftService craftService) {
        this.craftService = craftService;
    }

    @GetMapping(path = "/crafts/{id}")
    public Craft getCraftById(@PathVariable Long id) {
        return craftService.findCraftById(id);
    }

    @PostMapping(value = "/craft-dtos")
    public Page<CraftDto> getCraftPage(@RequestParam(required = false) Long projectId,
                                       @RequestBody PagingRequest pagingRequest) {
        return craftService.getCraftPage(pagingRequest, projectId);
    }

    // TM-421
    @GetMapping(value = "/craft-dtos")
    public List<CraftDto> getCraftDtos(@RequestParam(required = false) Long projectId,
                                       @RequestParam(required = false) LocalDate start) {
        return craftService.getCraftDtosByProjectIdAndDateRange(projectId, start);
    }

    @PostMapping(value = "/crafts-filtered")
    public Craft[] getCraftsFiltered(@RequestParam(required = false) Long projectId,
                                       @RequestBody Collection<String> codes) {
        return craftService.findByProjectIdAndCodes(projectId, codes);
    }

    @GetMapping(value = "/crafts")
    public Craft[] getCrafts(@RequestParam(required = false) Long projectId,
                             @RequestParam(required = false) LocalDate startDate,
                             @RequestParam(required = false) LocalDate endDate) {
        return craftService.getCraftsByProjectIdAndDateRange(projectId, startDate, endDate);
    }
}
