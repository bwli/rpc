package com.timaven.provider.controller;

import com.timaven.provider.model.Craft;
import com.timaven.provider.model.Employee;
import com.timaven.provider.service.CraftService;
import com.timaven.provider.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class CraftWriteController extends BaseController {

    private final CraftService craftService;

    @Autowired
    public CraftWriteController(CraftService craftService) {
        this.craftService = craftService;
    }

    @RequestMapping(path = "/crafts", method = {RequestMethod.POST, RequestMethod.PUT})
    public Craft addOrUpdateCraft(@RequestBody Craft craft) {
        return craftService.saveCraft(craft);
    }

    @PostMapping(value = "/craft-collection")
    public void saveCrafts(@RequestBody Collection<Craft> crafts) {
        craftService.saveCrafts(crafts);
    }
}
