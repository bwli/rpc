package com.timaven.provider.controller;

import com.timaven.provider.model.Employee;
import com.timaven.provider.model.EmployeeFilter;
import com.timaven.provider.model.EmployeeView;
import com.timaven.provider.model.dto.EmpWorkdays;
import com.timaven.provider.model.dto.EmployeeAuditDto;
import com.timaven.provider.model.dto.EmployeeDto;
import com.timaven.provider.model.dto.EmployeeIdNameDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class EmployeeReadController extends BaseController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeReadController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(path = "/sign-in-sheets")
    public Set<String> getSignInSheets(@RequestBody EmployeeFilter filter) {
        if (null == filter || filter.getStartDate() == null) return null;
        return employeeService.getSignInSheets(filter);
    }

    @PostMapping(path = "/team-names")
    public Set<String> getTeamNames(@RequestBody EmployeeFilter filter) {
        if (null == filter || filter.getStartDate() == null) return null;
        return employeeService.getTeamNames(filter);
    }

    @PostMapping(path = "/employees/id/name")
    public Set<EmployeeIdNameDto> getEmployeeIdNames(@RequestBody EmployeeFilter filter) {
        if (null == filter || filter.getStartDate() == null) return null;
        return employeeService.getEmployeeIdNames(filter);
    }

    @PostMapping(path = "/employees-filtered")
    public Employee[] getEmployees(@RequestBody EmployeeFilter filter) {
        if (null == filter || filter.getStartDate() == null) return null;
        return employeeService.findEmployees(filter).stream().sorted().toArray(Employee[]::new);
    }

    @GetMapping(path = "/employees/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeService.findById(id);
    }

    @PostMapping(value = "/employee-audit-dtos")
    public Page<EmployeeAuditDto> getEmployeeAuditDtos(@RequestParam(required = false) Long projectId,
                                                          @RequestBody PagingRequest pagingRequest) {
        return employeeService.getEmployeeAudit(pagingRequest, projectId);
    }

    @PostMapping(value = "/employee-dtos")
    public Page<EmployeeDto> getEmployeeDtos(@RequestParam(required = false) Long projectId,
                                             @RequestBody PagingRequest pagingRequest) {
        return employeeService.getEmployees(pagingRequest, projectId);
    }

    @GetMapping(path = "/employee-tag-string-collection")
    public String[] getEmployeeTags(@RequestParam(required = false) Long projectId) {
        return employeeService.findEmployeeTagStringsByProjectId(projectId);
    }

    @PostMapping(path = "/employees-day-off")
    public EmpWorkdays[] getEmployeesDayOff(@RequestParam LocalDate dateOfService,
                                            @RequestParam long days,
                                            @RequestBody Collection<String> empIds) {
        return employeeService.findEmployeesDayOff(empIds, dateOfService, days).toArray(EmpWorkdays[]::new);
    }

    // TM-412
    /**
     * Get the number of active employee by projectId
     * @param projectId
     * @return
     */
    @GetMapping(path = "/employees/count")
    public int getActiveEmployeeCount(@RequestParam(required = false) Long projectId) {
        return employeeService.countByProjectId(projectId);
    }

    @PostMapping(path = "/employee-views-filtered")
    public EmployeeView[] getEmployeesViewByProjectIdAndEmpId(@RequestParam Set<Long> projectIds,
                                                          @RequestParam Set<String> employeeIds) {
        return employeeService.getEmployeeViewByProjectIdAndEmpId(projectIds, employeeIds);
    }

    @PostMapping(path = "/employee-view-collection")
    public List<EmployeeView> getEmployeesExoprtView(@RequestParam(required = false) Long projectId,
                                                     @RequestParam(required = false) Boolean showAll) {
        return employeeService.findEmployeeByProjectIdAndShowAll(projectId, showAll);
    }

    @PostMapping(path = "/employee-collection", params = {"employeeIds"})
    public List<Employee> getEmployeesByIds(@RequestParam Set<Long> employeeIds) {
        return employeeService.findEmployeeByIds(employeeIds);
    }

    @PostMapping(path = "/employee-collection", params = {"dateOfService"})
    public Set<Employee> getEmployeeByTeamNameAndProjectIdAndDateOfService(@RequestParam String teamName,
                                                                            @RequestParam Long projectId,
                                                                            @RequestParam LocalDate dateOfService) {
        return employeeService.findByTeamNameAndProjectIdAndDateOfServic(teamName, projectId, dateOfService);
    }
}
