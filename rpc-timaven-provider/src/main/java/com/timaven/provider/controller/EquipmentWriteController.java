package com.timaven.provider.controller;

import com.timaven.provider.model.Equipment;
import com.timaven.provider.model.EquipmentCostCodePerc;
import com.timaven.provider.model.EquipmentPrice;
import com.timaven.provider.model.EquipmentUsage;
import com.timaven.provider.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class EquipmentWriteController extends BaseController {

    private final EquipmentService equipmentService;

    @Autowired
    public EquipmentWriteController(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @RequestMapping(value = "/equipment", method = {RequestMethod.POST, RequestMethod.PUT})
    public Equipment addOrUpdateEquipment(@RequestBody Equipment equipment) {
        equipmentService.saveEquipment(equipment);
        return equipment;
    }

    @PutMapping(path = "/equipment/{id}/active")
    public String changeEquipmentValidity(@PathVariable Long id, @RequestParam boolean active) {
        if (id == null) return RESPONSE_FAILED;
        return equipmentService.changeEquipmentValidity(id, active) ? RESPONSE_SUCCESS : RESPONSE_FAILED;
    }

    @RequestMapping(value = "/equipment-usages", method = {RequestMethod.POST, RequestMethod.PUT})
    public EquipmentUsage addOrUpdateEquipmentUsage(@RequestParam Long projectId,
                                                    @RequestParam Long equipmentId,
                                                    @RequestParam(required = false) Long id,
                                                    @RequestParam LocalDate date,
                                                    @RequestParam String type) {
        return equipmentService.saveEquipmentUsage(equipmentId, getUserId(), projectId, id, date, type);
    }

    @RequestMapping(value = "/equipment-usage-collection", method = {RequestMethod.POST})
    public List<EquipmentUsage> saveEquipmentUsageAll(@RequestBody Collection<EquipmentUsage> equipmentUsages) {
        // TM-300 add import equipment log
        return equipmentService.saveEquipmentUsageAll(equipmentUsages, getUserId());
    }

    @DeleteMapping(value = "/equipment-usages/{id}")
    public String deactivateEquipmentUsageById(@PathVariable Long id) {
        if (id == null) return RESPONSE_FAILED;
        equipmentService.deactivateEquipmentUsage(id);
        return RESPONSE_SUCCESS;
    }

    @RequestMapping(value = "/equipment-prices", method = {RequestMethod.POST, RequestMethod.PUT})
    public EquipmentPrice addOrUpdateEquipmentPrice(@RequestBody EquipmentPrice equipmentPrice) {
        equipmentService.saveEquipmentPrice(equipmentPrice);
        return equipmentPrice;
    }

    @PostMapping(value = "/equipment-price-collection")
    public List<EquipmentPrice> addEquipmentPrices(@RequestBody Collection<EquipmentPrice> equipmentPrices) {
        return equipmentService.saveEquipmentPrices(equipmentPrices);
    }

    @PostMapping(value = "/equipment-collection")
    public List<Equipment> addEquipmentList(@RequestBody Collection<Equipment> equipmentList) {
        return equipmentService.saveEquipmentList(equipmentList);
    }

    @DeleteMapping(value = "/equipment-prices/{id}")
    public String deleteEquipmentPriceById(@PathVariable Long id) {
        if (id == null) return RESPONSE_FAILED;
        equipmentService.deleteEquipmentPriceById(id);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(path = "/equipment-tags")
    public String saveEquipmentTag(@RequestParam String equipmentId,
                                  @RequestParam String tag) {
        equipmentService.saveEquipmentTag(equipmentId, tag);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/equipment-tags")
    public String deleteEquipmentTag(@RequestParam String equipmentId,
                                    @RequestParam String tag) {
        equipmentService.deleteEquipmentTag(equipmentId, tag);
        return RESPONSE_SUCCESS;
    }

    // TM-275
    @DeleteMapping(path = "/equipment-cost-code-perc-collection")
    public String deleteEquipmentCostCodePercBatch(@RequestBody List<EquipmentCostCodePerc> list) {
        equipmentService.deleteEquipmentCostCodePercBatch(list);
        return RESPONSE_SUCCESS;
    }
}
