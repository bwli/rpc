package com.timaven.provider.controller;

import com.timaven.provider.model.Notification;
import com.timaven.provider.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class NotificationReadController extends BaseController {

    private final NotificationService notificationService;

    @Autowired
    public NotificationReadController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping(path = "/notifications")
    public Notification[] getNotificationByUserId() {
        return notificationService.getNotificationsByUserId(getUserId());
    }

    @GetMapping(path = "/notifications", params = {"read"})
    public int getNotificationByReadCount(@RequestParam boolean read) {
        return notificationService.getNotificationCountByUserIdAndRead(getUserId(), read);
    }
}
