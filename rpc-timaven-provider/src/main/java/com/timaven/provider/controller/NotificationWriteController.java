package com.timaven.provider.controller;

import com.timaven.provider.model.Notification;
import com.timaven.provider.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class NotificationWriteController extends BaseController {

    private final NotificationService notificationService;

    @Autowired
    public NotificationWriteController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PostMapping(value = "/notifications")
    public List<Notification> addNotifications(@RequestBody List<Notification> notifications) {
        notificationService.saveNotifications(notifications);
        return notifications;
    }

    @PutMapping(path = "/notifications/{id}/read")
    public String updateNotificationRead(@PathVariable Long id, @RequestParam boolean read) {
        if (id == null) return RESPONSE_FAILED;
        return notificationService.updateNotificationRead(id, read) ? RESPONSE_SUCCESS : RESPONSE_FAILED;
    }
}
