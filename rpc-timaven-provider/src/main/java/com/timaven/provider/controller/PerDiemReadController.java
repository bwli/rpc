package com.timaven.provider.controller;

import com.timaven.provider.model.PerDiem;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.PerDiemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class PerDiemReadController extends BaseController {

    private final PerDiemService perDiemService;

    @Autowired
    public PerDiemReadController(PerDiemService perDiemService) {
        this.perDiemService = perDiemService;
    }

    @PostMapping(path = "/per-diem-list", params = "projectId")
    public Page<PerDiem> getPerDiemPage(@RequestParam(required = false) Long projectId,
                                        @RequestBody PagingRequest pagingRequest) {
        return perDiemService.getPerDiemPage(projectId, pagingRequest);
    }

    @GetMapping(path = "/per-diems/{id}")
    public PerDiem getPerDiem(@PathVariable Long id) {
        return perDiemService.findById(id);
    }

    @GetMapping(path = "/per-diems")
    public PerDiem[] getPerDiemsByProjectId(@RequestParam(required = false) Long projectId) {
        return perDiemService.getPerDiemsByProjectId(projectId);
    }
}
