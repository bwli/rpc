package com.timaven.provider.controller;

import com.timaven.provider.model.PerDiem;
import com.timaven.provider.service.CraftService;
import com.timaven.provider.service.EmployeeService;
import com.timaven.provider.service.PerDiemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class PerDiemWriteController extends BaseController {

    private final PerDiemService perDiemService;
    private final CraftService craftService;
    private final EmployeeService employeeService;

    @Autowired
    public PerDiemWriteController(PerDiemService perDiemService, CraftService craftService, EmployeeService employeeService) {
        this.perDiemService = perDiemService;
        this.craftService = craftService;
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/per-diems", method = {RequestMethod.POST, RequestMethod.PUT})
    public PerDiem addOrUpdatePerDiem(@RequestBody PerDiem perDiem) {
        return perDiemService.savePerDiem(perDiem);
    }

    @DeleteMapping(value = "/per-diems/{id}")
    public String deletePerDiem(@PathVariable Long id) {
        perDiemService.deleteById(id);
        return RESPONSE_SUCCESS;
    }
}
