package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.PurchaseOrderBillingDto;
import com.timaven.provider.model.dto.RequisitionDto;
import com.timaven.provider.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class PurchaseOrderWriteController extends BaseController {

    private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public PurchaseOrderWriteController(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @DeleteMapping(value = "/requisitions")
    public String deleteRequisitionByRequisitionNumber(@RequestParam String requisitionNumber) {
        if (!StringUtils.hasText(requisitionNumber)) return RESPONSE_FAILED;
        purchaseOrderService.deleteByRequisitionNumber(requisitionNumber);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(path = "/requisitions")
    public String saveRequisitions(@RequestBody RequisitionDto requisitionDto, @RequestParam Long projectId) {
        purchaseOrderService.saveRequisition(requisitionDto, projectId);
        return RESPONSE_SUCCESS;
    }

    @PutMapping(path = "/requisitions")
    public Requisition updateRequisition(@RequestBody Requisition requisition) {
        return purchaseOrderService.saveRequisition(requisition);
    }

    @PostMapping(path = "/purchase-order-billings")
    public PurchaseOrderBilling savePurchaseOrderBilling(@RequestBody PurchaseOrderBillingDto purchaseOrderBillingDto) {
        return purchaseOrderService.savePurchaseOrderBilling(purchaseOrderBillingDto);
    }

    @DeleteMapping(value = "/purchase-order-billings/{id}")
    public String deletePurchaseOrderBilling(@PathVariable Long id) {
        purchaseOrderService.deletePurchaseOrderBillingById(id);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(path = "/purchase-order-groups/{po}/comment")
    public PurchaseOrderGroup savePurchaseOrderGroupComment(@PathVariable String po,
                                                            @RequestParam String comment) {
        return purchaseOrderService.savePurchaseOrderGroupComment(po, comment);
    }

    @PostMapping(path = "/purchase-order-groups/{po}/flag")
    public PurchaseOrderGroup savePurchaseOrderGroupFlag(@PathVariable String po,
                                                            @RequestParam boolean flag) {
        return purchaseOrderService.savePurchaseOrderGroupFlag(po, flag);
    }

    @PostMapping(path = "/purchase-order-submissions")
    public PurchaseOrderSubmission savePurchaseOrderSubmission(@RequestBody PurchaseOrderSubmission purchaseOrderSubmission) {
        return purchaseOrderService.savePurchaseOrderSubmission(purchaseOrderSubmission);
    }

    @DeleteMapping(value = "/purchase-order-groups")
    public void deletePurchaseOrderGroups(@RequestBody Collection<String> purchaseOrderNumbers) {
        purchaseOrderService.deletePurchaseOrderGroupByPurchaseOrderNumbers(purchaseOrderNumbers);
    }

    @RequestMapping(path = "/purchase-order-groups", method = {RequestMethod.POST, RequestMethod.PUT})
    public PurchaseOrderGroup saveOrUpdatePurchaseOrderGroup(@RequestBody PurchaseOrderGroup purchaseOrderGroup) {
        return purchaseOrderService.savePurchaseOrderGroup(purchaseOrderGroup);
    }

    @PostMapping(path = "/purchase-order-collection")
    public List<PurchaseOrder> savePurchaseOrders(@RequestBody Collection<PurchaseOrder> purchaseOrders) {
        return purchaseOrderService.savePurchaseOrders(purchaseOrders);
    }

    @PostMapping(path = "/purchase-vendor-roster")
    public List<PurchaseVendorRoster> savePurchaseVendorRoster(@RequestBody Collection<PurchaseVendorRoster> purchaseOrders) {
        return purchaseOrderService.savePurchaseVendorRoster(purchaseOrders);
    }
}
