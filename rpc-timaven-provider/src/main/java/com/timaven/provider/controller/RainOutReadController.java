package com.timaven.provider.controller;

import com.timaven.provider.model.RainOut;
import com.timaven.provider.service.RainOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class RainOutReadController extends BaseController {

    private final RainOutService rainOutService;

    @Autowired
    public RainOutReadController(RainOutService rainOutService) {
        this.rainOutService = rainOutService;
    }

    @GetMapping(path = "/rain-outs")
    public RainOut[] getAllRainOuts(@RequestParam(required = false) Long projectId) {
        return rainOutService.getAllRainOuts(projectId).toArray(RainOut[]::new);
    }

    @GetMapping(path = "/rain-outs/{id}")
    public RainOut getRainOutById(@PathVariable Long id) {
        return rainOutService.getRainOutById(id);
    }
}
