package com.timaven.provider.controller;

import com.timaven.provider.model.dto.ReconcileDto;
import com.timaven.provider.service.ReconcileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class ReconcileReadController extends BaseController {

    private final ReconcileService reconcileService;

    @Autowired
    public ReconcileReadController(ReconcileService reconcileService) {
        this.reconcileService = reconcileService;
    }

    @GetMapping(path = "/reconcile-dtos")
    public ReconcileDto getReconcileDto(@RequestParam LocalDate weekEndDate) {
        return reconcileService.getReconcileData(weekEndDate);
    }
}
