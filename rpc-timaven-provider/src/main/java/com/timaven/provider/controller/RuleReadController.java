package com.timaven.provider.controller;

import com.timaven.provider.model.Rule;
import com.timaven.provider.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class RuleReadController extends BaseController {

    private final RuleService ruleService;

    @Autowired
    public RuleReadController(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @GetMapping(path = "/rules", params = "projectId")
    public Rule getRuleByProjectId(@RequestParam(required = false) Long projectId) {
        return ruleService.getRuleByProjectId(projectId);
    }

    @GetMapping(path = "/rules")
    public Rule[] getRules() {
        return ruleService.getRules().toArray(Rule[]::new);
    }

    @GetMapping(path = "/rules/{id}")
    public Rule getRuleById(@PathVariable Long id) {
        return ruleService.getRuleById(id);
    }
}
