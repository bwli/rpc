package com.timaven.provider.controller;

import com.timaven.provider.model.Rule;
import com.timaven.provider.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class RuleWriteController extends BaseController {

    private final RuleService ruleService;

    @Autowired
    public RuleWriteController(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @RequestMapping(value = "/rules", method = {RequestMethod.POST, RequestMethod.PUT})
    public Rule addOrUpdateRule(@Valid @RequestBody Rule rule) {
        ruleService.saveRule(rule);
        return rule;
    }
}
