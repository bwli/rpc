package com.timaven.provider.controller;

import com.timaven.provider.model.Shift;
import com.timaven.provider.service.ProjectShiftService;
import com.timaven.provider.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class ShiftReadController extends BaseController {

    private final ShiftService shiftService;
    private final ProjectShiftService projectShiftService;

    @Autowired
    public ShiftReadController(ShiftService shiftService, ProjectShiftService projectShiftService) {
        this.shiftService = shiftService;
        this.projectShiftService = projectShiftService;
    }

    @GetMapping(path = "/shifts", params = {"projectId"})
    public Shift[] getShiftsByProjectId(@RequestParam Long projectId) {
        return shiftService.getShiftsByProjectId(projectId).toArray(Shift[]::new);
    }

    @GetMapping(path = "/shifts", params = {"projectId", "name"})
    public Shift getShiftsByProjectId(@RequestParam Long projectId, @RequestParam String name) {
        return shiftService.getShiftByProjectIdAndName(projectId, name);
    }

    @GetMapping(path = "/shifts/{id}")
    public Shift getShiftsById(@PathVariable Long id) {
        return shiftService.getShiftById(id);
    }

    @GetMapping(path = "/shift-names")
    public Set<String> getAllShiftNamesByProjectId(@RequestParam Long projectId) {
        return shiftService.getAllShiftNamesByProjectId(projectId);
    }

    @GetMapping(path = "/default-shifts", params = {"projectId"})
    public Shift getDefaultShift(@RequestParam Long projectId) {
        return shiftService.findDefaultShiftByProjectId(projectId);
    }
}
