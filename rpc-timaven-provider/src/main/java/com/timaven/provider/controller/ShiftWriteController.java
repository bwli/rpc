package com.timaven.provider.controller;

import com.timaven.provider.model.Shift;
import com.timaven.provider.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class ShiftWriteController extends BaseController {

    private final ShiftService shiftService;

    @Autowired
    public ShiftWriteController(ShiftService shiftService) {
        this.shiftService = shiftService;
    }

    @RequestMapping(path = "/shifts", method = {RequestMethod.POST, RequestMethod.PUT})
    public Shift addOrUpdateShift(@RequestBody Shift shift) {
        return shiftService.saveShift(shift);
    }

    @DeleteMapping(path = "/shifts/{id}")
    public String deleteShiftById(@PathVariable Long id) {
        shiftService.deleteShiftById(id);
        return RESPONSE_SUCCESS;
    }
}
