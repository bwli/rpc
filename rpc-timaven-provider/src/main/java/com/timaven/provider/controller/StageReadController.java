package com.timaven.provider.controller;

import com.timaven.provider.model.StageCraft;
import com.timaven.provider.model.StageRoster;
import com.timaven.provider.model.StageSubmission;
import com.timaven.provider.model.StageTeamCostCode;
import com.timaven.provider.service.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class StageReadController extends BaseController {

    private final StageService stageService;

    @Autowired
    public StageReadController(StageService stageService) {
        this.stageService = stageService;
    }

    @GetMapping(path = "/stage-submissions")
    public StageSubmission[] getStageSubmissions(@RequestParam LocalDate effectiveOn,
                                                 @RequestParam StageSubmission.Status status,
                                                 @RequestParam StageSubmission.Type type) {
        return stageService.findStageSubmissions(effectiveOn, status, type);
    }

    @GetMapping(path = "/stage-rosters", params = {"submissionId"})
    public StageRoster[] getStageRostersBySubmissionId(@RequestParam Long submissionId) {
        return stageService.getStageRostersBySubmissionId(submissionId);
    }

    @GetMapping(path = "/stage-team-cost-codes", params = {"submissionId"})
    public StageTeamCostCode[] getStageTeamCostCodesBySubmissionId(@RequestParam Long submissionId) {
        return stageService.getStageTeamCostCodesBySubmissionId(submissionId);
    }

    @GetMapping(path = "/stage-crafts", params = {"submissionId"})
    public StageCraft[] getStageCraftsBySubmissionId(@RequestParam Long submissionId) {
        return stageService.getStageCraftsBySubmissionId(submissionId);
    }
}
