package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.service.ParserService;
import com.timaven.provider.service.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class StageWriteController extends BaseController {

    private final StageService stageService;
    private final ParserService parserService;

    @Autowired
    public StageWriteController(StageService stageService, ParserService parserService) {
        this.stageService = stageService;
        this.parserService = parserService;
    }

    @PostMapping(value = "/stage-submissions")
    public StageSubmission saveStageSubmission(@RequestBody StageSubmission stageSubmission) {
        return stageService.saveStageSubmission(stageSubmission);
    }

    @PostMapping(value = "/stage-roster-collection")
    public List<StageRoster> saveStageRosters(@RequestBody Collection<StageRoster> stageRosters) {
        return stageService.saveRosters(stageRosters);
    }

    @PostMapping(value = "/stage-cost-code-collection")
    public List<StageCostCode> saveStageCostCodes(@RequestBody Collection<StageCostCode> stageCostCodes) {
        return stageService.saveCostCodes(stageCostCodes);
    }

    @PostMapping(value = "/stage-team-cost-code-collection")
    public List<StageTeamCostCode> saveStageTeamCostCodes(@RequestBody Collection<StageTeamCostCode> stageTeamCostCodes) {
        return stageService.saveTeamCostCodes(stageTeamCostCodes);
    }

    @PostMapping(value = "/stage-craft-collection")
    public List<StageCraft> saveStageCrafts(@RequestBody Collection<StageCraft> stageCrafts) {
        return stageService.saveCrafts(stageCrafts);
    }

    @PostMapping(value = "/stage-project")
    public CommonResult updateProjectSetup() {
        return parserService.updateProjectSetUp();
    }

    @PostMapping(value = "/stage-craft")
    public CommonResult updateCraftSubmission() {
        return parserService.updateCraftSubmission();
    }
}
