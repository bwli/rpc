package com.timaven.provider.controller;

import com.timaven.provider.model.UserTeam;
import com.timaven.provider.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class TeamReadController extends BaseController {

    private final TeamService teamService;

    @Autowired
    public TeamReadController(TeamService teamService) {
        this.teamService = teamService;
    }

    @GetMapping(path = "/user-teams")
    public UserTeam[] getUserTeams(@RequestParam Long userId, @RequestParam Long projectId) {
        return teamService.getUserTeams(userId, projectId);
    }
}
