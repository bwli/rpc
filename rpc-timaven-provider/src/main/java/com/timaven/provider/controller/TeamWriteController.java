package com.timaven.provider.controller;

import com.timaven.provider.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class TeamWriteController extends BaseController {

    private final TeamService teamService;

    @Autowired
    public TeamWriteController(TeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping(path = "/user-teams")
    public String addOrUpdateTeam(@RequestBody Set<String> teams,
                                  @RequestParam Long projectId,
                                  @RequestParam Long userId) {
        if (CollectionUtils.isEmpty(teams)) return RESPONSE_FAILED;
        teamService.saveUserTeams(userId, projectId, teams);
        return RESPONSE_SUCCESS;
    }
}
