package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.AllocationSubmissionDto;
import com.timaven.provider.model.dto.AllocationTimeFilter;
import com.timaven.provider.model.dto.EquipmentAllocationTimeFilter;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.EquipmentWeeklyProcessType;
import com.timaven.provider.service.ContentService;
import com.timaven.provider.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class TimeReadController extends BaseController {

    private final ContentService contentService;
    private final EquipmentService equipmentService;

    @Autowired
    public TimeReadController(ContentService contentService, EquipmentService equipmentService) {
        this.contentService = contentService;
        this.equipmentService = equipmentService;
    }

    @GetMapping(path = "/report-submissions")
    public ReportSubmission[] getReportSubmissions(@RequestParam Long projectId,
                                                   @RequestParam String teamName,
                                                   @RequestParam LocalDate startDate,
                                                   @RequestParam(required = false) LocalDate endDate,
                                                   @RequestParam(value = "includes", required = false) List<String> includes) {
        return contentService.getReportSubmissions(projectId, getUserId(), teamName, startDate, endDate, includes);
    }

    @GetMapping(path = "/stage-transactions")
    public StageTransaction[] getStageTransactions(@RequestParam Long projectId,
                                                   @RequestParam(required = false) String teamName,
                                                   @RequestParam LocalDate startDate,
                                                   @RequestParam(required = false) LocalDate endDate,
                                                   @RequestParam(value = "includes", required = false) List<String> includes) {
        return contentService.getStageTransactions(projectId, teamName, startDate, endDate, includes);
    }

    @GetMapping(path = "/trans-submissions")
    public TransSubmission[] getTransSubmissions(@RequestParam Long projectId,
                                                 @RequestParam LocalDate startDate,
                                                 @RequestParam(required = false) LocalDate endDate,
                                                 @RequestParam(value = "includes", required = false) List<String> includes) {
        return contentService.getTransSubmissionsByProjectIdAndDates(projectId, startDate, endDate, includes);
    }

    @GetMapping(path = "/allocation-submissions")
    public AllocationSubmission[] getAllocationSubmissions(@RequestParam(value = "startDate", required = false) LocalDate startDate,
                                                           @RequestParam(value = "endDate", required = false) LocalDate endDate,
                                                           @RequestParam(required = false) Long projectId,
                                                           @RequestParam(required = false) String teamName,
                                                           @RequestParam(required = false) AllocationSubmissionStatus status,
                                                           @RequestParam(required = false) AllocationSubmissionStatus notStatus,
                                                           @RequestParam(value = "includes", required = false) List<String> includes) {
        return contentService.getAllocationSubmissionsByProjectIdAndDateAndStatus(startDate, endDate, projectId, teamName, status, notStatus, includes);
    }

    @GetMapping(path = "/allocation-submission-dtos/{id}")
    public AllocationSubmissionDto getAllocationSubmissionDtoById(@PathVariable Long id) {
        return contentService.getAllocationSubmissionDtoById(id);
    }

    @GetMapping(path = "/allocation-submission-dtos")
    public AllocationSubmissionDto getAllocationSubmissionDtoByDateOfService(@RequestParam Long projectId,
                                                                             @RequestParam(required = false) String teamName,
                                                                             @RequestParam LocalDate dateOfService) {
        return contentService.getAllocationSubmissionDtoByDateOfService(getUserId(), projectId, teamName, dateOfService);
    }

    @GetMapping(path = "/allocation-submissions/{id}")
    public AllocationSubmission getAllocationSubmissionById(@PathVariable Long id) {
        return contentService.getAllocationSubmissionById(id);
    }

    @PostMapping(path = "/equipment-allocation-times-filtered")
    public EquipmentAllocationTime[] getEquipmentAllocationTimes(@RequestBody EquipmentAllocationTimeFilter filter) {
        return equipmentService.getEquipmentAllocationTimesByProjectIdAndDateAndStatus(filter);
    }

    @PostMapping(path = "/allocation-times-filtered")
    public AllocationTime[] getAllocationTimes(@RequestBody AllocationTimeFilter filter) {
        return contentService.getAllocationTimesByProjectIdAndDateAndStatus(filter);
    }

    @PostMapping(path = "/allocation-time-collection")
    public AllocationTime[] getAllocationTimesByIds(@RequestBody Set<Long> ids,
                                                    @RequestParam(required = false) Long projectId,
                                                    @RequestParam(value = "includes", required = false) Set<String> includes) {
        return contentService.getAllocationTimesById(projectId, ids, includes).toArray(AllocationTime[]::new);
    }

    @PostMapping(path = "/equipment-allocation-time-collection")
    public EquipmentAllocationTime[] getEquipmentAllocationTimesByIds(@RequestBody Set<Long> ids) {
        return equipmentService.getEquipmentAllocationTimesById(ids).toArray(EquipmentAllocationTime[]::new);
    }

    @GetMapping(path = "/equipment-weekly-processes", params = {"projectId", "weekEndDate", "type"})
    public EquipmentWeeklyProcess getEquipmentWeeklyProcessByProjectIdAndDateAndType(@RequestParam Long projectId,
                                                                                     @RequestParam LocalDate weekEndDate,
                                                                                     @RequestParam EquipmentWeeklyProcessType type) {
        return equipmentService.getEquipmentWeeklyProcessByProjectIdAndDateAndType(projectId, weekEndDate, type);
    }

    @GetMapping(path = "/last-allocation-submission-dtos")
    public AllocationSubmissionDto getLastAllocationSubmissionDto(@RequestParam Long projectId,
                                                                  @RequestParam String teamName,
                                                                  @RequestParam LocalDate dateOfService) {
        return contentService.getLastAllocationSubmissionDto(projectId, teamName, dateOfService);
    }
}
