package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.enums.WeeklyProcessStatus;
import com.timaven.provider.service.ContentService;
import com.timaven.provider.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class TimeWriteController extends BaseController {

    private final ContentService contentService;
    private final EquipmentService equipmentService;

    @Autowired
    public TimeWriteController(ContentService contentService, EquipmentService equipmentService) {
        this.contentService = contentService;
        this.equipmentService = equipmentService;
    }

    @PostMapping(path = "/reports")
    public String addReports(@RequestBody List<ReportRecord> reportRecords,
                             @RequestParam String teamName,
                             @RequestParam Long projectId) {
        contentService.saveReports(getUserId(), teamName, reportRecords, projectId);
        return RESPONSE_SUCCESS;
    }

    @PatchMapping(path = "/allocation-time-collection")
    public String addOrUpdateAllocationTimes(@RequestBody Set<AllocationTime> allocationTimes) {
        contentService.saveAllocationTimes(allocationTimes);
        return RESPONSE_SUCCESS;
    }

    @RequestMapping(path = "/allocation-submissions", method = {RequestMethod.POST, RequestMethod.PUT})
    public AllocationSubmission addOrUpdateAllocationSubmission(@RequestBody AllocationSubmission allocationSubmission) {
        try {
            return contentService.saveAllocationSubmission(allocationSubmission);
        } catch (Exception ex) {
            if (!(ex instanceof UnexpectedRollbackException)) {
                ex.printStackTrace();
                throw ex;
            }
        }
        return null;
    }

    @RequestMapping(path = "/allocation-submission-collection", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateAllocationSubmissions(@RequestBody Set<AllocationSubmission> allocationSubmissions) {
        contentService.saveAllocationSubmissions(allocationSubmissions);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/allocation-submissions/{id}")
    public String deleteAllocationSubmissionsById(@PathVariable Long id) {
        contentService.deleteAllocationSubmissionById(id);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/allocation-times/{id}")
    public void deleteAllocationTimesById(@PathVariable Long id) {
        contentService.deleteAllocationTimeById(id);
    }

    @DeleteMapping(path = "/stage-transactions", params = {"projectId", "dateOfService"})
    public String deleteStageTransactionsByProjectIdAndTeamAndDateOfService(@RequestParam Long projectId,
                                                                            @RequestParam(required = false) String team,
                                                                            @RequestParam LocalDate dateOfService) {
        contentService.deleteStageTransactionsByTeamAndDateOfService(team, dateOfService, projectId);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/stage-transactions", params = {"projectId", "empId", "dateOfService"})
    public void deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(@RequestParam Long projectId,
                                                                            @RequestParam String empId,
                                                                            @RequestParam(required = false) String teamName,
                                                                            @RequestParam LocalDate dateOfService) {
        contentService.deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(projectId, empId,  teamName, dateOfService);
    }

    @PostMapping(path = "/trans-submissions")
    public TransSubmission saveTransSubmission(@RequestBody TransSubmission transSubmission) {
        return contentService.saveTransSubmission(transSubmission);
    }

    @RequestMapping(path = "/equipment-allocation-submission-collection", method = {RequestMethod.POST, RequestMethod.PUT})
    public String addOrUpdateEquipmentAllocationSubmissions(@RequestBody Set<EquipmentAllocationSubmission> equipmentAllocationSubmissions) {
        equipmentService.saveEquipmentAllocationSubmissions(equipmentAllocationSubmissions);
        return RESPONSE_SUCCESS;
    }

    @PatchMapping(path = "/equipment-allocation-time-collection")
    public String addOrUpdateEquipmentAllocationTimes(@RequestBody Set<EquipmentAllocationTime> equipmentAllocationTimes) {
        equipmentService.saveEquipmentAllocationTimes(equipmentAllocationTimes);
        return RESPONSE_SUCCESS;
    }

    @RequestMapping(path = "/equipment-weekly-processes", method = {RequestMethod.POST, RequestMethod.PUT})
    public EquipmentWeeklyProcess addOrUpdateEquipmentWeeklyProcess(@RequestBody EquipmentWeeklyProcess equipmentWeeklyProcess) {
        return equipmentService.saveEquipmentWeeklyProcess(equipmentWeeklyProcess);
    }

    @PutMapping(path = "/allocation-submissions/exported")
    public void updateAllocationSubmissionExport(@RequestParam boolean exported, @RequestBody Collection<Long> ids) {
        contentService.updateAllocationSubmissionExport(exported, ids);
    }

    @PutMapping(path = "/weekly-processes/update")
    public void updateWeeklyProcesses(@RequestBody Collection<Long> ids, WeeklyProcessStatus weeklyProcessStatus) {
        contentService.updateWeeklyProcessesStatus(ids, weeklyProcessStatus);
    }
}
