package com.timaven.provider.controller;

import com.timaven.provider.model.User;
import com.timaven.provider.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class UserReadController extends BaseController {

    private final AccountService accountService;

    @Autowired
    public UserReadController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(path = "/users")
    public User[] getUsers(@RequestParam(value = "includeRoles", required = false) Set<String> includeRoles,
                           @RequestParam(value = "notIncludeRoles", required = false) Set<String> notIncludeRoles,
                           @RequestParam(value = "includes", required = false) Set<String> includes) {
        return accountService.getUsers(includeRoles, notIncludeRoles, includes);
    }

    @GetMapping(path = "/users", params = "roles")
    public User[] getUsers(@RequestParam(required = false) Long projectId,
                           @RequestParam List<String> roles,
                           @RequestParam(required = false) Boolean includeSelf,
                           @RequestParam(required = false) Boolean includeNoRole) {
        if (CollectionUtils.isEmpty(roles)) return null;
        return accountService.getUsersByProjectIdAndRoles(projectId, roles, includeSelf, includeNoRole);
    }

    @GetMapping(path = "/users/{id}")
    public User getUserById(@PathVariable Long id) {
        if (id == null) return null;
        return accountService.getUserById(id);
    }

    @GetMapping(path = "/users", params = {"username"})
    public User getUserByUsername(@RequestParam String username,
                                  @RequestParam(value = "includes", required = false) Collection<String> includes) {
        if (!StringUtils.hasText(username)) return null;
        return accountService.loadUserByUsername(username, includes);
    }

    @PostMapping(path = "/user-collection")
    public List<User> getUsersById(@RequestBody Collection<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) return new ArrayList<>();
        return accountService.getUsersById(userIds);
    }
}
