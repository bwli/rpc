package com.timaven.provider.controller;

import com.timaven.provider.model.UserRole;
import com.timaven.provider.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class UserRoleWriteController extends BaseController {

    private final AccountService accountService;

    @Autowired
    public UserRoleWriteController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(path = "/user-roles")
    public UserRole addUserRole(@RequestBody UserRole userRole) {
        if (userRole.getUserId() == null
                || userRole.getRoleId() == null) return null;
        accountService.saveUserRole(userRole);
        return userRole;
    }
}
