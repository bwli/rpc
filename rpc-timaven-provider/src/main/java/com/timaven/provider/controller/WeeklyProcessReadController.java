package com.timaven.provider.controller;

import com.timaven.provider.model.WeeklyProcess;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.WeeklyProcessType;
import com.timaven.provider.service.WeeklyProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class WeeklyProcessReadController extends BaseController {

    private final WeeklyProcessService weeklyProcessService;

    @Autowired
    public WeeklyProcessReadController(WeeklyProcessService weeklyProcessService) {
        this.weeklyProcessService = weeklyProcessService;
    }

    @GetMapping(path = "/weekly-processes", params = {"endDate"})
    public WeeklyProcess[] getWeeklyProcesses(@RequestParam(required = false) LocalDate startDate,
                                              @RequestParam LocalDate endDate,
                                              @RequestParam(required = false) WeeklyProcessType type,
                                              @RequestParam(required = false) Long projectId,
                                              @RequestParam(required = false) String teamName,
                                              @RequestParam(value = "includes", required = false) Set<String> includes) {
        return weeklyProcessService.findByDateAndTypeAndProjectId(startDate, endDate, type, projectId, teamName, includes);
    }

    @GetMapping(path = "/weekly-processes", params = {"weekEndDate"})
    public WeeklyProcess getWeeklyProcess(@RequestParam(value = "weekEndDate") LocalDate weekEndDate,
                                          @RequestParam(required = false) WeeklyProcessType type,
                                          @RequestParam(required = false) Long projectId,
                                          @RequestParam(required = false) String teamName,
                                          @RequestParam(value = "includes", required = false) Set<String> includes) {
        return weeklyProcessService.findOrGenerateByDateAndTypeAndProjectId(weekEndDate, type, projectId, teamName, includes);
    }

    @GetMapping(path = "/weekly-processes/{id}")
    public WeeklyProcess getWeeklyProcessById(@PathVariable Long id,
                                              @RequestParam(value = "includes", required = false) Set<String> includes) {
        return weeklyProcessService.findById(id, includes);
    }

    @PostMapping(path = "/weekly-process-collection")
    public WeeklyProcess[] getWeeklyProcessesById(@RequestBody Set<Long> ids,
                                                 @RequestParam(value = "includes", required = false) Set<String> includes) {
        return weeklyProcessService.findByIds(ids, includes);
    }

    @PostMapping(path = "/weekly-process-collection", params = {"weekEndDate"})
    public WeeklyProcess[] getWeeklyProcessesByProjectIdsAndWeeklyEenDate(@RequestBody Set<Long> projectIds,
                                                                          @RequestParam LocalDate weekEndDate,
                                                                          @RequestParam LocalDate weekStartDate,
                                                                          @RequestParam(required = false) AllocationSubmissionStatus status) {
        return weeklyProcessService.findByProjectIdAndWeeklyEndDate(projectIds, weekEndDate, weekStartDate, status);
    }
}
