package com.timaven.provider.dao.repository;

import com.timaven.provider.model.ActivityCodePerc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;
import java.util.List;

public interface ActivityCodePercRepository extends JpaRepository<ActivityCodePerc, Long> {
    List<ActivityCodePerc> findActivityCodePercsByEmployeeIdIn(Set<String> empIds);
}
