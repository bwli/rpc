package com.timaven.provider.dao.repository;

import org.hibernate.envers.query.criteria.AuditCriterion;

import java.util.ArrayList;
import java.util.List;

public interface AuditRepository {
    ArrayList<Object[]> findAuditEntity(Class<?> c, AuditCriterion auditCriterion, Integer firstResult,
                                        Integer maxResults);

    List<Object> findAuditEntityOnly(Class<?> c, AuditCriterion auditCriterion);
}
