package com.timaven.provider.dao.repository;

import com.timaven.provider.model.BillingCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface BillingCodeRepository extends JpaRepository<BillingCode, Long>, JpaSpecificationExecutor<BillingCode> {

    @Query(value = "select bc from BillingCode bc left join fetch bc.billingCodeType bct " +
            "inner join bc.costCodeBillingCodes ccbc " +
            "where ccbc.costCodeId = :costCodeId " +
            "order by bct.id, bc.codeName, bc.clientAlias")
    List<BillingCode> findByCostCodeId(Long costCodeId);

    @Query("select bc from BillingCode bc left join fetch bc.billingCodeOverrides " +
            "where bc.typeId = :id")
    List<BillingCode> findByTypeId(Long id);

    BillingCode findByTypeIdAndCodeName(Long typeId, String codeName);

    List<BillingCode> findByTypeIdAndCodeNameIn(Long typeId, Set<String> codeNames);
}
