package com.timaven.provider.dao.repository;

import com.timaven.provider.model.BillingCodeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BillingCodeTypeRepository extends JpaRepository<BillingCodeType, Long> {
    List<BillingCodeType> findByLevelNotNullOrderByLevel();

    List<BillingCodeType> findByIdIn(List<Long> ids);

    @Modifying
    @Query(value = "update project.billing_code_type set level = null where level is not null", nativeQuery = true)
    void resetLevel();
}
