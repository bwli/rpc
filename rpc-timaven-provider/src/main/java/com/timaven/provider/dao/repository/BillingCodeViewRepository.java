package com.timaven.provider.dao.repository;

import com.timaven.provider.model.BillingCodeView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface BillingCodeViewRepository extends JpaRepository<BillingCodeView, Long>, JpaSpecificationExecutor<BillingCodeView> {
    @Query(value = "select distinct on (billing_code_id) id from project.billing_code_view " +
            "where project_id is null or project_id = :projectId " +
            "order by billing_code_id, project_id nulls last", nativeQuery = true)
    Set<String> findDistinctIds(Long projectId);

    @Query(value = "select distinct on (billing_code_id) id from project.billing_code_view " +
            "where project_id is null", nativeQuery = true)
    Set<String> findDistinctIds();
}
