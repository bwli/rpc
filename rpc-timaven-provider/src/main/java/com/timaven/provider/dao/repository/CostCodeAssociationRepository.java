package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeAssociation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface CostCodeAssociationRepository extends JpaRepository<CostCodeAssociation, Long>,
        JpaSpecificationExecutor<CostCodeAssociation> {

    @Query(value = "with t0 as (" +
            "select distinct on (cca.cost_code) cca.id, cca.end_date from project.cost_code_association cca " +
            "where project_id is null or project_id = :projectId " +
            "and cca.start_date <= current_date " +
            "order by cca.cost_code, project_id nulls last, created_at desc) " +
            "select t0.id from t0 where (t0.end_date is null or t0.end_date >= current_date) ", nativeQuery = true)
    Set<Long> findDistinctIds(@NotNull Long projectId);

    @Query(value = "with t0 as (" +
            "select distinct on (cca.cost_code) cca.id, cca.end_date from project.cost_code_association cca " +
            "where project_id is null or project_id = :projectId " +
            "and cca.start_date <= :dateOfService " +
            "order by cca.cost_code, project_id nulls last, created_at desc) " +
            "select t0.id from t0 where (t0.end_date is null or t0.end_date >= :dateOfService) ", nativeQuery = true)
    Set<Long> findDistinctIds(@NotNull Long projectId, @NotNull LocalDate dateOfService);

    @Query(value = "select cca.* from project.cost_code_association cca " +
            "where (:projectId = 0 or cca.project_id = :projectId) " +
            "and (:ignoreCostCodes is true or cca.cost_code in :codes) " +
            "and cca.start_date <= :endDate " +
            "and (cca.end_date is null or cca.end_date >= :startDate) " +
            "order by cca.cost_code, cca.project_id nulls last, cca.created_at desc", nativeQuery = true)
    List<CostCodeAssociation> findByProjectIdAndCodesAndDates(Long projectId, boolean ignoreCostCodes, Set<String> codes, LocalDate startDate, LocalDate endDate);

    @Query(value = "with t0 as (" +
            "select distinct on (cca.cost_code) cca.* from project.cost_code_association cca " +
            "where (:projectId = 0 or cca.project_id = :projectId) " +
            "and cca.cost_code in :codes " +
            "and cca.start_date <= :dateOfService " +
            "order by cca.cost_code, cca.project_id nulls last) " +
            "select * from t0 where (t0.end_date is null or t0.end_date >= :dateOfService) ", nativeQuery = true)
    Set<CostCodeAssociation> findByProjectIdAndCodes(Long projectId, Set<String> codes, LocalDate dateOfService);
}
