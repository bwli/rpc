package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodePerc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Set;

public interface CostCodePercRepository extends JpaRepository<CostCodePerc, Long> {
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<CostCodePerc> findByAllocationTimeIdIn(Set<Long> allocationTimeIds);
}
