package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Set;

public interface CostCodeTagRepository extends JpaRepository<CostCodeTag, Long>,
        JpaSpecificationExecutor<CostCodeTag> {
    CostCodeTag findByTypeIdAndCodeName(Long typeId, String codeName);

    Set<CostCodeTag> findByTypeId(Long typeId);
}
