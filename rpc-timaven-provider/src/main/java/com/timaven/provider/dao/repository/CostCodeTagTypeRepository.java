package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeTagType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Set;

public interface CostCodeTagTypeRepository extends JpaRepository<CostCodeTagType, Long>,
        JpaSpecificationExecutor<CostCodeTagType> {
    @Query("select cctt from CostCodeTagType cctt " +
            "where (:projectId is null and cctt.projectId is null or cctt.projectId = :projectId) " +
            "and cctt.tagType = :tagType ")
    CostCodeTagType findByProjectIdAndTagType(Long projectId, String tagType);

    Set<CostCodeTagType> findByProjectId(Long projectId);
}
