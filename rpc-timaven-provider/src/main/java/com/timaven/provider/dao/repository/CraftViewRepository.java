package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CraftView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface CraftViewRepository extends JpaRepository<CraftView, Long>, JpaSpecificationExecutor<CraftView> {

    // TM-421
    @Query(value = "select c from CraftView c "
            + "where (:projectId is null and c.projectId is null or c.projectId = :projectId) "
            + "  and (cast(:start as date) is null "
            + "    or (c.endDate is null or :start <= c.endDate))")
    List<CraftView> findByProjectIdAndDateRange(Long projectId, LocalDate start);
}
