package com.timaven.provider.dao.repository;

public interface CustomRepository<T> {
    void refresh(T t);
}
