package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Employee;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;


public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee>, CustomRepository<Employee> {
    List<Employee> findAll(Specification<Employee> specificationAll);

    @Query(value = "select distinct e from Employee e " +
            "where (:projectId is null and e.projectId is null or e.projectId = :projectId) " +
            "and e.effectedOn <= :today " +
            "and (cast(:startDate as date) is null " +
            "or ((cast(:endDate as date) is null " +
            "and e.hiredAt <= :startDate " +
            "or cast(:endDate as date) is not null " +
            "and e.hiredAt <= :endDate and e.effectedOn <= :endDate " +
            "and :endDate >= :startDate) " +
            "and (e.terminatedAt is null or e.hiredAt <= e.terminatedAt)))")
    Set<Employee> findByProjectIsAndDateRange(Long projectId, LocalDate startDate, LocalDate endDate, LocalDate today);

    @Query(value = "with t0 as (" +
            "select distinct on (e.emp_id) e.* from project.employee e " +
            "where (:projectId = 0 and e.project_id is null or e.project_id = :projectId) " +
            "and e.effected_on <= current_date and e.effected_on <= :dateOfService " +
            "order by e.emp_id, e.created_at desc) " +
            "select * from t0 where t0.hired_at <= :dateOfService and " +
            "(t0.terminated_at is null or t0.terminated_at >= :dateOfService)", nativeQuery = true)
    Set<Employee> findByProjectIsAndDateOfService(Long projectId, LocalDate dateOfService);

    @Query(value = "with t0 as (" +
            "select distinct on (e.emp_id) e.* from project.employee e " +
            "where e.emp_id = :empId " +
            "and (:projectId = 0 and e.project_id is null or e.project_id = :projectId) " +
            "and e.effected_on <= :effectedOn order by e.emp_id, e.created_at desc) " +
            "select * from t0 where (t0.terminated_at is null or t0.terminated_at >= t0.hired_at)", nativeQuery = true)
    Employee findByEmpIdAndProjectIdAndDate(String empId, Long projectId, LocalDate effectedOn);

    @Query(value = "with cte as ( " +
            "    with t0 as ( " +
            "        select distinct a.emp_id, s.date_of_service " +
            "        from time.allocation_time a " +
            "                 inner join time.allocation_submission s on a.submission_id = s.id " +
            "        where a.emp_id in :employeeIds " +
            "          and s.date_of_service >= (cast(:dateOfService as date) - cast(:days || ' days' as interval)) " +
            "          and s.date_of_service <= (cast(:dateOfService as date) - interval '1 day') " +
            "          and s.status = 2 " +
            "        group by a.emp_id, s.date_of_service) " +
            "    select emp_id, " +
            "           date_of_service, " +
            "           cast(date_of_service - cast(row_number() over (partition by emp_id order by date_of_service) || ' days' as interval) as date) as groupingSet " +
            "    from t0) " +
            "select emp_id, " +
            "       min(date_of_service) as startDate, " +
            "       max(date_of_service) as endDate, " +
            "       count(*) " +
            "from cte " +
            "group by emp_id, groupingSet " +
            "having count(*) > 0 and max(date_of_service) = (cast(:dateOfService as date) - interval '1 day') " +
            "order by emp_id, startDate", nativeQuery = true)
    Set<Object[]> findDayOffEmployees(Set<String> employeeIds, LocalDate dateOfService, long days);

    @Query(value = "select distinct e.signInSheet from Employee e " +
            "where (:projectId is null and e.projectId is null or e.projectId = :projectId) " +
            "and e.effectedOn <= :today and e.effectedOn <= :dateOfService " +
            "and e.hiredAt <= :dateOfService " +
            "and (e.terminatedAt is null or e.terminatedAt >= :dateOfService) " +
            "and e.signInSheet is not null")
    Set<String> findDistinctSignInSheets(Long projectId, LocalDate dateOfService, LocalDate today);

    @Query(value = "with t0 as (" +
            "select distinct on (e.emp_id) e.* from project.employee e " +
            "where (:projectId = 0 and e.project_id is null or e.project_id = :projectId) " +
            "and e.effected_on <= :today and e.effected_on <= :dateOfService " +
            "and e.hired_at <= :dateOfService " +
            "and (e.team_name is not null and e.team_name <> '')  " +
            "order by e.emp_id, e.created_at desc) " +
            "select t0.team_name from t0 where (t0.terminated_at is null or t0.terminated_at >= :dateOfService)", nativeQuery = true)
    Set<String> findDistinctTeamNames(Long projectId, LocalDate dateOfService, LocalDate today);

    @Query(value = "select distinct on (e.emp_id) e.id, e.first_name, e.last_name, e.craft, e.crew from project.employee e " +
            "where (:projectId = 0 and e.project_id is null or e.project_id = :projectId) " +
            "and e.effected_on <= current_date and e.effected_on <= :dateOfService " +
            "and e.hired_at <= :dateOfService " +
            "and (e.terminated_at is null or e.terminated_at >= :dateOfService) " +
            "order by e.emp_id, e.created_at desc", nativeQuery = true)
    Set<Object[]> findDistinctEmployeeIdNames(Long projectId, LocalDate dateOfService);

    @Query(value = "with t0 as (" +
            "select distinct on (e.project_id, e.emp_id) e.shift, e.terminated_at, e.project_id " +
            "from project.employee e " +
            "where e.is_active and e.project_id = :projectId " +
            "and e.effected_on <= current_date and e.effected_on <= :dateOfService " +
            "and e.hired_at <= :dateOfService " +
            "order by e.project_id, e.emp_id, e.created_at desc) " +
            "select t0.shift from t0 " +
            "where (t0.terminated_at is null or t0.terminated_at >= :dateOfService)", nativeQuery = true)
    Set<String> findShiftsByProjectIdAndDate(Long projectId, LocalDate dateOfService);

    List<Employee> findByPerDiemId(Long perDiemId);

    @Query(value = "with t0 as (" +
            "select distinct on (e.emp_id) e.* from project.employee e " +
            "where e.effected_on = :effectedOn and e.hired_at <= :effectedOn " +
            "order by e.emp_id, e.created_at desc) " +
            "select * from t0 where (t0.terminated_at is null or t0.terminated_at >= t0.hired_at)", nativeQuery = true)
    Set<Employee> findByEffectedOn(LocalDate effectedOn);

    // TM-412
    /**
     * Get the number of active employee by projectId
     * @param projectId should not be null
     * @return
     */
    @Query(value = "with t0 as (" +
            "select distinct on (e.emp_id) e.* from project.employee e " +
            "where (:projectId = 0 and e.project_id is null or e.project_id = :projectId) " +
            "  and e.effected_on <= current_date and e.hired_at <= current_date " +
            "order by e.emp_id, e.created_at desc) " +
            "select count(1) from t0 " +
            "where t0.terminated_at is null or t0.terminated_at >= current_date", nativeQuery = true)
    int countByProjectId(Long projectId);

    @Query(value = "with t0 as (" +
            "select distinct on (e.emp_id) e.* from project.employee e " +
            "where (:projectId = 0 and e.project_id is null or e.project_id = :projectId) " +
            "and e.effected_on <= current_date and e.effected_on <= :dateOfService " +
            "and (:teamName = '' or e.team_name = :teamName) " +
            "order by e.emp_id, e.created_at desc) " +
            "select * from t0 where t0.hired_at <= :dateOfService and " +
            "(t0.terminated_at is null or t0.terminated_at >= :dateOfService)", nativeQuery = true)
    Set<Employee> findByTeamNameAndProjectIdAndDateOfService(String teamName, Long projectId, LocalDate dateOfService);

}
