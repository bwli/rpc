package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentAllocationTime;
import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface EquipmentAllocationTimeRepository extends JpaRepository<EquipmentAllocationTime, Long> {

    @Query(value = "select t from EquipmentAllocationTime t " +
            "inner join fetch t.equipmentAllocationSubmission s " +
            "left join fetch t.equipmentCostCodePercs " +
            "where (:ignoreDates is true or s.dateOfService in :dates) " +
            "and (cast(:endDate as date) is null or s.dateOfService <= :endDate) " +
            "and (cast(:startDate as date) is null or s.dateOfService >= :startDate) " +
            "and (cast(:payrollStartDate as date) is null or t.payrollDate >= :payrollStartDate) " +
            "and (cast(:payrollEndDate as date) is null or t.payrollDate <= :payrollEndDate) " +
            "and (:projectId is null and s.projectId is null or s.projectId = :projectId) " +
            "and (:status is null or s.status = :status) " +
            "and (:notStatus is null or s.status <> :notStatus) " +
            "order by s.createdAt desc")
    Set<EquipmentAllocationTime> findByDateRangeAndProjectIdAndStatus(LocalDate startDate, LocalDate endDate,
                                                                      boolean ignoreDates, Set<LocalDate> dates,
                                                                      LocalDate payrollStartDate, LocalDate payrollEndDate,
                                                                      Long projectId,
                                                                      EquipmentAllocationSubmissionStatus status,
                                                                      EquipmentAllocationSubmissionStatus notStatus);

    @Query(value = "select t from EquipmentAllocationTime t " +
            "left join fetch t.equipmentCostCodePercs " +
            "inner join fetch t.equipmentAllocationSubmission s " +
            "where t.id in :ids")
    Set<EquipmentAllocationTime> findAllByIdFetching(Set<Long> ids);

    @Query(value = "select t from EquipmentAllocationTime t " +
            "left join fetch t.equipmentCostCodePercs " +
            "inner join fetch t.equipmentAllocationSubmission s " +
            "where s.projectId = :projectId " +
            "and s.dateOfService = :dateOfService " +
            "and t.payrollDate = :payrollDate " +
            "and s.status = :status " +
            "and t.ownershipType = :type ")
    List<EquipmentAllocationTime> findEquipmentAllocationTimesByDate(Long projectId, LocalDate dateOfService,
                                                                     LocalDate payrollDate,
                                                                     EquipmentAllocationSubmissionStatus status,
                                                                     EquipmentOwnershipType type);

    @Query(value = "select t from EquipmentAllocationTime t " +
            "inner join fetch t.equipmentAllocationSubmission s " +
            "where s.projectId = :projectId " +
            "and t.payrollDate >= :startDate " +
            "and t.payrollDate <= :endDate " +
            "and s.status in :statuses")
    List<EquipmentAllocationTime> findEquipmentAllocationTimesByDateRange(Long projectId, LocalDate startDate,
                                                                          LocalDate endDate,
                                                                          Set<EquipmentAllocationSubmissionStatus> statuses);
}
