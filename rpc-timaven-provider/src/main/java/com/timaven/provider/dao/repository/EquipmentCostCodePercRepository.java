package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentCostCodePerc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentCostCodePercRepository extends JpaRepository<EquipmentCostCodePerc, Long> {
}
