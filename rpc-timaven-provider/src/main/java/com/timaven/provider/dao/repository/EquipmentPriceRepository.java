package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;


public interface EquipmentPriceRepository extends JpaRepository<EquipmentPrice, Long>,
        JpaSpecificationExecutor<EquipmentPrice> {
    Optional<EquipmentPrice> findByProjectIdAndEquipmentClass(Long projectId, String equipmentClass);

    Set<EquipmentPrice> findByProjectId(Long projectId);

    @Query("select ep from EquipmentPrice ep " +
            "where ep.projectId = :projectId " +
            "and ep.equipmentClass in :equipmentClasses")
    Set<EquipmentPrice> findAllByProjectIdAndEquipmentClassIn(Long projectId, Collection<String> equipmentClasses);
}
