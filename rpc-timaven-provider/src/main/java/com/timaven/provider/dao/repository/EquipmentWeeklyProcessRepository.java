package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentWeeklyProcess;
import com.timaven.provider.model.enums.EquipmentWeeklyProcessType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface EquipmentWeeklyProcessRepository extends JpaRepository<EquipmentWeeklyProcess, Long> {
    EquipmentWeeklyProcess findByProjectIdAndWeekEndDateAndType(Long projectId, LocalDate weekEndDate, EquipmentWeeklyProcessType type);
}
