package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface NotificationRepository extends JpaRepository<Notification, Long> {
    List<Notification> findByUserIdOrderByCreatedAtDesc(Long id);

    int countByUserIdAndRead(Long userId, boolean read);
}
