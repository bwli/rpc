package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PerDiem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


public interface PerDiemRepository extends JpaRepository<PerDiem, Long>, JpaSpecificationExecutor<PerDiem> {
    List<PerDiem> findByProjectIdOrderByNameAsc(Long projectId);
}
