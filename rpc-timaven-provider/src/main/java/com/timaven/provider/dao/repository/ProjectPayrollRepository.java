package com.timaven.provider.dao.repository;

import com.timaven.provider.model.ProjectPayroll;
import com.timaven.provider.model.ProjectPayrollPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProjectPayrollRepository extends JpaRepository<ProjectPayroll, ProjectPayrollPK>,
        JpaSpecificationExecutor<ProjectPayroll> {
}
