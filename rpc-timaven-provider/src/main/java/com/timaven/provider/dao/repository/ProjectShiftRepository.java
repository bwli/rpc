package com.timaven.provider.dao.repository;

import com.timaven.provider.model.ProjectShift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ProjectShiftRepository extends JpaRepository<ProjectShift, Long> {
    @Query(value = "insert into rule.project_shift (project_id, shift_id) values (:projectId, :shiftId) " +
            "on conflict on constraint project_shift_project_id_key " +
            "do update set shift_id=:shiftId " +
            "where rule.project_shift.project_id=:projectId",
            nativeQuery = true)
    @Modifying
    void upsertProjectShift(@Param("projectId") Long projectId, @Param("shiftId") Long shiftId);

    @Query(value = "select ps from ProjectShift ps where ps.projectId = :projectId")
    ProjectShift getProjectShiftByProjectId(@Param("projectId") Long projectId);
}
