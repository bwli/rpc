package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrderBillingDetailView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PurchaseOrderBillingDetailViewRepository extends JpaRepository<PurchaseOrderBillingDetailView, Long>,
        JpaSpecificationExecutor<PurchaseOrderBillingDetailView> {
}
