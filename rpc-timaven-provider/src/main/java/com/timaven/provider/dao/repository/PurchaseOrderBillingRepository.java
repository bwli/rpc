package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrderBilling;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PurchaseOrderBillingRepository extends JpaRepository<PurchaseOrderBilling, Long> {
    @Query("select b from PurchaseOrderBilling b " +
            "left join fetch b.purchaseOrderBillingDetails " +
            "where b.id = :id")
    PurchaseOrderBilling findByIdFetchingDetails(Long id);
}
