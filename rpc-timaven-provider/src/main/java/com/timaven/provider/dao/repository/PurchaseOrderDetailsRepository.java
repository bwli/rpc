package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PurchaseOrderDetailsRepository extends JpaRepository<PurchaseOrderDetails, Long>,
        JpaSpecificationExecutor<PurchaseOrderDetails> {
}
