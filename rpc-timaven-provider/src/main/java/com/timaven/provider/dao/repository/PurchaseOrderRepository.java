package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long>,
        JpaSpecificationExecutor<PurchaseOrder> {
    @Query("select distinct po.purchaseOrderNumber from PurchaseOrder po " +
            "where po.submissionId = (select max(pos.id) from PurchaseOrderSubmission pos)")
    Set<String> findDistinctLatestPurchaserOderNumbers();
}
