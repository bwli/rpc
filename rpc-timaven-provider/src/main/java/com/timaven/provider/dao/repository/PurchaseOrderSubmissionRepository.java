package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrderSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PurchaseOrderSubmissionRepository extends JpaRepository<PurchaseOrderSubmission, Long> {
    @Query(value = "select s from PurchaseOrderSubmission s " +
            "left join fetch s.purchaseOrders  " +
            "order by s.id desc")
    List<PurchaseOrderSubmission> findPurchaseOrderSubmissionsOrderByIdDesc();
}
