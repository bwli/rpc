package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseVendorRoster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Set;

public interface PurchaseVendorRosterRepository extends JpaRepository<PurchaseVendorRoster, Long>,
        JpaSpecificationExecutor<PurchaseVendorRoster> {

    Set<PurchaseVendorRoster> findPurchaseVendorRosterByVendorIdIn(Set<Long> vendorIds);

}
