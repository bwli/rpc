package com.timaven.provider.dao.repository;

import com.timaven.provider.model.RPCUserPool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RPCUserPoolRepository extends JpaRepository<RPCUserPool, Long> {

    /**
     * getActiveUserPools
     * @return
     */
    @Query("select r from RPCUserPool r where r.active = true")
    List<RPCUserPool> getActiveUserPools();
}
