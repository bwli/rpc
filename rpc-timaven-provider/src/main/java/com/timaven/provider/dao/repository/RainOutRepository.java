package com.timaven.provider.dao.repository;

import com.timaven.provider.model.RainOut;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface RainOutRepository extends JpaRepository<RainOut, Long> {
    Set<RainOut> findByProjectIdNullOrProjectId(Long projectId);
}
