package com.timaven.provider.dao.repository;

import com.timaven.provider.model.RequisitionGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RequisitionGroupRepository extends JpaRepository<RequisitionGroup, Long>,
        JpaSpecificationExecutor<RequisitionGroup> {
}
