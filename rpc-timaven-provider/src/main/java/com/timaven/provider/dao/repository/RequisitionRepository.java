package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Requisition;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface RequisitionRepository extends JpaRepository<Requisition, Long> {
    List<Requisition> findByRequisitionNumber(String requisitionNumber);

    void deleteByRequisitionNumber(String requisitionNumber);

    Set<Requisition> findByJobNumberAndSubJob(String jobNumber, String subJob);

    List<Requisition> findByPurchaseOrderNumberNull();
}
