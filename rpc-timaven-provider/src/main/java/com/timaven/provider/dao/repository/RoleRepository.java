package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query(value = "select r from Role r where r.name = :name")
    Role findByName(@Param("name") String name);

    Set<Role> findByNameIn(List<String> roleNames);
}
