package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface RuleRepository extends JpaRepository<Rule, Long> {
    @Query(value = "select r from Rule r " +
            "where :projectId is null and r.projectId is null or r.projectId = :projectId")
    Rule getRuleByProjectId(Long projectId);

    Rule findByProjectIdIsNull();

    List<Rule> findByPerDiemId(Long id);
}
