package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface ShiftRepository extends JpaRepository<Shift, Long> {
    @Query(value = "select s from Shift s where s.projectId = :projectId")
    Set<Shift> getShiftsByProjectId(@Param("projectId") Long projectId);

    Shift findByProjectIdAndName(Long projectId, String name);
}
