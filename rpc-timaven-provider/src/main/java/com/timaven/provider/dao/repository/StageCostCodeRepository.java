package com.timaven.provider.dao.repository;

import com.timaven.provider.model.StageCostCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface StageCostCodeRepository extends JpaRepository<StageCostCode, Long> {
    List<StageCostCode> findBySubmissionIdIs(Long id);
}
