package com.timaven.provider.dao.repository;

import com.timaven.provider.model.StageCraft;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StageCraftRepository extends JpaRepository<StageCraft, Long> {
    List<StageCraft> findBySubmissionIdIs(Long id);
}
