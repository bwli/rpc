package com.timaven.provider.dao.repository;

import com.timaven.provider.model.StageTeamCostCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface StageTeamCostCodeRepository extends JpaRepository<StageTeamCostCode, Long> {
    List<StageTeamCostCode> findBySubmissionIdIs(Long id);
}
