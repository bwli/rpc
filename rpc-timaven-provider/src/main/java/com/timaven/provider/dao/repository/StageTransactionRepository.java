package com.timaven.provider.dao.repository;

import com.timaven.provider.model.StageTransaction;
import com.timaven.provider.model.TransSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface StageTransactionRepository extends JpaRepository<StageTransaction, Long> {
    @Query(value = "select distinct st.* from time.stage_trans st " +
            "inner join time.trans_submission ts on ts.id = st.submission_id " +
            "left outer join time.stage_exception se on st.id = se.stage_trans_id " +
            "left outer join time.stage_record sr on st.id = sr.stage_trans_id " +
            "where ts.project_id = :projectId " +
            "and st.date_of_service = :dateOfService " +
            "and (:teamName = '' and st.team_name is null or st.team_name = :teamName) " +
            "and st.status = 0 " +
            "order by st.date_of_service, st.created_at desc", nativeQuery = true)
    List<StageTransaction> findByProjectIdTeamAndDateOfService(Long projectId, String teamName, LocalDate dateOfService);

    @Query("select distinct st from StageTransaction st " +
            "inner join st.transSubmission ts " +
            "where ts.projectId = :projectId " +
            "and st.dateOfService >= :startDate " +
            "and (:teamName is null or st.teamName = :teamName) " +
            "and (cast(:endDate as date) is null or st.dateOfService <= :endDate) " +
            "and st.status = :status " +
            "order by st.dateOfService, st.createdAt desc")
    List<StageTransaction> findByProjectIdAndTeamAndDates(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, StageTransaction.Status status);

    @Query("select distinct st from StageTransaction st " +
            "inner join st.transSubmission ts " +
            "left join fetch st.stageExceptions " +
            "left join fetch st.stageRecords " +
            "where ts.projectId = :projectId " +
            "and st.dateOfService >= :startDate " +
            "and (:teamName is null or st.teamName = :teamName) " +
            "and (cast(:endDate as date) is null or st.dateOfService <= :endDate) " +
            "and st.status = :status " +
            "order by st.dateOfService, st.createdAt desc")
    List<StageTransaction> findByProjectIdAndTeamAndDatesFetching(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, StageTransaction.Status status);

    @Modifying
    @Query(value = "delete from time.stage_trans st " +
            "using time.trans_submission ts " +
            "where ts.id = st.submission_id " +
            "and ts.project_id = :projectId " +
            "and (:team = '' and (st.team_name is null or trim(st.team_name) = '') or trim(st.team_name) = trim(:team)) " +
            "and st.date_of_service = :dateOfService", nativeQuery = true)
    void deleteByTeamNameAndDateOfService(String team, LocalDate dateOfService, Long projectId);

    @Query(value = "select distinct st from StageTransaction st " +
            "inner join st.transSubmission ts " +
            "left join fetch st.stageExceptions " +
            "left join fetch st.stageRecords " +
            "where ts.projectId = :projectId " +
            "and st.dateOfService = :dateOfService " +
            "and (:teamName is null or st.teamName = :teamName) " +
            "and st.createdAt >= :statusChangedAt " +
            "and st.status = :status " +
            "order by st.createdAt desc")
    List<StageTransaction> findByProjectIdTeamAndDateOfServiceAndTime(Long projectId, String teamName, LocalDate dateOfService, LocalDateTime statusChangedAt, StageTransaction.Status status);

    @Modifying
    @Query(value = "delete from time.stage_trans st " +
            "using time.trans_submission ts " +
            "where ts.id = st.submission_id " +
            "and ts.project_id = :projectId " +
            "and st.emp_id = :empId " +
            "and (:team = '' and (st.team_name is null or trim(st.team_name) = '') or trim(st.team_name) = trim(:team)) " +
            "and st.date_of_service = :dateOfService", nativeQuery = true)
    void deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(Long projectId, String empId, String team, LocalDate dateOfService);

    @Query(value = "select distinct st from StageTransaction st " +
            "inner join st.transSubmission ts " +
            "left join fetch st.stageExceptions " +
            "left join fetch st.stageRecords " +
            "where ts.projectId = :projectId " +
            "and st.dateOfService = :dateOfService " +
            "and (:teamName is null or st.teamName = :teamName) " +
            "and st.status = :status " +
            "order by st.createdAt desc")
    Set<StageTransaction> findByProjectIdTeamAndDateOfServiceAndStatus(Long projectId, String teamName, LocalDate dateOfService, StageTransaction.Status status);
}
