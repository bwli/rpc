package com.timaven.provider.dao.repository;

import com.timaven.provider.model.TeamCostCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;


public interface TeamCostCodeRepository extends JpaRepository<TeamCostCode, Long> {
    @Query(value = "select tcc from TeamCostCode tcc " +
            "where tcc.projectId = :projectId " +
            "and tcc.teamName = :teamName " +
            "and tcc.startDate <= :dateOfService " +
            "and (tcc.endDate is null or tcc.endDate >= :dateOfService)")
    List<TeamCostCode> findByProjectIdIsAndTeamNameAndDateOfService(Long projectId, String teamName,
                                                                    LocalDate dateOfService);

    @Query(value = "select tcc from TeamCostCode tcc " +
            "where (:projectId is null and tcc.projectId is null or tcc.projectId = :projectId) " +
            "and (cast(:startDate as date) is null " +
            "or ((cast(:endDate as date) is null and tcc.startDate <= :startDate or cast(:endDate as date) is not null " +
            "and tcc.startDate <= :endDate) and (tcc.endDate is null or tcc.endDate >= :startDate)))")
    List<TeamCostCode> findByProjectIdAndDateRange(Long projectId, LocalDate startDate, LocalDate endDate);
}
