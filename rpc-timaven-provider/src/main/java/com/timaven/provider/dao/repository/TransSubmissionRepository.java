package com.timaven.provider.dao.repository;

import com.timaven.provider.model.TransSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface TransSubmissionRepository extends JpaRepository<TransSubmission, Long> {
    @Query("select t from TransSubmission t " +
            "where t.projectId = :projectId " +
            "and t.createdAt >= :startDate " +
            "and (cast(:endDate as date) is null or t.createdAt <= :endDate) " +
            "order by t.createdAt desc")
    List<TransSubmission> findTransSubmissionByProjectIdAndDates(Long projectId, LocalDateTime startDate, LocalDateTime endDate);

    @Query("select distinct t from TransSubmission t " +
            "left join fetch t.stageTransactions " +
            "where t.projectId = :projectId " +
            "and t.createdAt >= :startDate " +
            "and (cast(:endDate as date) is null or t.createdAt <= :endDate) " +
            "order by t.createdAt desc")
    List<TransSubmission> findTransSubmissionByProjectIdAndDatesFetching(Long projectId, LocalDateTime startDate, LocalDateTime endDate);
}
