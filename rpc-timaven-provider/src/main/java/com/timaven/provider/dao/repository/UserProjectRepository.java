package com.timaven.provider.dao.repository;

import com.timaven.provider.model.UserProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;


public interface UserProjectRepository extends JpaRepository<UserProject, Long> {
    @Query(value = "select up from UserProject up left join fetch up.project p " +
            "where up.userId = :userId and p.deleted = false")
    Set<UserProject> getUserProjectByUserId(@Param("userId") Long userId);

    Set<UserProject> findByProjectIdIs(Long id);

    UserProject findByUserIdIsAndProjectIdIs(Long userId, Long projectId);

    Set<UserProject> findByUserIdIn(Set<Long> accountantIds);
}
