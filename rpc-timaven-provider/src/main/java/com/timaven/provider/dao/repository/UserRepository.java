package com.timaven.provider.dao.repository;

import com.timaven.provider.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select u from User u where u.username = :username")
    User findUserByUsername(String username);

    @Query(value = "select u from User u " +
            "inner join u.userRoles ur " +
            "where ur.role.name in ('ROLE_accountant', 'ROLE_timekeeper', 'ROLE_accountant_foreman')")
    List<User> findAccountants();

    @Query(value = "select u from User u " +
            "inner join u.userRoles ur " +
            "where ur.role.name = 'ROLE_admin' or (ur.role.name = 'ROLE_project_manager' and u.id in :userIds)")
    Set<User> findAdminManagerInProject(Set<Long> userIds);

    @Query(value = "select u from User u left join fetch u.userRoles ur left join fetch ur.role")
    List<User> findAllFetchingRoles(Sort sort);

    @Query(value = "select u from User u " +
            "left join fetch u.userRoles ur " +
            "left join fetch ur.role r " +
            "left join u.userProjects up " +
            "where r.name in :corporateRoles " +
            "or (:includeNoRole is not null and :includeNoRole = true and r.id is null)")
    Set<User> findUsersByProjectIdAndCorporateRoles(Set<String> corporateRoles, Boolean includeNoRole);

    @Query(value = "select u from User u " +
            "left join fetch u.userRoles ur " +
            "left join fetch ur.role r " +
            "left join u.userProjects up " +
            "where ((:projectId is null or up.projectId = :projectId) and r.name in :projectRoles) " +
            "or (:includeNoRole is not null and :includeNoRole = true and r.id is null)")
    Set<User> findUsersByProjectIdAndProjectRoles(Long projectId, Set<String> projectRoles, Boolean includeNoRole);

    @Query(value = "select u from User u " +
            "left join fetch u.userRoles ur " +
            "left join fetch ur.role r " +
            "left join u.userProjects up " +
            "where r.name in :corporateRoles " +
            "or ((:projectId is null or up.projectId = :projectId) and r.name in :projectRoles) " +
            "or (:includeNoRole is not null and :includeNoRole = true and r.id is null)")
    Set<User> findUsersByProjectIdAndRoles(Long projectId, Set<String> corporateRoles, Set<String> projectRoles, Boolean includeNoRole);

    @Query("select u from User u " +
            "left join fetch u.roleIds " +
            "left join fetch u.userRoles ur " +
            "left join fetch ur.role " +
            "where u.username = :username")
    User findUserByUsernameFetchingRoles(String username);
}
