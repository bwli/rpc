package com.timaven.provider.dao.repository;

import com.timaven.provider.model.WeeklyProcess;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.WeeklyProcessType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface WeeklyProcessRepository extends JpaRepository<WeeklyProcess, Long> {
    WeeklyProcess findByProjectIdAndWeekEndDateAndType(Long projectId, LocalDate weekEndDate, WeeklyProcessType type);

    @Query(value = "select wp from WeeklyProcess wp " +
            "left join fetch wp.allocationTimes t " +
            "inner join fetch t.allocationSubmission " +
            "left join fetch t.costCodePercs " +
            "left join fetch t.activityCodePercs " +
            "where wp.id = :id")
    WeeklyProcess findByIdFetching(Long id);

    @Query(value = "select distinct wp from WeeklyProcess wp " +
            "left join fetch wp.allocationTimes t " +
            "left join fetch t.allocationSubmission " +
            "where wp.id in :ids")
    List<WeeklyProcess> findAllByIdFetchingSubmissions(Set<Long> ids);

    @Query(value = "select distinct wp from WeeklyProcess wp " +
            "left join fetch wp.allocationTimes t " +
            "left join fetch t.allocationSubmission " +
            "left join fetch t.costCodePercs " +
            "where wp.id in :ids")
    List<WeeklyProcess> findAllByIdFetchingCostCodePercs(Set<Long> ids);

    @Query(value = "select wp from WeeklyProcess wp " +
            "where (:projectId is null and wp.projectId is null or wp.projectId = :projectId) " +
            "and (:teamName is null or wp.teamName = :teamName) " +
            "and (:type is null or wp.type = :type) " +
            "and (cast(:startDate as date) is null or wp.weekEndDate >= :startDate) " +
            "and (cast(:endDate as date) is null or wp.weekEndDate <= :endDate)")
    Set<WeeklyProcess> findByProjectIdByDateAndTypeAndProjectId(LocalDate startDate, LocalDate endDate, WeeklyProcessType type, Long projectId, String teamName);

    @Query(value = "select wp from WeeklyProcess wp " +
            "left join fetch wp.allocationTimes t " +
            "inner join fetch t.allocationSubmission " +
            "left join fetch t.costCodePercs " +
            "left join fetch t.activityCodePercs " +
            "where (:projectId is null and wp.projectId is null or wp.projectId = :projectId) " +
            "and (:teamName is null or wp.teamName = :teamName) " +
            "and (:type is null or wp.type = :type) " +
            "and (cast(:startDate as date) is null or wp.weekEndDate >= :startDate) " +
            "and (cast(:endDate as date) is null or wp.weekEndDate <= :endDate)")
    Set<WeeklyProcess> findByProjectIdByDateAndTypeAndProjectIdFetching(LocalDate startDate, LocalDate endDate, WeeklyProcessType type, Long projectId, String teamName);

    @Query(value = "select wp from WeeklyProcess wp " +
            "left join fetch wp.allocationTimes " +
            "where (:projectId is null and wp.projectId is null or wp.projectId = :projectId) " +
            "and (:teamName is null and wp.teamName is null or wp.teamName = :teamName) " +
            "and (:type is null or wp.type = :type) " +
            "and wp.weekEndDate = :weekEndDate")
    WeeklyProcess findByProjectIdByDateAndTypeAndProjectIdFetching(LocalDate weekEndDate, WeeklyProcessType type, Long projectId, String teamName);

    @Query(value = "select wp from WeeklyProcess wp " +
            "where (:projectId is null and wp.projectId is null or wp.projectId = :projectId) " +
            "and (:teamName is null and wp.teamName is null or wp.teamName = :teamName) " +
            "and (:type is null or wp.type = :type) " +
            "and wp.weekEndDate = :weekEndDate")
    WeeklyProcess findByProjectIdByDateAndTypeAndProjectId(LocalDate weekEndDate, WeeklyProcessType type, Long projectId, String teamName);

    @Query(value = "select distinct wp from WeeklyProcess wp " +
            "left join fetch wp.allocationTimes t " +
            "left join fetch t.allocationSubmission s " +
            "where wp.projectId in :projectIds " +
            "and (:status is null or s.status = :status) " +
            "and wp.weekEndDate <= :weekEndDate " +
            "and wp.weekEndDate >= :weekStartDate")
    List<WeeklyProcess> findAllByProjectIdAndWeeklyEndDate(Set<Long> projectIds, LocalDate weekEndDate, LocalDate weekStartDate, AllocationSubmissionStatus status);
}
