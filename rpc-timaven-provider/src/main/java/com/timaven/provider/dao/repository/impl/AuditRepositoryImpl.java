package com.timaven.provider.dao.repository.impl;

import com.timaven.provider.dao.repository.AuditRepository;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.AuditCriterion;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class AuditRepositoryImpl implements AuditRepository {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public ArrayList<Object[]> findAuditEntity(Class<?> c, AuditCriterion auditCriterion, Integer firstResult,
                                               Integer maxResults) {
        AuditQuery query = AuditReaderFactory.get(entityManager)
                .createQuery()
                .forRevisionsOfEntity(c, false, true);
        query.addOrder(AuditEntity.revisionNumber().desc());
        if (null != auditCriterion) query.add(auditCriterion);
        if (null != firstResult) query.setFirstResult(firstResult);
        if (null != maxResults) query.setMaxResults(maxResults);
        //This return a list of array triplets of changes concerning the specified revision.
        // The array triplet contains the entity, entity revision information and at last the revision type.
        List list = query.getResultList();
        return CollectionUtils.isEmpty(list) ? new ArrayList<>() : (ArrayList<Object[]>) list;
    }

    @Override
    public List<Object> findAuditEntityOnly(Class<?> c, AuditCriterion auditCriterion) {
        AuditQuery query = AuditReaderFactory.get(entityManager)
                .createQuery()
                .forRevisionsOfEntity(c, true, true);
        query.addOrder(AuditEntity.revisionNumber().desc());
        if (null != auditCriterion) query.add(auditCriterion);
        //This return a list of array triplets of changes concerning the specified revision.
        // The array triplet contains the entity, entity revision information and at last the revision type.
        List list = query.getResultList();
        return CollectionUtils.isEmpty(list) ? new ArrayList<>() : (ArrayList<Object>) list;
    }
}
