package com.timaven.provider.model;

import com.timaven.provider.model.enums.AllocationTimeHourType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Audited
@Entity
@Table(name = "activity_code_perc", schema = "time")
public class ActivityCodePerc {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "st_hour")
    private BigDecimal stHour;

    @Column(name = "ot_hour")
    private BigDecimal otHour;

    @Column(name = "dt_hour")
    private BigDecimal dtHour;

    @Column(name = "total_hour")
    private BigDecimal totalHour;

    @Column(name = "emp_id")
    private String employeeId;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "activity_code")
    private String activityCodeText;

    @Column(name = "activity_id")
    private Long activityId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "allocation_time_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private AllocationTime allocationTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activity_code_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private ActivityCode activityCode;

    @Column(name = "extra_time_type")
    private AllocationTimeHourType extraTimeType;
}
