package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Comparator;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationRecord.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "allocation_record", schema = "time")
public class AllocationRecord implements Comparable<AllocationRecord> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "allocation_time_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    @JsonBackReference
    private AllocationTime allocationTime;

    @Column(name = "door_name")
    private String doorName;

    @Column(name = "side")
    private Boolean side;

    @Column(name = "access_granted")
    private boolean accessGranted = false;

    @Column(name = "valid")
    private boolean valid = false;

    @Column(name = "adjusted_time")
    private LocalDateTime adjustedTime;

    @Column(name = "net_event_time")
    private LocalDateTime netEventTime;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationRecord)) return false;
        AllocationRecord that = (AllocationRecord) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(AllocationRecord allocationRecord) {
        return Comparator.comparing(AllocationRecord::getAdjustedTime, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, allocationRecord);
    }
}
