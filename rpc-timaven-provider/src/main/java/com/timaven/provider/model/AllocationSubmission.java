package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationSubmission.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "allocation_submission", schema = "time")
public class AllocationSubmission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "submit_user_id", columnDefinition = "BIGINT")
    private Long submitUserId;
    @Column(name = "approve_user_id", columnDefinition = "BIGINT")
    private Long approveUserId;
    @Column(name = "finalize_user_id", columnDefinition = "BIGINT")
    private Long finalizeUserId;
    @Column(name = "approved_at")
    private LocalDateTime approvedAt;
    @Column(name = "finalized_at")
    private LocalDateTime finalizedAt;
    @Column(name = "report_submission_id", columnDefinition = "BIGINT")
    private Long reportSubmissionId;
    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinColumn(name = "report_submission_id", referencedColumnName = "id", insertable = false, updatable = false,
            columnDefinition = "BIGINT")
    private ReportSubmission reportSubmission;
    @Column(name = "team_name")
    private String teamName;
    @Column(name = "status")
    private AllocationSubmissionStatus status;
    @Column(name = "date_of_service")
    private LocalDate dateOfService;
    @Column(name = "payroll_date")
    private LocalDate payrollDate;
    @Column(name = "by_system")
    private Boolean isBySystem = false;
    @Column(name = "billing_purpose_only")
    private Boolean billingPurposeOnly = false;
    @Column(name = "project_id")
    private Long projectId;
    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt = LocalDateTime.now();
    @Column(name = "status_changed_at", insertable = false, updatable = false)
    private LocalDateTime statusChangedAt = LocalDateTime.now();
    @NotAudited
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "allocationSubmission", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    @OrderBy(value = "lastName asc, firstName asc")
    private Set<AllocationTime> allocationTimes;

    @NotAudited
    @Transient
    private Boolean isManual;

    @NotAudited
    @Transient
    private User submitUser;
    @NotAudited
    @Transient
    private User approveUser;

    @Column(name = "exported")
    private Boolean exported = false;

    public Set<AllocationTime> getAllocationTimes() {
        return allocationTimes == null ? new HashSet<>() : allocationTimes;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (AllocationSubmissionStatus.DENIED.equals(status)) {
            hcb.append(id);
        } else {
            hcb.append(projectId);
            hcb.append(teamName);
            hcb.append(dateOfService);
            hcb.append(payrollDate);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationSubmission)) return false;
        AllocationSubmission that = (AllocationSubmission) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (AllocationSubmissionStatus.DENIED.equals(status)) {
            eb.append(id, that.id);
        } else {
            eb.append(projectId, that.projectId);
            eb.append(teamName, that.teamName);
            eb.append(dateOfService, that.dateOfService);
            eb.append(payrollDate, that.payrollDate);
        }
        return eb.isEquals();
    }

}
