package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.AllocationTimeHourType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationTime.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "allocation_time", schema = "time")
public class AllocationTime implements Comparable<AllocationTime> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "emp_id")
    private String empId;

    @Column(name = "client_emp_id")
    private String clientEmpId;

    @Column(name = "badge")
    private String badge;

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "st_hour")
    private BigDecimal stHour;

    @Column(name = "ot_hour")
    private BigDecimal otHour;

    @Column(name = "dt_hour")
    private BigDecimal dtHour;

    @Column(name = "extra_time_type")
    private AllocationTimeHourType extraTimeType;

    @Column(name = "total_hour")
    private BigDecimal totalHour;

    @Column(name = "allocated_hour")
    private BigDecimal allocatedHour;

    @Column(name = "net_hour")
    private BigDecimal netHour;

    @Column(name = "has_per_diem")
    private boolean hasPerDiem;

    @Column(name = "has_mob")
    private boolean hasMob;

    @Column(name = "mob_cost_code")
    private String mobCostCode;

    @Column(name = "mob_amount")
    private BigDecimal mobAmount = BigDecimal.ZERO;

    @Column(name = "mob_mileage_rate")
    private BigDecimal mobMileageRate = BigDecimal.ZERO;

    @Column(name = "mob_mileage")
    private BigDecimal mobMileage = BigDecimal.ZERO;

    @Column(name = "per_diem_amount")
    private BigDecimal perDiemAmount = BigDecimal.ZERO;

    @Column(name = "rig_pay")
    private BigDecimal rigPay;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private AllocationSubmission allocationSubmission;

    @Column(name = "submission_id", insertable = false, updatable = false)
    private Long allocationSubmissionId;

    @NotAudited
    @OneToMany(mappedBy = "allocationTime", fetch = FetchType.LAZY)
    @JsonIgnoreProperties
    private Set<CostCodePerc> costCodePercs;

    @NotAudited
    @OneToMany(mappedBy = "allocationTime", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties
    @OrderBy("order asc")
    private Set<PunchException> punchExceptions;

    @NotAudited
    @OneToMany(mappedBy = "allocationTime", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties
    @OrderBy("adjustedTime asc")
    private Set<AllocationRecord> allocationRecords;

    @NotAudited
    @OneToMany(mappedBy = "allocationTime", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties
    private Set<AllocationException> allocationExceptions;

    @Column(name = "mob_display_order")
    private Integer mobDisplayOrder;

    @NotAudited
    @Transient
    @JsonIgnoreProperties
    private AllocationException allocationException;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "max_time_cost_code")
    private String maxTimeCostCode;

    @Column(name = "per_diem_cost_code")
    private String perDiemCostCode;

    @Column(name = "weekly_process_id")
    private Long weeklyProcessId;

    // TM-307
    @Column(name = "is_offsite")
    private Boolean isOffsite = false;

    @NotAudited
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "weekly_process_id", referencedColumnName = "id", insertable = false, updatable = false,
            columnDefinition = "BIGINT")
    private WeeklyProcess weeklyProcess;

    @NotAudited
    @Transient
    private List<CostCodeTagAssociation> mobCostCodeTagAssociations;

    @NotAudited
    @Transient
    private List<CostCodeTagAssociation> perDiemCostCodeTagAssociations;

    @NotAudited
    @Transient
    private Employee employee;

    @NotAudited
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @Transient
    private PerDiem perDiem;

    @NotAudited
    @OneToMany(mappedBy = "allocationTime", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonIgnoreProperties
    private Set<ActivityCodePerc> activityCodePercs;

    public void addCostCodePerc(CostCodePerc costCodePerc) {
        if (null == costCodePercs) {
            costCodePercs = new HashSet<>();
        }
        costCodePercs.add(costCodePerc);
    }

    public BigDecimal getStHour() {
        return stHour == null ? BigDecimal.ZERO : stHour;
    }

    public BigDecimal getOtHour() {
        return otHour == null ? BigDecimal.ZERO : otHour;
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? BigDecimal.ZERO : dtHour;
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? BigDecimal.ZERO : totalHour;
    }

    public BigDecimal getNetHour() {
        return netHour == null ? BigDecimal.ZERO : netHour;
    }

    public BigDecimal getAllocatedHour() {
        return allocatedHour == null ? BigDecimal.ZERO : allocatedHour;
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationTime)) return false;
        if (null != id) {
            AllocationTime that = (AllocationTime) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(AllocationTime o) {
        return Comparator
                .comparing(AllocationTime::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AllocationTime::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
