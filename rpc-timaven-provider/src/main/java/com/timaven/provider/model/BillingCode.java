package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.dto.BillingCodeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingCode.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "billing_code", schema = "project")
public class BillingCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id", referencedColumnName = "id", columnDefinition = "BIGINT",
            insertable = false, updatable = false)
    private BillingCodeType billingCodeType;

    @OneToMany(mappedBy = "billingCode")
    private Set<CostCodeBillingCode> costCodeBillingCodes;

    @Column(name = "type_id")
    private Long typeId;

    @Column(name = "code_name")
    private String codeName;

    @Column(name = "client_alias")
    private String clientAlias;

    @Column(name = "description")
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "billingCode")
    private Set<BillingCodeOverride> billingCodeOverrides;

    public BillingCode(BillingCodeView billingCodeView) {
        this.id = billingCodeView.getBillingCodeId();
        this.typeId = billingCodeView.getTypeId();
        this.codeName = billingCodeView.getCodeName();
        this.clientAlias = billingCodeView.getClientAlias();
        this.description = billingCodeView.getDescription();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCode)) return false;
        BillingCode that = (BillingCode) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}
