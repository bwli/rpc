package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingCodeOverride.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "billing_code_override", schema = "project")
public class BillingCodeOverride {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billing_code_id", referencedColumnName = "id", columnDefinition = "BIGINT",
            insertable = false, updatable = false)
    private BillingCode billingCode;

    @Column(name = "billing_code_id")
    private Long billingCodeId;

    @Column(name = "code_name")
    private String codeName;

    @Column(name = "client_alias")
    private String clientAlias;

    @Column(name = "description")
    private String description;

    @Column(name = "project_id")
    private Long projectId;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCodeOverride)) return false;
        BillingCodeOverride that = (BillingCodeOverride) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}
