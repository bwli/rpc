package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.BillingCodeTypeParsedBy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingCodeType.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "billing_code_type", schema = "project")
public class BillingCodeType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code_type")
    private String codeType;

    @Column(name = "level")
    private Integer level;

    @NotNull
    @Column(name = "is_required")
    private Boolean required = false;

    @NotNull
    @Column(name = "manageable_by_project")
    private boolean manageableByProject;

    @NotNull
    @Column(name = "parsed_by")
    private BillingCodeTypeParsedBy billingCodeTypeParsedBy;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(codeType);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCodeType)) return false;
        BillingCodeType that = (BillingCodeType) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(codeType, that.codeType);
        }
        return eb.isEquals();
    }
}
