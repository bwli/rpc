package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = BillingCodeView.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "billing_code_view", schema = "project")
public class BillingCodeView {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "billing_code_id")
    private Long billingCodeId;

    @Column(name = "type_id")
    private Long typeId;

    @Column(name = "code_type")
    private String codeType;

    @Column(name = "code_name")
    private String codeName;

    @Column(name = "client_alias")
    private String clientAlias;

    @Column(name = "description")
    private String description;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "override_id")
    private Long overrideId;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingCodeView)) return false;
        BillingCodeView that = (BillingCodeView) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}
