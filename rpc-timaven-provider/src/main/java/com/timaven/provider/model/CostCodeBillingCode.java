package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeBillingCode.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cost_code_billing_code", schema = "project")
public class CostCodeBillingCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "cost_code_id", insertable = false, updatable = false)
    private Long costCodeId;

    @Column(name = "billing_code_id")
    private Long billingCodeId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cost_code_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private CostCodeLog costCodeLog;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billing_code_id", referencedColumnName = "id", columnDefinition = "BIGINT",
            insertable = false, updatable = false)
    private BillingCode billingCode;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeId);
        hcb.append(billingCodeId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeBillingCode)) return false;
        CostCodeBillingCode that = (CostCodeBillingCode) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeId, that.costCodeId);
        eb.append(billingCodeId, that.billingCodeId);
        return eb.isEquals();
    }
}
