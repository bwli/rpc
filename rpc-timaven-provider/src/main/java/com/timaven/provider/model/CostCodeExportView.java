package com.timaven.provider.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cost_code_export", schema = "project")
public class CostCodeExportView {

    @Id
    @Column(name = "row_num")
    private Long rowNumber;

    @Column(name = "id")
    private Long costCodeId;

    @Column(name = "cost_code_full")
    private String costCodeFull;

    @Column(name = "description")
    private String description;

    @Column(name = "code_name")
    private String costCode;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "sub_job")
    private String subJob;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "billing_code_type_id")
    private Long billingCodeTypeId;

    @Column(name = "start_date")
    LocalDate startDate;

    @Column(name = "end_date")
    LocalDate endDate;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeId);
        hcb.append(billingCodeTypeId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeExportView)) return false;
        CostCodeExportView that = (CostCodeExportView) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeId, that.costCodeId);
        eb.append(billingCodeTypeId, that.billingCodeTypeId);
        return eb.isEquals();
    }

}
