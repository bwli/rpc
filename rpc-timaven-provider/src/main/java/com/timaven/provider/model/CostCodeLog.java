package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeLog.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cclg", schema = "project")
public class CostCodeLog implements Comparable<CostCodeLog> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cost_code_full", unique = true)
    private String costCodeFull;

    @Column(name = "description")
    private String description;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @NotNull
    @Column(name = "is_locked")
    private boolean locked = false;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @NotAudited
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "costCodeLog")
    private Set<CostCodeBillingCode> costCodeBillingCodes;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeFull);
        hcb.append(projectId);
        hcb.append(startDate);
        hcb.append(endDate);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeLog)) return false;
        CostCodeLog that = (CostCodeLog) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeFull, that.costCodeFull);
        eb.append(projectId, that.projectId);
        eb.append(startDate, that.startDate);
        eb.append(endDate, that.endDate);
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeLog costCodeLog) {
        if (projectId == null) return 1;
        if (costCodeLog.projectId == null) return -1;
        int result = projectId.compareTo(costCodeLog.projectId);
        if (result == 0) {
            result = costCodeFull.compareTo(costCodeLog.costCodeFull);
        }
        return result;
    }
}
