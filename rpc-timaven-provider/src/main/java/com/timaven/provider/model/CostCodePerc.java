package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodePerc.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cost_code_perc", schema = "time")
public class CostCodePerc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cost_code_full")
    private String costCodeFull;

    @Column(name = "order_code")
    private String purchaseOrder;

    @Column(name = "st_hour")
    private BigDecimal stHour;

    @Column(name = "ot_hour")
    private BigDecimal otHour;

    @Column(name = "dt_hour")
    private BigDecimal dtHour;

    @Column(name = "total_hour")
    private BigDecimal totalHour;

    @NotNull
    @Column(name = "percentage")
    private Integer percentage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "allocate_time_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private AllocationTime allocationTime;

    @Column(name = "allocate_time_id", insertable = false, updatable = false)
    private Long allocationTimeId;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "display_order")
    private Integer displayOrder;

    @NotAudited
    @Transient
    private List<CostCodeTagAssociation> costCodeTagAssociations;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodePerc)) return false;
        if (null != id) {
            CostCodePerc that = (CostCodePerc) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
