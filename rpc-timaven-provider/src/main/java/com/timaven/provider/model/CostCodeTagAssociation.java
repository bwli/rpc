package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.CostCodeType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeTagAssociation.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cost_code_tag_association", schema = "time")
public class CostCodeTagAssociation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "tag_type")
    private String tagType;

    @Column(name = "type_description")
    private String typeDescription;

    @Column(name = "code_name")
    private String codeName;

    @Column(name = "code_description")
    private String codeDescription;

    @Column(name = "cost_code_type")
    private CostCodeType costCodeType;

    @Column(name = "key")
    private Long key;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeTagAssociation)) return false;
        if (null != id) {
            CostCodeTagAssociation that = (CostCodeTagAssociation) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
