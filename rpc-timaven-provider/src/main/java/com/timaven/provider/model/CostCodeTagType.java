package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeTagType.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cost_code_tag_type", schema = "project")
public class CostCodeTagType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "tag_type")
    private String tagType;

    @Column(name = "description")
    private String description;

    @Column(name = "project_id")
    private Long projectId;

    @OneToMany(mappedBy = "costCodeTagType", fetch = FetchType.LAZY)
    private Set<CostCodeTag> costCodeTags;

    @Transient
    private Map<String, Object> idType = new HashMap<>();

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(tagType);
            hcb.append(projectId);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeTagType)) return false;
        CostCodeTagType that = (CostCodeTagType) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(tagType, that.tagType);
            eb.append(projectId, that.projectId);
        }
        return eb.isEquals();
    }
}
