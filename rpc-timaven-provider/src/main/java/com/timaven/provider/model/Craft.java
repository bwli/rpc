package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "craft", schema = "project")
public class Craft implements Comparable<Craft>, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "billable_st")
    private BigDecimal billableST;

    @Column(name = "billable_ot")
    private BigDecimal billableOT;

    @Column(name = "billable_dt")
    private BigDecimal billableDT;

    @Column(name = "description")
    private String description;

    @Column(name = "per_diem")
    private BigDecimal perDiem;

    @Column(name = "per_diem_id")
    private Long perDiemId;

    @Column(name = "rig_pay")
    private BigDecimal rigPay;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "company")
    private String company;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (null != id) {
            hcb.append(id);
            return hcb.toHashCode();
        } else {
            hcb.append(projectId);
            hcb.append(code);
            hcb.append(startDate);
            hcb.append(endDate);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Craft)) return false;
        Craft that = (Craft) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (null != id || null != that.id) {
            eb.append(id, that.id);
        } else {
            eb.append(projectId, that.projectId);
            eb.append(code, that.code);
            eb.append(startDate, that.startDate);
            eb.append(endDate, that.endDate);
        }
        return eb.isEquals();
    }

    @Override
    public int compareTo(Craft o) {
        return Comparator.comparing(Craft::getCode, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getDescription,  Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableST, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableOT, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableDT, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getPerDiem, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getPerDiemId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getRigPay, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getStartDate, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getEndDate, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getCompany, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
