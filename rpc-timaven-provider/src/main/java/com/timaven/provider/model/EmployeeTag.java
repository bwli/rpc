package com.timaven.provider.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee_tag", schema = "project")
public class EmployeeTag implements Comparable<EmployeeTag> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "emp_id")
    private String empId;

    @NotNull
    @Column(name = "tag")
    private String tag;

    @Column(name = "project_id")
    private Long projectId;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (id != null) {
            hcb.append(id);
        } else {
            hcb.append(empId);
            hcb.append(tag);
            hcb.append(projectId);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmployeeTag)) return false;
        EmployeeTag that = (EmployeeTag) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (this.id != null && that.id != null) {
            eb.append(id, that.id);
        } else {
            eb.append(empId, that.empId);
            eb.append(projectId, that.projectId);
            eb.append(tag, that.tag);
        }
        return eb.isEquals();
    }

    @Override
    public int compareTo(EmployeeTag o) {
        return Comparator.comparing(EmployeeTag::getTag, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
