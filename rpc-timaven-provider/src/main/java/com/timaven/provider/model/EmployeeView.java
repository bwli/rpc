package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(of = "id")
@Table(name = "employee_view", schema = "project")
public class EmployeeView {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "emp_id")
    private String empId;

    @Column(name = "client_emp_id")
    private String clientEmpId;

    @Column(name = "craft")
    private String craft;

    @Column(name = "per_diem_id_")
    private Long perDiemId;

    @Column(name = "per_diem_name_")
    private String perDiemName;

    @Column(name = "client_craft")
    private String clientCraft;

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "crew")
    private String crew;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    // Either F or M
    @Column(name = "gender")
    private String gender;

    @Column(name = "company")
    private String company;

    @Column(name = "badge")
    private String badge;

    @Column(name = "sign_in_sheet")
    private String signInSheet;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "hired_at")
    private LocalDate hiredAt;

    @Column(name = "effected_on")
    private LocalDate effectedOn;

    @Column(name = "terminated_at")
    private LocalDate terminatedAt;

    @Column(name = "has_per_diem")
    private Boolean hasPerDiem = false;

    @Column(name = "has_rig_pay")
    private Boolean hasRigPay = false;

    @Column(name = "tags")
    private String tags;

    // TM-307
    @Column(name = "is_offsite")
    private Boolean isOffsite = false;

    @Column(name = "activity_code")
    private String activityCode;
}
