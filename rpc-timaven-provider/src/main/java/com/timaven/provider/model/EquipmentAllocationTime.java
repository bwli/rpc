package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.EquipmentHourlyType;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentAllocationTime.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment_allocation_time", schema = "time")
public class EquipmentAllocationTime {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "equipment_id", updatable = false)
    private String equipmentId;

    @Column(name = "description")
    private String description;

    @Column(name = "alias")
    private String alias;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "department")
    private String department;

    @NotNull
    @Column(name = "class")
    private String equipmentClass;

    @Column(name = "hourly_type")
    private EquipmentHourlyType hourlyType;

    @Column(name = "ownership_type")
    private EquipmentOwnershipType ownershipType;

    @Column(name = "emp_id")
    private String empId;

    @Column(name = "total_hour")
    private BigDecimal totalHour;

    @Column(name = "total_charge")
    private BigDecimal totalCharge;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private EquipmentAllocationSubmission equipmentAllocationSubmission;

    @NotAudited
    @OneToMany(mappedBy = "equipmentAllocationTime", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<EquipmentCostCodePerc> equipmentCostCodePercs;

    @Column(name = "weekly_process_id")
    private Long weeklyProcessId;

    @NotAudited
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "weekly_process_id", referencedColumnName = "id", columnDefinition = "BIGINT",
            insertable = false, updatable = false)
    private EquipmentWeeklyProcess equipmentWeeklyProcess;

    @Column(name = "payroll_date")
    private LocalDate payrollDate;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentAllocationTime)) return false;
        if (null != id) {
            EquipmentAllocationTime that = (EquipmentAllocationTime) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
