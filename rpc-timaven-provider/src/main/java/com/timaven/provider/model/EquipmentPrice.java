package com.timaven.provider.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment_price", schema = "project")
public class EquipmentPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "class")
    private String equipmentClass;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "hourly_rate")
    private BigDecimal hourlyRate;

    @Column(name = "daily_rate")
    private BigDecimal dailyRate;

    @Column(name = "weekly_rate")
    private BigDecimal weeklyRate;

    @Column(name = "monthly_rate")
    private BigDecimal monthlyRate;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentClass);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentPrice)) return false;
        EquipmentPrice that = (EquipmentPrice) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentClass, that.equipmentClass);
        return eb.isEquals();
    }
}
