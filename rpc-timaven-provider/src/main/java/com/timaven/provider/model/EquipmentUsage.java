package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentUsage.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment_usage", schema = "project")
public class EquipmentUsage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "equipment_id", updatable = false)
    private Long equipmentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipment_id", referencedColumnName = "id", columnDefinition = "BIGINT", insertable = false,
            updatable = false)
    private Equipment equipment;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "started_by", columnDefinition = "BIGINT")
    private Long startedBy;

    @Column(name = "ended_by", columnDefinition = "BIGINT")
    private Long endedBy;

    @Column(name = "project_id")
    private Long projectId;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Project project;

    @Column(name = "daily_rate")
    private BigDecimal dailyRate;

    @Column(name = "weekly_rate")
    private BigDecimal weeklyRate;

    @Column(name = "monthly_rate")
    private BigDecimal monthlyRate;

    @NotNull
    @Column(name = "is_active")
    private boolean active = true;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentUsage)) return false;
        EquipmentUsage that = (EquipmentUsage) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentId, that.equipmentId);
        return eb.isEquals();
    }
}
