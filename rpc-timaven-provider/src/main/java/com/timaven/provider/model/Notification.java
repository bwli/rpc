package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "notification", schema = "project")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "message_from")
    private String from;

    @Column(name = "summary")
    private String summary;

    @Column(name = "message")
    private String message;

    @Column(name = "is_read")
    private boolean read;

    @Column(name = "user_id", columnDefinition = "BIGINT")
    private Long userId;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    public Notification(String from, String summary, String message, Long userId) {
        this.from = from;
        this.summary = summary;
        this.message = message;
        this.userId = userId;
        this.read = false;
    }
}
