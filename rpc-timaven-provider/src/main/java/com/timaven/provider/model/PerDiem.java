package com.timaven.provider.model;

import com.timaven.provider.model.enums.PerDiemMode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Where(clause = "(is_active)")
@Entity
@Table(name = "per_diem", schema = "rule")
public class PerDiem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "mode")
    private PerDiemMode perDiemMode = PerDiemMode.PAY_AS_YOU_GO;

    @Column(name = "cost_code")
    private String costCode;

    @Column(name = "daily_threshold")
    private Integer dailyThreshold;

    @Column(name = "weekly_threshold")
    private Integer weeklyThreshold;

    @Column(name = "project_id")
    private Long projectId;

    @NotNull
    @Column(name = "is_active")
    private Boolean active = true;

    @Column(name = "overwrite_amount")
    private BigDecimal overwriteAmount;
}
