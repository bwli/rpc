package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Where(clause = "(not is_deleted)")
@Entity
@Table(name = "project", schema = "pm")
public class Project implements Comparable<Project>, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "sub_job")
    private String subJob;

    @NotNull
    @Column(name = "is_active")
    private Boolean active = true;

    @NotNull
    @Column(name = "is_deleted")
    private boolean deleted = false;

    // TM-192 Start
    @Column(name = "client_name")
    private String clientName;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;
    // TM-192 End

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
    @JsonManagedReference("project")
    private List<UserProject> userProjects;

    @Transient
    private Set<Long> userIds;

    public Set<Long> getUserIds() {
        if (!CollectionUtils.isEmpty(userProjects)) {
            userIds = userProjects.stream().map(UserProject::getUserId).collect(Collectors.toSet());
        }
        return userIds == null ? new HashSet<>() : userIds;
    }

    public List<UserProject> getUserProjects() {
        return userProjects == null ? new ArrayList<>() : userProjects;
    }

    public String getJobSubJob() {
        if (StringUtils.hasText(subJob) && !StringUtils.hasText(jobNumber)) {
            return subJob;
        } else if (!StringUtils.hasText(subJob) && StringUtils.hasText(jobNumber)) {
            return jobNumber;
        } else if (StringUtils.hasText(subJob) && StringUtils.hasText(jobNumber)) {
            return String.format("%s-%s", jobNumber, subJob);
        }
        return description;
    }

    @Override
    public int compareTo(Project project) {
        return Comparator.comparing(Project::getJobNumber, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Project::getSubJob,Comparator.nullsFirst(Comparator.naturalOrder()))
                .compare(this, project);
    }
}
