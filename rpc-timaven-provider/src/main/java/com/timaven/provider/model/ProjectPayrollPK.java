package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Embeddable
@EqualsAndHashCode(of = {"projectId", "weekEndDate", "type"})
public class ProjectPayrollPK implements Serializable {
    @Basic
    @Column(name = "project_id", insertable = false, updatable = false)
    private Long projectId;

    @Basic
    @Column(name = "week_end_date", insertable = false, updatable = false)
    private LocalDate weekEndDate;

    @Basic
    @Column(name = "type", insertable = false, updatable = false)
    private int type;
}
