package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@EqualsAndHashCode(of = {"projectId", "shiftId"})
@NoArgsConstructor
@Entity
@Table(name = "project_shift", schema = "rule")
public class ProjectShift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "project_id", columnDefinition = "BIGINT")
    private Long projectId;

    @Column(name = "shift_id", columnDefinition = "BIGINT")
    private Long shiftId;

    @ManyToOne
    @JoinColumn(name = "shift_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Shift shift;
}
