package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.dto.PurchaseOrderBillingDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PurchaseOrderBilling.class)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "purchase_order_billing", schema = "project")
public class PurchaseOrderBilling {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;

    @Column(name = "billing_number")
    private String billingNumber;

    @Column(name = "invoice_number")
    private String invoiceNumber;

    @Column(name = "freight_shipping")
    private BigDecimal freightShipping;

    @Column(name = "other_charge")
    private String otherCharge;

    @Column(name = "other_charge_amount")
    private BigDecimal otherChargeAmount;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchaseOrderBilling", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<PurchaseOrderBillingDetail> purchaseOrderBillingDetails;

    public PurchaseOrderBilling(PurchaseOrderBillingDto dto) {
        this.purchaseOrderNumber = dto.getPurchaseOrderNumber();
        this.billingNumber = dto.getBillingNumber();
        this.invoiceNumber = dto.getInvoiceNumber();
        this.freightShipping = dto.getFreightShipping();
        this.otherCharge = dto.getOtherCharge();
        this.otherChargeAmount = dto.getOtherChargeAmount();
        if (!CollectionUtils.isEmpty(dto.getPurchaseOrderBillingDetailDtos())) {
            this.purchaseOrderBillingDetails = dto.getPurchaseOrderBillingDetailDtos().stream()
                    .map(PurchaseOrderBillingDetail::new).collect(Collectors.toSet());
            this.purchaseOrderBillingDetails.forEach(d -> {
                d.setPurchaseOrderBilling(this);
            });
        }
    }
}
