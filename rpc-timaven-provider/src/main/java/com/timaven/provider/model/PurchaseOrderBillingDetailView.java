package com.timaven.provider.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "purchase_order_billing_detail_view", schema = "project")
public class PurchaseOrderBillingDetailView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "billing_id")
    private Long billingId;

    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;

    @Column(name = "billing_number")
    private String billingNumber;

    @Column(name = "invoice_number")
    private String invoiceNumber;

    @Column(name = "part")
    private String part;

    @Column(name = "billing_quantity")
    private BigDecimal billingQuantity;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "tax")
    private BigDecimal tax;
}
