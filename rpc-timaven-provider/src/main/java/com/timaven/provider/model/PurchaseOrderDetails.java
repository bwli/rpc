package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@EqualsAndHashCode(of = {"purchaseOrderNumber", "lineNumber"})
@Table(name = "purchase_order_details", schema = "project")
public class PurchaseOrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Basic
    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;
    @Basic
    @Column(name = "purchase_order_date")
    private LocalDate purchaseOrderDate;
    @Basic
    @Column(name = "vendor_number")
    private Integer vendorNumber;
    @Basic
    @Column(name = "vendor_name")
    private String vendorName;
    @Basic
    @Column(name = "confirm_to")
    private String confirmTo;
    @Basic
    @Column(name = "requestor")
    private String requestor;
    @Basic
    @Column(name = "buyer")
    private String buyer;
    @Basic
    @Column(name = "requisition_number")
    private String requisitionNumber;
    @Basic
    @Column(name = "job_number")
    private String jobNumber;
    @Basic
    @Column(name = "sub_job")
    private String subJob;
    @Basic
    @Column(name = "address_1")
    private String address1;
    @Basic
    @Column(name = "delivery_point")
    private String deliveryPoint;
    @Basic
    @Column(name = "terms")
    private String terms;
    @Basic
    @Column(name = "reference")
    private String reference;
    @Basic
    @Column(name = "state_tax")
    private BigDecimal stateTax;
    @Basic
    @Column(name = "local_tax")
    private BigDecimal localTax;
    @Basic
    @Column(name = "purchase_order_location")
    private String purchaseOrderLocation;
    @Basic
    @Column(name = "po_type")
    private String poType;
    @Basic
    @Column(name = "line_number")
    private Integer lineNumber;
    @Basic
    @Column(name = "part_number")
    private String partNumber;
    @Basic
    @Column(name = "part_description")
    private String partDescription;
    @Basic
    @Column(name = "delivery_date")
    private LocalDate deliveryDate;
    @Basic
    @Column(name = "quantity")
    private BigDecimal quantity;
    @Basic
    @Column(name = "adjust_quantity_ordered")
    private BigDecimal adjustQuantityOrdered;
    @Basic
    @Column(name = "quantity_received")
    private BigDecimal quantityReceived;
    @Basic
    @Column(name = "unit_cost")
    private BigDecimal unitCost;
    @Basic
    @Column(name = "extended_cost")
    private BigDecimal extendedCost;
    @Basic
    @Column(name = "price_code")
    private String priceCode;
    @Basic
    @Column(name = "original_po_dollar_amount")
    private BigDecimal originalPoDollarAmount;
    @Basic
    @Column(name = "current_po_dollar_amount")
    private BigDecimal currentPoDollarAmount;
    @Basic
    @Column(name = "open_amount")
    private BigDecimal openAmount;
    @Basic
    @Column(name = "formatted_cost_distribution")
    private String formattedCostDistribution;
    @Basic
    @Column(name = "cost_type")
    private String costType;
    @Basic
    @Column(name = "gl_account_number_formatted")
    private String glAccountNumberFormatted;
    @Basic
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "submission_id", updatable = false, insertable = false)
    private Long submissionId;
    @Column(name = "billed_quantity")
    private BigDecimal billedQuantity;
}
