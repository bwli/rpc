package com.timaven.provider.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "purchase_order_group", schema = "project")
public class PurchaseOrderGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Basic
    @NotNull
    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;
    @Basic
    @Column(name = "comment")
    private String comment;
    @Basic
    @Column(name = "flag")
    private Boolean flag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrderGroup that = (PurchaseOrderGroup) o;
        if (purchaseOrderNumber != null && that.purchaseOrderNumber != null)
            return purchaseOrderNumber.equals(that.getPurchaseOrderNumber());
        return false;
    }

    @Override
    public int hashCode() {
        if (purchaseOrderNumber != null) return Objects.hash(purchaseOrderNumber);
        return super.hashCode();
    }
}
