package com.timaven.provider.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "purchase_order_submission", schema = "project")
public class PurchaseOrderSubmission {
    private Long id;
    private String submittedBy;
    private LocalDateTime createdAt;
    private Set<PurchaseOrder> purchaseOrders;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "submitted_by")
    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }

    @Basic
    @Column(name = "created_at")
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @OneToMany(mappedBy = "purchaseOrderSubmission", fetch = FetchType.LAZY)
    public Set<PurchaseOrder> getPurchaseOrders() {
        return purchaseOrders;
    }

    public void setPurchaseOrders(Set<PurchaseOrder> purchaseOrders) {
        this.purchaseOrders = purchaseOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrderSubmission that = (PurchaseOrderSubmission) o;
        if (id != null && that.id != null) return id.equals(that.id);
        return false;
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        return super.hashCode();
    }
}
