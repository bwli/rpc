package com.timaven.provider.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "purchase_vendor_roster", schema = "project")
public class PurchaseVendorRoster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "vendor_id")
    private Long vendorId;
    @Column(name = "vendor_name")
    private String vendorName;
    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;
}
