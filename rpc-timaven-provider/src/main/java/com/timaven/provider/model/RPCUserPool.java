package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Where;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;

@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Where(clause = "(is_active)")
@Table(name = "rpc_user_pool", schema = "stage")
public class RPCUserPool implements Comparable<RPCUserPool>, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "emp_id")
    private String empId;

    @Column(name = "client_emp_id")
    private String clientEmpId;

    @Column(name = "craft")
    private String craft;

    @Column(name = "per_diem_id")
    private Long perDiemId;

    @Column(name = "base_st")
    private BigDecimal baseST;

    @Column(name = "base_ot")
    private BigDecimal baseOT;

    @Column(name = "base_dt")
    private BigDecimal baseDT;

    @Column(name = "holiday_rate")
    private BigDecimal holidayRate;

    @Column(name = "sick_leave_rate")
    private BigDecimal sickLeaveRate;

    @Column(name = "travel_rate")
    private BigDecimal travelRate;

    @Column(name = "vacation_rate")
    private BigDecimal vacationRate;

    @Column(name = "client_craft")
    private String clientCraft;

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "crew")
    private String crew;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    // Either F or M
    @Column(name = "gender")
    private String gender;

    @Column(name = "company")
    private String company;

    @Column(name = "badge")
    private String badge;

    @Column(name = "sign_in_sheet")
    private String signInSheet;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "effected_on")
    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate effectedOn;

    @Column(name = "terminated_at")
    private LocalDate terminatedAt;

    @Column(name = "rehired_at")
    private LocalDate rehiredAt;

    @Column(name = "hired_at")
    private LocalDate hiredAt;

    @NotNull
    @Column(name = "is_active")
    private Boolean active = true;

    @Column(name = "has_per_diem")
    private Boolean hasPerDiem = false;

    @Column(name = "has_rig_pay")
    private Boolean hasRigPay = false;

    @Column(name = "shift")
    private String shift;

    @Column(name = "schedule_start")
    private LocalTime scheduleStart;

    @Column(name = "schedule_end")
    private LocalTime scheduleEnd;

    @Column(name = "lunch_start")
    private LocalTime lunchStart;

    @Column(name = "lunch_end")
    private LocalTime lunchEnd;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "is_offsite")
    private Boolean isOffsite = false;

    @Column(name = "activity_code")
    private String activityCode;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @Transient
    private Craft craftEntity;

    @Transient
    private String invalidReason;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (id != null) {
            hcb.append(id);
        } else {
            hcb.append(empId);
            hcb.append(hiredAt);
            hcb.append(terminatedAt);
            hcb.append(projectId);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RPCUserPool)) return false;
        RPCUserPool that = (RPCUserPool) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (this.id != null && that.id != null) {
            eb.append(id, that.id);
        } else {
            eb.append(empId, that.empId);
            eb.append(projectId, that.projectId);
            eb.append(hiredAt, that.hiredAt);
            eb.append(terminatedAt, that.terminatedAt);
        }
        return eb.isEquals();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(RPCUserPool o) {
        return Comparator.comparing(RPCUserPool::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(RPCUserPool::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
