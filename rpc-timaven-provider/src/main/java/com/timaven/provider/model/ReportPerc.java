package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ReportPerc.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "report_perc", schema = "time")
public class ReportPerc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cost_code_full")
    private String costCodeFull;

    @Column(name = "order_code")
    private String orderCode;

    @NotNull
    @Column(name = "percentage")
    private Integer percentage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private ReportSubmission reportSubmission;

    public ReportPerc(@NotNull String costCodeFull, @NotNull String orderCode,
                      @NotNull Integer percentage) {
        this.costCodeFull = costCodeFull;
        this.orderCode = orderCode;
        this.percentage = percentage;
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReportPerc)) return false;
        if (null != id) {
            ReportPerc that = (ReportPerc) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
