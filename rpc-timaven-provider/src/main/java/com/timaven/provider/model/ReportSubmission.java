package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ReportSubmission.class)
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "report_submission", schema = "time")
public class ReportSubmission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "team_name")
    private String teamName;
    @Column(name = "reported_at", insertable = false, updatable = false)
    private LocalDateTime reportedAt;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "reportSubmission", fetch = FetchType.LAZY, cascade =
            CascadeType.PERSIST)
    private Set<ReportPerc> reportPercs;
    @NotNull
    @Column(name = "project_id")
    private Long projectId;

    public ReportSubmission(Long userId, String teamName, Long projectId) {
        this.userId = userId;
        this.teamName = teamName;
        this.projectId = projectId;
    }
}
