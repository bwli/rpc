package com.timaven.provider.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "requisition_group", schema = "project")
public class RequisitionGroup {
    @Id
    @Column(name = "requisition_number")
    private String requisitionNumber;
    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;
    @Column(name = "requisition_date")
    private LocalDate requisitionDate;
    @Column(name = "vendor_name")
    private String vendorName;
    @Column(name = "requestor")
    private String requestor;
    @Column(name = "job_number")
    private String jobNumber;
    @Column(name = "sub_job")
    private String subJob;
    @Column(name = "item_count")
    private long itemCount;
}
