package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;

/**
 * Entity bean with JPA annotations Hibernate provides JPA implementation
 */
@Getter
@Setter
@EqualsAndHashCode(of = "name")
@NoArgsConstructor
@Entity
@Where(clause = "(name <> 'ROLE_super_admin')")
@Table(name = "role", schema = "auth")
public class Role implements Comparator<Role> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "display_name")
    private String displayName;

    @Column(name = "display_order")
    private int displayOrder;

    @Override
    public int compare(Role role0, Role role1) {
        return role0.name.compareTo(role1.name);
    }
}
