package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "craft", schema = "stage")
public class StageCraft {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "submission_id")
    private Long submissionId;

    @Column(name = "craft_code")
    private String craftCode;

    @Column(name = "description")
    private String description;

    @Column(name = "billable_st")
    private BigDecimal billableST;

    @Column(name = "billable_ot")
    private BigDecimal billableOT;

    @Column(name = "billable_dt")
    private BigDecimal billableDT;

    @Column(name = "per_diem")
    private BigDecimal perDiem;

    @Column(name = "rig_pay")
    private BigDecimal rigPay;

    @Column(name = "company")
    private String company;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "per_diem_rule")
    private String perDiemRule;

}
