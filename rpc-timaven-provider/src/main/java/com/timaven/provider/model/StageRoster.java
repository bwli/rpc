package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "roster", schema = "stage")
public class StageRoster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "submission_id")
    private Long submissionId;

    @Column(name = "badge")
    private String badge;

    @Column(name = "name")
    private String name;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "client_emp_id")
    private String clientEmpId;

    @Column(name = "team")
    private String team;

    @Column(name = "crew")
    private String crew;

    @Column(name = "company")
    private String company;

    @Column(name = "craft_code")
    private String craftCode;

    @Column(name = "base_st")
    private BigDecimal baseST;

    @Column(name = "base_ot")
    private BigDecimal baseOT;

    @Column(name = "base_dt")
    private BigDecimal baseDT;

    @Column(name = "holiday_rate")
    private BigDecimal holidayRate;

    @Column(name = "sick_leave_rate")
    private BigDecimal sickLeaveRate;

    @Column(name = "travel_rate")
    private BigDecimal travelRate;

    @Column(name = "vacation_rate")
    private BigDecimal vacationRate;

    @Column(name = "client_craft")
    private String clientCraft;

    @Column(name = "has_per_diem")
    private Boolean hasPerDiem = false;

    @Column(name = "has_rig_pay")
    private Boolean hasRigPay = false;

    @Column(name = "hired_at")
    private LocalDate hiredAt;

    @Column(name = "terminated_at")
    private LocalDate terminatedAt;

    @Column(name = "shift")
    private String shift;

    @Column(name = "schedule_start")
    private LocalTime scheduleStart;

    @Column(name = "schedule_end")
    private LocalTime scheduleEnd;

    @Column(name = "lunch_start")
    private LocalTime lunchStart;

    @Column(name = "lunch_end")
    private LocalTime lunchEnd;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "sign_in_sheet")
    private String signInSheet;
}
