package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "submission", schema = "stage")
public class StageSubmission {

    public enum Type {
        Roster,
        Team,
        CostCode,
        TeamCC,
        Crafts,
        Unknown,
    }

    public enum Status {
        Pending,
        Imported,
        Ignored,
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "project_id")
    private Long projectId;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "effective_on")
    private LocalDate effectiveOn;

    @Column(name = "type")
    private Type type;

    @Column(name = "status")
    private Status status;

    @Column(name = "processed_at")
    private LocalDateTime processedAt;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;
}
