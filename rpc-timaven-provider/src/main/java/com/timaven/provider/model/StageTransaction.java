package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "stage_trans", schema = "time")
public class StageTransaction {

    public enum Status {
        Pending,
        Processed,
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "contract_id")
    private Long contractId;

    @Column(name = "date_of_service")
    private LocalDate dateOfService;

    @Column(name = "st_rate")
    private BigDecimal stRate;

    @Column(name = "ot_rate")
    private BigDecimal otRate;

    @Column(name = "dt_rate")
    private BigDecimal dtRate;

    @Column(name = "st_hour")
    private BigDecimal stHour;

    @Column(name = "ot_hour")
    private BigDecimal otHour;

    @Column(name = "dt_hour")
    private BigDecimal dtHour;

    @Column(name = "total_hour")
    private BigDecimal totalHour;

    @Column(name = "net_hour")
    private BigDecimal netHour;

    @Column(name = "area_id")
    private Long areaId;

    @Column(name = "work_unit_id")
    private String workUnitId;

    @Column(name = "emp_id")
    private String empId;

    @Column(name = "client_emp_id")
    private String clientEmpId;

    @Column(name = "company")
    private String company;

    @Column(name = "badge")
    private String badge;

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "pd")
    private BigDecimal perDiem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    @JsonBackReference
    private TransSubmission transSubmission;

    @Column(name = "submission_id", updatable = false, insertable = false)
    private Long transSubmissionId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "stageTransaction", cascade = CascadeType.PERSIST)
    private Set<StageException> stageExceptions;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "stageTransaction", cascade = CascadeType.PERSIST)
    private Set<StageRecord> stageRecords;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "status")
    private Status status;

    @Column(name = "processed_at")
    private LocalDateTime processedAt;

    @Column(name = "by_upload")
    private boolean byUpload;

    public BigDecimal getStHour() {
        return stHour == null ? null : stHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getOtHour() {
        return otHour == null ? null : otHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? null : dtHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? null : totalHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }

    public BigDecimal getNetHour() {
        return netHour == null ? null : netHour.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
    }
}
