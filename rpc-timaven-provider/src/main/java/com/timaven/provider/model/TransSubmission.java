package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "trans_submission", schema = "time")
public class TransSubmission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "transSubmission", fetch = FetchType.LAZY, cascade =
            CascadeType.PERSIST, orphanRemoval = true)
    private Set<StageTransaction> stageTransactions;

    @Transient
    private Map<String, Set<LocalDate>> teamDateSet;

    @Transient
    private Set<LocalDate> localDateSet;

    public TransSubmission(Long userId, Long projectId) {
        this.userId = userId;
        this.projectId = projectId;
    }
}
