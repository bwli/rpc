package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity bean with JPA annotations Hibernate provides JPA implementation
 */
@Getter
@Setter
@EqualsAndHashCode(of = "username")
@NoArgsConstructor
@Entity
@Table(name = "user", schema = "auth")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @Column(name = "display_name")
    private String displayName;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "logged_at")
    private LocalDateTime loggedInAt;

    @NotNull
    @Column(name = "is_active")
    private Boolean active = true;

    @Transient
    private String passwordConfirm;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @JsonManagedReference("userRoles")
    private Set<UserRole> userRoles;

    @ElementCollection
    @CollectionTable(name = "user_role", schema = "auth", joinColumns = @JoinColumn(name = "user_id",
            columnDefinition = "BIGINT"))
    @Column(name = "role_id")
    private Set<Long> roleIds;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @JsonManagedReference("userProjects")
    private Set<UserProject> userProjects;

    @Transient
    private Set<String> teams;

    public Set<String> getTeams() {
        if (teams == null) {
            teams = new HashSet<>();
        }
        return teams;
    }

    public User(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getRoleNamesStr() {
        if (!CollectionUtils.isEmpty(userRoles)) {
            List<String> roleNames =
                    userRoles.stream().sorted(Comparator.comparingInt(o -> o.getRole().getDisplayOrder()))
                            .map(e -> e.getRole().getName()).map(e -> e.substring(5)).collect(Collectors.toList());
            return StringUtils.join(roleNames, ",");
        }
        return "";
    }
}
