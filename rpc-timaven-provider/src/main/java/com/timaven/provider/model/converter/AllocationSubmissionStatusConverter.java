package com.timaven.provider.model.converter;

import com.timaven.provider.model.enums.AllocationSubmissionStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

// Used by AllocationSubmission Status mapping
@Converter(autoApply = true)
public class AllocationSubmissionStatusConverter implements AttributeConverter<AllocationSubmissionStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(AllocationSubmissionStatus status) {
        return status == null ? null : status.ordinal();
    }

    @Override
    public AllocationSubmissionStatus convertToEntityAttribute(Integer dbData) {
        return null != dbData ? AllocationSubmissionStatus.values()[dbData] : AllocationSubmissionStatus.PENDING;
    }
}
