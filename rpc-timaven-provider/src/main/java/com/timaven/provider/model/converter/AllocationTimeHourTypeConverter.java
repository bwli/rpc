package com.timaven.provider.model.converter;

import com.timaven.provider.model.enums.AllocationTimeHourType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

// Used by AllocationSubmission Status mapping
@Converter(autoApply = true)
public class AllocationTimeHourTypeConverter implements AttributeConverter<AllocationTimeHourType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(AllocationTimeHourType type) {
        return type == null ? AllocationTimeHourType.OT.ordinal() : type.ordinal();
    }

    @Override
    public AllocationTimeHourType convertToEntityAttribute(Integer dbData) {
        return null != dbData ? AllocationTimeHourType.values()[dbData] : AllocationTimeHourType.OT;
    }
}
