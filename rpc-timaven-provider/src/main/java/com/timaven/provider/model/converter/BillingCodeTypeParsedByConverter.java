package com.timaven.provider.model.converter;

import com.timaven.provider.model.enums.BillingCodeTypeParsedBy;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class BillingCodeTypeParsedByConverter implements AttributeConverter<BillingCodeTypeParsedBy, Integer> {

    @Override
    public Integer convertToDatabaseColumn(BillingCodeTypeParsedBy type) {
        return type == null ? BillingCodeTypeParsedBy.CODE_NAME.ordinal() : type.ordinal();
    }

    @Override
    public BillingCodeTypeParsedBy convertToEntityAttribute(Integer dbData) {
        return null != dbData ? BillingCodeTypeParsedBy.values()[dbData] : BillingCodeTypeParsedBy.CODE_NAME;
    }
}
