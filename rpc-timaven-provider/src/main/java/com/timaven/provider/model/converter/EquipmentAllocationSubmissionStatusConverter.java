package com.timaven.provider.model.converter;


import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

// Used by AllocationSubmission Status mapping
@Converter(autoApply = true)
public class EquipmentAllocationSubmissionStatusConverter implements AttributeConverter<EquipmentAllocationSubmissionStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EquipmentAllocationSubmissionStatus status) {
        return status == null ? null : status.ordinal();
    }

    @Override
    public EquipmentAllocationSubmissionStatus convertToEntityAttribute(Integer dbData) {
        return null != dbData ? EquipmentAllocationSubmissionStatus.values()[dbData] :
                EquipmentAllocationSubmissionStatus.PENDING;
    }
}
