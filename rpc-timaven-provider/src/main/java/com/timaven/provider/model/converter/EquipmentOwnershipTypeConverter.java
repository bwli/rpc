package com.timaven.provider.model.converter;


import com.timaven.provider.model.enums.EquipmentOwnershipType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class EquipmentOwnershipTypeConverter implements AttributeConverter<EquipmentOwnershipType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EquipmentOwnershipType type) {
        return type == null ? 0 : type.ordinal();
    }

    @Override
    public EquipmentOwnershipType convertToEntityAttribute(Integer dbData) {
        return null != dbData ? EquipmentOwnershipType.values()[dbData] : EquipmentOwnershipType.COMPANY_OWNED;
    }
}
