package com.timaven.provider.model.converter;

import com.timaven.provider.model.enums.EquipmentWeeklyProcessType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

// Used by AllocationSubmission Status mapping
@Converter(autoApply = true)
public class EquipmentWeeklyProcessTypeConverter implements AttributeConverter<EquipmentWeeklyProcessType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EquipmentWeeklyProcessType type) {
        return type == null ? EquipmentWeeklyProcessType.NORMAL.getValue() : type.getValue();
    }

    @Override
    public EquipmentWeeklyProcessType convertToEntityAttribute(Integer dbData) {
        return null != dbData ? EquipmentWeeklyProcessType.values()[dbData] : EquipmentWeeklyProcessType.NORMAL;
    }
}
