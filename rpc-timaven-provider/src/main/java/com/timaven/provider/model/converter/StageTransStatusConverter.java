package com.timaven.provider.model.converter;

import com.timaven.provider.model.StageTransaction.Status;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StageTransStatusConverter implements AttributeConverter<Status, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Status status) {
        return status == null ? Status.Pending.ordinal() : status.ordinal();
    }

    @Override
    public Status convertToEntityAttribute(Integer dbData) {
        return null != dbData ? Status.values()[dbData] : Status.Pending;
    }
}
