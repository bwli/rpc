package com.timaven.provider.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.DayOfWeek;


@Converter(autoApply = true)
public class WeekEndDateConverter implements AttributeConverter<DayOfWeek, Integer> {

    @Override
    public Integer convertToDatabaseColumn(DayOfWeek dayOfWeek) {
        return dayOfWeek == null ? DayOfWeek.SUNDAY.ordinal() : dayOfWeek.ordinal();
    }

    @Override
    public DayOfWeek convertToEntityAttribute(Integer dbData) {
        return null != dbData ? DayOfWeek.values()[dbData] : DayOfWeek.SUNDAY;
    }
}
