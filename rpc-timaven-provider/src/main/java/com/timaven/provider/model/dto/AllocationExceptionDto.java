package com.timaven.provider.model.dto;

import com.timaven.provider.model.AllocationException;
import lombok.Data;

@Data
public class AllocationExceptionDto {
    private Long id;
    private String exception;

    public AllocationExceptionDto(){}

    public AllocationExceptionDto(AllocationException exception) {
        if (null == exception) return;
        this.id = exception.getId();
        this.exception = exception.getException();
    }
}
