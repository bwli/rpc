package com.timaven.provider.model.dto;

import com.timaven.provider.model.AllocationRecord;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Comparator;

@Data
public class AllocationRecordDto implements Comparable<AllocationRecordDto> {
    private Long id;
    private String doorName;
    private Boolean side;
    private boolean accessGranted = false;
    private boolean valid = false;
    private LocalDateTime adjustedTime;
    private LocalDateTime netEventTime;

    public AllocationRecordDto(){}

    public AllocationRecordDto(AllocationRecord allocationRecord) {
        this.id = allocationRecord.getId();
        this.doorName = allocationRecord.getDoorName();
        this.side = allocationRecord.getSide();
        this.accessGranted = allocationRecord.isAccessGranted();
        this.valid = allocationRecord.isValid();
        this.adjustedTime = allocationRecord.getAdjustedTime();
        this.netEventTime = allocationRecord.getNetEventTime();
    }

    @Override
    public int compareTo(AllocationRecordDto allocationRecord) {
        return Comparator.comparing(AllocationRecordDto::getAdjustedTime, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, allocationRecord);
    }
}
