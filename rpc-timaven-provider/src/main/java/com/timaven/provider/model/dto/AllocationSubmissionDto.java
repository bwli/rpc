package com.timaven.provider.model.dto;

import com.timaven.provider.model.AllocationSubmission;
import com.timaven.provider.model.User;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class AllocationSubmissionDto {
    private Long id;
    private Long submitUserId;
    private Long approveUserId;
    private Long finalizeUserId;
    private LocalDateTime approvedAt;
    private LocalDateTime finalizedAt;
    private Long reportSubmissionId;
    private String teamName;
    private AllocationSubmissionStatus status;
    private AllocationSubmissionStatus previousStatus;
    private LocalDate dateOfService;
    private LocalDate payrollDate;
    private Long projectId;
    private List<AllocationTimeDto> allocationTimes;
    private Boolean billingPurposeOnly = false;

    private User submitUser;
    private User approveUser;

    private Boolean exported = false;

    public AllocationSubmissionDto(){}

    public AllocationSubmissionDto(AllocationSubmission submission) {
        this.id = submission.getId();
        this.submitUserId = submission.getSubmitUserId();
        this.projectId = submission.getProjectId();
        this.reportSubmissionId = submission.getReportSubmissionId();
        this.teamName = submission.getTeamName();
        this.status = submission.getStatus();
        this.previousStatus = submission.getStatus();
        this.dateOfService = submission.getDateOfService();
        this.approveUserId = submission.getApproveUserId();

        this.allocationTimes = submission.getAllocationTimes().stream()
                .map(AllocationTimeDto::new).sorted().collect(Collectors.toList());
        this.billingPurposeOnly = submission.getBillingPurposeOnly();
        this.exported = submission.getExported();
        this.payrollDate = submission.getPayrollDate();
    }
}
