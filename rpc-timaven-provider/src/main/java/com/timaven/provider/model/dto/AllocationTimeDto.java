package com.timaven.provider.model.dto;

import com.timaven.provider.model.AllocationTime;
import com.timaven.provider.model.PerDiem;
import com.timaven.provider.model.enums.AllocationTimeHourType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class AllocationTimeDto implements Comparable<AllocationTimeDto> {
    private Long id;
    private String empId;
    private String clientEmpId;
    private String badge;
    private String teamName;
    private BigDecimal stHour;
    private BigDecimal otHour;
    private BigDecimal dtHour;
    private AllocationTimeHourType extraTimeType;
    private BigDecimal totalHour;
    private BigDecimal allocatedHour;
    private BigDecimal netHour;
    private boolean hasPerDiem;
    private boolean hasMob;
    private BigDecimal rigPay;
    private Set<CostCodePercDto> costCodePercs;
    private List<PunchExceptionDto> punchExceptions;
    private List<AllocationRecordDto> allocationRecords;
    private AllocationExceptionDto allocationException;
    private String firstName;
    private String lastName;
    private String jobNumber;
    private String maxTimeCostCode;
    private String perDiemCostCode;
    private String mobCostCode;
    private BigDecimal mobAmount;
    private BigDecimal mobMileageRate = BigDecimal.ZERO;
    private BigDecimal mobMileage = BigDecimal.ZERO;
    private Integer mobDisplayOrder;
    private BigDecimal perDiemAmount;
    private Long weeklyProcessId;

    private Boolean employeeHasPerDiem = false;
    private Boolean employeeHasRigPay = false;
    private BigDecimal craftRigPay = BigDecimal.ZERO;
    private String employeeInvalidReason;
    private String employeeCrew;
    private String employeeCraft;
    private PerDiem perDiem;

    private List<CostCodeTagAssociationDto> perDiemCostCodeTagAssociations;
    private List<CostCodeTagAssociationDto> mobCostCodeTagAssociations;

    public AllocationTimeDto(AllocationTime allocationTime) {
        this.id = allocationTime.getId();
        this.perDiem = allocationTime.getPerDiem();
        if (allocationTime.getEmployee() != null) {
            this.employeeHasPerDiem = allocationTime.getEmployee().getHasPerDiem();
            this.employeeHasRigPay = allocationTime.getEmployee().getHasRigPay();
            this.employeeInvalidReason = allocationTime.getEmployee().getInvalidReason();
            this.employeeCrew = allocationTime.getEmployee().getCrew();
            this.employeeCraft = allocationTime.getEmployee().getCraft();
            if (allocationTime.getEmployee().getCraftEntity() != null) {
                this.craftRigPay = allocationTime.getEmployee().getCraftEntity().getRigPay();
            }
        }

        this.firstName = allocationTime.getFirstName();
        this.lastName = allocationTime.getLastName();
        this.empId = allocationTime.getEmpId();
        this.badge = allocationTime.getBadge();
        this.clientEmpId = allocationTime.getClientEmpId();
        this.teamName = allocationTime.getTeamName();
        this.stHour = allocationTime.getStHour();
        this.otHour = allocationTime.getOtHour();
        this.dtHour = allocationTime.getDtHour();
        this.totalHour = allocationTime.getTotalHour();
        this.netHour = allocationTime.getNetHour();
        this.extraTimeType = allocationTime.getExtraTimeType();
        this.allocatedHour = allocationTime.getAllocatedHour();

        if (!CollectionUtils.isEmpty(allocationTime.getPunchExceptions())) {
            this.punchExceptions = allocationTime.getPunchExceptions().stream()
                    .map(PunchExceptionDto::new).sorted().collect(Collectors.toList());
        }

        if (!CollectionUtils.isEmpty(allocationTime.getAllocationRecords())) {
            this.allocationRecords = allocationTime.getAllocationRecords().stream()
                    .map(AllocationRecordDto::new).sorted().collect(Collectors.toList());
        }

        if (!CollectionUtils.isEmpty(allocationTime.getCostCodePercs())) {
            this.costCodePercs = allocationTime.getCostCodePercs().stream()
                    .map(CostCodePercDto::new).collect(Collectors.toSet());
        }

        if (allocationTime.getAllocationExceptions() != null) {
            this.allocationException = new AllocationExceptionDto(allocationTime.getAllocationExceptions().stream().findFirst().orElse(null));
        }

        this.hasPerDiem = allocationTime.isHasPerDiem();
        this.rigPay = allocationTime.getRigPay();
        this.mobCostCode = allocationTime.getMobCostCode();
        this.hasMob = allocationTime.isHasMob();
        this.mobAmount = allocationTime.getMobAmount();
        this.mobMileage = allocationTime.getMobMileage();
        this.mobMileageRate = allocationTime.getMobMileageRate();
        this.mobDisplayOrder = allocationTime.getMobDisplayOrder();
        this.perDiemAmount = allocationTime.getPerDiemAmount();
        this.weeklyProcessId = allocationTime.getWeeklyProcessId();
        this.perDiemCostCode = allocationTime.getPerDiemCostCode();

        if (!CollectionUtils.isEmpty(allocationTime.getMobCostCodeTagAssociations())) {
            this.mobCostCodeTagAssociations = allocationTime.getMobCostCodeTagAssociations().stream()
                    .map(CostCodeTagAssociationDto::new).sorted().collect(Collectors.toList());
        }
        if (!CollectionUtils.isEmpty(allocationTime.getPerDiemCostCodeTagAssociations())) {
            this.perDiemCostCodeTagAssociations = allocationTime.getPerDiemCostCodeTagAssociations().stream()
                    .map(CostCodeTagAssociationDto::new).sorted().collect(Collectors.toList());
        }
    }

    @Override
    public int compareTo(AllocationTimeDto o) {
        return Comparator.comparing(AllocationTimeDto::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AllocationTimeDto::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}
