package com.timaven.provider.model.dto;

import com.timaven.provider.model.BillingCode;
import com.timaven.provider.model.BillingCodeOverride;
import com.timaven.provider.model.BillingCodeView;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BillingCodeDto {
    private Long id;
    private String codeName;
    private String codeType;
    private String clientAlias;
    private String description;
    private Long typeId;

    public BillingCodeDto(BillingCode billingCode) {
        this.clientAlias = billingCode.getClientAlias();
        this.description = billingCode.getDescription();
        this.id = billingCode.getId();
        this.codeName = billingCode.getCodeName();
        this.codeType = billingCode.getBillingCodeType().getCodeType();
    }

    public BillingCodeDto(BillingCodeView billingCodeView) {
        this.id = billingCodeView.getBillingCodeId();
        this.codeName = billingCodeView.getCodeName();
        this.codeType = billingCodeView.getCodeType();
        this.clientAlias = billingCodeView.getClientAlias();
        this.description = billingCodeView.getDescription();
        this.typeId = billingCodeView.getTypeId();
    }

    public BillingCodeDto(BillingCode billingCode, BillingCodeOverride billingCodeOverride) {
        this.clientAlias = billingCodeOverride.getClientAlias();
        this.description = billingCodeOverride.getDescription();
        this.id = billingCode.getId();
        this.codeName = billingCode.getCodeName();
        this.codeType = billingCode.getBillingCodeType().getCodeType();
    }
}
