package com.timaven.provider.model.dto;

import com.timaven.provider.model.CostCodePerc;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class CostCodePercDto {
    private Long id;
    private String costCodeFull;
    private String purchaseOrder;
    private BigDecimal stHour;
    private BigDecimal otHour;
    private BigDecimal dtHour;
    private BigDecimal totalHour;
    private Integer percentage;
    private Integer displayOrder;
    private List<CostCodeTagAssociationDto> costCodeTagAssociationDtos = new ArrayList<>();

    public CostCodePercDto(CostCodePerc costCodePerc) {
        this.id = costCodePerc.getId();
        this.costCodeFull = costCodePerc.getCostCodeFull();
        this.purchaseOrder = costCodePerc.getPurchaseOrder();
        this.stHour = costCodePerc.getStHour();
        this.otHour = costCodePerc.getOtHour();
        this.dtHour = costCodePerc.getDtHour();
        this.totalHour = costCodePerc.getTotalHour();
        this.percentage = costCodePerc.getPercentage();
        this.displayOrder = costCodePerc.getDisplayOrder();
        if (!CollectionUtils.isEmpty(costCodePerc.getCostCodeTagAssociations())) {
            this.costCodeTagAssociationDtos = costCodePerc.getCostCodeTagAssociations().stream()
                    .map(CostCodeTagAssociationDto::new).sorted().collect(Collectors.toList());
        }
    }
}
