package com.timaven.provider.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "empId")
public class EmpCostCodeProjectDto {
    private String empId;
    private String firstName;
    private String lastName;
    private Set<String> costCodes;
    private BigDecimal ot;

    public EmpCostCodeProjectDto(String empId, String firstName, String lastName) {
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private Map<String, BigDecimal> empTimeMap;
    private Map<String, BigDecimal> empTotalTimeMap;
//    private Map<String, BigDecimal> empAdjustedTimeMap;

    public Map<String, BigDecimal> getEmpTimeMap() {
        if (empTimeMap == null) {
            empTimeMap = new HashMap<>();
        }
        return empTimeMap;
    }

    public Map<String, BigDecimal> getEmpTotalTimeMap() {
        if (empTotalTimeMap == null) {
            empTotalTimeMap = new HashMap<>();
        }
        return empTotalTimeMap;
    }

    //    public Map<String, BigDecimal> getEmpAdjustedTimeMap() {
//        if (empAdjustedTimeMap == null) {
//            empAdjustedTimeMap = new HashMap<>();
//        }
//        return empAdjustedTimeMap;
//    }

    public Set<String> getCostCodes() {
        if (null == costCodes) {
            costCodes = new TreeSet<>();
        }
        return costCodes;
    }
}
