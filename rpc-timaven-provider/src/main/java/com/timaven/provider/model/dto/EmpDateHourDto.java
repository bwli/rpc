package com.timaven.provider.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class EmpDateHourDto {
    private int type;
    private BigDecimal hour;

    // Used by data binding
    public EmpDateHourDto(){}

    public EmpDateHourDto(int type, BigDecimal stHour) {
        this.type = type;
        this.hour = stHour;
    }
}
