package com.timaven.provider.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class EmpDateKey {
    @NotNull
    private final String empId;
    @NotNull
    private final LocalDate dateOfService;
}
