package com.timaven.provider.model.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class EmpWorkdays {
    private String empId;
    private LocalDate startDate;
    private LocalDate endDate;
    private long consecutiveDays;
}
