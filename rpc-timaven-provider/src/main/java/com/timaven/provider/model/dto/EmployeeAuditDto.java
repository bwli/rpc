package com.timaven.provider.model.dto;

import com.timaven.provider.audit.CustomRevisionEntity;
import com.timaven.provider.model.Employee;
import lombok.Data;
import org.hibernate.envers.RevisionType;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Data
public class EmployeeAuditDto {
    private String empId;
    private String craft;
    private String team;
    private String crew;
    private String name;
    private String company;
    private String jobNumber;
    private String activityCode;
    private Boolean isOffsite;
    private String badge;
    private String signInSheet;
    private LocalDate effectedOn;
    private LocalDate hiredAt;
    private LocalDate terminatedAt;
    private Boolean hasPerDiem = false;
    private Boolean hasRigPay = false;
    private int rev;
    private String revType;
    private String by;
    private ZonedDateTime revisionTime;

    public EmployeeAuditDto(Employee employee, CustomRevisionEntity customRevisionEntity, RevisionType revisionType) {
        this.empId = employee.getEmpId();
        this.craft = employee.getCraft();
        this.team = employee.getTeamName();
        this.crew = employee.getCrew();
        this.name = String.format("%s, %s", employee.getLastName(), employee.getFirstName());
        this.company = employee.getCompany();
        this.badge = employee.getBadge();
        this.jobNumber = employee.getJobNumber();
        this.activityCode = employee.getActivityCode();
        this.isOffsite = employee.getIsOffsite();
        this.signInSheet = employee.getSignInSheet();
        this.effectedOn = employee.getEffectedOn();
        this.hiredAt = employee.getHiredAt();
        this.terminatedAt = employee.getTerminatedAt();
        this.hasPerDiem = employee.getHasPerDiem();
        this.hasRigPay = employee.getHasRigPay();
        this.rev = customRevisionEntity.getId();
        this.revisionTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(customRevisionEntity.getTimestamp()),
                ZoneId.systemDefault());
        this.revType = revisionType.name();
        this.by = customRevisionEntity.getUsername();
    }
}
