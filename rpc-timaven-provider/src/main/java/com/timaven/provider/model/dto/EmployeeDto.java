package com.timaven.provider.model.dto;

import com.timaven.provider.model.EmployeeView;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Data
public class EmployeeDto {
    private Long id;
    private String empId;
    private String craft;
    private String perDiemName;
    private Long perDiemId;
    private String team;
    private String crew;
    private String name;
    private String company;
    private String badge;
    private String signInSheet;
    private LocalDate hiredAt;
    private LocalDate effectedOn;
    private LocalDate terminatedAt;
    private Boolean hasPerDiem = false;
    private Boolean hasRigPay = false;
    private String jobNumber;
    private List<String> tags = new ArrayList<>();
    // TM-307
    private Boolean isOffsite = false;
    private String activityCode;

    public EmployeeDto(EmployeeView employee) {
        this.empId = employee.getEmpId();
        this.craft = employee.getCraft();
        this.perDiemName = employee.getPerDiemName();
        this.perDiemId = employee.getPerDiemId();
        this.team = employee.getTeamName();
        this.crew = employee.getCrew();
        this.id = employee.getId();
        this.name = String.format("%s, %s", employee.getLastName(), employee.getFirstName());
        this.company = employee.getCompany();
        this.badge = employee.getBadge();
        this.signInSheet = employee.getSignInSheet();
        this.hiredAt = employee.getHiredAt();
        this.effectedOn = employee.getEffectedOn();
        this.terminatedAt = employee.getTerminatedAt();
        this.hasPerDiem = employee.getHasPerDiem();
        this.hasRigPay = employee.getHasRigPay();
        this.jobNumber = employee.getJobNumber();
        if (StringUtils.hasText(employee.getTags())) {
            this.tags = Arrays.stream(employee.getTags().split(",")).sorted().collect(Collectors.toList());
        }
        // TM-307
        this.isOffsite = employee.getIsOffsite();
        this.activityCode = employee.getActivityCode();
    }
}
