package com.timaven.provider.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author li
 */
@Data
public class EmployeeProfitDetailsDTO {
    private Long projectId;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate startDate;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private LocalDate endDate;
    private Set<String> employeeIds;
}
