package com.timaven.provider.model.dto;

import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EquipmentAllocationTimeFilter {
    Set<LocalDate> dates;
    LocalDate startDate;
    LocalDate endDate;
    LocalDate payrollStartDate;
    LocalDate payrollEndDate;
    Long projectId;
    EquipmentAllocationSubmissionStatus status;
    EquipmentAllocationSubmissionStatus notStatus;
    List<String> includes;
}
