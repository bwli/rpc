package com.timaven.provider.model.dto;

import com.timaven.provider.model.Equipment;
import com.timaven.provider.model.EquipmentUsage;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EquipmentDto {
    private Equipment equipment;
    private boolean available = true;
    private Long availableDays;
    private List<EquipmentUsage> equipmentUsages = new ArrayList<>();

    public EquipmentDto(Equipment equipment) {
        this.equipment = equipment;
    }
}
