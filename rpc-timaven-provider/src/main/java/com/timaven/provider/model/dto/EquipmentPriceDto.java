package com.timaven.provider.model.dto;

import com.timaven.provider.model.EquipmentPrice;
import com.timaven.provider.util.NumberUtil;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
public class EquipmentPriceDto {
    private Map<String, Object> equipmentClass;
    private String description;
    private String hourlyRate;
    private String dailyRate;
    private String weeklyRate;
    private String monthlyRate;
    private Long id;

    public EquipmentPriceDto(EquipmentPrice price) {
        this.equipmentClass = new HashMap<>();
        this.equipmentClass.put("id", price.getId());
        this.equipmentClass.put("class", price.getEquipmentClass());
        this.description = price.getDescription();
        this.hourlyRate = NumberUtil.currencyFormat(price.getHourlyRate());
        this.dailyRate = NumberUtil.currencyFormat(price.getDailyRate());
        this.weeklyRate = NumberUtil.currencyFormat(price.getWeeklyRate());
        this.monthlyRate = NumberUtil.currencyFormat(price.getMonthlyRate());
        this.id = price.getId();
    }
}
