package com.timaven.provider.model.dto;

import com.timaven.provider.model.CostCodePerc;
import com.timaven.provider.model.EquipmentCostCodePerc;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class EquipmentProcessCostCodePercDto {
    private Long id;
    private String costCodeFull;
    private BigDecimal totalHour;
    private BigDecimal totalCharge;
}
