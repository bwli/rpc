package com.timaven.provider.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Principal implements Serializable {
    private Long id;
    private String username;
    private String displayName;
    private String ipAddress;
    private String tenantId;
    private LocalDateTime loggedInAt;
}
