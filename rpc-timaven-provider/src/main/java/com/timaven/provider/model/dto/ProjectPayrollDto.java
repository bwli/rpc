package com.timaven.provider.model.dto;

import com.timaven.provider.model.ProjectPayroll;
import com.timaven.provider.model.enums.WeeklyProcessType;
import com.timaven.provider.util.NumberUtil;
import lombok.Data;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Data
public class ProjectPayrollDto {
    private Long projectId;
    private String weekEndDate;
    private String employeeCount;
    private String totalHours;
    private Long timesheets;
    private String approvedAt;
    private String finalizedAt;
    private String payrollType;
    private Map<String, Object>  weekEndDateStatus;

    public ProjectPayrollDto(ProjectPayroll payroll) {

        this.projectId = payroll.getProjectId();
        this.weekEndDate = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(payroll.getWeekEndDate());
        this.employeeCount = NumberUtil.format(payroll.getEmployeeCount());
        this.totalHours = NumberUtil.format(payroll.getTotalHours());
        this.timesheets = payroll.getTimesheets();
        if (payroll.getApprovedAt() != null) {
            this.approvedAt = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(payroll.getApprovedAt());
        }
        if (payroll.getFinalizedAt() != null) {
            this.finalizedAt = DateTimeFormatter.ofPattern("MM/dd/yyyy").format(payroll.getFinalizedAt());
        }
        int type = payroll.getType();
        this.payrollType = WeeklyProcessType.fromValue(type).toString();
        this.weekEndDateStatus = new HashMap<>();
        this.weekEndDateStatus.put("submitted", approvedAt != null || finalizedAt != null);
        this.weekEndDateStatus.put("weekEndDate", payroll.getId().getWeekEndDate());
        this.weekEndDateStatus.put("type", type);
    }
}
