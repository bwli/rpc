package com.timaven.provider.model.dto;

import lombok.Data;

@Data
public class ProjectsDashboardProfitDto {
    private Long projectId;
    private String jobSubJob;
    private String description;
    private int empCount;
    private String committedValue;
    private String committedCost;
}
