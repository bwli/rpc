package com.timaven.provider.model.dto;

import com.timaven.provider.model.PunchException;
import lombok.Data;

import java.util.Comparator;

@Data
public class PunchExceptionDto implements Comparable<PunchExceptionDto> {
    private Long id;
    private String exception;
    private int order;
    private int exceptionType;

    public PunchExceptionDto(){}

    public PunchExceptionDto(PunchException punchException) {
        if (punchException == null) return;
        this.id = punchException.getId();
        this.exception = punchException.getException();
        this.order = punchException.getOrder();
        this.exceptionType = punchException.getExceptionType();
    }

    @Override
    public int compareTo(PunchExceptionDto e) {
        return Comparator.comparing(PunchExceptionDto::getOrder).compare(this, e);
    }
}
