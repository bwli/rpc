package com.timaven.provider.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PurchaseOrderBillingDto {
    private String purchaseOrderNumber;
    private String billingNumber;
    private String invoiceNumber;
    private BigDecimal freightShipping;
    private String otherCharge;
    private BigDecimal otherChargeAmount;
    private List<PurchaseOrderBillingDetailDto> purchaseOrderBillingDetailDtos;
}
