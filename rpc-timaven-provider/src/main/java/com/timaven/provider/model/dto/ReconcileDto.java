package com.timaven.provider.model.dto;

import com.timaven.provider.model.Project;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class ReconcileDto {
    private List<EmpCostCodeProjectDto> empCostCodeProjectDtos;
    private List<Project> projects;
    private Set<Long> projectIds;
    private Map<Long, String> projectUserNames;
    private String allocationSubmissionIds;

    public ReconcileDto(List<EmpCostCodeProjectDto> empCostCodeProjectDtos, List<Project> projects,
                        Map<Long, String> projectUserNames, Set<Long> allocationSubmissionIdSet) {
        if (CollectionUtils.isEmpty(empCostCodeProjectDtos)) {
            empCostCodeProjectDtos = new ArrayList<>();
        } else {
            empCostCodeProjectDtos.sort(Comparator.comparing(EmpCostCodeProjectDto::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(EmpCostCodeProjectDto::getLastName, Comparator.nullsLast(Comparator.naturalOrder())));
        }
        this.empCostCodeProjectDtos = empCostCodeProjectDtos;
        if (CollectionUtils.isEmpty(projects)) {
            projects = new ArrayList<>();
        }
        this.projects = projects.stream().sorted().collect(Collectors.toList());
        this.projectIds = projects.stream().map(Project::getId).collect(Collectors.toSet());
        this.projectUserNames = projectUserNames;
        if (null == allocationSubmissionIdSet) {
            allocationSubmissionIdSet = new HashSet<>();
        }
        this.allocationSubmissionIds = allocationSubmissionIdSet.stream()
                .map(String::valueOf).collect(Collectors.joining(","));
    }
}
