package com.timaven.provider.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.timaven.provider.model.Requisition;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class RequisitionDto {
    private String purchaseOrderNumber;

    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate requisitionDate;
    private String vendorName;
    private String requestor;
    private String requisitionNumber;

    private boolean newItem = true;

    private Integer itemCount;

    List<RequisitionLineDto> requisitionLineDtos = new ArrayList<>();

    public RequisitionDto() {}

    public RequisitionDto(List<Requisition> requisitions) {
        if (!CollectionUtils.isEmpty(requisitions)) {
            Requisition requisition = requisitions.get(0);
            this.purchaseOrderNumber = requisition.getPurchaseOrderNumber();
            this.requisitionDate = requisition.getRequisitionDate();
            this.vendorName = requisition.getVendorName();
            this.requestor = requisition.getRequestor();
            this.requisitionNumber = requisition.getRequisitionNumber();
            this.itemCount = requisitions.size();
            this.newItem = false;
            requisitionLineDtos = requisitions.stream()
                    .map(RequisitionLineDto::new)
                    .sorted(Comparator.comparing(RequisitionLineDto::getLineNumber))
                    .collect(Collectors.toList());
        }
    }
}
