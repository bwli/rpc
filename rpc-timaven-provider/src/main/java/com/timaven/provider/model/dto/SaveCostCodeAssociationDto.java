package com.timaven.provider.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class SaveCostCodeAssociationDto {
    private Long id;
    private String costCode;
    private String description;
    // Global bind LocalDate only works with first level string
    private String startDate;
    private String endDate;
    private List<Long> billingCodeIds;
}
