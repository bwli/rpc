package com.timaven.provider.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WeeklyPayrollDto {
    private Long id;
    private String project;
    private String description;
    private int employeeCount;
    private BigDecimal totalHours;
    private String approvedAt;
    private String generatedAt;
    private boolean taxablePerDiem;
    private boolean isTimeLeftOff = false;
    private String comment;
}
