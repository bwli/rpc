package com.timaven.provider.model.enums;

public enum AllocationSubmissionStatus {
    PENDING,
    RELEASED,
    APPROVED,
    DENIED,
    APPROVED_PENDING;

    public int getValue() {
        return switch (this) {
            case PENDING -> 0;
            case RELEASED -> 1;
            case APPROVED -> 2;
            case DENIED -> 3;
            case APPROVED_PENDING -> 4;
        };
    }
}
