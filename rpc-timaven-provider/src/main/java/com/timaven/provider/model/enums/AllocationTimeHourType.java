package com.timaven.provider.model.enums;

public enum AllocationTimeHourType {
    OT,
    SICK,
    HOLIDAY,
    VACATION,
    OTHER;

    public static AllocationTimeHourType fromString(String s) {
        if ("ot".equalsIgnoreCase(s)) {
            return OT;
        } else if ("sick".equalsIgnoreCase(s)) {
            return SICK;
        } else if ("holiday".equalsIgnoreCase(s)) {
            return HOLIDAY;
        } else if ("vacation".equalsIgnoreCase(s)) {
            return VACATION;
        } else if ("other".equalsIgnoreCase(s)) {
            return OTHER;
        }
        return OT;
    }

    public int getValue() {
        return switch (this) {
            case OT -> 0;
            case SICK -> 1;
            case HOLIDAY -> 2;
            case VACATION -> 3;
            case OTHER -> 4;
        };
    }
}
