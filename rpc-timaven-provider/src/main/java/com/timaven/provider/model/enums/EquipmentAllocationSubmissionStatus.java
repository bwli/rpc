package com.timaven.provider.model.enums;

public enum EquipmentAllocationSubmissionStatus {
    PENDING,
    APPROVED;

    public static EquipmentAllocationSubmissionStatus fromString(String s) {
        if ("pending".equalsIgnoreCase(s)) {
            return PENDING;
        } else if ("approved".equalsIgnoreCase(s)) {
            return APPROVED;
        }
        return PENDING;
    }
}
