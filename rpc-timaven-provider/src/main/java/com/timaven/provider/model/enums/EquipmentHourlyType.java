package com.timaven.provider.model.enums;

public enum EquipmentHourlyType {
    DAILY("Non-hourly"),
    HOURLY("Hourly");

    private final String displayValue;

    EquipmentHourlyType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public static EquipmentHourlyType fromDisplayValue(String displayValue) {
        if (displayValue.equalsIgnoreCase("Non-hourly")) return DAILY;
        if (displayValue.equalsIgnoreCase("Hourly")) return HOURLY;
        return null;
    }
}
