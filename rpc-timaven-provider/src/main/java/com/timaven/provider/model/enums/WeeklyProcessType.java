package com.timaven.provider.model.enums;

public enum WeeklyProcessType {
    NORMAL,
    TLO;

    public int getValue() {
        return switch (this) {
            case NORMAL -> 0;
            case TLO -> 1;
        };
    }

    public static WeeklyProcessType fromValue(int type) {
        if (type == 1) return TLO;
        return NORMAL;
    }
}
