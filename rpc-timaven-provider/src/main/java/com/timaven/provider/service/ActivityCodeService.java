package com.timaven.provider.service;

import com.timaven.provider.model.ActivityCode;

import java.util.List;

public interface ActivityCodeService {

    List<ActivityCode> getActivityCode();

    ActivityCode saveActivityCode(ActivityCode activityCode);
}
