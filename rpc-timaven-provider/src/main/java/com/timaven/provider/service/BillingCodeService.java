package com.timaven.provider.service;

import com.timaven.provider.model.BillingCode;
import com.timaven.provider.model.BillingCodeOverride;
import com.timaven.provider.model.BillingCodeType;
import com.timaven.provider.model.dto.BillingCodeDto;
import com.timaven.provider.model.dto.SaveCostCodeAssociationDto;
import com.timaven.provider.model.enums.BillingCodeTypeParsedBy;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface BillingCodeService {
    List<BillingCodeType> findAllBillingCodeTypes();

    BillingCodeType findBillingCodeTypeById(Long id);

    Page<BillingCodeDto> findBillingCodesByTypeIdAndProjectId(Long typeId, PagingRequest pagingRequest, Long projectId);

    BillingCode findBillingCodeById(Long billingCodeId);

    void deleteBillingCodeTypeById(Long id);

    void deleteBillingCodeById(Long id);

    BillingCodeType saveBillingCodeType(BillingCodeType billingCodeType);

    BillingCodeDto[] findBillingCodeByCostCodeIdAndProjectId(Long id, Long projectId);

    BillingCodeDto[] findBillingCodesByTypeIdAndProjectId(Long id, Long projectId);

    void saveCostCodeBillingCode(Long projectId, SaveCostCodeAssociationDto dto);

    List<BillingCodeType> findBillingCodeTypesSortByLevel();

    void saveCostCodeStructure(List<Long> ids);

    void resetCostCodeLevel();

    BillingCodeOverride findBillingCodeOverride(Long billingCodeId, Long projectId);

    BillingCodeOverride saveBillingCodeOverride(BillingCodeOverride billingCodeOverride);

    void deleteBillingCodeOverrideByBillingCodeIdAndProjectId(Long id, Long projectId);

    BillingCode saveBillingCode(BillingCode billingCode);

    BillingCode findByTypeIdAndCodeName(Long typeId, String codeName);

    BillingCodeOverride[] findBillingCodeOverrides(Long projectId);

    BillingCode[] findByTypeIdAndCodeNameIn(Long typeId, Collection<String> codeNames);

    BillingCodeOverride[] findBillingCodeOverridesByProjectIdAndTypeIdAndCodeNames(Long projectId, Long typeId, Collection<String> codeNames);

    List<BillingCodeOverride> saveBillingCodeOverrides(Collection<BillingCodeOverride> billingCodeOverrides);

    List<BillingCode> saveBillingCodes(Collection<BillingCode> billingCodes);

    BillingCodeOverride[] findBillingCodeOverridesByProjectIdAndBillingCodeIds(Long projectId, Collection<Long> billingCodeIds);

    List<BillingCode> findByTypeIdAndValues(Long typeId, Long projectId, Collection<String> values, BillingCodeTypeParsedBy parsedBy);

    List<BillingCodeType> findBillingCodeTypeSort(String column);
}
