package com.timaven.provider.service;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.CostCodeAssociationDto;
import com.timaven.provider.model.enums.CostCodeType;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface CostCodeService {

    void deleteById(Long id);

    Page<CostCodeAssociationDto> findCostCodeAssociations(PagingRequest pagingRequest, Long projectId);

    CostCodeLog getCostCodeById(Long id);

    CostCodeBillingCode[] getCostCodeBillingCodes(Set<Long> costCodeIds, Set<String> includes);

    void lockCostCodes(Set<String> costCodes, Long projectId);

    CostCodeLog[] findCostCodesByProjectAndDate(Long projectId, LocalDate startDate, LocalDate endDate, Collection<String> costCodes, boolean includeGlobal);

    CostCodeLog[] getCostCodeLogsByProjectIdAndTeamNameAndDateOfService(Long projectId, String teamName, LocalDate dateOfService);

    Page<CostCodeTagType> getCostCodeTagTypesByProjectId(Long projectId, PagingRequest pagingRequest);

    CostCodeTagType saveCostCodeTagType(CostCodeTagType costCodeTagType);

    void deleteCostCodeTagTypeById(Long id);

    Page<CostCodeTag> getCostCodeTagsByProjectId(Long typeId, PagingRequest pagingRequest);

    CostCodeTag saveCostCodeTag(CostCodeTag costCodeTag);

    void deleteCostCodeTagById(Long id);

    CostCodeTagType getCostCodeTagTypeById(Long id);

    CostCodeTag getCostCodeTagById(Long id);

    CostCodeTagType[] getAllCostCodeTagTypeByProjectId(Long projectId);

    CostCodeTag[] getCostCodeTagsByTypeId(Long typeId);

    void deleteCostCodeTagAssociationsByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type);

    void saveCostCodeTagAssociations(Set<CostCodeTagAssociation> costCodeTagAssociations);

    CostCodeTagAssociation[] getCostCodeTagAssociationsByKeysAndType(Set<Long> keys, CostCodeType costCodeType);

    void saveCostCodeLogTag(Long projectId, String costCode, String tag);

    void deleteCostCodeLogTag(Long projectId, String costCode, String tag);

    String[] findCostCodeLogTagStringsByProjectId(Long projectId);

    CostCodeLogTag[] findCostCodeLogTagsByProjectId(Long projectId);

    TeamCostCode[] getTeamCostCodesByProjectIdAndTeamNames(Long projectId, Collection<String> teamNames, LocalDate startDate, LocalDate endDate);

    void saveTeamCostCodes(Set<TeamCostCode> teamCostCodes);

    CostCodeLog[] saveCostCodeLogs(Collection<CostCodeLog> costCodeLogs);

    void saveCostCodeBillingCodeById(Long costCodeId, Long billingCodeId);

    CostCodeTagType getCostCodeTagByProjectIdAndTagType(Long projectId, String tagType);

    CostCodeTag getCostCodeTagByTypeIdAndCodeName(Long typeId, String codeName);

    CostCodeTagType[] getAllCostCodeTagTypes();

    List<CostCodeAssociation> findCostCodeAssociations(Long projectId, LocalDate dateOfService, Collection<String> costCodes);

    List<CostCodeAssociation> findCostCodeAssociations(Long projectId, LocalDate startDate, LocalDate endDate, Collection<String> costCodes);

    Set<CostCodeAssociation> findCostCodeAssociationsByProjectIdAndCodes(Long projectId, Set<String> codes, LocalDate dateOfService);

    List<CostCodeExportView> findCostCodeExportViewByProjectId(Long projectId);
}
