package com.timaven.provider.service;


import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.AllocationTimeFilter;
import com.timaven.provider.model.dto.EquipmentAllocationTimeFilter;
import com.timaven.provider.model.dto.EquipmentPriceDto;
import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import com.timaven.provider.model.enums.EquipmentWeeklyProcessType;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface EquipmentService {

    List<EquipmentUsage> getCurrentEquipmentUsages();

    Equipment getEquipmentById(Long id);

    Equipment getEquipmentByEquipmentId(String equipmentId);

    void saveEquipment(Equipment equipment);

    boolean changeEquipmentValidity(Long id, boolean active);

    EquipmentUsage saveEquipmentUsage(Long equipmentId, Long userId, Long projectId, Long id, LocalDate date, String type);

    List<EquipmentUsage> saveEquipmentUsageAll(Collection<EquipmentUsage> equipmentUsagesList, Long userId);

    void deactivateEquipmentUsage(Long id);

    void checkEquipmentScheduleConflict();

    void saveEquipmentAllocationSubmissions(Set<EquipmentAllocationSubmission> submissions);

    void saveEquipmentAllocationTimes(Set<EquipmentAllocationTime> equipmentAllocationTimes);

    EquipmentWeeklyProcess saveEquipmentWeeklyProcess(EquipmentWeeklyProcess weeklyProcess);

    Equipment[] getAllEquipmentByOwnershipType(EquipmentOwnershipType ownershipType);

    Page<EquipmentPriceDto> getEquipmentPricesByProjectId(Long projectId, PagingRequest pagingRequest);

    EquipmentPrice getEquipmentPriceById(Long id);

    void saveEquipmentPrice(EquipmentPrice equipmentPrice);

    void deleteEquipmentPriceById(Long id);

    Equipment[] getRentalAndBeingUsedEquipments(Long projectId);

    Equipment[] getAllEquipmentByActive(boolean active);

    EquipmentUsage[] getEquipmentUsagesByIdsAndDates(Long equipmentId, Long projectId, LocalDate startDate, LocalDate endDate);

    EquipmentAllocationTime[] getEquipmentAllocationTimesByProjectIdAndDateAndStatus(EquipmentAllocationTimeFilter filter);

    Set<EquipmentAllocationTime> getEquipmentAllocationTimesById(Set<Long> ids);

    EquipmentPrice[] getEquipmentPricesByProjectIdAndClasses(Long projectId, Collection<String> equipmentClasses);

    EquipmentWeeklyProcess getEquipmentWeeklyProcessByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, EquipmentWeeklyProcessType type);

    EquipmentTag[] getEquipmentTags();

    void saveEquipmentTag(String equipmentId, String tag);

    void deleteEquipmentTag(String equipmentId, String tag);

    Equipment[] getEquipmentByEquipmentIds(Set<String> equipmentIds);

    List<Equipment> saveEquipmentList(Collection<Equipment> equipmentList);

    EquipmentPrice[] getEquipmentPricesByProjectId(Long projectId);

    List<EquipmentPrice> saveEquipmentPrices(Collection<EquipmentPrice> equipmentPrices);

    // TM-275
    void deleteEquipmentCostCodePercBatch(List<EquipmentCostCodePerc> list);

    List<EquipmentAllocationTime> getEquipmentAllocationTimesByDate(Long projectId, LocalDate dateOfService, LocalDate payrollDate,
                                                                    EquipmentAllocationSubmissionStatus status, EquipmentOwnershipType type);

    List<EquipmentAllocationTime> getEquipmentAllocationTimesByDateRange(Long projectId, LocalDate startDate,
                                                                         LocalDate endDate,
                                                                         Set<EquipmentAllocationSubmissionStatus> statuses);

    List<Equipment> getEquipmentAllOrByActiveTrue(Boolean isAll);
}
