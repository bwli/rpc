package com.timaven.provider.service;

import com.timaven.provider.model.CommonResult;

public interface ParserService {
    CommonResult updateProjectSetUp();

    CommonResult updateCraftSubmission();
}
