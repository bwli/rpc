package com.timaven.provider.service;

import com.timaven.provider.model.PerDiem;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

public interface PerDiemService {
    Page<PerDiem> getPerDiemPage(Long projectId, PagingRequest pagingRequest);

    PerDiem findById(Long id);

    PerDiem savePerDiem(PerDiem perDiem);

    void deleteById(Long id);

    PerDiem[] getPerDiemsByProjectId(Long projectId);
}
