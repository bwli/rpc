package com.timaven.provider.service;

import com.timaven.provider.model.Project;

import java.util.Set;

public interface ProjectService {
    Project getProjectById(Long projectId);

    Project saveProject(Project project);

    void deleteProject(Long id);

    Project getProjectByJobNumberAndSubJob(String jobNumber, String subJob);

    void addUsersToProject(Long id, Set<Long> userIds);

    Project[] getProjectByProjectIds(Set<Long> projectIds);
}
