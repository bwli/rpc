package com.timaven.provider.service;

import com.timaven.provider.model.ProjectShift;


public interface ProjectShiftService {
    ProjectShift saveProjectShift(ProjectShift projectShift);

    void upsertProjectShift(ProjectShift projectShift);

    ProjectShift getProjectShiftByProjectId(Long projectId);
}
