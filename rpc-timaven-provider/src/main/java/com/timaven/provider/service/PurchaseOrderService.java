package com.timaven.provider.service;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.*;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PurchaseOrderService {
    Page<PurchaseOrderSummaryDto> getLatestPurchaseOrderSummaries(PagingRequest pagingRequest, Long projectId);

    Page<PurchaseOrderDetailDto> getLatestPurchaseOrderDetails(PagingRequest pagingRequest, String purchaseOrderNumber);

    RequisitionDto getRequisitionDto(String requisitionNumber);

    void deleteByRequisitionNumber(String requisitionNumber);

    void saveRequisition(RequisitionDto requisitionDto, Long projectId);

    Page<RequisitionGroup> getRequisitionGroups(PagingRequest pagingRequest, Long projectId, boolean purchaseNumberNull);

    Set<String> getDistinctLatestPurchaseOrderNumbers();

    Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderBillingDetailView(String purchaseOrderNumber, PagingRequest pagingRequest);

    void deletePurchaseOrderBillingById(Long id);

    PurchaseOrderBilling savePurchaseOrderBilling(PurchaseOrderBillingDto purchaseOrderBillingDto);

    Requisition[] getRequisitionsByJobNumberAndSubJob(String jobNumber, String subJob);

    PurchaseOrderGroup savePurchaseOrderGroupComment(String purchaseOrderNumber, String comment);

    PurchaseOrderGroup savePurchaseOrderGroupFlag(String purchaseOrderNumber, boolean flag);

    PurchaseOrderSubmission savePurchaseOrderSubmission(PurchaseOrderSubmission purchaseOrderSubmission);

    Requisition[] getRequisitionsByPurchaseOrderNumberNull();

    Requisition saveRequisition(Requisition requisition);

    void deletePurchaseOrderGroupByPurchaseOrderNumbers(Collection<String> purchaseOrderNumbers);

    PurchaseOrderGroup[] getPurchaseOrderGroups(Collection<String> purchaseOrderNumbers);

    PurchaseOrderGroup savePurchaseOrderGroup(PurchaseOrderGroup purchaseOrderGroup);

    PurchaseOrderSubmission getLatestPurchaseOrderSubmission();

    List<PurchaseOrder> savePurchaseOrders(Collection<PurchaseOrder> purchaseOrders);

    Page<PurchaseVendorRoster> getPurchaseVendorRoster(PagingRequest pagingRequest);

    List<PurchaseVendorRoster> savePurchaseVendorRoster(Collection<PurchaseVendorRoster> purchaseVendorRosterList);

    PurchaseOrderBilling getPurchaseOrderBillingDetails(Long id);
}
