package com.timaven.provider.service;

import com.timaven.provider.model.RPCUserPool;

import java.util.List;

public interface RPCUserPoolService {
    /**
     * getUserPools
     * @return
     */
    List<RPCUserPool> getUserPools();
}
