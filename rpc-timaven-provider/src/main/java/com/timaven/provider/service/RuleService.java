package com.timaven.provider.service;

import com.timaven.provider.model.Rule;

import java.util.List;

public interface RuleService {
    Rule getRuleByProjectId(Long projectId);

    void saveRule(Rule rule);

    List<Rule> getRules();

    Rule getCorporateRule();

    Rule getRuleById(Long id);
}
