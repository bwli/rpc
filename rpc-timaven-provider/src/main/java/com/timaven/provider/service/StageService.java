package com.timaven.provider.service;

import com.timaven.provider.model.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

public interface StageService {
    StageSubmission[] findStageSubmissions(LocalDate effectiveOn, StageSubmission.Status status, StageSubmission.Type type);

    StageRoster[] getStageRostersBySubmissionId(Long submissionId);

    StageTeamCostCode[] getStageTeamCostCodesBySubmissionId(Long submissionId);

    StageCraft[] getStageCraftsBySubmissionId(Long submissionId);

    StageSubmission saveStageSubmission(StageSubmission stageSubmission);

    List<StageRoster> saveRosters(Collection<StageRoster> stageRosters);

    List<StageCostCode> saveCostCodes(Collection<StageCostCode> stageCostCodes);

    List<StageTeamCostCode> saveTeamCostCodes(Collection<StageTeamCostCode> stageTeamCostCodes);

    List<StageCraft> saveCrafts(Collection<StageCraft> stageCrafts);
}
