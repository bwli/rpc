package com.timaven.provider.service;


import com.timaven.provider.model.Project;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

import java.util.List;

public interface UserProjectService {
    Project[] getProjectsByUsername(String principal);

    Page<Project> getProjectsByUsername(String principal, PagingRequest pagingRequest, boolean isAll);

    Project[] getAllProjects(List<String> includes);
}
