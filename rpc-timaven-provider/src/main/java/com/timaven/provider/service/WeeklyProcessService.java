package com.timaven.provider.service;

import com.timaven.provider.model.AllocationTime;
import com.timaven.provider.model.WeeklyProcess;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.WeeklyProcessType;

import java.time.LocalDate;
import java.util.Set;

public interface WeeklyProcessService {
    WeeklyProcess findByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, WeeklyProcessType type);

    WeeklyProcess save(WeeklyProcess weeklyProcess);

    void calculateCost(Long projectId, LocalDate weekEndDate, WeeklyProcessType type);

    void calculateCost(WeeklyProcess weeklyProcess, Set<AllocationTime> allocationTimes);

    WeeklyProcess[] findByDateAndTypeAndProjectId(LocalDate startDate, LocalDate endDate, WeeklyProcessType type, Long projectId, String teamName, Set<String> includes);

    WeeklyProcess findById(Long id, Set<String> includes);

    WeeklyProcess[] findByIds(Set<Long> ids, Set<String> includes);

    WeeklyProcess findOrGenerateByDateAndTypeAndProjectId(LocalDate weekEndDate, WeeklyProcessType type, Long projectId, String teamName, Set<String> includes);

    WeeklyProcess[] findByProjectIdAndWeeklyEndDate(Set<Long> projectIds, LocalDate weekEndDate, LocalDate weekStartDate, AllocationSubmissionStatus status);

}
