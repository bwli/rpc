package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.RoleRepository;
import com.timaven.provider.dao.repository.UserProjectRepository;
import com.timaven.provider.dao.repository.UserRepository;
import com.timaven.provider.dao.repository.UserRoleRepository;
import com.timaven.provider.model.Role;
import com.timaven.provider.model.User;
import com.timaven.provider.model.UserProject;
import com.timaven.provider.model.UserRole;
import com.timaven.provider.service.AccountService;
import com.timaven.provider.util.AuthenticationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final UserRepository mUserRepository;
    private final RoleRepository mRoleRepository;
    private final UserRoleRepository mUserRoleRepository;
    private final UserProjectRepository mUserProjectRepository;

    @Autowired
    public AccountServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
                              UserRoleRepository userRoleRepository, UserProjectRepository mUserProjectRepository) {
        mUserRepository = userRepository;
        mRoleRepository = roleRepository;
        mUserRoleRepository = userRoleRepository;
        this.mUserProjectRepository = mUserProjectRepository;
    }

    @Override
    public void saveUser(User user) {
        mUserRepository.save(user);
    }

    @Override
    public User getUserById(Long userId) {
        return mUserRepository.findById(userId).orElseThrow(RuntimeException::new);
    }

    @Override
    public void saveUserRole(UserRole userRole) {
        mUserRoleRepository.save(userRole);
    }

    @Override
    public boolean changeUserValidity(Long id, boolean active) {
        Optional<User> userOptional = mUserRepository.findById(id);
        userOptional.ifPresent(user -> user.setActive(active));
        return userOptional.isPresent();
    }

    @Override
    public User loadUserByUsername(String username, Collection<String> includes) {
        if (CollectionUtils.isEmpty(includes)) includes = new HashSet<>();
        if (includes.contains("roles")) {
            return mUserRepository.findUserByUsernameFetchingRoles(username);
        } else {
            return mUserRepository.findUserByUsername(username);
        }
    }

    @Override
    public void addUser(User user, Long projectId) {
        mUserRepository.save(user);
        UserProject userProject = new UserProject();
        userProject.setUserId(user.getId());
        userProject.setProjectId(projectId);
        mUserProjectRepository.save(userProject);
    }

    @Override
    public Set<User> getAdminManagerUsersByProjectId(Long projectId) {
        Set<UserProject> userProjects = mUserProjectRepository.findByProjectIdIs(projectId);
        Set<Long> userIds = userProjects.stream().map(UserProject::getUserId).collect(Collectors.toSet());
        if (userIds.size() == 0) return new HashSet<>();
        return mUserRepository.findAdminManagerInProject(userIds);
    }

    @Override
    public User[] getUsersByProjectIdAndRoles(Long projectId, List<String> roles, Boolean includeSelf, Boolean includeNoRole) {

        Set<String> corporateRoles = roles.stream()
                .filter(r -> "ROLE_super_admin".equals(r) || "ROLE_admin".equals(r) || "ROLE_corporate_accountant".equals(r) || "ROLE_corporate_timekeeper".equals(r))
                .collect(Collectors.toSet());

        Set<String> projectRoles = roles.stream()
                .filter(r -> !("ROLE_super_admin".equals(r) || "ROLE_admin".equals(r) || "ROLE_corporate_accountant".equals(r) || "ROLE_corporate_timekeeper".equals(r)))
                .collect(Collectors.toSet());
        Set<User> users;
        if (!CollectionUtils.isEmpty(corporateRoles) && !CollectionUtils.isEmpty(corporateRoles)) {
            users = mUserRepository.findUsersByProjectIdAndRoles(projectId, corporateRoles, projectRoles, includeNoRole);
        } else if (!CollectionUtils.isEmpty(corporateRoles)) {
            users = mUserRepository.findUsersByProjectIdAndCorporateRoles(corporateRoles, includeNoRole);
        } else {
            users = mUserRepository.findUsersByProjectIdAndProjectRoles(projectId, projectRoles, includeNoRole);
        }

        if (includeSelf != null && !includeSelf) {
            users = users.stream()
                    .filter(u -> !u.getUsername().equals(AuthenticationUtil.getUsername()))
                    .collect(Collectors.toSet());
        }
        return users.toArray(User[]::new);
    }

    @Override
    public Role getRoleByName(String roleName) {
        return mRoleRepository.findByName(roleName);
    }

    @Override
    public Role[] getRolesByNames(List<String> roleNames) {
        Set<Role> roles = mRoleRepository.findByNameIn(roleNames);
        return roles.toArray(Role[]::new);
    }

    @Override
    public User[] getUsers(Set<String> roles, Set<String> notIncludeRoles, Set<String> includes) {
        List<User> users;
        if (!CollectionUtils.isEmpty(includes) && includes.contains("roles")
                || !CollectionUtils.isEmpty(roles)
                || !CollectionUtils.isEmpty(notIncludeRoles)) {
            users = mUserRepository.findAllFetchingRoles(Sort.by(Sort.Direction.ASC, "username"));
        } else {
            users = mUserRepository.findAll(Sort.by(Sort.Direction.ASC, "username"));
        }
        if (!CollectionUtils.isEmpty(roles)) {
            users = users.stream()
                    .filter(u -> u.getUserRoles().stream().map(UserRole::getRole).map(Role::getName).anyMatch(roles::contains))
                    .collect(Collectors.toList());
        }
        if (!CollectionUtils.isEmpty(notIncludeRoles)) {
            users = users.stream()
                    .filter(u -> u.getUserRoles().stream().map(UserRole::getRole).map(Role::getName).noneMatch(notIncludeRoles::contains))
                    .collect(Collectors.toList());
        }
        return users.toArray(User[]::new);
    }

    @Override
    public List<User> getUsersById(Collection<Long> userIds) {
        return mUserRepository.findAllById(new HashSet<>(userIds));
    }
}
