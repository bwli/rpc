package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.service.ActivityCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class ActivityCodeServiceImpl implements ActivityCodeService {

    private final ActivityCodeRepository activityCodeRepository;

    @Autowired
    public ActivityCodeServiceImpl(ActivityCodeRepository activityCodeRepository) {
        this.activityCodeRepository = activityCodeRepository;
    }


    @Override
    public List<ActivityCode> getActivityCode() {
        return activityCodeRepository.findAll();
    }

    @Override
    public ActivityCode saveActivityCode(ActivityCode activityCode) {
        return activityCodeRepository.save(activityCode);
    }
}
