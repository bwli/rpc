package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.BillingCodeDto;
import com.timaven.provider.model.dto.SaveCostCodeAssociationDto;
import com.timaven.provider.model.enums.BillingCodeTypeParsedBy;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.BillingCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class BillingCodeServiceImpl implements BillingCodeService {

    private final BillingCodeTypeRepository billingCodeTypeRepository;
    private final BillingCodeRepository billingCodeRepository;
    private final CostCodeLogRepository costCodeLogRepository;
    private final BillingCodeOverrideRepository billingCodeOverrideRepository;
    private final CostCodeBillingCodeRepository costCodeBillingCodeRepository;
    private final BillingCodeViewRepository billingCodeViewRepository;

    @Autowired
    public BillingCodeServiceImpl(BillingCodeTypeRepository billingCodeTypeRepository,
                                  BillingCodeRepository billingCodeRepository,
                                  CostCodeLogRepository costCodeLogRepository,
                                  BillingCodeOverrideRepository billingCodeOverrideRepository, CostCodeBillingCodeRepository costCodeBillingCodeRepository, BillingCodeViewRepository billingCodeViewRepository) {
        this.billingCodeTypeRepository = billingCodeTypeRepository;
        this.billingCodeRepository = billingCodeRepository;
        this.costCodeLogRepository = costCodeLogRepository;
        this.billingCodeOverrideRepository = billingCodeOverrideRepository;
        this.costCodeBillingCodeRepository = costCodeBillingCodeRepository;
        this.billingCodeViewRepository = billingCodeViewRepository;
    }

    @Override
    public List<BillingCodeType> findAllBillingCodeTypes() {
        return billingCodeTypeRepository.findAll(Sort.by("codeType"));
    }

    @Override
    public BillingCodeType findBillingCodeTypeById(Long id) {
        return billingCodeTypeRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Page<BillingCodeDto> findBillingCodesByTypeIdAndProjectId(Long typeId, PagingRequest pagingRequest, Long projectId) {
        Page<BillingCodeDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());

        final Set<String> billingCodeIds;
        if (projectId != null) {
            billingCodeIds = billingCodeViewRepository.findDistinctIds(projectId);
        } else {
            billingCodeIds = billingCodeViewRepository.findDistinctIds();
        }

        Specification<BillingCodeView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("typeId"), typeId));
            predicates.add(root.get("id").in(billingCodeIds));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        page.setRecordsTotal(billingCodeViewRepository.count(specificationAll));
        Map<String, String> columnMap = new HashMap<>();
        Specification<BillingCodeView> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(root.get("id").in(billingCodeIds));
            predicates.add(criteriaBuilder.equal(root.get("typeId"), typeId));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        long recordsFiltered;
        List<BillingCodeView> billingCodeViews;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<BillingCodeView> rowPage = billingCodeViewRepository.findAll(specification,
                    pageable);
            billingCodeViews = rowPage.getContent();
            recordsFiltered = billingCodeViewRepository.count(specification);
        } else {
            billingCodeViews = billingCodeViewRepository.findAll(specification);
            recordsFiltered = billingCodeViews.size();
        }
        page.setRecordsFiltered(recordsFiltered);

        page.setData(billingCodeViews.stream()
                .map(BillingCodeDto::new).collect(Collectors.toList()));
        return page;
    }

    @Override
    public BillingCode findBillingCodeById(Long billingCodeId) {
        return billingCodeRepository.findById(billingCodeId).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void deleteBillingCodeTypeById(Long id) {
        billingCodeTypeRepository.deleteById(id);
    }

    @Override
    public void deleteBillingCodeById(Long id) {
        billingCodeRepository.deleteById(id);
    }

    @Override
    public BillingCodeType saveBillingCodeType(BillingCodeType billingCodeType) {
        billingCodeTypeRepository.save(billingCodeType);
        return billingCodeType;
    }

    @Override
    public BillingCodeDto[] findBillingCodeByCostCodeIdAndProjectId(Long id, Long projectId) {
        List<BillingCode> billingCodes = billingCodeRepository.findByCostCodeId(id);
        return getBillingCodeDtos(billingCodes, projectId);
    }

    @Override
    public BillingCodeDto[] findBillingCodesByTypeIdAndProjectId(Long id, Long projectId) {
        List<BillingCode> billingCodes = billingCodeRepository.findByTypeId(id);
        return getBillingCodeDtos(billingCodes, projectId);
    }

    private BillingCodeDto[] getBillingCodeDtos(List<BillingCode> billingCodes, Long projectId) {
        List<BillingCodeDto> billingCodeDtos;
        if (projectId == null) {
            billingCodeDtos = billingCodes.stream().map(BillingCodeDto::new).collect(Collectors.toList());
        } else {
            billingCodeDtos = billingCodes.stream()
                    .map(bc -> bc.getBillingCodeOverrides().stream()
                            .filter(bco -> projectId.equals(bco.getProjectId()))
                            .findFirst()
                            .map(bco -> new BillingCodeDto(bc, bco))
                            .orElseGet(() -> new BillingCodeDto(bc)))
                    .collect(Collectors.toList());
        }
        return billingCodeDtos.toArray(BillingCodeDto[]::new);
    }

    @Override
    public void saveCostCodeBillingCode(Long projectId, SaveCostCodeAssociationDto dto) {
        CostCodeLog costCodeLog;
        List<Long> toBeAdded;
        Long id = dto.getId();
        List<Long> ids = dto.getBillingCodeIds();
        String costCode = dto.getCostCode();
        String description = dto.getDescription();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate startDate = StringUtils.hasText(dto.getStartDate()) ? LocalDate.parse(dto.getStartDate(), formatter) : null;
        LocalDate endDate = StringUtils.hasText(dto.getEndDate()) ? LocalDate.parse(dto.getEndDate(), formatter) : null;

        Set<CostCodeBillingCode> costCodeBillingCodesToBeDeleted = new HashSet<>();
        Set<CostCodeBillingCode> costCodeBillingCodesToBeAdded = new HashSet<>();

        if (null != id) {
            costCodeLog = costCodeLogRepository.findByIdFetchingBillingCode(id)
                    .orElseThrow(IllegalArgumentException::new);

            costCodeBillingCodesToBeDeleted.addAll(costCodeLog.getCostCodeBillingCodes().stream()
                    .filter(b -> !ids.contains(b.getBillingCodeId())).collect(Collectors.toSet()));

            costCodeLog.getCostCodeBillingCodes().removeIf(b -> !ids.contains(b.getBillingCodeId()));

            List<Long> existingIds = costCodeLog.getCostCodeBillingCodes().stream()
                    .map(CostCodeBillingCode::getBillingCodeId).collect(Collectors.toList());

            toBeAdded = ids.stream()
                    .filter(i -> !existingIds.contains(i)).collect(Collectors.toList());

        } else {
            costCodeLog = new CostCodeLog();
            costCodeLog.setProjectId(projectId);
            toBeAdded = ids;
        }
        costCodeLog.setCostCodeFull(costCode);
        costCodeLog.setDescription(description);
        costCodeLog.setStartDate(startDate);
        costCodeLog.setEndDate(endDate);

        for (Long billingCodeId : toBeAdded) {
            CostCodeBillingCode costCodeBillingCode = new CostCodeBillingCode();
            costCodeBillingCode.setBillingCodeId(billingCodeId);
            costCodeBillingCodesToBeAdded.add(costCodeBillingCode);
        }

        final CostCodeLog log = costCodeLogRepository.save(costCodeLog);
        costCodeBillingCodesToBeAdded.forEach(b -> b.setCostCodeLog(log));
        costCodeBillingCodeRepository.saveAll(costCodeBillingCodesToBeAdded);
        costCodeBillingCodeRepository.deleteAll(costCodeBillingCodesToBeDeleted);
    }

    @Override
    public List<BillingCodeType> findBillingCodeTypesSortByLevel() {
        return billingCodeTypeRepository.findByLevelNotNullOrderByLevel();
    }

    @Override
    public void saveCostCodeStructure(List<Long> ids) {
        List<BillingCodeType> billingCodeTypes = billingCodeTypeRepository.findByIdIn(ids);
        Map<Long, BillingCodeType> idTypeMap = billingCodeTypes.stream()
                .collect(Collectors.toMap(BillingCodeType::getId, Function.identity()));
        for (int i = 0; i < ids.size(); i++) {
            idTypeMap.get(ids.get(i)).setLevel(i + 1);
        }
    }

    @Override
    public void resetCostCodeLevel() {
        billingCodeTypeRepository.resetLevel();
    }

    @Override
    public BillingCodeOverride findBillingCodeOverride(Long billingCodeId, Long projectId) {
        return billingCodeOverrideRepository.findByBillingCodeIdAndProjectId(billingCodeId, projectId);
    }

    @Override
    public BillingCodeOverride saveBillingCodeOverride(BillingCodeOverride billingCodeOverride) {
        billingCodeOverrideRepository.save(billingCodeOverride);
        return billingCodeOverride;
    }

    @Override
    public void deleteBillingCodeOverrideByBillingCodeIdAndProjectId(Long id, Long projectId) {
        billingCodeOverrideRepository.deleteByBillingCodeIdAndProjectId(id, projectId);
    }

    @Override
    public BillingCode saveBillingCode(BillingCode billingCode) {
        billingCodeRepository.save(billingCode);
        return billingCode;
    }

    @Override
    public BillingCode findByTypeIdAndCodeName(Long typeId, String codeName) {
        return billingCodeRepository.findByTypeIdAndCodeName(typeId, codeName);
    }

    @Override
    public BillingCodeOverride[] findBillingCodeOverrides(Long projectId) {
        List<BillingCodeOverride> overrides = billingCodeOverrideRepository.findByProjectId(projectId);
        return overrides.toArray(BillingCodeOverride[]::new);
    }

    @Override
    public BillingCode[] findByTypeIdAndCodeNameIn(Long typeId, Collection<String> codeNames) {
        if (CollectionUtils.isEmpty(codeNames)) return new BillingCode[0];
        return billingCodeRepository.findByTypeIdAndCodeNameIn(typeId, new HashSet<>(codeNames)).toArray(BillingCode[]::new);
    }

    @Override
    public BillingCodeOverride[] findBillingCodeOverridesByProjectIdAndTypeIdAndCodeNames(Long projectId, Long typeId
            , Collection<String> codeNames) {
        if (CollectionUtils.isEmpty(codeNames)) return new BillingCodeOverride[0];
        return billingCodeOverrideRepository.findByCodeNameInAndProjectId(typeId, new HashSet<>(codeNames), projectId).toArray(BillingCodeOverride[]::new);
    }

    @Override
    public List<BillingCodeOverride> saveBillingCodeOverrides(Collection<BillingCodeOverride> billingCodeOverrides) {
        return billingCodeOverrideRepository.saveAll(billingCodeOverrides);
    }

    @Override
    public List<BillingCode> saveBillingCodes(Collection<BillingCode> billingCodes) {
        return billingCodeRepository.saveAll(billingCodes);
    }

    @Override
    public BillingCodeOverride[] findBillingCodeOverridesByProjectIdAndBillingCodeIds(Long projectId, Collection<Long> billingCodeIds) {
        if (CollectionUtils.isEmpty(billingCodeIds)) return new BillingCodeOverride[0];
        return billingCodeOverrideRepository.findByProjectIdAndBillingCodeIdIn(projectId, billingCodeIds).toArray(BillingCodeOverride[]::new);
    }

    @Override
    public List<BillingCode> findByTypeIdAndValues(Long typeId, Long projectId, Collection<String> values, BillingCodeTypeParsedBy parsedBy) {
        if (CollectionUtils.isEmpty(values)) return new ArrayList<>();
        if (parsedBy == BillingCodeTypeParsedBy.CODE_NAME) {
            return billingCodeRepository.findByTypeIdAndCodeNameIn(typeId, new HashSet<>(values));
        } else if (parsedBy == BillingCodeTypeParsedBy.CLIENT_ALIAS) {
            final Set<String> billingCodeIds;
            if (projectId != null) {
                billingCodeIds = billingCodeViewRepository.findDistinctIds(projectId);
            } else {
                billingCodeIds = billingCodeViewRepository.findDistinctIds();
            }
            Specification<BillingCodeView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(criteriaBuilder.equal(root.get("typeId"), typeId));
                predicates.add(root.get("id").in(billingCodeIds));
                return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            };
            List<BillingCodeView> billingCodeViews = billingCodeViewRepository.findAll(specificationAll);
            billingCodeViews = billingCodeViews.stream()
                    .filter(billingCodeView -> values.contains(billingCodeView.getClientAlias()))
                    .collect(Collectors.toList());
            return billingCodeViews.stream().map(BillingCode::new).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<BillingCodeType> findBillingCodeTypeSort(String column) {
        return billingCodeTypeRepository.findAll(Sort.by(column));
    }
}
