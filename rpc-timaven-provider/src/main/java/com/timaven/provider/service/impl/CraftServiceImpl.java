package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.CraftRepository;
import com.timaven.provider.dao.repository.CraftViewRepository;
import com.timaven.provider.model.AllocationTime;
import com.timaven.provider.model.Craft;
import com.timaven.provider.model.CraftView;
import com.timaven.provider.model.Employee;
import com.timaven.provider.model.dto.CraftDto;
import com.timaven.provider.model.dto.CraftDto;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.CraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class CraftServiceImpl implements CraftService {

    private final CraftRepository craftRepository;
    private final CraftViewRepository craftViewRepository;

    @Autowired
    public CraftServiceImpl(CraftRepository craftRepository, CraftViewRepository craftViewRepository) {
        this.craftRepository = craftRepository;
        this.craftViewRepository = craftViewRepository;
    }

    @Override
    public Page<CraftDto> getCraftPage(PagingRequest pagingRequest, Long projectId) {
        Page<CraftDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        LocalDate today = LocalDate.now();
        Specification<CraftView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("endDate")),
                    criteriaBuilder.greaterThan(root.get("endDate"), today)));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        List<CraftView> allValidCrafts = craftViewRepository.findAll(specificationAll);
        page.setRecordsTotal(allValidCrafts.size());

        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("perDiemAmt", "perDiem");

        Specification<CraftView> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("endDate")),
                    criteriaBuilder.greaterThan(root.get("endDate"), today)));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        List<CraftView> craftList = craftViewRepository.findAll(specification);
        page.setRecordsFiltered(craftList.size());
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<CraftView> rowPage = craftViewRepository.findAll(specification,
                    pageable);
            craftList = rowPage.getContent();
        }
        List<CraftDto> rows = craftList.stream().map(CraftDto::new).collect(toList());
        page.setData(rows);
        return page;
    }

    @Override
    public Craft findCraftById(Long id) {
        return craftRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Craft saveCraft(Craft craft) {
//        LocalDate startDate = craft.getStartDate() == null ? LocalDate.now() : craft.getStartDate();
        Craft existingCraft;
        if (craft.getId() != null) {
            existingCraft = craftRepository.findById(craft.getId()).orElseThrow(IllegalArgumentException::new);
        } else {
            Long projectId = craft.getProjectId() == null ? 0L : craft.getProjectId();
            existingCraft = craftRepository.findByProjectIdAndCodeAndDate(projectId, craft.getCode(), craft.getStartDate());
        }
        Comparator<Craft> craftComparator = (o1, o2) -> Comparator.comparing(Craft::getCode, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableST, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableOT, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getBillableDT, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getDescription, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getPerDiem, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getPerDiemId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getCompany, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getRigPay, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getStartDate, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Craft::getEndDate, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(o1, o2);
        if (existingCraft == null) {
            return craftRepository.save(craft);
        } else if (craftComparator.compare(existingCraft, craft) != 0) {
//            if (existingCraft.getEndDate() == null) {
//                existingCraft.setEndDate(startDate);
//            }
            craft.setId(null);
            return craftRepository.save(craft);
        }
        // else Nothing changed.
        return existingCraft;
    }

    @Override
    public Craft[] findByProjectIdAndCodes(Long projectId, Collection<String> codes) {
        if (CollectionUtils.isEmpty(codes)) return new Craft[0];
        return craftRepository.findByProjectIdAndCodes(projectId, codes).toArray(Craft[]::new);
    }

    @Override
    public void saveCrafts(Collection<Craft> crafts) {
        craftRepository.saveAll(crafts);
    }

    @Override
    public Craft[] getCraftsByProjectIdAndDateRange(Long projectId, LocalDate startDate, LocalDate endDate) {
        return craftRepository.findByProjectIdAndDateRange(projectId, startDate, endDate).toArray(Craft[]::new);
    }

    @Override
    public List<CraftDto> getCraftDtosByProjectIdAndDateRange(Long projectId, LocalDate start) {
        List<CraftView> craftViewList = craftViewRepository.findByProjectIdAndDateRange(projectId, start);
        return craftViewList.stream().map(CraftDto::new).collect(toList());
    }
}
