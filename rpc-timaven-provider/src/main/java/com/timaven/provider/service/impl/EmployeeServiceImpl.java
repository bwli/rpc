package com.timaven.provider.service.impl;

import com.timaven.provider.audit.CustomRevisionEntity;
import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.EmpWorkdays;
import com.timaven.provider.model.dto.EmployeeAuditDto;
import com.timaven.provider.model.dto.EmployeeDto;
import com.timaven.provider.model.dto.EmployeeIdNameDto;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.EmployeeService;
import com.timaven.provider.util.DateRange;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.criteria.AuditCriterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final AuditRepository auditRepository;
    private final AllocationTimeRepository allocationTimeRepository;
    private final EmployeeViewRepository employeeViewRepository;
    private final EmployeeTagRepository employeeTagRepository;
    private final ActivityCodeRepository activityCodeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, AuditRepository auditRepository,
                               AllocationTimeRepository allocationTimeRepository,
                               EmployeeViewRepository employeeViewRepository, EmployeeTagRepository employeeTagRepository,
                               ActivityCodeRepository activityCodeRepository) {
        this.employeeRepository = employeeRepository;
        this.auditRepository = auditRepository;
        this.allocationTimeRepository = allocationTimeRepository;
        this.employeeViewRepository = employeeViewRepository;
        this.employeeTagRepository = employeeTagRepository;
        this.activityCodeRepository = activityCodeRepository;
    }

    @Override
    public Page<EmployeeDto> getEmployees(PagingRequest pagingRequest, Long projectId) {
        Page<EmployeeDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        boolean showAll = false;
        if (pagingRequest.getExtra() instanceof Boolean) {
            showAll = (boolean) pagingRequest.getExtra();
        }
        boolean finalShowAll = showAll;
        Specification<EmployeeView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            if (!finalShowAll) {
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("terminatedAt")), criteriaBuilder.greaterThan(root.get("terminatedAt"), LocalDate.now())));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        List<EmployeeView> allValidEmployees = employeeViewRepository.findAll(specificationAll);
        page.setRecordsTotal(allValidEmployees.size());

        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("name", "lastName,firstName");
        columnMap.put("team", "teamName");

        Specification<EmployeeView> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            if (!finalShowAll) {
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("terminatedAt")), criteriaBuilder.greaterThan(root.get("terminatedAt"), LocalDate.now())));
            }
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        List<EmployeeView> employeeList = employeeViewRepository.findAll(specification);
        page.setRecordsFiltered(employeeList.size());
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<EmployeeView> rowPage = employeeViewRepository.findAll(specification,
                    pageable);
            employeeList = rowPage.getContent();
        }
        List<EmployeeDto> rows = employeeList.stream().map(EmployeeDto::new).collect(toList());
        page.setData(rows);
        return page;
    }

    @Override
    public Employee findById(Long id) {
        return employeeRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        if (employee.getTeamName() != null) {
            employee.setTeamName(employee.getTeamName().trim());
        }
        LocalDate effectedOn = employee.getEffectedOn();
        effectedOn = effectedOn == null ? LocalDate.now() : effectedOn;
        if (employee.getHiredAt() == null) {
            employee.setHiredAt(effectedOn);
        }
        Employee existingEmployee;
        if (employee.getId() != null) {
            existingEmployee = employeeRepository.findById(employee.getId()).orElseThrow(IllegalArgumentException::new);
        } else {
            Long projectId = employee.getProjectId() == null ? 0L : employee.getProjectId();
            existingEmployee = employeeRepository.findByEmpIdAndProjectIdAndDate(employee.getEmpId(), projectId, effectedOn);
        }
        boolean hasChanges = false;
        if (existingEmployee == null) {
            employee = employeeRepository.save(employee);
            hasChanges = true;
        } else {
            Comparator<Employee> employeeComparator = (o1, o2) -> Comparator.comparing(Employee::getEmpId, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getMiddleName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getGender, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getSignInSheet, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBadge, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getCraft, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBaseST, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBaseOT, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBaseDT, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHolidayRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getSickLeaveRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getTravelRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getVacationRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getClientEmpId, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getClientCraft, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getJobNumber, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getPerDiemId, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getCompany, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getTeamName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getCrew, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getEffectedOn, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHiredAt, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getTerminatedAt, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHasPerDiem, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHasRigPay, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getShift, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getScheduleStart, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getScheduleEnd, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getLunchStart, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getLunchEnd, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getIsOffsite, Comparator.nullsLast(Comparator.naturalOrder())) // TM-307
                    .thenComparing(Employee::getActivityCode, Comparator.nullsLast(Comparator.naturalOrder()))
                    .compare(o1, o2);


            if (employeeComparator.compare(existingEmployee, employee) != 0) {
                hasChanges = true;

//                if (existingEmployee.getTerminatedAt() == null) {
//                    existingEmployee.setTerminatedAt(employee.getEffectedOn());
//                }
                employee.setId(null);
                employee = employeeRepository.save(employee);
            }
            // else Nothing changed.
        }
        if (hasChanges) {
            if (effectedOn.isEqual(LocalDate.now()) || effectedOn.isBefore(LocalDate.now())) {
                Comparator<Employee> employeeComparator =
                        (o1, o2) -> Comparator.comparing(Employee::getEmpId, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getCraft, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getTeamName, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getCrew, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getHiredAt, Comparator.nullsLast(Comparator.naturalOrder()))
                                .thenComparing(Employee::getEffectedOn, Comparator.nullsLast(Comparator.naturalOrder()))
                                .compare(o1, o2);
                Set<String> empIds = new HashSet<>();
                empIds.add(employee.getEmpId());
                if (existingEmployee != null) {
                    empIds.add(existingEmployee.getEmpId());
                }
                if (existingEmployee == null || employeeComparator.compare(existingEmployee, employee) != 0) {
                    List<AllocationTime> allocationTimes = allocationTimeRepository.findByEmpIdAndDateOfServiceAndStatuses(empIds,
                            employee.getHiredAt(), List.of(AllocationSubmissionStatus.PENDING,
                                    AllocationSubmissionStatus.APPROVED_PENDING));
                    Employee finalEmployee = employee;
                    allocationTimes.forEach(t -> {
                        t.setEmpId(finalEmployee.getEmpId());
                        t.setLastName(finalEmployee.getLastName());
                        t.setFirstName(finalEmployee.getFirstName());
                        t.setTeamName(finalEmployee.getTeamName());
                    });
                }
                if (StringUtils.hasText(employee.getActivityCode())) {
                    if (activityCodeRepository.findByActivityCode(employee.getActivityCode()).isEmpty()) {
                        ActivityCode activityCode = new ActivityCode();
                        activityCode.setActivityCode(employee.getActivityCode());
                        activityCodeRepository.save(activityCode);
                    }
                }
            } // else handle by schedule task
        }
        return employee;
    }

    @Override
    public Page<EmployeeAuditDto> getEmployeeAudit(PagingRequest pagingRequest, Long projectId) {
        Page<EmployeeAuditDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        AuditCriterion criterion;
        if (null != projectId) {
            criterion = AuditEntity.property("projectId").eq(projectId);
        } else {
            criterion = AuditEntity.property("projectId").isNull();
        }
        List<Object> allEmployeeAudits = auditRepository.findAuditEntityOnly(Employee.class, criterion);
        page.setRecordsTotal(allEmployeeAudits.size());
        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("name", "lastName,firstName");
        columnMap.put("team", "teamName");
        Map<String, String> columnTypeMap = new HashMap<>();
        columnTypeMap.put("effectedOn", "LocalDate");
        columnTypeMap.put("hiredAt", "LocalDate");
        columnTypeMap.put("terminatedAt", "LocalDate");
        columnTypeMap.put("isOffsite", "Boolean");
        columnTypeMap.put("hasPerDiem", "Boolean");
        columnTypeMap.put("hasRigPay", "Boolean");

        AuditCriterion searchCriterion = PagingRequestUtil.getAuditCriterion(pagingRequest, columnMap, columnTypeMap);
        if (null != searchCriterion) {
            criterion = AuditEntity.and(criterion, searchCriterion);
        }
        List<Object[]> employeeAudits = auditRepository.findAuditEntity(Employee.class, criterion,
                pagingRequest.getStart(), pagingRequest.getLength());
        page.setRecordsFiltered(employeeAudits.size());

        List<EmployeeAuditDto> employeeAuditDtos = employeeAudits.stream()
                .map(triplet -> new EmployeeAuditDto((Employee) triplet[0],
                        (CustomRevisionEntity) triplet[1],
                        (RevisionType) triplet[2]))
                .collect(toList());
        page.setData(employeeAuditDtos);
        return page;
    }

    @Override
    public Set<Employee> findEmployees(EmployeeFilter filter) {
        Long projectId = filter.getProjectId();
        Set<Employee> employees;
        String teamName = filter.getTeamName() == null ? null : filter.getTeamName().trim();
        LocalDate startDate = filter.getStartDate();
        LocalDate endDate = filter.getEndDate();
        Collection<String> empIds = filter.getEmpIds();
        Collection<String> badges = filter.getBadges();
        Collection<String> clientEmpIds = filter.getClientEmpIds();
        if (startDate != null && (endDate == null || startDate.isEqual(endDate))) {
            employees = employeeRepository.findByProjectIsAndDateOfService(projectId, startDate);
        } else {
            employees = employeeRepository.findByProjectIsAndDateRange(projectId, startDate, endDate, LocalDate.now());
        }
        if (StringUtils.hasText(teamName)) {
            employees = employees.stream()
                    .filter(e -> StringUtils.hasText(e.getTeamName()) && teamName.equalsIgnoreCase(e.getTeamName().trim()))
                    .collect(Collectors.toSet());
        }
        if (!CollectionUtils.isEmpty(empIds) || !CollectionUtils.isEmpty(badges) || !CollectionUtils.isEmpty(clientEmpIds)) {
            Collection<String> finalEmpIds = null == empIds ? new HashSet<>() : empIds;
            Collection<String> finalBadges = null == badges ? new HashSet<>() : badges;
            Collection<String> finalClientEmpIds = null == clientEmpIds ? new HashSet<>() : clientEmpIds;
            employees = employees.stream()
                    .filter(e -> (StringUtils.hasText(e.getEmpId()) && finalEmpIds.contains(e.getEmpId()))
                            || (StringUtils.hasText(e.getBadge()) && finalBadges.contains(e.getBadge()))
                            || (StringUtils.hasText(e.getClientEmpId()) && finalClientEmpIds.contains(e.getClientEmpId())))
                    .collect(Collectors.toSet());
        }

        return employees;
    }

    @Override
    public void refreshEmployees(Set<Employee> employees) {
        employees.forEach(employeeRepository::refresh);
    }

    @Override
    public Set<EmpWorkdays> findEmployeesDayOff(Collection<String> empIds, LocalDate dateOfService, long days) {
        Set<Object[]> result = employeeRepository.findDayOffEmployees(new HashSet<>(empIds), dateOfService, days);
        Set<EmpWorkdays> empWorkdaysSet = new HashSet<>();
        if (!CollectionUtils.isEmpty(result)) {
            empWorkdaysSet = result.stream().map(o -> {
                EmpWorkdays empWorkdays = new EmpWorkdays();
                empWorkdays.setEmpId((String) o[0]);
                empWorkdays.setStartDate(((java.sql.Date) o[1]).toLocalDate());
                empWorkdays.setEndDate(((java.sql.Date) o[2]).toLocalDate());
                empWorkdays.setConsecutiveDays(((BigInteger) o[3]).longValue());
                return empWorkdays;
            }).collect(Collectors.toSet());
        }
        return empWorkdaysSet;
    }

    @Override
    public Set<String> getSignInSheets(EmployeeFilter filter) {
        Long projectId = filter.getProjectId();
        LocalDate dateOfService = filter.getStartDate();
        return employeeRepository.findDistinctSignInSheets(projectId, dateOfService, LocalDate.now());
    }

    @Override
    public Set<String> getTeamNames(EmployeeFilter filter) {
        Long projectId = filter.getProjectId();
        LocalDate startDate = filter.getStartDate();
        LocalDate endDate = filter.getEndDate();
        if (endDate == null || endDate.equals(startDate)) {
            return employeeRepository.findDistinctTeamNames(projectId, startDate, LocalDate.now());
        } else {
            Set<Employee> employees = employeeRepository.findByProjectIsAndDateRange(projectId, startDate, endDate, LocalDate.now());
            Map<String, List<Employee>> idEmployeesMap = employees.stream()
                    .filter(e -> StringUtils.hasText(e.getEmpId()))
                    .collect(Collectors.groupingBy(Employee::getEmpId));
            Set<Employee> validEmployees = new HashSet<>();
            for (LocalDate dateOfService : new DateRange(startDate, endDate, false).toList()) {
                for (Map.Entry<String, List<Employee>> entry : idEmployeesMap.entrySet()) {
                    List<Employee> employeeList = entry.getValue();
                    Optional<Employee> employeeOpt = employeeList.stream()
                            .filter(e -> e.getEffectedOn().isBefore(dateOfService)
                                    || e.getEffectedOn().isEqual(dateOfService))
                            .filter(e -> e.getHiredAt().isBefore(dateOfService)
                                    || e.getHiredAt().isEqual(dateOfService))
                            .max(Comparator.comparing(Employee::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));
                    if (employeeOpt.isPresent() && (employeeOpt.get().getTerminatedAt() == null
                            || employeeOpt.get().getTerminatedAt().isAfter(dateOfService)
                            || employeeOpt.get().getTerminatedAt().isEqual(dateOfService))) {
                        validEmployees.add(employeeOpt.get());
                    }
                }
            }
            return validEmployees.stream()
                    .map(Employee::getTeamName)
                    .filter(StringUtils::hasText)
                    .collect(Collectors.toSet());
        }
    }

    @Override
    public Set<EmployeeIdNameDto> getEmployeeIdNames(EmployeeFilter filter) {
        Long projectId = filter.getProjectId();
        LocalDate dateOfService = filter.getStartDate();
        Set<Object[]> results = employeeRepository.findDistinctEmployeeIdNames(projectId, dateOfService);
        return results.stream()
                .map(r -> {
                    Long id = ((BigInteger) r[0]).longValue();
                    String firstName = r[1] == null ? "" : (String) r[1];
                    String lastName = r[2] == null ? "" : (String) r[2];
                    String fullName;
                    if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
                        fullName = String.format("%s, %s", lastName, firstName);
                    } else if (StringUtils.hasText(firstName)) {
                        fullName = firstName;
                    } else if (StringUtils.hasText(lastName)) {
                        fullName = lastName;
                    } else {
                        fullName = "Unknown";
                    }
                    String craft = r[3] == null ? "" : (String) r[3];
                    String crew = r[4] == null ? "" : (String) r[4];
                    return new EmployeeIdNameDto(id, fullName, craft, crew);
                }).collect(Collectors.toSet());
    }

    @Override
    public void updateAllocationTimes() {
        Set<Employee> employees = employeeRepository.findByEffectedOn(LocalDate.now());
        if (!CollectionUtils.isEmpty(employees)) {
            Set<String> empIds = employees.stream().map(Employee::getEmpId).collect(Collectors.toSet());
            List<AllocationTime> allocationTimes = allocationTimeRepository.findByEmpIdAndDateOfServiceAndStatuses(empIds,
                    LocalDate.now(), List.of(AllocationSubmissionStatus.PENDING, AllocationSubmissionStatus.APPROVED_PENDING));
            Map<String, List<AllocationTime>> empIdAllocationTimeMap = allocationTimes.stream()
                    .collect(Collectors.groupingBy(AllocationTime::getEmpId));
            Map<String, Employee> employeeMap = employees.stream()
                    .collect(Collectors.toMap(Employee::getEmpId, Function.identity(),
                            BinaryOperator.maxBy(Comparator.comparing(Employee::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())))));
            for (Map.Entry<String, List<AllocationTime>> entry : empIdAllocationTimeMap.entrySet()) {
                Employee employee = employeeMap.get(entry.getKey());
                if (employee != null) {
                    entry.getValue().forEach(t -> {
                        t.setEmpId(employee.getEmpId());
                        t.setLastName(employee.getLastName());
                        t.setFirstName(employee.getFirstName());
                        t.setTeamName(employee.getTeamName());
                    });
                }
            }
        }
    }

    @Override
    public void saveEmployeeTag(Long projectId, String empId, String tag) {
        EmployeeTag employeeTag = employeeTagRepository.findByProjectIdAndEmpIdAndTag(projectId, empId, tag);
        if (null == employeeTag) {
            employeeTag = new EmployeeTag(null, empId, tag, projectId);
            employeeTagRepository.save(employeeTag);
        }
    }

    @Override
    public void deleteEmployeeTag(Long projectId, String empId, String tag) {
        employeeTagRepository.deleteByProjectIdAndEmpIdAndTag(projectId, empId, tag);
    }

    @Override
    public String[] findEmployeeTagStringsByProjectId(Long projectId) {
        List<EmployeeTag> employeeTags = employeeTagRepository.findByProjectId(projectId);
        return employeeTags.stream().map(EmployeeTag::getTag).toArray(String[]::new);
    }

    @Override
    public List<Employee> saveEmployees(Collection<Employee> employees) {
        employees.forEach(e -> {
            e.setTeamName(e.getTeamName() != null ? e.getTeamName().trim() : null);
        });
        return employeeRepository.saveAll(employees);
    }

    @Override
    public int countByProjectId(Long projectId) {
        projectId = projectId == null ? 0 : projectId;
        return employeeRepository.countByProjectId(projectId);
    }

    @Override
    public EmployeeView[] getEmployeeViewByProjectIdAndEmpId(Set<Long> projectIds, Set<String> employeeIds) {
        Specification<EmployeeView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(!CollectionUtils.isEmpty(projectIds)){
                predicates.add(criteriaBuilder.in(root.get("projectId")).value(projectIds));
            }
            if(!CollectionUtils.isEmpty(employeeIds)){
                predicates.add(criteriaBuilder.in(root.get("empId")).value(employeeIds));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        return employeeViewRepository.findAll(specificationAll).toArray(EmployeeView[]::new);
    }


    @Override
    public List<EmployeeView> findEmployeeByProjectIdAndShowAll(Long projectId, Boolean showAll) {
        showAll = showAll != null && showAll;
        boolean finalShowAll = showAll;
        Specification<EmployeeView> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            if (!finalShowAll) {
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("terminatedAt")), criteriaBuilder.greaterThan(root.get("terminatedAt"), LocalDate.now())));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        return employeeViewRepository.findAll(specification);
    }

    @Override
    public List<Employee> findEmployeeByIds(Set<Long> employeeIds) {
        return employeeRepository.findAllById(employeeIds);
    }

    @Override
    public Set<Employee> findByTeamNameAndProjectIdAndDateOfServic(String teamName, Long projectId, LocalDate dateOfService) {
        return employeeRepository.findByTeamNameAndProjectIdAndDateOfService(teamName, projectId, dateOfService);
    }
}
