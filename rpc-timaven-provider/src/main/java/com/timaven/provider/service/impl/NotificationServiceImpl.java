package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.NotificationRepository;
import com.timaven.provider.model.Notification;
import com.timaven.provider.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository notificationRepository;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Override
    public Notification[] getNotificationsByUserId(Long id) {
        return notificationRepository.findByUserIdOrderByCreatedAtDesc(id).toArray(Notification[]::new);
    }

    @Override
    public boolean updateNotificationRead(Long id, boolean read) {
        Optional<Notification> notificationOptional = notificationRepository.findById(id);
        notificationOptional.ifPresent(n -> n.setRead(read));
        return notificationOptional.isPresent();
    }

    @Override
    public void saveNotifications(List<Notification> notifications) {
        if (CollectionUtils.isEmpty(notifications)) return;
        notificationRepository.saveAll(notifications);
    }

    @Override
    public int getNotificationCountByUserIdAndRead(Long userId, boolean read) {
        return notificationRepository.countByUserIdAndRead(userId, read);
    }
}
