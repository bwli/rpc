package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.ProjectRepository;
import com.timaven.provider.dao.repository.UserProjectRepository;
import com.timaven.provider.model.Project;
import com.timaven.provider.model.UserProject;
import com.timaven.provider.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository mProjectRepository;
    private final UserProjectRepository userProjectRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, UserProjectRepository userProjectRepository) {
        mProjectRepository = projectRepository;
        this.userProjectRepository = userProjectRepository;
    }

    @Override
    public Project getProjectById(Long projectId) {
        return mProjectRepository.findById(projectId).orElseThrow(() -> new RuntimeException("Invalid project id"));
    }

    @Override
    public Project saveProject(Project project) {
        return mProjectRepository.save(project);
    }

    @Override
    public void deleteProject(Long id) {
        mProjectRepository.findById(id).ifPresent(p -> p.setDeleted(true));
    }

    @Override
    public Project getProjectByJobNumberAndSubJob(String jobNumber, String subJob) {
        return mProjectRepository.findByJobNumberAndSubJob(jobNumber, subJob);
    }

    @Override
    public void addUsersToProject(Long id, Set<Long> userIds) {
        if (CollectionUtils.isEmpty(userIds)) userIds = new HashSet<>();
        Set<UserProject> userProjects = userProjectRepository.findByProjectIdIs(id);

        Set<Long> finalUserIds = userIds;
        Set<UserProject> toBeDeleted = userProjects.stream()
                .filter(up -> !finalUserIds.contains(up.getUserId())).collect(Collectors.toSet());

        Set<Long> curUserIds = userProjects.stream().map(UserProject::getUserId).collect(Collectors.toSet());

        Set<Long> newUserIds = userIds.stream().filter(u -> !curUserIds.contains(u)).collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(newUserIds)) {
            Set<UserProject> newUserProjects = newUserIds.stream()
                    .map(u -> new UserProject(u, id)).collect(Collectors.toSet());
            userProjectRepository.saveAll(newUserProjects);
        }
        if (!CollectionUtils.isEmpty(toBeDeleted)) {
            userProjectRepository.deleteAll(toBeDeleted);
        }

    }

    @Override
    public Project[] getProjectByProjectIds(Set<Long> projectIds) {
        return mProjectRepository.findByIdIn(projectIds).toArray(Project[]::new);
    }
}
