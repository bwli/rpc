package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.ProjectShiftRepository;
import com.timaven.provider.model.ProjectShift;
import com.timaven.provider.service.ProjectShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ProjectShiftServiceImpl implements ProjectShiftService {

    private final ProjectShiftRepository mProjectShiftRepository;

    @Autowired
    public ProjectShiftServiceImpl(ProjectShiftRepository projectShiftRepository) {
        mProjectShiftRepository = projectShiftRepository;
    }

    @Override
    public ProjectShift saveProjectShift(ProjectShift projectShift) {
        return mProjectShiftRepository.save(projectShift);
    }

    @Override
    public void upsertProjectShift(ProjectShift projectShift) {
        mProjectShiftRepository.upsertProjectShift(projectShift.getProjectId(), projectShift.getShiftId());
    }

    @Override
    public ProjectShift getProjectShiftByProjectId(Long projectId) {
        return mProjectShiftRepository.getProjectShiftByProjectId(projectId);
    }
}
