package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.RainOutRepository;
import com.timaven.provider.model.RainOut;
import com.timaven.provider.service.RainOutService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Transactional
public class RainOutServiceImpl implements RainOutService {

    private final RainOutRepository rainOutRepository;

    public RainOutServiceImpl(RainOutRepository rainOutRepository) {
        this.rainOutRepository = rainOutRepository;
    }

    @Override
    public Set<RainOut> getAllRainOuts(Long projectId) {
        return rainOutRepository.findByProjectIdNullOrProjectId(projectId);
    }

    @Override
    public RainOut getRainOutById(Long id) {
        return rainOutRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void saveRainOut(RainOut rainOut) {
        rainOutRepository.save(rainOut);
    }
}
