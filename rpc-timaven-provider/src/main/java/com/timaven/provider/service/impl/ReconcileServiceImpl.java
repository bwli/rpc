package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.AllocationTimeRepository;
import com.timaven.provider.dao.repository.ProjectRepository;
import com.timaven.provider.dao.repository.UserProjectRepository;
import com.timaven.provider.dao.repository.UserRepository;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.EmpCostCodeProjectDto;
import com.timaven.provider.model.dto.ReconcileDto;
import com.timaven.provider.service.ReconcileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static java.util.stream.Collectors.*;

@Service
@Transactional
public class ReconcileServiceImpl implements ReconcileService {

    private final AllocationTimeRepository mAllocationTimeRepository;
    private final ProjectRepository projectRepository;
    private final UserRepository mUserRepository;
    private final UserProjectRepository userProjectRepository;

    @Autowired
    public ReconcileServiceImpl(AllocationTimeRepository mAllocationTimeRepository,
                                ProjectRepository projectRepository, UserRepository mUserRepository,
                                UserProjectRepository userProjectRepository) {
        this.mAllocationTimeRepository = mAllocationTimeRepository;
        this.projectRepository = projectRepository;
        this.mUserRepository = mUserRepository;
        this.userProjectRepository = userProjectRepository;
    }

    @Override
    public ReconcileDto getReconcileData(LocalDate weekEndDate) {
        LocalDate weekStartDate = weekEndDate.minusDays(6);
        Set<AllocationTime> allocationTimes = mAllocationTimeRepository
                .getAllocationTimesForReconcile(weekStartDate, weekEndDate);
        List<EmpCostCodeProjectDto> empCostCodeProjectDtos = new ArrayList<>();

        if (CollectionUtils.isEmpty(allocationTimes)) {
            return new ReconcileDto(empCostCodeProjectDtos, new ArrayList<>(), new HashMap<>(), new HashSet<>());
        }

        Set<Long> allocationSubmissionIds = allocationTimes.stream()
                .map(AllocationTime::getAllocationSubmission).map(AllocationSubmission::getId)
                .collect(toSet());
        Set<Long> allocationTimeIds = allocationTimes.stream()
                .map(AllocationTime::getId).collect(toSet());

        allocationTimes = mAllocationTimeRepository
                .getAllocationTimesFetchingCostCodePercAndSubmission(allocationTimeIds);

        Set<Long> projectIds = allocationTimes.stream()
                .map(t -> t.getAllocationSubmission().getProjectId())
                .collect(toSet());
        List<Project> projects = projectRepository.findAllById(projectIds);

        if (projectIds.size() != projects.size()) {
            // Some projects are deleted
            Set<Long> activeProjectIds = projects.stream().map(Project::getId).collect(toSet());
            allocationTimes = allocationTimes.stream()
                    .filter(a -> activeProjectIds.contains(a.getAllocationSubmission().getProjectId()))
                    .collect(toSet());
        }

        List<User> accountants = mUserRepository.findAccountants();
        Map<Long, List<String>> projectUserNames = new HashMap<>();
        if (!CollectionUtils.isEmpty(accountants)) {
            Set<Long> accountantIds = accountants.stream().map(User::getId).collect(toSet());
            Set<UserProject> userProjects = userProjectRepository.findByUserIdIn(accountantIds);
            if (!CollectionUtils.isEmpty(userProjects)) {
                for (UserProject userProject : userProjects) {
                    List<String> names = projectUserNames.getOrDefault(userProject.getProjectId(), new ArrayList<>());
                    names.add(userProject.getUser().getDisplayName());
                    projectUserNames.putIfAbsent(userProject.getProjectId(), names);
                }
            }
        }

        Map<String, List<AllocationTime>> empIdAllocationTimesMap = allocationTimes.stream()
                .collect(groupingBy(AllocationTime::getEmpId));
        for (Map.Entry<String, List<AllocationTime>> entry : empIdAllocationTimesMap.entrySet()) {

            AllocationTime allocationTime0 = entry.getValue().get(0);
            EmpCostCodeProjectDto empCostCodeProjectDto = new EmpCostCodeProjectDto(entry.getKey(),
                    allocationTime0.getFirstName(), allocationTime0.getLastName());
            Set<String> costCodes = empCostCodeProjectDto.getCostCodes();
            for (AllocationTime allocationTime : entry.getValue()) {
                Long projectId = allocationTime.getAllocationSubmission().getProjectId();
                for (CostCodePerc costCodePerc : allocationTime.getCostCodePercs()) {
                    costCodes.add(costCodePerc.getCostCodeFull());
                    String totalKey = String.format("%d_%s", projectId, costCodePerc.getCostCodeFull());
                    String stTotalKey = String.format("%d_%s_st", projectId, costCodePerc.getCostCodeFull());
                    String otTotalKey = String.format("%d_%s_ot", projectId, costCodePerc.getCostCodeFull());
                    String dtTotalKey = String.format("%d_%s_dt", projectId, costCodePerc.getCostCodeFull());

                    BigDecimal costCodeTotal = empCostCodeProjectDto.getEmpTotalTimeMap()
                            .getOrDefault(totalKey, BigDecimal.ZERO);
                    BigDecimal stCostCodeTotal = empCostCodeProjectDto.getEmpTimeMap()
                            .getOrDefault(stTotalKey, BigDecimal.ZERO);
                    BigDecimal otCostCodeTotal = empCostCodeProjectDto.getEmpTimeMap()
                            .getOrDefault(otTotalKey, BigDecimal.ZERO);
                    BigDecimal dtCostCodeTotal = empCostCodeProjectDto.getEmpTimeMap()
                            .getOrDefault(dtTotalKey, BigDecimal.ZERO);
                    BigDecimal stHour = costCodePerc.getStHour();
                    stHour = stHour == null ? BigDecimal.ZERO : stHour;
                    BigDecimal otHour = costCodePerc.getOtHour();
                    otHour = otHour == null ? BigDecimal.ZERO : otHour;
                    BigDecimal dtHour = costCodePerc.getDtHour();
                    dtHour = dtHour == null ? BigDecimal.ZERO : dtHour;
                    costCodeTotal = costCodeTotal.add(stHour).add(otHour).add(dtHour);
                    stCostCodeTotal = stCostCodeTotal.add(stHour);
                    otCostCodeTotal = otCostCodeTotal.add(otHour);
                    dtCostCodeTotal = dtCostCodeTotal.add(dtHour);

                    empCostCodeProjectDto.getEmpTotalTimeMap()
                            .put(totalKey, costCodeTotal);
                    empCostCodeProjectDto.getEmpTimeMap()
                            .put(stTotalKey, stCostCodeTotal);
                    empCostCodeProjectDto.getEmpTimeMap()
                            .put(otTotalKey, otCostCodeTotal);
                    empCostCodeProjectDto.getEmpTimeMap()
                            .put(dtTotalKey, dtCostCodeTotal);
                }
            }

            BigDecimal ot = empCostCodeProjectDto.getEmpTotalTimeMap().values().stream().
                    reduce(BigDecimal.ZERO, BigDecimal::add).subtract(BigDecimal.valueOf(40));
            empCostCodeProjectDto.setOt(ot);
            empCostCodeProjectDtos.add(empCostCodeProjectDto);
        }

        Map<Long, String> projectUserNameJoin = projectUserNames.entrySet().stream()
                .filter(e -> !CollectionUtils.isEmpty(e.getValue()))
                .collect(toMap(Map.Entry::getKey, e -> String.join(", ", e.getValue())));
        return new ReconcileDto(empCostCodeProjectDtos, projects, projectUserNameJoin, allocationSubmissionIds);
    }
}
