package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.RuleRepository;
import com.timaven.provider.model.Rule;
import com.timaven.provider.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class RuleServiceImpl implements RuleService {

    private final RuleRepository mRuleRepository;

    @Autowired
    public RuleServiceImpl(RuleRepository ruleRepository) {
        mRuleRepository = ruleRepository;
    }

    @Override
    public Rule getRuleByProjectId(Long projectId) {
        return mRuleRepository.getRuleByProjectId(projectId);
    }

    @Override
    public void saveRule(Rule rule) {
        mRuleRepository.save(rule);
    }

    @Override
    public List<Rule> getRules() {
        return mRuleRepository.findAll();
    }

    @Override
    public Rule getCorporateRule() {
        return mRuleRepository.findByProjectIdIsNull();
    }

    @Override
    public Rule getRuleById(Long id) {
        return mRuleRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }
}
