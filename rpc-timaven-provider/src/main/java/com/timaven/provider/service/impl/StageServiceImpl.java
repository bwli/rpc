package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.service.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class StageServiceImpl implements StageService {

    private final StageSubmissionRepository stageSubmissionRepository;
    private final StageRosterRepository stageRosterRepository;
    private final StageCostCodeRepository stageCostCodeRepository;
    private final StageTeamCostCodeRepository stageTeamCostCodeRepository;
    private final StageCraftRepository stageCraftRepository;


    @Autowired
    public StageServiceImpl(StageSubmissionRepository stageSubmissionRepository,
                            StageRosterRepository stageRosterRepository,
                            StageCostCodeRepository stageCostCodeRepository,
                            StageTeamCostCodeRepository stageTeamCostCodeRepository,
                            StageCraftRepository stageCraftRepository) {
        this.stageSubmissionRepository = stageSubmissionRepository;
        this.stageRosterRepository = stageRosterRepository;
        this.stageCostCodeRepository = stageCostCodeRepository;
        this.stageTeamCostCodeRepository = stageTeamCostCodeRepository;
        this.stageCraftRepository = stageCraftRepository;
    }

    @Override
    public StageSubmission[] findStageSubmissions(LocalDate effectiveOn, StageSubmission.Status status,
                                                  StageSubmission.Type type) {
        return stageSubmissionRepository.findByEffectiveOnLessThanEqualAndStatusIsAndTypeIsOrderByCreatedAtDesc(effectiveOn, status, type).toArray(StageSubmission[]::new);
    }

    @Override
    public StageRoster[] getStageRostersBySubmissionId(Long submissionId) {
        return stageRosterRepository.findBySubmissionIdIs(submissionId).toArray(StageRoster[]::new);
    }

    @Override
    public StageTeamCostCode[] getStageTeamCostCodesBySubmissionId(Long submissionId) {
        return stageTeamCostCodeRepository.findBySubmissionIdIs(submissionId).toArray(StageTeamCostCode[]::new);
    }

    @Override
    public StageCraft[] getStageCraftsBySubmissionId(Long submissionId) {
        return stageCraftRepository.findBySubmissionIdIs(submissionId).toArray(StageCraft[]::new);
    }

    @Override
    public StageSubmission saveStageSubmission(StageSubmission stageSubmission) {
        return stageSubmissionRepository.save(stageSubmission);
    }

    @Override
    public List<StageRoster> saveRosters(Collection<StageRoster> stageRosters) {
        return stageRosterRepository.saveAll(stageRosters);
    }

    @Override
    public List<StageCostCode> saveCostCodes(Collection<StageCostCode> stageCostCodes) {
        return stageCostCodeRepository.saveAll(stageCostCodes);
    }

    @Override
    public List<StageTeamCostCode> saveTeamCostCodes(Collection<StageTeamCostCode> stageTeamCostCodes) {
        return stageTeamCostCodeRepository.saveAll(stageTeamCostCodes);
    }

    @Override
    public List<StageCraft> saveCrafts(Collection<StageCraft> stageCrafts) {
        return stageCraftRepository.saveAll(stageCrafts);
    }
}
