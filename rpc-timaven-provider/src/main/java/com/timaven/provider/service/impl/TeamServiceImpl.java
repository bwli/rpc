package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.UserProjectRepository;
import com.timaven.provider.dao.repository.UserTeamRepository;
import com.timaven.provider.model.UserProject;
import com.timaven.provider.model.UserTeam;
import com.timaven.provider.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class TeamServiceImpl implements TeamService {

    private final UserProjectRepository userProjectRepository;
    private final UserTeamRepository userTeamRepository;

    @Autowired
    public TeamServiceImpl(UserProjectRepository userProjectRepository, UserTeamRepository userTeamRepository) {
        this.userProjectRepository = userProjectRepository;
        this.userTeamRepository = userTeamRepository;
    }

    @Override
    public UserTeam[] getUserTeams(Long userId, Long projectId) {
        Set<UserTeam> result = new HashSet<>();
        UserProject userProject = userProjectRepository.findByUserIdIsAndProjectIdIs(userId, projectId);
        if (null != userProject) {
            result = userTeamRepository.findByUserProjectIdIs(userProject.getId());
        }
        return result.toArray(UserTeam[]::new);
    }

    @Override
    public void saveUserTeams(Long userId, Long projectId, Set<String> teams) {
        UserProject userProject = userProjectRepository.findByUserIdIsAndProjectIdIs(userId, projectId);
        assert userProject != null;
        Set<UserTeam> userTeams = userTeamRepository.findByUserProjectIdIs(userProject.getId());
        Set<String> existingTeams = userTeams.stream().map(UserTeam::getTeam).collect(Collectors.toSet());
        Set<UserTeam> toBeDeleted = userTeams.stream()
                .filter(ut -> !teams.contains(ut.getTeam())).collect(Collectors.toSet());
        Set<String> toBeAdded = teams.stream().filter(t -> !existingTeams.contains(t)).collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(toBeAdded)) {
            Set<UserTeam> userTeamSet = toBeAdded.stream()
                    .map(t -> new UserTeam(userProject.getId(), t)).collect(Collectors.toSet());
            userTeamRepository.saveAll(userTeamSet);
        }
        if (!CollectionUtils.isEmpty(toBeDeleted)) {
            userTeamRepository.deleteAll(toBeDeleted);
        }

    }
}
