package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.ProjectRepository;
import com.timaven.provider.dao.repository.UserProjectRepository;
import com.timaven.provider.dao.repository.UserRepository;
import com.timaven.provider.model.*;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.UserProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Transactional
public class UserProjectServiceImpl implements UserProjectService {

    private final UserRepository mUserRepository;
    private final UserProjectRepository mUserProjectRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    public UserProjectServiceImpl(UserRepository userRepository, UserProjectRepository userProjectRepository,
                                  ProjectRepository projectRepository) {
        mUserRepository = userRepository;
        mUserProjectRepository = userProjectRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Project[] getProjectsByUsername(String principal) {
        User user = mUserRepository.findUserByUsername(principal);
        Set<UserProject> userProjects = mUserProjectRepository.getUserProjectByUserId(user.getId());
        return userProjects.stream().map(UserProject::getProject).collect(Collectors.toSet()).toArray(Project[]::new);
    }

    @Override
    public Page<Project> getProjectsByUsername(String principal, PagingRequest pagingRequest, boolean isAll) {
        Page<Project> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());

        User user;
        Set<UserProject> userProjects = new HashSet<>();
        List<Long> userProjectIds = new ArrayList<>();

        if(!isAll){
            user = mUserRepository.findUserByUsername(principal);
            userProjects = mUserProjectRepository.getUserProjectByUserId(user.getId());
            userProjectIds = userProjects.stream()
                    .map(UserProject::getProjectId)
                    .collect(Collectors.toList());
        }

        List<Long> finalUserProjectIds = userProjectIds;
        Specification<Project> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(!isAll){
                predicates.add(criteriaBuilder.in(root.get("id")).value(finalUserProjectIds));
            }
            predicates.add(criteriaBuilder.isFalse(root.get("deleted")));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };

        Map<String , String> columnMap = new HashMap<>();
        columnMap.put("jobSubJob","jobNumber,subJob");
        columnMap.put("description","description");
        columnMap.put("clientName","clientName");
        columnMap.put("city","city");
        columnMap.put("state","state");

        Specification<Project> pageSpecificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(!isAll){
                predicates.add(criteriaBuilder.in(root.get("id")).value(finalUserProjectIds));
            }
            predicates.add(criteriaBuilder.isFalse(root.get("deleted")));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };

        List<Project> projectList = projectRepository.findAll(specificationAll);
        List<Project> pageProjectList = projectRepository.findAll(pageSpecificationAll);

        page.setRecordsTotal(projectList.size());
        page.setRecordsFiltered(pageProjectList.size());

        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<Project> rowPage = projectRepository.findAll(pageSpecificationAll,
                    pageable);
            projectList = rowPage.getContent();
        }

        page.setData(projectList);
        return page;
    }

    @Override
    public Project[] getAllProjects(List<String> includes) {
        if (includes == null) includes = new ArrayList<>();
        if (includes.contains("userProjects")) {
            return projectRepository.getAllProjectsFetchUserProjects().stream().sorted().toArray(Project[]::new);
        } else {
            return projectRepository.findAll().stream().sorted().toArray(Project[]::new);
        }
    }
}
