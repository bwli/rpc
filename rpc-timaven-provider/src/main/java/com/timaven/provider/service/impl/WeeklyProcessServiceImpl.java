package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.AllocationTimeRepository;
import com.timaven.provider.dao.repository.WeeklyProcessRepository;
import com.timaven.provider.model.AllocationTime;
import com.timaven.provider.model.Craft;
import com.timaven.provider.model.Employee;
import com.timaven.provider.model.WeeklyProcess;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.AllocationTimeHourType;
import com.timaven.provider.model.enums.WeeklyProcessType;
import com.timaven.provider.service.ContentService;
import com.timaven.provider.service.WeeklyProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class WeeklyProcessServiceImpl implements WeeklyProcessService {

    private final WeeklyProcessRepository weeklyProcessRepository;
    private final ContentService contentService;
    private final AllocationTimeRepository allocationTimeRepository;

    @Autowired
    public WeeklyProcessServiceImpl(WeeklyProcessRepository weeklyProcessRepository,
                                    ContentService contentService,
                                    AllocationTimeRepository allocationTimeRepository) {
        this.weeklyProcessRepository = weeklyProcessRepository;
        this.contentService = contentService;
        this.allocationTimeRepository = allocationTimeRepository;
    }

    @Override
    public WeeklyProcess findByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, WeeklyProcessType type) {
        return weeklyProcessRepository.findByProjectIdAndWeekEndDateAndType(projectId, weekEndDate, type);
    }

    @Override
    public WeeklyProcess save(WeeklyProcess weeklyProcess) {
        return weeklyProcessRepository.save(weeklyProcess);
    }

    @Override
    public void calculateCost(Long projectId, LocalDate weekEndDate, WeeklyProcessType type) {
        if (weekEndDate == null) return;
        WeeklyProcess weeklyProcess = findByProjectIdAndDateAndType(projectId, weekEndDate, type);
        boolean isNew = false;
        if (null == weeklyProcess) {
            weeklyProcess = new WeeklyProcess();
            weeklyProcess.setProjectId(projectId);
            weeklyProcess.setWeekEndDate(weekEndDate);
            weeklyProcess.setType(type);
            isNew = true;
        }
        LocalDate weekStartDate = weekEndDate.minusDays(6);
        Set<AllocationTime> allocationTimes;
        if (WeeklyProcessType.NORMAL == type) {
            allocationTimes = allocationTimeRepository
                    .findWeeklyAllocationTimes(projectId, weekStartDate, weekEndDate, AllocationSubmissionStatus.APPROVED);
        } else if (WeeklyProcessType.TLO == type) {
            allocationTimes = allocationTimeRepository
                    .findTimeLeftOffAllocationTimes(projectId, weekStartDate, weekEndDate, AllocationSubmissionStatus.APPROVED);
        } else {
            allocationTimes = new HashSet<>();
        }
        calculateCost(weeklyProcess, allocationTimes);
        if (isNew) {
            if (type != WeeklyProcessType.TLO || !CollectionUtils.isEmpty(allocationTimes)) {
                // Generate Normal or TLO if has TLO
                weeklyProcessRepository.saveAndFlush(weeklyProcess);
            }
        }
        Long id = weeklyProcess.getId();
        allocationTimes.forEach(t -> t.setWeeklyProcessId(id));
    }

    @Override
    public void calculateCost(WeeklyProcess weeklyProcess, Set<AllocationTime> allocationTimes) {
        if (weeklyProcess.getWeekEndDate() == null) return;
        contentService.fetchEmployeeAndCraft(allocationTimes, weeklyProcess.getProjectId());
        BigDecimal billableCost = allocationTimes.stream()
                .map(t -> {
                    Craft craft = t.getEmployee().getCraftEntity();
                    if (craft == null) return BigDecimal.ZERO;
                    BigDecimal billableST = craft.getBillableST() == null ? BigDecimal.ZERO : craft.getBillableST();
                    BigDecimal billableOT = craft.getBillableOT() == null ? BigDecimal.ZERO : craft.getBillableOT();
                    BigDecimal billableDT = craft.getBillableDT() == null ? BigDecimal.ZERO : craft.getBillableDT();
                    if (CollectionUtils.isEmpty(t.getCostCodePercs())) {
                        BigDecimal stHour = t.getStHour() == null ? BigDecimal.ZERO : t.getStHour();
                        BigDecimal otHour = BigDecimal.ZERO;
                        if (t.getExtraTimeType() != AllocationTimeHourType.OT) {
                            stHour = stHour.add(t.getOtHour());
                        } else {
                            otHour = t.getOtHour();
                        }
                        return stHour.multiply(billableST).add(otHour.multiply(billableOT));
                    } else {
                        return t.getCostCodePercs().stream()
                                .map(c -> {
                                    BigDecimal stHour = c.getStHour() == null ? BigDecimal.ZERO : c.getStHour();
                                    BigDecimal otHour = c.getOtHour() == null ? BigDecimal.ZERO : c.getOtHour();
                                    BigDecimal dtHour = c.getDtHour() == null ? BigDecimal.ZERO : c.getDtHour();
                                    return stHour.multiply(billableST).add(otHour.multiply(billableOT))
                                            .add(dtHour.multiply(billableDT));
                                }).reduce(BigDecimal.ZERO, BigDecimal::add);
                    }
                }).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal baseCost = allocationTimes.stream()
                .map(t -> {
                    if (CollectionUtils.isEmpty(t.getCostCodePercs())) return BigDecimal.ZERO;
                    Employee employee = t.getEmployee();
//                    Craft craft = t.getEmployee().getCraftEntity();
                    if (employee == null) return BigDecimal.ZERO;
                    BigDecimal baseST = employee.getBaseST() == null ? BigDecimal.ZERO : employee.getBaseST();
                    BigDecimal baseOT = employee.getBaseOT() == null ? BigDecimal.ZERO : employee.getBaseOT();
                    BigDecimal baseDT = employee.getBaseDT() == null ? BigDecimal.ZERO : employee.getBaseDT();
                    if (CollectionUtils.isEmpty(t.getCostCodePercs())) {
                        BigDecimal stHour = t.getStHour() == null ? BigDecimal.ZERO : t.getStHour();
                        BigDecimal otHour = BigDecimal.ZERO;
                        if (t.getExtraTimeType() != AllocationTimeHourType.OT) {
                            stHour = stHour.add(t.getOtHour());
                        } else {
                            otHour = t.getOtHour();
                        }
                        return stHour.multiply(baseST).add(otHour.multiply(baseOT));
                    } else {
                        return t.getCostCodePercs().stream()
                                .map(c -> {
                                    BigDecimal stHour = c.getStHour() == null ? BigDecimal.ZERO : c.getStHour();
                                    BigDecimal otHour = c.getOtHour() == null ? BigDecimal.ZERO : c.getOtHour();
                                    BigDecimal dtHour = c.getDtHour() == null ? BigDecimal.ZERO : c.getDtHour();
                                    return stHour.multiply(baseST).add(otHour.multiply(baseOT))
                                            .add(dtHour.multiply(baseDT));
                                }).reduce(BigDecimal.ZERO, BigDecimal::add);
                    }
                }).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalHours = allocationTimes.stream()
                .map(AllocationTime::getAllocatedHour)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        weeklyProcess.setBillableAmount(billableCost);
        weeklyProcess.setBaseAmount(baseCost);
        weeklyProcess.setTotalHours(totalHours);
    }

    @Override
    public WeeklyProcess[] findByDateAndTypeAndProjectId(LocalDate startDate, LocalDate endDate, WeeklyProcessType type, Long projectId, String teamName, Set<String> includes) {
        if (includes == null) includes = new HashSet<>();
        if (includes.contains("allocationTimes")) {
            return weeklyProcessRepository.findByProjectIdByDateAndTypeAndProjectIdFetching(startDate, endDate, type, projectId, teamName).toArray(WeeklyProcess[]::new);
        } else {
            return weeklyProcessRepository.findByProjectIdByDateAndTypeAndProjectId(startDate, endDate, type, projectId, teamName).toArray(WeeklyProcess[]::new);
        }
    }

    @Override
    public WeeklyProcess findById(Long id, Set<String> includes) {
        if (includes == null) includes = new HashSet<>();
        if (includes.contains("allocationTimes")) {
            return weeklyProcessRepository.findByIdFetching(id);
        } else {
            return weeklyProcessRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        }
    }

    @Override
    public WeeklyProcess[] findByIds(Set<Long> ids, Set<String> includes) {
        if (includes == null) includes = new HashSet<>();
        List<WeeklyProcess> weeklyProcesses;
        if (includes.contains("allocationTimes") || includes.contains("allocationSubmission")) {
            weeklyProcesses = weeklyProcessRepository.findAllByIdFetchingSubmissions(ids);
        } else if (includes.contains("costCodePercs")) {
            weeklyProcesses = weeklyProcessRepository.findAllByIdFetchingCostCodePercs(ids);
        } else {
            weeklyProcesses = weeklyProcessRepository.findAllById(ids);
        }
        if (includes.contains("crafts") && !CollectionUtils.isEmpty(weeklyProcesses)) {
            weeklyProcesses.forEach(wp -> {
                contentService.fetchEmployeeAndCraft(wp.getAllocationTimes(), wp.getProjectId(), true);
            });
        }
        return weeklyProcesses.toArray(WeeklyProcess[]::new);
    }

    @Override
    public WeeklyProcess findOrGenerateByDateAndTypeAndProjectId(LocalDate weekEndDate, WeeklyProcessType type, Long projectId, String teamName, Set<String> includes) {
        if (includes == null) includes = new HashSet<>();
        WeeklyProcess weeklyProcess;
        if (includes.contains("allocationTimes")) {
            weeklyProcess = weeklyProcessRepository.findByProjectIdByDateAndTypeAndProjectIdFetching(weekEndDate, type, projectId, teamName);
        } else {
            weeklyProcess = weeklyProcessRepository.findByProjectIdByDateAndTypeAndProjectId(weekEndDate, type, projectId, teamName);
        }
        if (weeklyProcess == null) {
            weeklyProcess = new WeeklyProcess();
            weeklyProcess.setProjectId(projectId);
            weeklyProcess.setWeekEndDate(weekEndDate);
            weeklyProcess.setTeamName(teamName);
            weeklyProcess.setType(type);
            weeklyProcess = weeklyProcessRepository.save(weeklyProcess);
        }
        return weeklyProcess;
    }

    @Override
    public WeeklyProcess[] findByProjectIdAndWeeklyEndDate(Set<Long> projectIds, LocalDate weekEndDate, LocalDate weekStartDate, AllocationSubmissionStatus status) {
        return weeklyProcessRepository.findAllByProjectIdAndWeeklyEndDate(projectIds, weekEndDate, weekStartDate, status).toArray(WeeklyProcess[]::new);
    }
}
