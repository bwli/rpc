package com.timaven.provider.util;

import com.timaven.provider.model.dto.Principal;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationUtil {
    public static String getUsername() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return ((Principal) principal).getUsername();
    }
}
