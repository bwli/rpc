package com.timaven.provider.util;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class LocalDateUtil {
    public static boolean isWeekend(LocalDate localDate) {
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        return dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY;
    }
}
