create table if not exists pm.project
(
    id          bigserial primary key,
    name        varchar,
    description varchar,
    user_id    bigint,
    create_at   timestamp with time zone default now()
);
