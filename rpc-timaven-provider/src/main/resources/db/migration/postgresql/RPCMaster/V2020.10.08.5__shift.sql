create table if not exists rule.shift
(
    id                  bigserial primary key,
    name                varchar,
    schedule_start      time,
    schedule_end        time,
    break_start         time,
    break_end           time,
    paid_break_duration integer,
    project_id          bigint not null,
    created_at          timestamp with time zone default now(),
    unique (name, project_id)
);
