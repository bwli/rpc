create table if not exists rule.rule
(
    id                        bigserial primary key,
    rounding_minutes          integer                                                not null default 0,
    rounding_mode             varchar                                                not null default 0,
    grace_in                  integer                                                not null default 0,
    grace_out                 integer                                                not null default 0,
    description               text,
    per_diem_threshold        integer,
    per_diem_mode             integer check (per_diem_mode in (0, 1, 2, 3) )         not null default 0,
    per_diem_weekly_threshold integer,
    rig_pay_threshold         integer                                                not null default 0,
    week_end_day              integer check ( week_end_day in (0, 1, 2, 3, 4, 5, 6)) not null default 6,
    project_id                bigint unique                                          not null,
    created_at                timestamp with time zone                                        default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'rule'
                    and table_schema = 'rule'
                    and column_name = 'per_diem_mode')
        then comment on column rule.rule.per_diem_mode
            is '0: Pay as you go. 1: N+1. 2: 7 days. 3: Manual';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'rule'
                    and table_schema = 'rule'
                    and column_name = 'week_end_day')
        then comment on column rule.rule.week_end_day
            is '0: Monday. 1: Tuesday. 2: Wednesday. 3: Thursday. 4: Friday. 5: Saturday. 6: Sunday.';
        end if;
    END ;
$$;
