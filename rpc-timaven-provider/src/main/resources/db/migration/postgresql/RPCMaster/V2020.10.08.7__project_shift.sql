create table if not exists rule.project_shift
(
    id         bigserial primary key,
    project_id bigint unique not null,
    shift_id   bigint        not null references rule.shift (id) on delete cascade on update cascade,
    created_at timestamp with time zone default now()
);
DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'project_shift'
                    and table_schema = 'rule'
                    and column_name = 'shift_id')
        then comment on column rule.project_shift.shift_id
            is 'this is the default shift';
        end if;
    END ;
$$;
