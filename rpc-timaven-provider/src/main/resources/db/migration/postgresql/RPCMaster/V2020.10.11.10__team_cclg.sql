create table if not exists project.team_cclg
(
    id         bigserial primary key,
    team_name  varchar not null,
    cost_code  varchar not null,
    project_id bigint  not null,
    start_date date,
    end_date   date,
    created_at timestamp with time zone default now()
);
