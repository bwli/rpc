create table if not exists time.trans_submission
(
    id         bigserial primary key,
    project_id bigint not null,
    user_id    bigint not null,
    created_at timestamp with time zone default now()
);
