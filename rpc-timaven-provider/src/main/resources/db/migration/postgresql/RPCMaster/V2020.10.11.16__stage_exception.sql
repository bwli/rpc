create table if not exists time.stage_exception
(
    id              bigserial primary key,
    stage_trans_id  bigint  not null references time.stage_trans (id) on update cascade on delete cascade,
    exception       varchar,
    exception_order integer not null         default 0,
    exception_type  int     not null         default 0 check ( exception_type in (1, 2, 3)),
    created_at      timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_exception'
                    and table_schema = 'time'
                    and column_name = 'exception_type')
        then comment on column time.stage_exception.exception_type
            is '1: illegal employee, 2:missing out, 3:missing in';
        end if;
    END ;
$$;
