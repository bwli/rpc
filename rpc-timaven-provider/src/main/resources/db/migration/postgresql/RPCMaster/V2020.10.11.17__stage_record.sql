create table if not exists time.stage_record
(
    id             bigserial primary key,
    stage_trans_id bigint  not null references time.stage_trans (id) on update cascade on delete cascade,
    door_name      varchar,
    side           boolean,
    valid          boolean not null         default false,
    access_granted boolean not null         default false,
    adjusted_time  timestamp with time zone,
    net_event_time timestamp with time zone,
    created_at     timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_record'
                    and table_schema = 'time'
                    and column_name = 'valid')
        then comment on column time.stage_record.valid
            is 'only a record that is last punch in and first punch out is valid';
        end if;
    END ;
$$;
