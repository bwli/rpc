create table if not exists time.allocation_time
(
    id             bigserial primary key,
    client_emp_id  bigint  not null,
    team_name      varchar not null,
    first_name     varchar,
    last_name      varchar,
    st_hour        numeric,
    ot_hour        numeric,
    dt_hour        numeric,
    total_hour     numeric,
    allocated_hour numeric,
    net_hour       numeric,
    has_per_diem   bool not null default false,
    rig_pay        numeric,
    payroll_date   date,
    submission_id  bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    created_at     timestamp with time zone default now()
);
