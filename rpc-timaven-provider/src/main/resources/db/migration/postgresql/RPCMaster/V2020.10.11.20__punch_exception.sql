create table if not exists time.punch_exception
(
    id                 bigserial primary key,
    allocation_time_id bigint  not null references time.allocation_time (id) on update cascade on delete cascade,
    exception          varchar,
    exception_order    integer not null         default 0,
    exception_type     int     not null         default 0 check ( exception_type in (0, 1, 2, 3)),
    created_at         timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'punch_exception'
                    and table_schema = 'time'
                    and column_name = 'exception_type')
        then comment on column time.punch_exception.exception_type
            is '0: unknown, 1: illegal employee, 2:missing out, 3:missing in';
        end if;
    END ;
$$;
