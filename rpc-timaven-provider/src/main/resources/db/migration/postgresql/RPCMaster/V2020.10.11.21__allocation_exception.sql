create table if not exists time.allocation_exception
(
    id                 bigserial primary key,
    allocation_time_id bigint unique not null references time.allocation_time (id) on update cascade on delete cascade,
    exception          varchar,
    created_at         timestamp with time zone default now()
);
