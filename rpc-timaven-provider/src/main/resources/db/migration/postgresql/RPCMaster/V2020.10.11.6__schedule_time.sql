-- schedule_time and employee_team is one to one relationship
create table if not exists project.schedule_time
(
    id             bigserial primary key,
    shift          varchar,
    schedule_start time,
    schedule_end   time,
    lunch_start    time,
    lunch_end      time,
    employee_id    bigint unique not null references project.employee (id) on update cascade on delete cascade,
    created_at     timestamp with time zone default now()
);
