create table if not exists stage.submission
(
    id           bigserial primary key,
    project_id   bigint  not null,
    username     varchar not null,
    effective_on date    not null,
    type         int     not null check (type in (0, 1, 2, 3, 4) ),
    status       int     not null         default 0 check ( status in (0, 1, 2)),
    processed_at timestamp with time zone,
    created_at   timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'submission'
                    and table_schema = 'stage'
                    and column_name = 'status')
        then comment on column stage.submission.status
            is '0: Pending. 1:Imported. 2:ignored';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'submission'
                    and table_schema = 'stage'
                    and column_name = 'type')
        then comment on column stage.submission.type
            is '0: Roster. 1: Team. 2: CostCode 3: Team-CC 4: Crafts';
        end if;
    END ;
$$;

