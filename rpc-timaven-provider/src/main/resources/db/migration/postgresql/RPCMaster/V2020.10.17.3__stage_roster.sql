create table if not exists stage.roster
(
    id             bigserial primary key,
    submission_id  bigint references stage.submission (id) on delete cascade on update cascade,
    badge          integer,
    name           varchar,
    employee_id    bigint,
    company        varchar,
    craft_code     varchar,
    has_per_diem   boolean,
    has_rig_pay    boolean,
    hired_at       date,
    terminated_at  date,
    shift          varchar,
    schedule_start time,
    schedule_end   time,
    lunch_start    time,
    lunch_end      time,
    created_at     timestamp with time zone default now()
);
