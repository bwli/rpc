create sequence if not exists public.hibernate_sequence increment 1 minvalue 1
    maxvalue 9223372036854775807 START 1 CACHE 1;
