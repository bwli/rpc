create table if not exists public.custom_revision_entity
(
    id        integer not null primary key,
    timestamp bigint  not null,
    username  varchar(255)
);
