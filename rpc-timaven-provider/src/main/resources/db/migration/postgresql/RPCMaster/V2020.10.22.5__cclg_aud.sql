create table if not exists project.cclg_aud
(
    id             bigint  not null,
    rev            integer not null references public.custom_revision_entity (id),
    revtype        smallint,
    cost_code_full varchar(255),
    description    varchar(255),
    project_id     bigint,
    start_date     date,
    end_date       date,
    created_at     timestamp,
    constraint cclg_aud_pkey
        primary key (id, rev)
)
