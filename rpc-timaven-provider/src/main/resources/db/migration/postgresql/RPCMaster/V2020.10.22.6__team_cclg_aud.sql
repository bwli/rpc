create table if not exists project.team_cclg_aud
(
    id         bigint  not null,
    rev        integer not null references public.custom_revision_entity (id),
    team_name  varchar not null,
    cost_code  varchar not null,
    project_id bigint  not null,
    start_date date,
    end_date   date,
    revtype    smallint,
    created_at timestamp with time zone default now(),
    constraint team_cclg_aud_pkey
        primary key (id, rev)
);
