create table if not exists project.user_team
(
    id              bigserial primary key,
    user_project_id bigint  not null,
    team            varchar not null,
    created_at      timestamp with time zone default now()
);
