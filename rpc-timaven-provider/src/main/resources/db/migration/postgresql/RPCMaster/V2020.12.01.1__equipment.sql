create table if not exists project.equipment
(
    id             BIGSERIAL primary key,
    equipment_id   bigint unique not null,
    description    varchar,
    alias          varchar,
    class          varchar not null,
    is_active      boolean not null         default true,
    created_at     timestamp with time zone default now()
);
