create table if not exists project.equipment_price
(
    id           BIGSERIAL primary key,
    class        varchar not null,
    project_id   bigint not null,
    daily_rate   numeric                  default 0,
    weekly_rate  numeric                  default 0,
    monthly_rate numeric                  default 0,
    created_at   timestamp with time zone default now(),
    unique (class, project_id)
);
