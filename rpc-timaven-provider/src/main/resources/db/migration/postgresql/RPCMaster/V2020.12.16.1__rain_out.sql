create table if not exists project.rain_out
(
    id         bigserial primary key,
    name       varchar not null,
    time       int     not null,
    project_id bigint,
    created_at timestamp with time zone default now()
);
