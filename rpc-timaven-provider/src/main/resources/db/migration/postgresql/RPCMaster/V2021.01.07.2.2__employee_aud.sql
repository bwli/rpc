-- create table if not exists project.employee_aud
-- (
--     id            bigint  not null,
--     rev           integer not null references public.custom_revision_entity (id),
--     revtype       smallint,
--     is_active     boolean,
--     badge         integer,
--     team_name     varchar,
--     crew          varchar,
--     emp_id        varchar,
--     client_emp_id varchar,
--     company       varchar(255),
--     created_at    timestamp,
--     dob           date,
--     first_name    varchar(255),
--     gender        varchar(255),
--     has_per_diem  boolean,
--     has_rig_pay   boolean,
--     hired_at      date,
--     last_name     varchar(255),
--     middle_name   varchar(255),
--     project_id    bigint,
--     rehired_at    date,
--     effected_on   date,
--     terminated_at date,
--     craft         varchar,
--     client_craft  varchar,
--     constraint employee_aud_pkey
--         primary key (id, rev)
-- );

DO
$$
    BEGIN
        BEGIN
            alter table project.employee_aud
                add column client_craft varchar;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
        END;
    END ;
$$;
