-- create table if not exists stage.craft
-- (
--     id                bigserial primary key,
--     submission_id     bigint references stage.submission (id) on delete cascade on update cascade,
--     craft_code        varchar,
--     description       varchar,
--     billable_st       numeric                  default 0,
--     billable_ot       numeric                  default 0,
--     billable_dt       numeric                  default 0,
--     base_st           numeric                  default 0,
--     base_ot           numeric                  default 0,
--     base_dt           numeric                  default 0,
--     per_diem          numeric,
--     rig_pay           numeric,
--     created_at        timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'craft'
                    and table_schema = 'stage'
                    and column_name = 'client_craft_code')
        then alter table stage.craft
            drop column client_craft_code;
        end if;
    END ;
$$;
