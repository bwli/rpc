create table if not exists project.billing_code
(
    id           bigserial primary key,
    type_id      bigint not null references project.billing_code_type (id) on update cascade on delete cascade,
    code_name    varchar,
    client_alias varchar,
    description  varchar,
    create_at    timestamp with time zone default now(),
    unique (type_id, code_name)
);

