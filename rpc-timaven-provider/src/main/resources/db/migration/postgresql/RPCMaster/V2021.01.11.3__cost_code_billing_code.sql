create table if not exists project.cost_code_billing_code
(
    id              bigserial primary key,
    cost_code       varchar not null,
    billing_code_id bigint  not null references project.billing_code (id) on update cascade on delete cascade,
    create_at       timestamp with time zone default now(),
    unique (cost_code, billing_code_id)
);

