-- create table if not exists project.cclg
-- (
--     id             bigserial primary key,
--     cost_code_full varchar not null,
--     description    varchar,
--     project_id     bigint,
--     start_date     date,
--     end_date       date,
--     created_at     timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'cclg'
                    and table_schema = 'project'
                    and column_name = 'project_id')
        then alter table project.cclg
            alter column project_id drop not null;
        end if;
    END ;
$$;
