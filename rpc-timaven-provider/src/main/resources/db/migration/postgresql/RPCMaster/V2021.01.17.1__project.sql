-- create table if not exists pm.project
-- (
--     id          bigserial primary key,
--     name        varchar,
--     description varchar,
--     user_id     bigint,
--     job_number  varchar,
--     sub_job     varchar unique,
--     is_active   boolean not null         default true,
--     is_deleted  boolean not null         default false,
--     create_at   timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        alter table pm.project
            add column job_number varchar;
        alter table pm.project
            add column sub_job varchar unique;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
