create table if not exists time.reconcile
(
    id                  bigserial primary key,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date unique,
    created_at          timestamp with time zone default now()
);
