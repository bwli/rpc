create table if not exists project.purchase_order_submission
(
    id           bigserial primary key,
    submitted_by varchar,
    created_at   timestamp with time zone default now()
);
