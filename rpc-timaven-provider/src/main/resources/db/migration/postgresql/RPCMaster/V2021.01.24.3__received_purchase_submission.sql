create table if not exists project.received_purchase_submission
(
    id           bigserial primary key,
    submitted_by varchar,
    created_at   timestamp with time zone default now()
);
