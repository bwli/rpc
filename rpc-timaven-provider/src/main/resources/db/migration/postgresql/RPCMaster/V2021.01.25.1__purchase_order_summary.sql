drop view if exists project.purchase_order_summary cascade;
create or replace view project.purchase_order_summary as
select purchase_order_number,
       purchase_order_date,
       vendor_name,
       requestor,
       job_number,
       sub_job,
       submission_id,
       sum(case when extended_cost is null then 0 else extended_cost end) as total_extended_cost
from project.purchase_order
group by purchase_order_number, purchase_order_date, vendor_name, requestor, job_number, sub_job, submission_id;
