create table if not exists project.requisition
(
    id                          bigserial primary key,
    purchase_order_number       varchar,
    requisition_date            date,
    vendor_name                 varchar,
    requestor                   varchar,
    requisition_number          varchar,
    job_number                  varchar,
    sub_job                     varchar,
    line_number                 int,
    part_number                 varchar,
    part_description            varchar,
    quantity                    int,
    unit_cost                   numeric,
    extended_cost               numeric,
    formatted_cost_distribution varchar,
    cost_type                   varchar,
    created_at                  timestamp with time zone default now()
);
