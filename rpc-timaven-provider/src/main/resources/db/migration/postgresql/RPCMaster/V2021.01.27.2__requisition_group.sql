drop view if exists project.requisition_group cascade;
create or replace view project.requisition_group as
select purchase_order_number,
       requisition_date,
       vendor_name,
       requestor,
       requisition_number,
       job_number,
       sub_job,
       count(*) as item_count
from project.requisition
group by purchase_order_number, requisition_date, vendor_name, requestor, requisition_number, job_number, sub_job
order by requisition_number desc;
