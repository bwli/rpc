create table if not exists project.billing_code_override
(
    id              bigserial primary key,
    billing_code_id bigint not null references project.billing_code (id) on update cascade on delete cascade,
    code_name       varchar,
    client_alias    varchar,
    description     varchar,
    project_id      bigint not null,
    create_at       timestamp with time zone default now(),
    unique (billing_code_id, project_id)
);

