create table if not exists time.equipment_allocation_time
(
    id            bigserial primary key,
    equipment_id  bigint  not null,
    description   varchar,
    alias         varchar,
    class         varchar not null,
    serial_number varchar,
    department    varchar,
    type          integer not null         default 0,
    emp_id        varchar,
    total_hour    numeric,
    payroll_date  date,
    submission_id bigint  not null references time.equipment_allocation_submission (id) on update cascade on delete cascade,
    created_at    timestamp with time zone default now()
);
