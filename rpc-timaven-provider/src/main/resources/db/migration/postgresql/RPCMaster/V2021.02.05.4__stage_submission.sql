-- create table if not exists stage.submission
-- (
--     id           bigserial primary key,
--     project_id   bigint,
--     username     varchar not null,
--     effective_on date    not null,
--     type         int     not null check (type in (0, 1, 2, 3, 4) ),
--     status       int     not null         default 0 check ( status in (0, 1, 2)),
--     processed_at timestamp with time zone,
--     created_at   timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'submission'
                    and table_schema = 'stage'
                    and column_name = 'project_id')
        then alter table stage.submission
            alter column project_id drop not null;
        end if;
    END ;
$$;

