-- create table if not exists stage.roster
-- (
--     id             bigserial primary key,
--     submission_id  bigint references stage.submission (id) on delete cascade on update cascade,
--     badge          integer,
--     name           varchar,
--     employee_id    varchar,
--     client_emp_id  varchar,
--     client_craft   varchar,
--     company        varchar,
--     craft_code     varchar,
--     has_per_diem   boolean,
--     has_rig_pay    boolean,
--     department     varchar,
--     job_number     varchar,
--     hired_at       date,
--     terminated_at  date,
--     shift          varchar,
--     schedule_start time,
--     schedule_end   time,
--     lunch_start    time,
--     lunch_end      time,
--     created_at     timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        BEGIN
            alter table stage.roster
                add column department varchar;
            alter table stage.roster
                add column job_number varchar;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
        END;
    END ;
$$;
