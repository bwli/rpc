create table if not exists time.corporate_weekly_process
(
    id            bigserial primary key,
    approved_by   varchar,
    approved_at   timestamp with time zone,
    reviewed_by   varchar,
    reviewed_at   timestamp with time zone,
    week_end_date date unique not null,
    created_at    timestamp with time zone default now()
);
