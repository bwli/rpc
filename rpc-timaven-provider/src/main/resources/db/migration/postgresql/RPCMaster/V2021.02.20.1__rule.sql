create table if not exists rule.rule
(
    id                        bigserial primary key,
    rounding_minutes          integer                                                not null default 0,
    rounding_mode             varchar                                                not null default 0,
    grace_in                  integer                                                not null default 0,
    grace_out                 integer                                                not null default 0,
    description               text,
    per_diem_threshold        integer,
    per_diem_mode             integer check (per_diem_mode in (0, 1, 2, 3) )         not null default 0,
    per_diem_weekly_threshold integer,
    rig_pay_threshold         integer                                                not null default 0,
    week_end_day              integer check ( week_end_day in (0, 1, 2, 3, 4, 5, 6)) not null default 6,
    equipment_workday_hour    numeric                                                not null default 8,
    equipment_exclude_weekend boolean                                                not null default true,
    equipment_cost_code       varchar,
    per_diem_cost_code        varchar,
    st_cost_code              varchar,
    ot_cost_code              varchar,
    dt_cost_code              varchar,
    sick_cost_code            varchar,
    holiday_cost_code         varchar,
    vacation_cost_code        varchar,
    other_cost_code           varchar,
    day_off_threshold         integer                                                         default 13,
    project_id                bigint unique,
    created_at                timestamp with time zone                                        default now()
);

comment on column rule.rule.per_diem_mode
    is '0: Pay as you go. 1: N+1. 2: 7 days. 3: Manual';

comment on column rule.rule.week_end_day
    is '0: Monday. 1: Tuesday. 2: Wednesday. 3: Thursday. 4: Friday. 5: Saturday. 6: Sunday.';

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'rule'
                    and table_schema = 'rule'
                    and column_name = 'project_id')
        then alter table rule.rule
            alter column project_id drop not null;
        end if;
        alter table rule.rule
            add column st_cost_code       varchar,
            add column ot_cost_code       varchar,
            add column dt_cost_code       varchar,
            add column sick_cost_code     varchar,
            add column holiday_cost_code  varchar,
            add column vacation_cost_code varchar,
            add column other_cost_code    varchar;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
