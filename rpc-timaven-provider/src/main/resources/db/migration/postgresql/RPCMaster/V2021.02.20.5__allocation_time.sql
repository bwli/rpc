create table if not exists time.allocation_time
(
    id                 bigserial primary key,
    emp_id             bigint  not null,
    team_name          varchar not null,
    first_name         varchar,
    last_name          varchar,
    department         varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    st_type            integer check ( st_type in (0, 1, 2, 3, 4) ) default 0,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool    not null                             default false,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    created_at         timestamp with time zone                     default now()
);

DO
$$
    BEGIN
        alter table time.allocation_time
            add column department varchar,
            add column job_number varchar,
            alter column team_name drop not null,
            add column st_type    int check ( st_type in (0, 1, 2, 3, 4) ) default 0;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_time'
                    and table_schema = 'time'
                    and column_name = 'st_type')
        then comment on column time.allocation_time.st_type
            is '0: st. 1: sick. 2: holiday. 3: vacation. 4: other';
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'columns already exists.';
    END ;
$$;
