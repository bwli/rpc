create table if not exists time.equipment_weekly_process
(
    id                  bigserial primary key,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    type                integer check (type in (0, 1)) default 0,
    project_id          bigint,
    billable_amount     numeric                        default 0,
    base_amount         numeric                        default 0,
    total_hours         numeric                        default 0,
    created_at          timestamp with time zone       default now(),
    unique (week_end_date, project_id, type)
);

comment on column time.equipment_weekly_process.type
    is '0:Normal, 1:Time left off';
