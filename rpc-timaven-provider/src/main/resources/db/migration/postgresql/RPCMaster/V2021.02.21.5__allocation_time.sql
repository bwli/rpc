create table if not exists time.allocation_time
(
    id                 bigserial primary key,
    emp_id             bigint  not null,
    team_name          varchar not null,
    first_name         varchar,
    last_name          varchar,
    department         varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    st_type            integer check ( st_type in (0, 1, 2, 3, 4) ) default 0,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool    not null                             default false,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    weekly_process_id  bigint references time.weekly_process (id),
    submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    created_at         timestamp with time zone                     default now()
);

DO
$$
    BEGIN
        alter table time.allocation_time
            add column weekly_process_id bigint references time.weekly_process (id);
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column claim_number already exists in billing.invoice.';
    END;
$$;
