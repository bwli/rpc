create table if not exists time.allocation_submission
(
    id                   bigserial primary key,
    submit_user_id       bigint not null,
    approve_user_id      bigint,
    finalize_user_id     bigint,
    status               integer check ( status in (0, 1, 2, 3, 4, 5)) default 0,
    report_submission_id bigint references time.report_submission (id) on delete cascade on update cascade,
    team_name            varchar,
    date_of_service      date,
    approved_at          timestamp with time zone,
    finalized_at         timestamp with time zone,
    project_id           bigint,
    created_at           timestamp with time zone                      default now()
);

comment on column time.allocation_submission.status
    is '0:pending, 1:released, 2:approved, 3:denied, 4:approved pending';

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_submission'
                    and table_schema = 'time'
                    and column_name = 'weekly_approve_user_id')
        then alter table time.allocation_submission
            drop column weekly_approve_user_id;
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_submission'
                    and table_schema = 'time'
                    and column_name = 'weekly_approved_at')
        then alter table time.allocation_submission
            drop column weekly_approved_at;
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_submission'
                    and table_schema = 'time'
                    and column_name = 'weekly_process_id')
        then alter table time.allocation_submission
            drop column weekly_process_id;
        end if;
    END ;
$$;

