create table if not exists time.weekly_process
(
    id                  bigserial primary key,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    type                integer check (type in (0, 1)) default 0,
    project_id          bigint,
    billable_amount     numeric                        default 0,
    base_amount         numeric                        default 0,
    total_hours         numeric                        default 0,
    taxable_per_diem    boolean not null               default false,
    created_at          timestamp with time zone       default now(),
    unique (week_end_date, project_id, type)
);

DO
$$
    BEGIN
        alter table time.weekly_process
            add column type integer check (type in (0, 1)) default 0,
            drop constraint if exists weekly_process_week_end_date_project_id_key;
        comment on column time.weekly_process.type
            is '0:Normal, 1:Time left off';
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists in billing.invoice.';
    END;
$$;

DO
$$
    BEGIN
        create unique index if not exists week_end_date_project_id_type_not_null_uni_idx
            on time.weekly_process (week_end_date, project_id, type)
        where project_id is not null;
        create unique index if not exists week_end_date_project_id_type_null_uni_idx
            on time.weekly_process (week_end_date, type)
        where project_id is null;
    END;
$$;
