create table if not exists project.equipment
(
    id            BIGSERIAL primary key,
    equipment_id  varchar unique not null,
    description   varchar,
    alias         varchar,
    class         varchar       not null,
    serial_number varchar,
    department    varchar,
    type          integer       not null   default 0,
    emp_id        varchar,
    is_active     boolean       not null   default true,
    created_at    timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'equipment'
                    and table_schema = 'project'
                    and column_name = 'equipment_id')
        then alter table project.equipment
            alter column equipment_id type varchar;
        end if;
    END ;
$$;
