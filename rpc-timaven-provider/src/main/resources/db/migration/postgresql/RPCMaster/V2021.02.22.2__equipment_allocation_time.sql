create table if not exists time.equipment_allocation_time
(
    id                bigserial primary key,
    equipment_id      varchar  not null,
    description       varchar,
    alias             varchar,
    class             varchar not null,
    serial_number     varchar,
    department        varchar,
    type              integer not null         default 0,
    emp_id            varchar,
    total_hour        numeric,
    payroll_date      date,
    weekly_process_id bigint references time.weekly_process (id),
    submission_id     bigint  not null references time.equipment_allocation_submission (id) on update cascade on delete cascade,
    created_at        timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'equipment_allocation_time'
                    and table_schema = 'time'
                    and column_name = 'equipment_id')
        then alter table time.equipment_allocation_time
            alter column equipment_id type varchar;
        end if;
    END ;
$$;
