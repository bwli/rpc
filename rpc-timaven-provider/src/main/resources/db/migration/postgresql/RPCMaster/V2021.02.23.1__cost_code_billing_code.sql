create table if not exists project.cost_code_billing_code
(
    id              bigserial primary key,
    cost_code_id    bigint not null references project.cclg (id) on update cascade on delete cascade,
    billing_code_id bigint not null references project.billing_code (id) on update cascade on delete cascade,
    create_at       timestamp with time zone default now(),
    unique (cost_code_id, billing_code_id)
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'cost_code_billing_code'
                    and table_schema = 'project'
                    and column_name = 'cost_code')
        then
            alter table project.cost_code_billing_code
                drop constraint if exists cost_code_billing_code_cost_code_billing_code_id_key,
                add column cost_code_id bigint references project.cclg (id) on update cascade on delete cascade,
                add unique (cost_code_id, billing_code_id);
            delete from project.cost_code_billing_code
            where cost_code not in (select distinct on (cost_code_full) cost_code_full
                                    from project.cclg
                                    order by cost_code_full, project_id nulls first, created_at desc);
            with t0 as (
                select distinct on (cost_code_full) id, cost_code_full
                from project.cclg
                order by cost_code_full, project_id nulls first, created_at desc
            )
            update project.cost_code_billing_code ccbc
            set cost_code_id = t0.id
            from t0
            where t0.cost_code_full = ccbc.cost_code;
            alter table project.cost_code_billing_code
                alter column cost_code_id set not null,
                drop column cost_code;
        end if;
    END ;
$$;
