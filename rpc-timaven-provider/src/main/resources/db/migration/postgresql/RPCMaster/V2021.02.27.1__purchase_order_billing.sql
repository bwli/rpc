create table if not exists project.purchase_order_billing
(
    id                    bigserial primary key,
    purchase_order_number varchar not null,
    line_number           int not null,
    billing_quantity      int,
    markup_quantity       int,
    markup_amount         numeric,
    billed_on             date,
    created_at            timestamp with time zone default now(),
    unique (purchase_order_number, line_number)
);
