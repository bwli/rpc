drop view if exists project.purchase_order_summary cascade;
create or replace view project.purchase_order_summary as
select po.purchase_order_number,
       po.purchase_order_date,
       po.vendor_name,
       po.requestor,
       po.job_number,
       po.sub_job,
       po.submission_id,
       sum(case when po.extended_cost is null then 0 else po.extended_cost end) as total_extended_cost,
       sum(coalesce(po.quantity, 0))::int                                       as quantity,
       sum(coalesce(pob.billing_quantity, 0))::int                              as quantity_billed,
       sum(coalesce(pob.markup_quantity, 0) * po.unit_cost * (1 + ((coalesce(pob.markup_amount, 0)::decimal / 100))) +
           (pob.billing_quantity - pob.markup_quantity) * po.unit_cost)         as amount_billed,
       pob.billed_on
from project.purchase_order po
         left join project.purchase_order_billing pob
                   on po.purchase_order_number = pob.purchase_order_number and po.line_number = pob.line_number
group by po.purchase_order_number, po.purchase_order_date, po.vendor_name, po.requestor, po.job_number, po.sub_job,
         po.submission_id, pob.billed_on;
