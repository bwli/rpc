create table if not exists project.cclg
(
    id             bigserial primary key,
    cost_code_full varchar not null,
    description    varchar,
    project_id     bigint,
    start_date     date,
    end_date       date,
    is_locked      boolean not null         default false,
    created_at     timestamp with time zone default now()
);
alter table project.cclg add constraint cost_project_unique unique (cost_code_full,project_id);
DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'cclg'
                        and table_schema = 'project'
                        and column_name = 'is_locked')
        then alter table project.cclg
            add column is_locked boolean not null default false;
        end if;
        comment on column project.cclg.is_locked is 'Locked if the code is already used.';
        with t0 as (
            select distinct ccp.cost_code_full, project_id
            from time.cost_code_perc ccp
                     inner join time.allocation_time t on t.id = ccp.allocate_time_id
                     inner join time.allocation_submission s on t.submission_id = s.id
            where s.status = 2
        )
        update project.cclg
        set is_locked = true
        from t0
        where t0.cost_code_full = cclg.cost_code_full
        and (t0.project_id is null and cclg.project_id is null or t0.project_id = cclg.project_id);
    END ;
$$;
