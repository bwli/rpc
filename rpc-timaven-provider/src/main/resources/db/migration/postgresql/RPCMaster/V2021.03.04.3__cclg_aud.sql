create table if not exists project.cclg_aud
(
    id             bigint  not null,
    rev            integer not null references public.custom_revision_entity (id),
    revtype        smallint,
    cost_code_full varchar(255),
    description    varchar(255),
    project_id     bigint,
    start_date     date,
    end_date       date,
    is_locked      boolean,
    created_at     timestamp,
    constraint cclg_aud_pkey
        primary key (id, rev)
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'cclg_aud'
                        and table_schema = 'project'
                        and column_name = 'is_locked')
        then alter table project.cclg_aud
            add column is_locked boolean;
        end if;
        update project.cclg_aud
        set is_locked = false where 1 = 1;
        with t0 as (
            select distinct ccp.cost_code_full, project_id
            from time.cost_code_perc ccp
                     inner join time.allocation_time t on t.id = ccp.allocate_time_id
                     inner join time.allocation_submission s on t.submission_id = s.id
            where s.status = 2
        )
        update project.cclg_aud
        set is_locked = true
        from t0
        where t0.cost_code_full = cclg_aud.cost_code_full
        and (t0.project_id is null and cclg_aud.project_id is null or t0.project_id = cclg_aud.project_id);
    END ;
$$;
