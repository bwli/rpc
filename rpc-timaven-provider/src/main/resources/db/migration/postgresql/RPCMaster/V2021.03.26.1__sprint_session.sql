create table if not exists public.spring_session
(
    primary_id            char(36) not null,
    session_id            char(36) not null,
    creation_time         bigint   not null,
    last_access_time      bigint   not null,
    max_inactive_interval int      not null,
    expiry_time           bigint   not null,
    principal_name        text,
    constraint spring_session_pk primary key (primary_id)
);


DO
$$
    BEGIN
        if exists(select *
                      from information_schema.columns
                      where table_name = 'spring_session'
                        and table_schema = 'public')
        then alter table public.spring_session
            alter column principal_name type text;
        end if;
    END;
$$;

