create table if not exists project.employee
(
    id            BIGSERIAL primary key,
    emp_id        varchar not null,
    client_emp_id varchar,
    craft         varchar,
    client_craft  varchar,
    first_name    varchar,
    last_name     varchar,
    middle_name   varchar,
    department    varchar,
    job_number    varchar,
    gender        varchar check (gender in ('f', 'm')),
    company       varchar,
    badge         varchar,
    team_name     varchar,
    crew          varchar,
    dob           date,
    effected_on   date,
    terminated_at date,
    rehired_at    date,
    hired_at      date,
    has_per_diem  boolean,
    has_rig_pay   boolean,
    is_active     boolean not null         default true,
    project_id    bigint,
    created_at    timestamp with time zone default now()
);

comment on column project.employee.is_active
    is 'decide whether to use this record or not';

DO
$$
    BEGIN
        if exists(select *
                      from information_schema.columns
                      where table_name = 'employee'
                        and table_schema = 'project'
                        and column_name = 'badge')
        then alter table project.employee
            alter column badge type varchar;
        end if;
    END;
$$;
