-- spring_session has been moved to master database so that it's shared by all tenants
drop table if exists public.spring_session_attributes;
drop table if exists public.spring_session;
