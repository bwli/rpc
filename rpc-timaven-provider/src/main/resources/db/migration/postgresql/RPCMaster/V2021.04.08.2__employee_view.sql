drop view if exists project.employee_view cascade;
create or replace view project.employee_view as
select distinct on (e.emp_id, e.effected_on) e.*
from project.employee e
where e.is_active
order by e.emp_id, e.effected_on, e.created_at desc;
