create table if not exists time.allocation_time
(
    id                 bigserial primary key,
    emp_id             bigint  not null,
    team_name          varchar not null,
    first_name         varchar,
    last_name          varchar,
    department         varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    extra_time_type    integer                  default 0,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool    not null         default false,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    mob_cost_code      varchar,
    mob_amount         numeric not null         default 0,
    submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    created_at         timestamp with time zone default now()
);

DO
$$
    BEGIN

        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_time'
                    and table_schema = 'time'
                    and column_name = 'st_type')
        then alter table time.allocation_time
            rename st_type to extra_time_type;
        end if;
        alter table time.allocation_time
            drop constraint if exists allocation_time_st_type_check;
    END ;
$$;
