-- schedule_time and employee_team is one to one relationship
create table if not exists project.schedule_time
(
    id             bigserial primary key,
    shift          varchar,
    schedule_start time,
    schedule_end   time,
    lunch_start    time,
    lunch_end      time,
    emp_id         varchar not null,
    project_id     bigint,
    created_at     timestamp with time zone default now(),
    unique (emp_id, project_id)
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'schedule_time'
                        and table_schema = 'project'
                        and column_name = 'emp_id')
        then alter table project.schedule_time
            add column emp_id     varchar not null default '',
            add column project_id bigint;
            update project.schedule_time s
            set emp_id     = e.emp_id,
                project_id = e.project_id
            from project.employee e
            where s.employee_id = e.id;
            delete
            from project.schedule_time a
                using project.schedule_time b
            where a.id < b.id
              and (a.emp_id = b.emp_id and a.project_id = b.project_id);
        end if;
        alter table project.schedule_time
            drop constraint if exists schedule_time_emp_id_project_id_key;
        alter table project.schedule_time
            add constraint schedule_time_emp_id_project_id_key unique (emp_id, project_id);
        alter table project.schedule_time
            drop column if exists employee_id;
    END ;
$$;