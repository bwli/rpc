create table if not exists project.equipment_price
(
    id           BIGSERIAL primary key,
    class        varchar not null,
    description  varchar,
    project_id   bigint  not null,
    hourly_rate  numeric                  default 0,
    daily_rate   numeric                  default 0,
    weekly_rate  numeric                  default 0,
    monthly_rate numeric                  default 0,
    created_at   timestamp with time zone default now(),
    unique (class, project_id)
);

DO
$$
    BEGIN
        alter table project.equipment_price
            add column description varchar;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
