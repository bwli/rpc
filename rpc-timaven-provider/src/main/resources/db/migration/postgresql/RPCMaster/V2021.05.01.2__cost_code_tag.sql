create table if not exists project.cost_code_tag
(
    id          bigserial primary key,
    type_id     bigint not null references project.cost_code_tag_type (id) on update cascade on delete cascade,
    code_name   varchar,
    description varchar,
    create_at   timestamp with time zone default now(),
    unique (type_id, code_name)
);

