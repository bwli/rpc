create table if not exists time.cost_code_tag_association
(
    id               bigserial primary key,
    tag_type         varchar,
    type_description varchar,
    code_name        varchar,
    code_description varchar,
    cost_code_type   int    not null          default 0,
    key              bigint not null,
    created_at       timestamp with time zone default now()
);
comment on column time.cost_code_tag_association.cost_code_type
    is '0: default, 1: perdiem, 2: mob';