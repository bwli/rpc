create table if not exists pm.project
(
    id          bigserial primary key,
    name        varchar,
    description varchar,
    user_id     bigint,
    job_number  varchar,
    sub_job     varchar,
    is_active   boolean not null         default true,
    is_deleted  boolean not null         default false,
    create_at   timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table pm.project
            drop constraint if exists project_sub_job_key;

        create unique index if not exists project_job_number_not_null_uni_idx
            on pm.project (job_number, sub_job)
            where project.job_number is not null;

        create unique index if not exists project_job_number_null_uni_idx
            on pm.project (sub_job)
            where project.job_number is null;
    END;
$$;
