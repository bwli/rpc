DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'purchase_order_billing'
                    and table_schema = 'project')
        then
            if not exists(select *
                          from information_schema.columns
                          where table_name = 'purchase_order_billing'
                            and table_schema = 'project'
                            and column_name = 'billing_number')
            then alter table project.purchase_order_billing
                drop constraint if exists purchase_order_billing_purchase_order_number_line_number_key;
                alter table project.purchase_order_billing
                    rename to purchase_order_billing_detail;
            end if;
        end if;
    END
$$;

create table if not exists project.purchase_order_billing_detail
(
    id                    bigserial primary key,
    purchase_order_number varchar not null,
    line_number           int     not null,
    part                  varchar,
    price                 numeric,
    billing_quantity      numeric,
    markup_quantity       numeric,
    markup_amount         numeric,
    billing_id            bigint  not null references project.purchase_order_billing (id) on delete cascade on update cascade,
    tax                   numeric,
    created_at            timestamp with time zone default now()
);

