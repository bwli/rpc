drop view if exists project.purchase_order_billing_detail_view cascade;
create or replace view project.purchase_order_billing_detail_view as
select row_number() over () as id,
       *
from (
         select *
         from (
                  select pob.id                                                        as billing_id,
                         pob.purchase_order_number,
                         pob.billing_number,
                         pob.invoice_number,
                         pobd.part,
                         pobd.billing_quantity,
                         coalesce(pobd.markup_quantity, 0) * coalesce(pobd.price, 0) *
                         (1 + ((coalesce(pobd.markup_amount, 0)::decimal / 100))) +
                         (coalesce(pobd.billing_quantity, 0) - coalesce(pobd.markup_quantity, 0)) *
                         coalesce(pobd.price, 0)                                       as amount,
                         (coalesce(pobd.markup_quantity, 0) * coalesce(pobd.price, 0) *
                          (1 + ((coalesce(pobd.markup_amount, 0)::decimal / 100))) +
                          (coalesce(pobd.billing_quantity, 0) - coalesce(pobd.markup_quantity, 0)) *
                          coalesce(pobd.price, 0)) * (coalesce(tax, 0)::decimal / 100) as tax,
                         pob.created_at,
                         1                                                             as level
                  from project.purchase_order_billing pob
                           left join project.purchase_order_billing_detail pobd
                                     on pob.id = pobd.billing_id
                  order by pobd.part) as t1
         union all
         select pob2.id               as billing_id,
                pob2.purchase_order_number,
                pob2.billing_number,
                pob2.invoice_number,
                'Freight'             as part,
                1                     as billing_quantity,
                pob2.freight_shipping as amount,
                0                     as tax,
                pob2.created_at,
                2                     as level
         from project.purchase_order_billing pob2
         where pob2.freight_shipping is not null
         union all
         select pob3.id                  as billing_id,
                pob3.purchase_order_number,
                pob3.billing_number,
                pob3.invoice_number,
                pob3.other_charge        as part,
                1                        as billing_quantity,
                pob3.other_charge_amount as amount,
                0                        as tax,
                pob3.created_at,
                3                        as level
         from project.purchase_order_billing pob3
         where pob3.other_charge is not null) as t0
order by t0.billing_id desc, level, part;


