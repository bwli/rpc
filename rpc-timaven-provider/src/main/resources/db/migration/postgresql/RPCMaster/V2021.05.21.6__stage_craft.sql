create table if not exists stage.craft
(
    id            bigserial primary key,
    submission_id bigint references stage.submission (id) on delete cascade on update cascade,
    craft_code    varchar,
    description   varchar,
    billable_st   numeric                  default 0,
    billable_ot   numeric                  default 0,
    billable_dt   numeric                  default 0,
    per_diem      numeric,
    rig_pay       numeric,
    created_at    timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table stage.craft
            drop column if exists base_st,
            drop column if exists base_ot,
            drop column if exists base_dt;
    END ;
$$;
