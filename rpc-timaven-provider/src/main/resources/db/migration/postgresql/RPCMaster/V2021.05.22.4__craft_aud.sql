create table if not exists project.craft_aud
(
    id          bigint  not null,
    rev         integer not null references public.custom_revision_entity (id),
    revtype     smallint,
    billable_dt numeric(19, 2),
    billable_ot numeric(19, 2),
    billable_st numeric(19, 2),
    code        varchar(255),
    description varchar(255),
    per_diem    numeric(19, 2),
    per_diem_id bigint,
    rig_pay     numeric(19, 2),
    project_id  bigint,
    start_date  date,
    end_date    date,
    created_at  timestamp,
    constraint craft_aud_pkey
        primary key (id, rev)
);

DO
$$
    BEGIN
        alter table project.craft_aud
            add column if not exists per_diem_id bigint;
    END ;
$$;
