create table if not exists project.employee
(
    id              BIGSERIAL primary key,
    emp_id          varchar not null,
    client_emp_id   varchar,
    craft           varchar,
    per_diem_id     bigint,
    base_st         numeric                  default 0,
    base_ot         numeric                  default 0,
    base_dt         numeric                  default 0,
    holiday_rate    numeric                  default 0,
    sick_leave_rate numeric                  default 0,
    travel_rate     numeric                  default 0,
    vacation_rate   numeric                  default 0,
    client_craft    varchar,
    first_name      varchar,
    last_name       varchar,
    middle_name     varchar,
    department      varchar,
    job_number      varchar,
    gender          varchar check (gender in ('f', 'm')),
    company         varchar,
    badge           varchar,
    team_name       varchar,
    crew            varchar,
    dob             date,
    effected_on     date,
    terminated_at   date,
    rehired_at      date,
    hired_at        date,
    has_per_diem    boolean,
    has_rig_pay     boolean,
    is_active       boolean not null         default true,
    project_id      bigint,
    created_at      timestamp with time zone default now()
);

comment on column project.employee.is_active
    is 'decide whether to use this record or not';

DO
$$
    BEGIN
        alter table project.employee
            add column per_diem_id bigint;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
