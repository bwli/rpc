DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'purchase_order_comment'
                    and table_schema = 'project')
        then
            alter table project.purchase_order_comment
                rename to purchase_order_group;
            alter table project.purchase_order_group
                add column flag boolean;
        end if;
    END;
$$;

create table if not exists project.purchase_order_group
(
    id                    bigserial primary key,
    purchase_order_number varchar not null unique,
    comment               varchar,
    flag                  boolean,
    created_at            timestamp with time zone default now()
);