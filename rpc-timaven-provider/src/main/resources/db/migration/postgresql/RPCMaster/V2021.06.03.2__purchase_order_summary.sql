drop view if exists project.purchase_order_summary cascade;
create or replace view project.purchase_order_summary as
with t1 as (
    select po.purchase_order_number,
           po.purchase_order_date,
           po.vendor_name,
           po.requestor,
           po.job_number,
           po.sub_job,
           po.submission_id,
           po.requisition_number,
           sum(coalesce(po.extended_cost, 0))     as total_extended_cost,
           sum(coalesce(po.quantity, 0))::numeric as quantity
    from project.purchase_order po
    group by po.purchase_order_number, po.purchase_order_date, po.vendor_name, po.requestor, po.job_number, po.sub_job,
             po.submission_id, po.requisition_number
)
select t1.purchase_order_number,
       t1.purchase_order_date,
       t1.vendor_name,
       t1.requestor,
       t1.job_number,
       t1.sub_job,
       t1.submission_id,
       t1.quantity,
       t1.total_extended_cost,
       pog.comment,
       coalesce(pog.flag, t1.total_extended_cost <> sum(coalesce(r.extended_cost, 0))) as flag,
       sum(t0.quantity_billed)  as quantity_billed,
       sum(t0.amount_billed)    as amount_billed,
       max(t0.created_at::date) as billed_on
from t1
         left join (select pob.id,
                           pob.purchase_order_number,
                           pob.created_at,
                           sum(coalesce(pobd.billing_quantity, 0))::numeric as quantity_billed,
                           sum((coalesce(pobd.markup_quantity, 0) * coalesce(po.unit_cost, 0) *
                                (1 + ((coalesce(pobd.markup_amount, 0)::decimal / 100))) +
                                (coalesce(pobd.billing_quantity, 0) - coalesce(pobd.markup_quantity, 0)) *
                                coalesce(po.unit_cost, 0)) * (1 + coalesce(pobd.tax, 0)::decimal / 100))
                               + coalesce(pob.freight_shipping, 0) +
                           coalesce(pob.other_charge_amount, 0)             as amount_billed,
                           max(pob.created_at::date)                        as billed_on
                    from project.purchase_order_billing pob
                             inner join project.purchase_order po
                                        on po.purchase_order_number = pob.purchase_order_number and
                                           po.submission_id =
                                           (select max(submission_id) from project.purchase_order)
                             inner join project.purchase_order_billing_detail pobd
                                        on pob.id = pobd.billing_id and pobd.line_number = po.line_number
                    group by pob.id, pob.purchase_order_number, pob.created_at) as t0
                   on t0.purchase_order_number = t1.purchase_order_number
         left join project.purchase_order_group pog
                   on t1.purchase_order_number = pog.purchase_order_number
         left join project.requisition r
                   on r.purchase_order_number = t1.purchase_order_number
                       and r.job_number = t1.job_number and r.sub_job = t1.sub_job
                       and r.requisition_number = t1.requisition_number
group by t1.purchase_order_number,
         t1.purchase_order_date,
         t1.vendor_name,
         t1.requestor,
         t1.job_number,
         t1.sub_job,
         t1.submission_id,
         t1.total_extended_cost,
         t1.quantity,
         pog.comment,
         pog.flag,
         r.requisition_number
order by t1.purchase_order_number
