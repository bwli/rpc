drop view if exists project.cost_code_export cascade;
create or replace view project.cost_code_export as
SELECT row_number() OVER () AS row_num,
       sub.id,
       sub.cost_code_full,
       sub.description,
       sub.job_number,
       sub.sub_job,
       sub.project_id,
       sub.start_date,
       sub.end_date,
       sub.created_at,
       sub.level,
       sub.code_name,
       sub.billing_code_type_id
FROM ( SELECT cc.id,
              cc.cost_code_full,
              cc.description,
              p.job_number,
              p.sub_job,
              cc.project_id,
              bct.level,
              bct.id AS billing_code_type_id,
              bc.code_name,
              cc.start_date,
              cc.end_date,
              cc.created_at
       FROM ( SELECT cclg.id,
                     cclg.cost_code_full,
                     cclg.project_id,
                     cclg.description,
                     cclg.start_date,
                     cclg.end_date,
                     cclg.created_at
              FROM project.cclg
                       LEFT JOIN project.cclg_tag ct ON ct.cost_code_full::text = cclg.cost_code_full::text AND (ct.project_id IS NULL AND cclg.project_id IS NULL OR ct.project_id = cclg.project_id)
              WHERE cclg.start_date <= CURRENT_DATE AND (cclg.end_date IS NULL OR cclg.end_date > CURRENT_DATE)
              GROUP BY cclg.id, cclg.cost_code_full, cclg.project_id, cclg.description, cclg.start_date, cclg.end_date, cclg.created_at
              ORDER BY cclg.created_at DESC) cc
                LEFT JOIN project.cost_code_billing_code ccbc ON cc.id = ccbc.cost_code_id
                LEFT JOIN project.billing_code bc ON ccbc.billing_code_id = bc.id
                LEFT JOIN project.billing_code_type bct ON bc.type_id = bct.id
                LEFT JOIN pm.project p ON cc.project_id = p.id) sub
ORDER BY sub.cost_code_full, sub.project_id NULLS FIRST;