create table if not exists rule.rule
(
    id                          bigserial primary key,
    rounding_minutes            integer                                                not null default 0,
    rounding_mode               varchar                                                not null default 0,
    grace_in                    integer                                                not null default 0,
    grace_out                   integer                                                not null default 0,
    description                 text,
    per_diem_id                 bigint references rule.per_diem (id) on update cascade,
    rig_pay_threshold           integer                                                not null default 0,
    week_end_day                integer check ( week_end_day in (0, 1, 2, 3, 4, 5, 6)) not null default 6,
    equipment_workday_hour      numeric                                                not null default 8,
    equipment_exclude_weekend   boolean                                                not null default true,
    equipment_cost_code         varchar,
    st_cost_code                varchar,
    ot_cost_code                varchar,
    dt_cost_code                varchar,
    sick_cost_code              varchar,
    holiday_cost_code           varchar,
    vacation_cost_code          varchar,
    other_cost_code             varchar,
    day_off_threshold           integer                                                         default 13,
    project_id                  bigint unique,
    taxable_per_diem            boolean                                                not null default false,
    mob_mileage_rate            numeric                                                not null default 0,
    hide_dt                     boolean                                                         default false,
    per_diem_by_labor_cost_code boolean                                                         default false,
    created_at                  timestamp with time zone                                        default now()
);

comment on column rule.rule.week_end_day
    is '0: Monday. 1: Tuesday. 2: Wednesday. 3: Thursday. 4: Friday. 5: Saturday. 6: Sunday.';

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'rule'
                        and table_schema = 'rule'
                        and column_name = 'per_diem_by_labor_cost_code')
        then alter table rule.rule
            add column per_diem_by_labor_cost_code boolean default false;
        end if;
    END;
$$;
