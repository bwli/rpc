create table if not exists rule.per_diem
(
    id               bigserial primary key,
    name             varchar,
    mode             integer check (mode in (0, 1, 2, 3) ) not null default 0,
    cost_code        varchar,
    daily_threshold  integer,
    weekly_threshold integer,
    project_id       bigint,
    is_active        boolean                               not null default true,
    overwrite_amount numeric,
    created_at       timestamp with time zone                       default now()
);

comment on column rule.per_diem.mode
    is '0: Pay as you go. 1: N+1. 2: 7 days. 3: Manual';

DO
$$
    BEGIN
        alter table rule.per_diem
            add column overwrite_amount numeric;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
