create table if not exists pm.project
(
    id                     bigserial primary key,
    name                   varchar,
    description            varchar,
    job_number             varchar,
    parent_job_name        varchar,
    parent_job_description varchar,
    sub_job                varchar,
    is_active              boolean not null         default true,
    is_deleted             boolean not null         default false,
    client_name            varchar,
    city                   varchar,
    state                  varchar,
    create_at              timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table pm.project
            drop column if exists parent_job_name,
            drop column if exists parent_job_description,
            drop column if exists user_id,
            drop column if exists name,
            alter column job_number set not null;



        create unique index if not exists job_number_sub_job_not_null_uni_idx
            on pm.project (job_number, sub_job)
            where project.sub_job is not null and project.is_active and not project.is_deleted;

        create unique index if not exists job_number_sub_job_null_uni_idx
            on pm.project (job_number)
            where project.sub_job is null and project.is_active and not project.is_deleted;
    END;
$$;