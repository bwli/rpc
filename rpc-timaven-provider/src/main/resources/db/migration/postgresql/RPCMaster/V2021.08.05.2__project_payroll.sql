drop view if exists time.project_payroll cascade;
create or replace view time.project_payroll as
with t3 as (
    with t2 as (
        with t1 as (
            with t0 as (
                select distinct on (r.project_id) r.week_end_day, r.project_id
                from rule.rule r
            ),
                 t4 as (
                     select distinct on (s.project_id) s.project_id, s.date_of_service
                     from time.allocation_submission s
                     where s.status = 2
                     order by s.project_id, s.date_of_service
                 )
            select (date_trunc('week', t4.date_of_service) +
                    cast(t0.week_end_day || ' days' as interval))::date as start_date,
                   (date_trunc('week', current_date) + cast(t0.week_end_day || ' days' as interval) -
                    cast('7 days' as interval))::date                   as end_date,
                   t0.project_id
            from t0,
                 t4
            where t0.project_id = t4.project_id
        )
        select *
        from (select generate_series(t1.start_date, t1.end_date, interval '7 days')::date as week_end_date,
                     t1.project_id
              from t1) as tt
    )
    select t2.week_end_date - integer '6' as week_start_date, t2.week_end_date, t2.project_id
    from t2
)
select q1.week_end_date,
       q1.employee_count,
       q4.total_hours,
       q1.timesheets,
       q4.approved_at,
       q4.finalized_at,
       q1.project_id,
       coalesce(q4.type, 0) as type
from (
         select count(*) as employee_count, count(distinct team_name) as timesheets, week_end_date, project_id
         from (select distinct on (e.emp_id, t3.week_end_date, e.project_id) e.emp_id,
                                                                             t3.week_end_date,
                                                                             e.project_id,
                                                                             e.team_name
               from t3
                        left join project.employee e on
                       e.project_id = t3.project_id
                       and e.effected_on <= t3.week_end_date
                       and (e.terminated_at is null or e.terminated_at >= t3.week_end_date)
               order by e.emp_id, t3.week_end_date, e.project_id, e.created_at desc) as tt
         group by week_end_date, project_id
     ) q1
         left join
     (select wp.approved_at::date, wp.finalized_at::date, wp.total_hours, wp.project_id, t3.week_end_date, wp.type
      from t3
               left join time.weekly_process wp
                         on t3.project_id = wp.project_id and t3.week_end_date = wp.week_end_date) q4
     on (q1.project_id = q4.project_id and q1.week_end_date = q4.week_end_date)
order by q1.week_end_date desc;