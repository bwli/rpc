DO
$$
    BEGIN
        -- Add default corporate rule
        insert into rule.rule
            (description, rounding_mode, rig_pay_threshold)
        select 'Default rule', 'ROUNDING_TO_NEAREST_NTH_MINUTE', 999
        where not exists(select id from rule.rule where project_id is null);
        -- Add project rule if not exists
        with t0 as (
            select p.id
            from pm.project p
                     left join rule.rule r on p.id = r.project_id
            where r.id is null
        )
        insert
        into rule.rule
        (description, rounding_mode, rig_pay_threshold, week_end_day, equipment_workday_hour, equipment_exclude_weekend,
         project_id)
        select 'Default rule', 'ROUNDING_TO_NEAREST_NTH_MINUTE', 999, 6, 8, true, t0.id
        from t0;
        with t0 as (
            select p.id
            from pm.project p
                     left join rule.shift s on p.id = s.project_id
                     left join rule.rule r on p.id = r.project_id
            where s.id is null
              and (r.id is not null and r.project_id is not null)) -- not corporate
        insert
        into rule.shift
        (name, schedule_start, schedule_end, break_start, break_end, paid_break_duration, project_id)
        select 'DAY', '06:00:00', '17:00:00', '12:00:00', '12:30:00', 0, t0.id
        from t0;
        with t0 as (
            select s.id, s.project_id
            from rule.shift s
            where s.name = 'DAY'
        ),
             t1 as (
                 select p.id
                 from pm.project p
                          left join rule.project_shift ps on p.id = ps.project_id
                          left join rule.rule r on p.id = r.project_id
                 where ps.id is null
                   and (r.id is not null and r.project_id is not null) -- not corporate
             )
        insert
        into rule.project_shift
            (project_id, shift_id)
        select t1.id, t0.id
        from t0,
             t1
        where t0.project_id = t1.id;
    END
$$;
