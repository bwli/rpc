create table if not exists project.employee
(
    id              BIGSERIAL primary key,
    emp_id          varchar not null,
    client_emp_id   varchar,
    craft           varchar,
    per_diem_id     bigint,
    base_st         numeric                  default 0,
    base_ot         numeric                  default 0,
    base_dt         numeric                  default 0,
    holiday_rate    numeric                  default 0,
    sick_leave_rate numeric                  default 0,
    travel_rate     numeric                  default 0,
    vacation_rate   numeric                  default 0,
    client_craft    varchar,
    first_name      varchar,
    last_name       varchar,
    middle_name     varchar,
    department      varchar,
    job_number      varchar,
    gender          varchar check (gender in ('f', 'm')),
    company         varchar,
    badge           varchar,
    team_name       varchar,
    crew            varchar,
    dob             date,
    sign_in_sheet   varchar,
    effected_on     date,
    terminated_at   date,
    rehired_at      date,
    hired_at        date,
    has_per_diem    boolean,
    has_rig_pay     boolean,
    is_active       boolean not null         default true,
    project_id      bigint,
    shift           varchar,
    schedule_start  time,
    schedule_end    time,
    lunch_start     time,
    lunch_end       time,
    created_at      timestamp with time zone default now()
);

comment on column project.employee.is_active
    is 'decide whether to use this record or not';

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'employee'
                        and table_schema = 'project'
                        and column_name = 'shift')
        then alter table project.employee
            add column shift          varchar,
            add column schedule_start time,
            add column schedule_end   time,
            add column lunch_start    time,
            add column lunch_end      time;
            update project.employee e
            set shift          = s.shift,
                schedule_start = s.schedule_start,
                schedule_end   = s.schedule_end,
                lunch_start    = s.lunch_start,
                lunch_end      = s.lunch_end
            from project.schedule_time s
            where e.emp_id = s.emp_id
              and e.project_id = s.project_id;
            with t0 as (
                select s.project_id,
                       s.name,
                       s.schedule_start,
                       s.schedule_end,
                       s.break_start,
                       s.break_end
                from rule.shift s
                         inner join rule.project_shift ps on s.id = ps.shift_id
            )
            update project.employee e
            set shift          = t0.name,
                schedule_start = t0.schedule_start,
                schedule_end   = t0.schedule_end,
                lunch_start    = t0.break_start,
                lunch_end      = t0.break_end
            from t0
            where t0.project_id = e.project_id
              and e.shift is null;
            drop table if exists project.schedule_time;
        end if;
    END;
$$;
