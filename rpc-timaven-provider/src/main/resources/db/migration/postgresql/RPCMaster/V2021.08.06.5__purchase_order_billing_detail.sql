create table if not exists project.purchase_order_billing_detail
(
    id                    bigserial primary key,
    purchase_order_number varchar not null,
    line_number           int     not null,
    part                  varchar,
    unit_cost             numeric,
    billing_quantity      numeric,
    markup_quantity       numeric,
    markup_amount         numeric,
    billing_id            bigint  not null references project.purchase_order_billing (id) on delete cascade on update cascade,
    tax                   numeric,
    created_at            timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'purchase_order_billing_detail'
                    and table_schema = 'project'
                    and column_name = 'price')
        then
            alter table project.purchase_order_billing_detail
                rename column price to unit_cost;
        end if;
    END
$$;

