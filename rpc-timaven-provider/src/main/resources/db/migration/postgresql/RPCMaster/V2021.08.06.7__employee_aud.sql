create table if not exists project.employee_aud
(
    id              bigint  not null,
    rev             integer not null references public.custom_revision_entity (id),
    revtype         smallint,
    is_active       boolean,
    badge           varchar,
    team_name       varchar,
    crew            varchar,
    emp_id          varchar,
    client_emp_id   varchar,
    company         varchar(255),
    created_at      timestamp,
    dob             date,
    sign_in_sheet   varchar,
    first_name      varchar(255),
    gender          varchar(255),
    has_per_diem    boolean,
    has_rig_pay     boolean,
    hired_at        date,
    last_name       varchar(255),
    middle_name     varchar(255),
    department      varchar,
    job_number      varchar,
    project_id      bigint,
    rehired_at      date,
    effected_on     date,
    terminated_at   date,
    craft           varchar,
    per_diem_id     bigint,
    base_st         numeric,
    base_ot         numeric,
    base_dt         numeric,
    holiday_rate    numeric,
    sick_leave_rate numeric,
    travel_rate     numeric,
    vacation_rate   numeric,
    shift           varchar,
    schedule_start  time,
    schedule_end    time,
    lunch_start     time,
    lunch_end       time,
    constraint employee_aud_pkey
        primary key (id, rev)
);

DO
$$
    BEGIN
        alter table project.employee_aud
            add column shift          varchar,
            add column schedule_start time,
            add column schedule_end   time,
            add column lunch_start    time,
            add column lunch_end      time;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
