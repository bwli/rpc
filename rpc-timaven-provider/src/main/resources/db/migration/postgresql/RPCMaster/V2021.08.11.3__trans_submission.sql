create table if not exists time.trans_submission
(
    id           bigserial primary key,
    project_id   bigint not null,
    user_id      bigint not null,
    status       int    not null          default 0 check ( status in (0, 1)),
    processed_at timestamp with time zone,
    created_at   timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'trans_submission'
                        and table_schema = 'time'
                        and column_name = 'status')
        then alter table time.trans_submission
            add column status       int not null default 0 check ( status in (0, 1)),
            add column processed_at timestamp with time zone;
            update time.trans_submission
            set status       = 1,
                processed_at = s.created_at
            from time.allocation_submission s,
                 time.stage_trans st
            where s.project_id = trans_submission.project_id
              and s.team_name = st.team_name
              and s.date_of_service = st.date_of_service
              and st.submission_id = trans_submission.id;
        end if;
    END;
$$;

comment on column time.trans_submission.status
    is '0:pending, 1:processed';
