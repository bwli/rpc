create or replace function status_change_audit() returns trigger as
$change_audit$
begin
    update time.allocation_submission s
    set status_changed_at = now()
    where new.id = s.id;
    return new;
end;
$change_audit$ language plpgsql;

drop trigger if exists tr_update_status_changed_at on time.allocation_submission;
create trigger tr_update_status_changed_at
    after insert or update of status
    on time.allocation_submission
    for each row
execute procedure status_change_audit();
