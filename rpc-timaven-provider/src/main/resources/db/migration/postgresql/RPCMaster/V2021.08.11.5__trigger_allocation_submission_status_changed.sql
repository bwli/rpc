drop trigger if exists tr_update_status_changed_at on time.allocation_submission;
drop function if exists public.status_change_audit;
create or replace function time.status_change_audit() returns trigger as
$change_audit$
begin
    update time.allocation_submission s
    set status_changed_at = now()
    where new.id = s.id;
    return new;
end;
$change_audit$ language plpgsql;

create trigger tr_update_status_changed_at
    after insert or update of status
    on time.allocation_submission
    for each row
execute procedure time.status_change_audit();
