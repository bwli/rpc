create table if not exists time.allocation_time
(
    id                 bigserial primary key,
    emp_id             varchar,
    client_emp_id      varchar,
    badge              varchar,
    team_name          varchar not null,
    first_name         varchar,
    last_name          varchar,
    department         varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    extra_time_type    integer                  default 0,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool    not null         default false,
    has_mob            bool    not null         default false,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    mob_cost_code      varchar,
    per_diem_amount    numeric not null         default 0,
    mob_amount         numeric not null         default 0,
    mob_mileage_rate   numeric not null         default 0,
    mob_mileage        numeric not null         default 0,
    mob_display_order  int,
    submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    is_offsite         boolean not null         default false,
    created_at         timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table time.allocation_time
            add column mob_display_order int;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END ;
$$;
