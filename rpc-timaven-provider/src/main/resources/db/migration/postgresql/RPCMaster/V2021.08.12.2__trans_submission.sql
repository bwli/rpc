create table if not exists time.trans_submission
(
    id         bigserial primary key,
    project_id bigint not null,
    user_id    bigint not null,
    created_at timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'trans_submission'
                    and table_schema = 'time'
                    and column_name = 'status')
        then alter table time.trans_submission
            drop column status,
            drop column processed_at;
        end if;
    END;
$$;