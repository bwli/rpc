drop view if exists project.employee_view cascade;
create or replace view project.employee_view as
select t0.id,
       t0.emp_id,
       t0.client_emp_id,
       t0.craft,
       t0.per_diem_id,
       p.name                  as per_diem_name,
       t0.client_craft,
       t0.team_name,
       t0.crew,
       t0.project_id,
       t0.first_name,
       t0.last_name,
       t0.middle_name,
       t0.gender,
       t0.company,
       t0.badge,
       t0.sign_in_sheet,
       t0.department,
       t0.job_number,
       t0.hired_at,
       t0.effected_on,
       t0.terminated_at,
       t0.has_per_diem,
       t0.has_rig_pay,
       t0.shift,
       t0.schedule_start,
       t0.schedule_end,
       t0.lunch_start,
       t0.lunch_end,
       t0.is_offsite,
       string_agg(et.tag, ',') as tags
from (select *
      from (select distinct on (e.project_id, e.emp_id) e.*
            from project.employee e
            where e.is_active
              and e.effected_on <= current_date
            order by e.project_id, e.emp_id, e.created_at desc) as t1
      where (t1.terminated_at is null or t1.terminated_at >= current_date)) as t0
         left join project.employee_tag et on t0.emp_id = et.emp_id and
                                              (t0.project_id is null and et.project_id is null or
                                               t0.project_id = et.project_id)
         left join rule.per_diem p on t0.per_diem_id = p.id
group by t0.id, t0.emp_id, t0.client_emp_id, t0.craft, t0.per_diem_id, p.name, t0.client_craft, t0.team_name, t0.crew,
         t0.project_id,
         t0.first_name, t0.last_name, t0.middle_name, t0.gender, t0.company, t0.badge, t0.sign_in_sheet, t0.department,
         t0.job_number, t0.hired_at, t0.effected_on, t0.terminated_at, t0.has_per_diem, t0.has_rig_pay, t0.shift,
         t0.schedule_start, t0.schedule_end, t0.lunch_start, t0.lunch_end, t0.is_offsite
union all
select t0.id,
       t0.emp_id,
       t0.client_emp_id,
       t0.craft,
       t0.per_diem_id,
       p.name                  as per_diem_name,
       t0.client_craft,
       t0.team_name,
       t0.crew,
       t0.project_id,
       t0.first_name,
       t0.last_name,
       t0.middle_name,
       t0.gender,
       t0.company,
       t0.badge,
       t0.sign_in_sheet,
       t0.department,
       t0.job_number,
       t0.hired_at,
       t0.effected_on,
       t0.terminated_at,
       t0.has_per_diem,
       t0.has_rig_pay,
       t0.shift,
       t0.schedule_start,
       t0.schedule_end,
       t0.lunch_start,
       t0.lunch_end,
       t0.is_offsite,
       string_agg(et.tag, ',') as tags
from (select e.*
      from project.employee e
      where e.is_active
        and e.effected_on > current_date
        and (e.terminated_at is null or e.terminated_at >= current_date)
      order by e.project_id, e.emp_id, e.hired_at, e.created_at desc) as t0
         left join project.employee_tag et on t0.emp_id = et.emp_id and
                                              (t0.project_id is null and et.project_id is null or
                                               t0.project_id = et.project_id)
         left join rule.per_diem p on t0.per_diem_id = p.id
group by t0.id, t0.emp_id, t0.client_emp_id, t0.craft, t0.per_diem_id, p.name, t0.client_craft, t0.team_name, t0.crew,
         t0.project_id,
         t0.first_name, t0.last_name, t0.middle_name, t0.gender, t0.company, t0.badge, t0.sign_in_sheet, t0.department,
         t0.job_number, t0.hired_at, t0.effected_on, t0.terminated_at, t0.has_per_diem, t0.has_rig_pay, t0.shift,
         t0.schedule_start, t0.schedule_end, t0.lunch_start, t0.lunch_end, t0.is_offsite;
