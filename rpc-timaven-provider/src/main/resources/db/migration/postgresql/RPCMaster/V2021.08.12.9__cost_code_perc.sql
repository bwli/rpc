create table if not exists time.cost_code_perc
(
    id               bigserial primary key,
    cost_code_full   varchar,
    order_code       varchar,
    st_hour          numeric,
    ot_hour          numeric,
    dt_hour          numeric,
    total_hour       numeric,
    percentage       integer not null,
    allocate_time_id bigint  not null references time.allocation_time (id) on delete cascade on update cascade,
    per_diem_amount  numeric,
    display_order    int,
    created_at       timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'cost_code_perc'
                        and table_schema = 'time'
                        and column_name = 'display_order')
        then alter table time.cost_code_perc
            add column display_order int;
        end if;
    END;
$$;
