create table if not exists time.weekly_process
(
    id                  bigserial primary key,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    reject_user_id      bigint,
    reject_at           timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    type                integer check (type in (0, 1)) default 0,
    project_id          bigint,
    billable_amount     numeric                        default 0,
    base_amount         numeric                        default 0,
    total_hours         numeric                        default 0,
    taxable_per_diem    boolean not null               default false,
    comment             varchar,
    team_name           varchar,
    created_at          timestamp with time zone       default now(),
    unique (week_end_date, project_id, type)
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'weekly_process'
                        and table_schema = 'time'
                        and column_name = 'team_name')
        then alter table time.weekly_process
            add column team_name varchar;
            drop index if exists time.week_end_date_project_id_type_null_uni_idx;
            update time.weekly_process set team_name = 'Corporate' where project_id is null;
        end if;
    END;
$$;

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'weekly_process'
                    and table_schema = 'time'
                    and column_name = 'team_name')
        then create unique index if not exists week_end_date_project_id_type_null_uni_idx
            on time.weekly_process (week_end_date, team_name, type)
            where project_id is null;
        end if;
    END;
$$;
