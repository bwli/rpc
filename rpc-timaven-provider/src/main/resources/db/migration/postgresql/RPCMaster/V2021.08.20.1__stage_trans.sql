create table if not exists time.stage_trans
(
    id              bigserial primary key,
    submission_id   bigint  not null references time.trans_submission (id) on update cascade on delete cascade,
    first_name      varchar,
    last_name       varchar,
    middle_name     varchar,
    contract_id     bigint,
    date_of_service date,
    payroll_date    date,
    st_rate         numeric                  default 0,
    ot_rate         numeric                  default 0,
    dt_rate         numeric                  default 0,
    st_hour         numeric,
    ot_hour         numeric,
    dt_hour         numeric,
    net_hour        numeric,
    total_hour      numeric,
    area_id         bigint,
    work_unit_id    varchar,
    emp_id          varchar,
    client_emp_id   varchar,
    badge           varchar,
    team_name       varchar,
    company         varchar,
    pd              numeric,
    by_upload       boolean not null         default false,
    status          int     not null         default 0 check ( status in (0, 1)),
    created_at      timestamp with time zone default now()
);

comment on column time.stage_trans.emp_id
    is 'employee or equipment_id';
comment on column time.stage_trans.st_rate
    is 'standard hour';
comment on column time.stage_trans.ot_rate
    is 'overtime hour';
comment on column time.stage_trans.dt_rate
    is 'double hour';
comment on column time.stage_trans.st_hour
    is 'standard rate';
comment on column time.stage_trans.ot_hour
    is 'overtime rate';
comment on column time.stage_trans.dt_hour
    is 'double rate';
comment on column time.stage_trans.team_name
    is 'refer to team name';
comment on column time.stage_trans.pd
    is 'stands for per diem. refer to extra charge. often an employee, per day to cover living expenses when traveling for work';
DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'stage_trans'
                        and table_schema = 'time'
                        and column_name = 'by_upload')
        then alter table time.stage_trans
            add column by_upload boolean not null default false;
            update time.stage_trans set by_upload = true where 1 = 1;
        end if;
    END ;
$$;

comment on column time.stage_trans.status
    is '0:pending, 1:processed';

