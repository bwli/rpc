create table if not exists time.activity_code
(
    id              bigserial primary key,
    activity_code   varchar      not null
);

