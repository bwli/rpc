create table if not exists time.activity_code_perc
(
    id                bigserial primary key,
    emp_id             varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    total_hour         numeric,
    activity_code_id   bigint not null,
    allocation_time_id bigint not null,
    extra_time_type    integer                  default 0,
    activity_code      varchar,
    activity_id        bigint not null,
    created_at         timestamp with time zone default now()
);

