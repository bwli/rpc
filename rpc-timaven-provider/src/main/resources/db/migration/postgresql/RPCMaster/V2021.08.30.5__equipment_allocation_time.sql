create table if not exists time.equipment_allocation_time
(
    id                bigserial
        constraint equipment_allocation_time_pkey
            primary key,
    equipment_id      varchar                            not null,
    description       varchar,
    alias             varchar,
    class             varchar                            not null,
    serial_number     varchar,
    department        varchar,
    hourly_type       integer                  default 0 not null,
    emp_id            varchar,
    total_hour        numeric,
    payroll_date      date,
    submission_id     bigint                             not null
        constraint equipment_allocation_time_submission_id_fkey
            references time.equipment_allocation_submission
            on update cascade on delete cascade,
    created_at        timestamp with time zone default now(),
    weekly_process_id bigint
        constraint equipment_allocation_time_weekly_process_id_fkey
            references time.equipment_weekly_process,
    ownership_type    integer                  default 0 not null,
    total_charge      numeric
);

comment on column time.equipment_allocation_time.ownership_type is '0: Company Owned, 1: Rental';
