create table if not exists time.activity_code_perc_aud
(
    id                 bigint,
    rev                integer not null references public.custom_revision_entity (id),
    revtype            smallint,
    emp_id             varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    total_hour         numeric,
    activity_code_id   bigint,
    allocation_time_id bigint,
    extra_time_type    integer,
    activity_code      varchar,
    activity_id        bigint,
    created_at         timestamp with time zone default now(),
    constraint activity_code_perc_aud_pkey
        primary key (id, rev)
);
