create table if not exists time.cost_code_perc_aud
(
    id               bigint,
    rev              integer not null references public.custom_revision_entity (id),
    revtype          smallint,
    cost_code_full   varchar,
    order_code       varchar,
    st_hour          numeric,
    ot_hour          numeric,
    dt_hour          numeric,
    total_hour       numeric,
    percentage       integer,
    allocate_time_id bigint,
    per_diem_amount  numeric,
    display_order    int,
    created_at       timestamp with time zone,
    constraint cost_code_perc_aud_pkey
        primary key (id, rev)
);
