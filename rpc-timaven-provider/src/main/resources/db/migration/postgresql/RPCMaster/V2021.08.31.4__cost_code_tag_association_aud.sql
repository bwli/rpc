create table if not exists time.cost_code_tag_association_aud
(
    id               bigint,
    rev              integer not null references public.custom_revision_entity (id),
    revtype          smallint,
    tag_type         varchar,
    type_description varchar,
    code_name        varchar,
    code_description varchar,
    cost_code_type   int,
    key              bigint,
    created_at       timestamp with time zone,
    constraint cost_code_tag_association_aud_pkey
        primary key (id, rev)
);
