create table if not exists time.equipment_allocation_submission_aud
(
    id              bigint,
    rev             integer not null references public.custom_revision_entity (id),
    revtype         smallint,
    approve_user_id bigint,
    status          integer,
    date_of_service date,
    approved_at     timestamp with time zone,
    project_id      bigint,
    created_at      timestamp with time zone,
    constraint equipment_allocation_submission_aud_pkey
        primary key (id, rev)
);
