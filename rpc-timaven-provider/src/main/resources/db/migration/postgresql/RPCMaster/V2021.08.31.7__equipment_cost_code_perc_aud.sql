create table if not exists time.equipment_cost_code_perc_aud
(
    id               bigint,
    rev              integer not null references public.custom_revision_entity (id),
    revtype          smallint,
    cost_code_full   varchar,
    total_hour       numeric,
    total_charge     numeric,
    allocate_time_id bigint,
    created_at       timestamp with time zone,
    constraint equipment_cost_code_perc_aud_pkey
        primary key (id, rev)
);
