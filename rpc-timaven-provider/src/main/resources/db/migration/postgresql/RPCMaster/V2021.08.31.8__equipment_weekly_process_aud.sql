create table if not exists time.equipment_weekly_process_aud
(
    id                  bigint,
    rev                 integer not null references public.custom_revision_entity (id),
    revtype             smallint,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    type                integer,
    project_id          bigint,
    billable_amount     numeric,
    base_amount         numeric,
    total_hours         numeric,
    created_at          timestamp with time zone,
    constraint equipment_weekly_process_aud_pkey
        primary key (id, rev)
);
