create table if not exists time.weekly_process_aud
(
    id                  bigint,
    rev                 integer not null references public.custom_revision_entity (id),
    revtype             smallint,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    reject_user_id      bigint,
    reject_at           timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    type                integer,
    project_id          bigint,
    billable_amount     numeric,
    base_amount         numeric,
    total_hours         numeric,
    taxable_per_diem    boolean,
    comment             varchar,
    team_name           varchar,
    created_at          timestamp with time zone,
    constraint weekly_process_aud_pkey
        primary key (id, rev)
);
