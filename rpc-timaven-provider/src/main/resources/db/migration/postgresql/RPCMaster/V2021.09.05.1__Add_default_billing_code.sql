INSERT INTO project.billing_code_type (code_type, is_required)
VALUES ('Class', true)
ON CONFLICT DO NOTHING;

with t0 as
         (select id
          from project.billing_code_type bct
          where bct.code_type = 'Class')
insert
into project.billing_code (type_id, code_name, client_alias, description)
select id, code_name, client_alias, description
from t0,
     (values ('BE', 'BEREAVEMENT', 'BEREAVEMENT'),
             ('CB', 'JOB COMPLETION BONUS', 'JOB COMPLETION BONUS'),
             ('CV', 'COVID-19 SELF CARE', 'COVID-19 SELF CARE'),
             ('DT', 'DOUBLE TIME', 'DOUBLE TIME'),
             ('FM', 'COVID-19 FAMILY CARE', 'COVID-19 FAMILY CARE'),
             ('HL', 'HOLIDAY', 'HOLIDAY'),
             ('OT', 'OVERTIME', 'OVERTIME'),
             ('RB', 'RETENTION BONUS', 'RETENTION BONUS'),
             ('SL', 'SICK LEAVE', 'SICK LEAVE'),
             ('SV', 'SEVERANCE PAY', 'SEVERANCE PAY'),
             ('TS', 'TRAVEL STIPEND', 'TRAVEL STIPEND'),
             ('TV', 'TRAVEL HOURS', 'TRAVEL HOURS'),
             ('VA', 'VACATION', 'VACATION'),
             ('VP', 'VACATION PAYOUT', 'VACATION PAYOUT')) as t(code_name, client_alias, description)
