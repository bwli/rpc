drop view if exists project.craft_view cascade;
create or replace view project.craft_view as
with t1 as (
    select *
    from (
             select distinct on (c.project_id, c.code) c.*, p.name as per_diem_name
             from project.craft c
                      left join rule.per_diem p on c.per_diem_id = p.id
             where c.start_date <= current_date
               and (c.end_date is null or c.start_date <= c.end_date)
             order by c.project_id, c.code, c.created_at desc) as t0)
select *
from t1
where (t1.end_date is null or t1.end_date >= current_date)