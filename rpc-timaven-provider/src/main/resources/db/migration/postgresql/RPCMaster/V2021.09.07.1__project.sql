create table if not exists pm.project
(
    id          bigserial primary key,
    name        varchar,
    project     char,
    description varchar,
    user_id     bigint,
    job_number  varchar,
    parent_job_name varchar,
    parent_job_description varchar,
    sub_job     varchar,
    is_active   boolean not null         default true,
    is_deleted  boolean not null         default false,
    create_at   timestamp with time zone default now(),
    client_name varchar,
    city        varchar,
    state       varchar
);

DO
$$
    BEGIN
        ALTER TABLE pm.project
            ADD COLUMN IF NOT EXISTS divisions varchar,
            ADD COLUMN IF NOT EXISTS name varchar;
    EXCEPTION
                WHEN duplicate_column THEN RAISE NOTICE 'column already exists in pm.project.';
    END;
$$;