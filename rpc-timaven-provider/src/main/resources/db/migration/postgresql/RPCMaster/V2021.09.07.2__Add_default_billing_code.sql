
DELETE FROM project.billing_code_type;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (1,'Phase', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (2,'CC', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (3,'CT', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (4,'CB', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (5,'Cost Element', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (6,'Invoice', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (7,'Job#', true)
    ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (id,code_type, is_required)
VALUES (8,'Sub Job', true)
    ON CONFLICT DO NOTHING;
