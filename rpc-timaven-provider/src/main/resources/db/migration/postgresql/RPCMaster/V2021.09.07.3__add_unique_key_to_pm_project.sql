CREATE OR REPLACE FUNCTION pm.create_constriant_if_not_exists (
  t_name TEXT, c_name TEXT, constraint_sql TEXT
)
RETURNS void AS $$
BEGIN
  -- Look for our constraint
  IF NOT EXISTS (SELECT constraint_name
    FROM information_schema.constraint_column_usage
    WHERE table_schema = 'pm'
      AND table_name = t_name
      AND constraint_name = c_name)
  THEN
    EXECUTE constraint_sql;
  END IF;
END;
$$ LANGUAGE plpgsql;

SELECT pm.create_constriant_if_not_exists(
  'project', 'job_number_and_sub_job_unique_key',
  'ALTER TABLE pm.project ADD CONSTRAINT job_number_and_sub_job_unique_key UNIQUE (job_number, sub_job)'
);
