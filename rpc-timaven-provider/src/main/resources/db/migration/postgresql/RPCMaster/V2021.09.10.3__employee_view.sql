drop view if exists project.employee_view cascade;
create or replace view project.employee_view as
with t0 as (
    select distinct on (e.project_id, e.emp_id) e.*
    from project.employee e
    where e.is_active
      and e.effected_on <= current_date
    order by e.project_id, e.emp_id, e.created_at desc
)
select t0.id,
       t0.emp_id,
       t0.client_emp_id,
       t0.craft,
       t0.base_st,
       t0.base_ot,
       t0.base_dt,
       t0.holiday_rate,
       t0.sick_leave_rate,
       t0.travel_rate,
       t0.vacation_rate,
       case
           when t0.per_diem_id is not null then t0.per_diem_id
           when cv.per_diem_id is not null then cv.per_diem_id
           else r.per_diem_id end                  as per_diem_id_,
       case
           when t0.per_diem_id is not null then emp_pd.name
           when cv.per_diem_id is not null then craft_pd.name
           else rule_pd.name end                   as per_diem_name_,
       t0.client_craft,
       t0.team_name,
       t0.crew,
       t0.project_id,
       t0.first_name,
       t0.last_name,
       t0.middle_name,
       t0.gender,
       t0.company,
       t0.badge,
       t0.sign_in_sheet,
       t0.job_number,
       t0.hired_at,
       t0.effected_on,
       t0.terminated_at,
       t0.has_per_diem,
       t0.has_rig_pay,
       t0.shift,
       t0.schedule_start,
       t0.schedule_end,
       t0.lunch_start,
       t0.lunch_end,
       t0.is_offsite,
       t0.activity_code,
       string_agg(et.tag, ',')                     as tags
from t0
         left join project.craft_view cv on t0.project_id = cv.project_id and t0.craft = cv.code
         left join rule.per_diem craft_pd on cv.per_diem_id = craft_pd.id
         left join rule.rule r on t0.project_id = r.project_id
         left join rule.per_diem rule_pd on r.per_diem_id = rule_pd.id
         left join rule.per_diem emp_pd on t0.per_diem_id = emp_pd.id
         left join project.employee_tag et on t0.emp_id = et.emp_id and
                                              (t0.project_id is null and et.project_id is null or
                                               t0.project_id = et.project_id)
group by t0.id, t0.emp_id, t0.client_emp_id, t0.craft, per_diem_id_, per_diem_name_, t0.client_craft, t0.team_name,
         t0.crew,
         t0.project_id, t0.base_st, t0.base_ot, t0.base_dt, t0.holiday_rate, t0.sick_leave_rate, t0.travel_rate,
         t0.vacation_rate, t0.first_name, t0.last_name, t0.middle_name, t0.gender, t0.company, t0.badge,
         t0.sign_in_sheet, t0.job_number, t0.hired_at, t0.effected_on, t0.terminated_at, t0.has_per_diem,
         t0.has_rig_pay, t0.shift, t0.schedule_start, t0.schedule_end, t0.lunch_start, t0.lunch_end, t0.is_offsite, t0.activity_code