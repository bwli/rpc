create table if not exists time.allocation_time_aud
(
    id                 bigint,
    rev                integer not null references public.custom_revision_entity (id),
    revtype            smallint,
    emp_id             varchar,
    client_emp_id      varchar,
    badge              varchar,
    team_name          varchar,
    first_name         varchar,
    last_name          varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    extra_time_type    integer,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool,
    has_mob            bool,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    mob_cost_code      varchar,
    per_diem_amount    numeric,
    mob_amount         numeric,
    mob_mileage_rate   numeric,
    mob_mileage        numeric,
    mob_display_order  int,
    submission_id      bigint,
    is_offsite         boolean,
    weekly_process_id  bigint,
    created_at         timestamp with time zone,
    constraint allocation_time_aud_pkey
        primary key (id, rev)
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_time_aud'
                    and table_schema = 'time'
                    and column_name = 'department')
        then alter table time.allocation_time_aud
            drop column department;
        end if;
    END;
$$;
