create table if not exists stage.roster
(
    id              bigserial primary key,
    submission_id   bigint references stage.submission (id) on delete cascade on update cascade,
    badge           varchar,
    name            varchar,
    employee_id     varchar,
    team            varchar,
    crew            varchar,
    client_emp_id   varchar,
    client_craft    varchar,
    company         varchar,
    craft_code      varchar,
    base_st         numeric,
    base_ot         numeric,
    base_dt         numeric,
    holiday_rate    numeric,
    sick_leave_rate numeric,
    travel_rate     numeric,
    vacation_rate   numeric,
    has_per_diem    boolean,
    has_rig_pay     boolean,
    job_number      varchar,
    sign_in_sheet   varchar,
    hired_at        date,
    terminated_at   date,
    shift           varchar,
    schedule_start  time,
    schedule_end    time,
    lunch_start     time,
    lunch_end       time,
    created_at      timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'roster'
                    and table_schema = 'stage'
                    and column_name = 'department')
        then alter table stage.roster
            drop column department;
        end if;
    END;
$$;
