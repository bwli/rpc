drop view if exists project.billing_code_view cascade;
create or replace view project.billing_code_view as
select cast(bc.id as text) || '-' || cast((bco.id) as text) as id,
       bc.id          as billing_code_id,
       bct.id         as type_id,
       bct.code_type,
       bc.code_name,
       bco.client_alias,
       bco.description,
       bco.project_id as project_id,
       bco.id         as override_id
from project.billing_code bc
         inner join project.billing_code_type bct on bc.type_id = bct.id
         inner join project.billing_code_override bco on bc.id = bco.billing_code_id
union all
select cast(bc.id as text) || '-0' as id,
       bc.id  as billing_code_id,
       bct.id as type_id,
       bct.code_type,
       bc.code_name,
       bc.client_alias,
       bc.description,
       null   as project_id,
       null   as override_id
from project.billing_code bc
         inner join project.billing_code_type bct on bc.type_id = bct.id
