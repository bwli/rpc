create table if not exists time.allocation_submission
(
    id                   bigserial primary key,
    submit_user_id       bigint not null,
    approve_user_id      bigint,
    finalize_user_id     bigint,
    status               integer check ( status in (0, 1, 2, 3, 4, 5)) default 0,
    report_submission_id bigint references time.report_submission (id) on delete cascade on update cascade,
    team_name            varchar,
    date_of_service      date   not null,
    approved_at          timestamp with time zone,
    finalized_at         timestamp with time zone,
    project_id           bigint,
    by_system            boolean                                       default false,
    billing_purpose_only boolean                                       default false,
    status_changed_at    timestamp with time zone,
    exported             boolean                                       default false,
    payroll_date         date   not null,
    created_at           timestamp with time zone                      default now()
);

comment on column time.allocation_submission.status
    is '0:pending, 1:released, 2:approved, 3:denied, 4:approved pending';

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'allocation_submission'
                        and table_schema = 'time'
                        and column_name = 'payroll_date')
        then alter table time.allocation_submission
            alter column date_of_service set not null,
            add column payroll_date date;
            update time.allocation_submission s
            set payroll_date = t.payroll_date
            from time.allocation_time t
            where t.submission_id = s.id;
            alter table time.allocation_submission
                alter column payroll_date set not null;
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;

do
$$
    BEGIN
        drop index if exists time.team_date_of_service_project_not_null_uni_idx;
        drop index if exists time.team_date_of_service_project_null_uni_idx;

        create temporary table temp_allocation_time
        (
            allocation_time_id       bigint,
            allocation_submission_id bigint,
            submit_user_id           bigint,
            approve_user_id          bigint,
            finalize_user_id         bigint,
            status                   integer,
            report_submission_id     bigint,
            team_name                varchar,
            date_of_service          date,
            approved_at              timestamp with time zone,
            finalized_at             timestamp with time zone,
            project_id               bigint,
            by_system                boolean,
            billing_purpose_only     boolean,
            status_changed_at        timestamp with time zone,
            exported                 boolean,
            payroll_date             date,
            created_at               timestamp with time zone
        );

        with t0 as (
            select t.id as allocation_time_id,
                   s.id,
                   s.submit_user_id,
                   s.approve_user_id,
                   s.finalize_user_id,
                   s.status,
                   s.report_submission_id,
                   s.team_name,
                   s.date_of_service,
                   s.approved_at,
                   s.finalized_at,
                   s.project_id,
                   s.by_system,
                   s.billing_purpose_only,
                   s.status_changed_at,
                   s.exported,
                   t.payroll_date,
                   s.created_at
            from time.allocation_submission s
                     inner join time.allocation_time t on s.id = t.submission_id
            where s.payroll_date != t.payroll_date)
        insert
        into temp_allocation_time (allocation_time_id, allocation_submission_id, submit_user_id, approve_user_id,
                                   finalize_user_id, status, report_submission_id, team_name, date_of_service,
                                   approved_at, finalized_at, project_id, by_system, billing_purpose_only,
                                   status_changed_at, exported, payroll_date, created_at)
        select *
        from t0;

        with t0 as (
            select distinct on (team_name,
                date_of_service,
                payroll_date,
                project_id) tt.submit_user_id,
                            tt.approve_user_id,
                            tt.status,
                            tt.report_submission_id,
                            tt.team_name,
                            tt.date_of_service,
                            tt.approved_at,
                            tt.project_id,
                            tt.created_at,
                            tt.finalize_user_id,
                            tt.finalized_at,
                            tt.by_system,
                            tt.billing_purpose_only,
                            tt.status_changed_at,
                            tt.exported,
                            tt.payroll_date
            from temp_allocation_time tt
        )
        insert
        into time.allocation_submission (submit_user_id, approve_user_id, status, report_submission_id, team_name,
                                         date_of_service, approved_at, project_id, created_at, finalize_user_id,
                                         finalized_at, by_system, billing_purpose_only, status_changed_at, exported,
                                         payroll_date)
        select *
        from t0;

        with t0 as (
            select tt.allocation_time_id, tt.team_name, tt.date_of_service, tt.payroll_date, tt.project_id
            from temp_allocation_time tt
        )
        update time.allocation_time t
        set submission_id = s.id
        from time.allocation_submission s,
             t0
        where t0.team_name = s.team_name
          and s.date_of_service = t0.date_of_service
          and t0.payroll_date = s.payroll_date
          and s.project_id = t0.project_id
          and t0.allocation_time_id = t.id;

--         select tt.payroll_date, t.payroll_date, s.payroll_date, s.id, s.payroll_date, s.*
--         from temp_allocation_time tt
--                  inner join time.allocation_time t on tt.allocation_time_id = t.id
--                  inner join time.allocation_submission s on t.submission_id = s.id;
--         select * from time.allocation_submission s where s.team_name = 'GOB - 1' and s.date_of_service = '2021-08-28';
--
--         select s.id, count(distinct t.payroll_date) as count_pd
--         from time.allocation_submission s
--                  inner join time.allocation_time t on s.id = t.submission_id
--         group by s.id
--         having count(distinct t.payroll_date) > 1;

        create unique index if not exists team_date_of_service_payroll_date_project_not_null_uni_idx
            on time.allocation_submission (team_name, date_of_service, payroll_date, project_id)
            where project_id is not null and status <> 3;

        create unique index if not exists team_date_of_service_payroll_date_project_null_uni_idx
            on time.allocation_submission (team_name, date_of_service, payroll_date)
            where project_id is null and status <> 3;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$
