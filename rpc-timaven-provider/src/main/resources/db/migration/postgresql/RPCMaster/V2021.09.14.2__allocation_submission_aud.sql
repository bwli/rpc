create table if not exists time.allocation_submission_aud
(
    id                   bigint,
    rev                  integer not null references public.custom_revision_entity (id),
    revtype              smallint,
    submit_user_id       bigint,
    approve_user_id      bigint,
    finalize_user_id     bigint,
    status               integer,
    report_submission_id bigint,
    team_name            varchar,
    date_of_service      date,
    approved_at          timestamp with time zone,
    finalized_at         timestamp with time zone,
    project_id           bigint,
    by_system            boolean,
    billing_purpose_only boolean,
    status_changed_at    timestamp with time zone,
    exported             boolean,
    payroll_date         date,
    created_at           timestamp with time zone,
    constraint allocation_submission_aud_pkey
        primary key (id, rev)
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'allocation_submission_aud'
                        and table_schema = 'time'
                        and column_name = 'payroll_date')
        then alter table time.allocation_submission_aud
            add column payroll_date date;
        end if;
    END;
$$;
