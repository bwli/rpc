create table if not exists time.allocation_time
(
    id                 bigserial
        constraint allocation_time_pkey
            primary key,
    emp_id             varchar,
    team_name          varchar,
    first_name         varchar,
    last_name          varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       boolean                  default false not null,
    rig_pay            numeric,
    submission_id      bigint                                 not null
        constraint allocation_time_submission_id_fkey
            references time.allocation_submission
            on update cascade on delete cascade,
    created_at         timestamp with time zone default now(),
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    job_number         varchar,
    extra_time_type    integer                  default 0,
    weekly_process_id  bigint
        constraint allocation_time_weekly_process_id_fkey
            references time.weekly_process,
    mob_cost_code      varchar,
    mob_amount         numeric                  default 0     not null,
    client_emp_id      varchar,
    badge              varchar,
    per_diem_amount    numeric                  default 0     not null,
    mob_mileage_rate   numeric                  default 0     not null,
    mob_mileage        numeric                  default 0     not null,
    has_mob            boolean                  default false not null,
    is_offsite         boolean                  default false not null,
    mob_display_order  integer
);

comment on column time.allocation_time.extra_time_type is '0: st. 1: sick. 2: holiday. 3: vacation. 4: other';


DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_time'
                    and table_schema = 'time'
                    and column_name = 'payroll_date')
        then alter table time.allocation_time
            drop column payroll_date;
        end if;
    END;
$$;
