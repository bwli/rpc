create table if not exists stage.craft
(
    id            bigserial primary key,
    submission_id bigint references stage.submission (id) on delete cascade on update cascade,
    craft_code    varchar,
    description   varchar,
    billable_st   numeric                  default 0,
    billable_ot   numeric                  default 0,
    billable_dt   numeric                  default 0,
    per_diem      numeric,
    rig_pay       numeric,
    created_at    timestamp with time zone default now(),
    company       varchar,
    start_date    date,
    end_date      date,
    per_diem_rule varchar
);

DO
$$
    BEGIN
    alter table stage.craft
        add column start_date date,
        add column end_date date,
        add column per_diem_rule varchar;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists in stage.craft.';
    END;
$$;
