create table if not exists stage.rpc_user_pool
(
    id              BIGSERIAL primary key,
    emp_id          varchar not null,
    client_emp_id   varchar,
    craft           varchar,
    per_diem_id     bigint,
    base_st         numeric                  default 0,
    base_ot         numeric                  default 0,
    base_dt         numeric                  default 0,
    holiday_rate    numeric                  default 0,
    sick_leave_rate numeric                  default 0,
    travel_rate     numeric                  default 0,
    vacation_rate   numeric                  default 0,
    client_craft    varchar,
    first_name      varchar,
    last_name       varchar,
    middle_name     varchar,
    job_number      varchar,
    gender          varchar check (gender in ('f', 'm')),
    company         varchar,
    badge           varchar,
    team_name       varchar,
    crew            varchar,
    dob             date,
    sign_in_sheet   varchar,
    effected_on     date,
    terminated_at   date,
    rehired_at      date,
    hired_at        date,
    has_per_diem    boolean,
    has_rig_pay     boolean,
    is_active       boolean not null         default true,
    project_id      bigint,
    shift           varchar,
    schedule_start  time,
    schedule_end    time,
    lunch_start     time,
    lunch_end       time,
    is_offsite      boolean                  default false not null,
    activity_code   varchar,
    created_at      timestamp with time zone default now()
);

comment on column stage.rpc_user_pool.is_active
    is 'decide whether to use this record or not';
